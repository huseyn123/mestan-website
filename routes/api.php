<?php

Route::get('/', 'DashboardAdminController@index')->name('admin.dashboard');
Route::get('/profile', 'AdminController@profile')->name('admin.profile');
Route::put('updatePassword', 'AdminController@updatePassword')->name('admins.updatePassword');
Route::put('updatePhoto', 'AdminController@updatePhoto')->name('admins.updatePhoto');

Route::resource('analytic', 'GoogleAnalyticsController')->only(['index', 'store']);

Route::resource('admins', 'AdminController')->except('destroy');
Route::group(['prefix'=>'admins'], function()
{
    Route::put('{id}/trash', 'AdminController@trash')->name('admins.trash');
    Route::put('{id}/restore', 'AdminController@restore')->name('admins.restore');
});

//Route::resource('sub_page', 'SubPageController', ['except' => ['show', 'destroy']]);

Route::resource('page', 'PageController', ['except' => ['show', 'destroy']]);
Route::group(['prefix'=>'page'], function()
{
    Route::put('{id}/updateSingle', 'PageController@updateSingle')->name('page.updateSingle');

});

Route::resource('pageTranslation', 'PageTranslationController')->only(['store', 'update', 'destroy'])->names(['destroy' => 'page.destroy']);
Route::group(['prefix'=>'pageTranslation'], function()
{
    Route::get('order', 'PageTranslationController@order')->name('pageTranslation.order');
    Route::post('postOrder', 'PageTranslationController@postOrder')->name('post.pageTranslation.order');
    Route::put('{id}/trash', 'PageTranslationController@trash')->name('page.trash');
    Route::put('{id}/restore', 'PageTranslationController@restore')->name('page.restore');
});

Route::resource('articles', 'ArticleController')->except(['show', 'destroy']);
Route::group(['prefix'=>'articles'], function()
{
    Route::put('{id}/updateSingle', 'ArticleController@updateSingle')->name('article.updateSingle');
});

Route::resource('articleTranslation', 'ArticleTranslationController')->only(['store', 'update', 'destroy'])->names(['destroy' => 'articles.destroy']);
Route::group(['prefix'=>'articles'], function()
{
    Route::put('{id}/trash', 'ArticleTranslationController@trash')->name('articles.trash');
    Route::put('{id}/restore', 'ArticleTranslationController@restore')->name('articles.restore');
});

// Product Category
Route::resource('toys', 'ProductController')->except(['show', 'destroy']);
Route::resource('books', 'ProductController')->except(['show', 'destroy']);
Route::resource('school_accessories', 'ProductController')->except(['show', 'destroy']);
Route::resource('food', 'ProductController')->except(['show', 'destroy']);
//-----------------------------------------------------------------------------//



Route::resource('product', 'ProductController')->except(['show', 'destroy']);
Route::group(['prefix'=>'product'], function()
{
    Route::get('{id}/get', 'ProductController@getProductDetail')->name('product.get');
    Route::get('{id}/duplicate', 'ProductController@duplicate')->name('product.duplicateView');
    Route::put('{id}/duplicate', 'ProductController@duplicatePost')->name('product.duplicate');
    Route::put('{id}/updateSingle', 'ProductController@updateSingle')->name('product.updateSingle');
    Route::put('{id}/fastUpdate', 'ProductController@fastUpdate')->name('product.fastUpdate');
    Route::get('updateAll', 'ProductController@updateAllProducts')->name('product.updateAll');
    Route::get('search/{barcode}', 'ProductController@searchByApi')->name('product.search');
});
Route::put('updateMinSalesCount', 'ProductController@updateMinSalesCount')->name('product.updateMinSalesCount');
Route::put('activeDiscount', 'ProductController@activeDiscount')->name('product.activeDiscount');


Route::resource('productTranslation', 'ProductTranslationController')->only(['store', 'update','destroy'])->names(['destroy' => 'product.destroy']);
Route::group(['prefix'=>'productTranslation'], function()
{
    Route::put('{id}/trash', 'ProductTranslationController@trash')->name('product.trash');
    Route::put('{id}/restore', 'ProductTranslationController@restore')->name('product.restore');
    Route::get('order', 'ProductTranslationController@order')->name('productTranslation.order');
    Route::get('index_order', 'ProductTranslationController@index_order')->name('IndexProductTranslation.order');
    Route::post('indexPostOrder', 'ProductTranslationController@postIndexOrder')->name('post.IndexProductTranslation.order');
    Route::post('postOrder', 'ProductTranslationController@postOrder')->name('post.productTranslation.order');
});

Route::resource('banners', 'BannerController');


Route::resource('videos', 'VideoController', ['except' => ['show']]);
Route::resource('vacancies', 'VacancyController', ['except' => ['show']]);
Route::resource('brands', 'BrandController', ['except' => ['show']]);
Route::resource('colors', 'ColorController', ['except' => ['show']]);
Route::resource('campaigns', 'CampaignController', ['except' => ['show']]);
Route::resource('faq', 'FaqController', ['except' => ['show']]);
Route::resource('slider', 'SliderController', ['except' => ['show']]);
Route::resource('tag', 'TagController', ['except' => ['show']]);
Route::resource('block', 'BlockController', ['except' => ['show']]);

Route::group(['prefix'=>'slider'], function()
{
    Route::post('reorder', 'SliderController@reorder')->name('slider.reorder');
    Route::put('{id}/updateImage', 'SliderController@updateImage')->name('update_image_slider');
    Route::put('{id}/trash', 'SliderController@trash')->name('slider.trash');
    Route::put('{id}/restore', 'SliderController@restore')->name('slider.restore');
});

Route::resource('promo_code', 'PromoCodeController', ['except' => ['show']]);
Route::group(['prefix'=>'promo_code'], function()
{
    Route::put('{id}/trash', 'PromoCodeController@trash')->name('promo_code.trash');
    Route::put('{id}/restore', 'PromoCodeController@restore')->name('promo_code.restore');
});


Route::get('cor_invoices', 'InvoiceController@index')->name('cor_invoices.index');
Route::resource('invoices', 'InvoiceController')->only(['index', 'show','destroy']);
Route::get('invoices/{id}/pdf', 'InvoiceController@pdf')->name('invoices.pdf');
Route::put('invoices/{id}/invUpdate', 'InvoiceController@invoiceUpdate')->name('invoices.invoiceUpdate');


Route::get('reg_user', 'RegUsersController@index')->name('reg.users');
Route::get('reg_email_create', 'RegUsersController@emailCreate')->name('reg_email.create');
Route::post('reg_email_create', 'RegUsersController@sendEmail')->name('reg_email.post');
Route::get('corporative_users', 'RegUsersController@corporative_users')->name('cor.users');
Route::get('corporative_user_create', 'RegUsersController@create')->name('cor.create');
Route::delete('/corporative_users/{id}', 'RegUsersController@destroy')->name('corporative_users.destroy');;
Route::post('corporative_user_create', 'RegUsersController@store')->name('cor.post');


Route::resource('user_birthdays', 'UserBirthdayController')->only(['index','destroy','update']);


Route::resource('post-terminal', 'PostTerminalController', ['except' => ['show', 'destroy']]);
Route::resource('partners', 'PartnerController');
Route::resource('{model}/{id}/gallery','GalleryController')->only(['index', 'store']);
Route::delete('gallery/{id}', 'GalleryController@destroy')->name('gallery.destroy');


Route::resource('subscribers', 'SubscriberController')->except(['edit']);
Route::resource('config', 'ConfigController', ['only' => ['index', 'edit', 'update']]);
Route::resource('dictionary', 'DictionaryController',['only' => ['index', 'edit', 'update', 'create', 'store']]);
Route::resource('sitemap', 'SitemapController')->only(['index', 'store']);


//

Route::resource('select', 'SelectController')->except(['show']);
Route::resource('product_filters', 'ProductFiltersController')->except(['show']);
Route::resource('product_filter_value', 'ProductFilterValueController')->except(['show']);
Route::resource('delivery', 'DeliveryController')->except(['show']);

Route::post('product/category/filter', 'ProductController@product_category_filter');



<?php

use Illuminate\Support\Facades\Route;

Auth::routes();

// patient routes
/*Route::prefix('user')->group(function (){
    Route::post('/logout', 'Auth\LoginController@logout')->name('user.logout');
});*/

Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');

// admin routes
Route::prefix('admin')->group(function (){
    Route::get('/login', 'Auth\AdminLoginController@showloginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
   // Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

    // Password reset routes
    Route::prefix('password')->group(function (){
        Route::post('/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
        Route::get('/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
        Route::post('/reset', 'Auth\AdminResetPasswordController@reset');
        Route::get('/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
    });
});


// site routes

Route::group(
[
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
],
function() {
    Route::get('/', 'Web\PageController@index')->name('home');
//    Route::get('/search', 'Web\SearchController@search')->name('search');
//    Route::get('/update/{token}', 'ProductController@updateNeededProducts');
    Route::get('/product/search', 'Web\SearchController@productSearch')->name('product.search');
//    Route::get('/product/discount', 'Web\PageController@discount')->name('product.discount');

    Route::resource('/cart', 'CartController')->except(['show', 'edit']);
    Route::get('/cart/checkout', 'CartController@checkout')->name('cart.checkout');
    Route::post('/cart/checkout', 'CartController@checkout')->name('cart.checkout');
    Route::resource('/wishlist', 'WhishListController')->only(['index','store']);
//    Route::post('/cart/checkout', 'CartController@checkout')->name('cart.checkout')->middleware('auth:web');

    Route::post('/order', 'OrderController@post')->name('order.post');
    Route::get('/orders', 'OrderController@index')->name('orders.list');
    Route::get('/order/{key}', 'OrderController@list')->name('order.list');
    Route::get('/order/complete/{payment_key}', 'OrderController@completeOrder')->name('order.complete');



//    Route::post('/order/complete', 'OrderController@complete')->name('order.complete');
//    Route::get('/order/invoice/{key}/pdf', 'OrderController@invoicePdf')->name('order.invoice.pdf');
//    Route::get('/pay/callback',        ['as' => 'order.callback', 'uses' => 'OrderController@callback']);

//    Route::get('load_more/{type}', 'Web\PageController@getProductList')->name('product_list');

    Route::get('discounted_products', 'Web\PageController@discountedProducts')->name('discounted.resume');
    Route::get('brand/{id}', 'Web\PageController@brand')->name('brand.resume');

    Route::get('{slug}/resume/{id}', 'Web\PageController@showResume')->name('show.resume');

    Route::get('{slug}/resume/{id}/apply', 'Web\PageController@applyResume')->name('apply.resume');
    Route::get('{slug1}/{slug2?}/{slug3?}',                ['as' => 'showPage', 'uses' => 'Web\PageController@showPage']);
});
Route::post('/form', 'ContactController@store')->name('post.form');

//Route::post('/resume',        ['as' => 'resume', 'uses' => 'ContactController@resume']);
//Route::post('/subscribe', 'SubscriberController@store')->name('subscribe');
//Route::post('/contact', 'Web\PageController@postContact')->name('web.contact');

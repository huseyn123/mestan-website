<?php

use Illuminate\Http\Request;


// user routes
Route::get('/', 'UserController@show')->name('user.profile');
Route::get('/cart', 'UserController@cart')->name('user.cart');
Route::put('/profile/update/{id}', 'UserController@update')->name('user.update');
Route::post('/profile/passwordUpdate', 'UserController@updatePassword')->name('user.password.update');
Route::get('/birthday', 'UserController@birthday')->name('user.birthday');
Route::put('/birthday/{id}', 'UserController@PostBirthday')->name('user.post.birthday');

<?php

namespace App\DataTables;

use App\Models\Product;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use DB;

class ProductDataTable extends DataTable
{
    protected $actions = ['excel', 'print'];

    protected $route;

    public function route($route) {
        $this->route = $route;
        return $this;
    }


    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('name', function($row) {
                return '<a href="#" class="editable-name" data-url="'.route('product.fastUpdate', $row->tid).'" data-type="text" data-pk="'.$row->tid.'" data-title="'.$row->name.'" data-token="'.csrf_token().'">'.$row->name.'</a>';
            })
            ->editColumn('percent', function($row) {
                if(is_null($row->percent) || $row->percent == 0){
                    return '-----';
                }else{
                    return $row->percent;
                }
            })
            ->editColumn('page_name', function($row) {
                if(is_null($row->page_id)){
                    return '<span class="text text-danger">Silinib və ya mövcud deyil!</span>';
                }
                elseif(is_null($row->deleted_page)){
                    return $row->page_name;
                }
                else{
                    return '<span class="text text-danger" style="text-decoration: line-through">'.$row->page_name.'</span>';
                }
            })
            ->editColumn('image', function($row) {
                if(!is_null($row->getFirstMedia())){
                    if(substr($row->getFirstMedia()->getFullUrl(), -3) == 'svg'){
                        $url = $row->getFirstMedia()->getFullUrl();
                        $style = 'width:100px;height:100px';
                    }else{
                        $url = $row->getFirstMedia()->getUrl('blade');
                        $style="max-width:100%";
                    }

                    $img = '<div class="input-group"><img src="'.asset($url).'" style="'.$style.'"></div>';

                    return '<img src="'.asset($url).'" style="'.$style.'">';
                }
            })
            ->editColumn('price', function($row) {
                if($row->old_price != $row->price && $row->price > 0){
                    return '<span style="text-decoration:line-through">'.$row->old_price.'</span> '.$row->price;
                }
                else{
                    return $row->old_price;
                }
            })
            ->editColumn('slug', function($row) {
                return '<a href="#" class="editable-slug" data-url="'.route('product.fastUpdate', $row->tid).'" data-type="text" data-pk="'.$row->tid.'" data-title="'.$row->slug.'" data-token="'.csrf_token().'">'.$row->slug.'</a>';
            })
            ->editColumn('status', function($row) {
                $status = config("config.status.$row->status");

                return '<a href="#" class="editable-status '.($row->status == 0 ? 'text-danger' : '').'" data-url="'.route('product.fastUpdate', $row->id).'" data-type="select" data-value="'.$row->status.'" data-pk="'.$row->id.'" data-token="'.csrf_token().'">'.$status.'</a>';
//                return '<span class="text-'.config("config.alert.$row->status").' text-bold">'.config("config.status.$row->status").'</span>';
            })
            ->editColumn('tag', function($row) {
                $tags = [];
                foreach ($row->productTags as $tag){

                    $tags[] = $tag->keyword;
                }

                return implode(', ', $tags);
            })
            ->with('status', encode(config("config.status")))
            ->rawColumns(['status', 'image', 'name', 'slug', 'price', 'action','page_name'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-product', [
                    'route' => 'product',
                    'editRoute' => $this->route,
                    'row' => $row,
                    'softDelete' => true,
                    'forceDelete' => true,
                    'largeModal' => true
                ])->render();
            });
    }


    public function query(Product $model)
    {
        $query = $model->newQuery()
            ->join('product_translations as prt', 'prt.product_id', '=', 'products.id')
            ->leftJoin('page_translations as pt', 'pt.id', '=', 'prt.page_id')
            ->leftJoin('pages', 'pages.id', '=', 'pt.page_id')
            ->leftJoin('brands', 'brands.id', '=', 'products.brand_id')
            ->where('pages.template_id',config('config.product_template.'.$this->route))
            ->with(['media'])
            ->select(
                'prt.*',
                'prt.id as tid',
                'products.id',
                'products.bar_code',
                'products.status',
                'products.published_by',
                'products.price',
                'products.old_price',
                'products.percent',
                'brands.title as brand',
                'pt.name as page_name',
                'pt.deleted_at as deleted_page',
                'pages.template_id'
            )
            ->with(['productTags']);

        if ($this->request()->has('lang') && $this->request()->get('lang') != 'all') {
            $query->where('prt.lang', $this->request()->get('lang'));
        }

        if ($this->request()->has('tag') && $this->request()->get('tag') != 'all') {
            $tag = $this->request()->get('tag');

            $query->whereHas('productTagsSearch',function($query) use($tag){
                $query->where("keyword", $tag);
            });
        }

        if ($this->request()->has('category') && $this->request()->get('category') != 'all') {
            $query->where('prt.page_id', $this->request()->get('category'));
        }

        if($this->request()->get('type') == 1){
            $query->whereNull('prt.deleted_at');
        }
        elseif($this->request()->get('type') == 2){
            $query->whereNotNull('prt.deleted_at');
        }

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '100px', 'title' => '', 'exportable' => false, 'printable' => false])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'prt.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'name', 'name' => 'prt.name', 'title' => 'Ad', 'orderable' => false],
            ['data' => 'slug', 'name' => 'prt.slug', 'title' => 'Slug', 'orderable' => false, 'searchable' => false, 'exportable' => false, 'printable' => false],
            ['data' => 'page_name', 'name' => 'pt.name', 'title' => 'Kateqoriya'],
            ['data' => 'bar_code', 'name' => 'products.bar_code', 'title' => 'Bar Kod', 'orderable' => false],
            ['data' => 'status', 'name' => 'products.status', 'title' => 'Status', 'searchable' => false],
            ['data' => 'price', 'name' => 'products.price', 'title' => 'Qiymət ₼', 'searchable' => false],
            ['data' => 'percent', 'name' => 'products.percent', 'title' => 'Faiz %', 'searchable' => false],
            ['data' => 'image', 'name' => 'media.image', 'title' => 'Şəkil', 'orderable' => false,'class' => 'none','searchable' => false],
            ['data' => 'brand', 'name' => 'brands.title', 'title' => 'Brand', 'orderable' => false, 'class' => 'none', 'searchable' => false, 'exportable' => false, 'printable' => false],
            ['data' => 'tag', 'name' => 'tags.keyword', 'title' => 'Teqlər', 'orderable' => false, 'class' => 'none', 'searchable' => false, 'exportable' => false, 'printable' => false],
            ['data' => 'created_at', 'name' => 'prt.created_at', 'title' => 'Yaradıldı', 'orderable' => false, 'class' => 'none', 'searchable' => false, 'exportable' => false, 'printable' => false],
            ['data' => 'updated_at', 'name' => 'prt.updated_at', 'title' => 'Yenilənib', 'orderable' => false, 'class' => 'none', 'searchable' => false, 'exportable' => false, 'printable' => false],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [25,40,50],
            'dom' => '<"col-md-6 hidden-xs pull-left"<"row"<"pull-left"l><"pull-left"<"col-md-12"B>>>><"pull-right col-md-6 custom-filter"f>rt<"col-md-3 col-sm-3"<"row"i>><"col-md-9"<"row"p>>',
            'buttons' => ['excel', 'print', 'editSales','activeDiscount'],
            'drawCallback' => 'function(settings) {
                $(".editsales-modal").attr("data-action", "'.url('/').'").attr("data-toggle", "modal").attr("data-target", "#edit-sales");
                $(".activeDiscount-modal").attr("data-action", "'.url('/').'").attr("data-toggle", "modal").attr("data-target", "#edit-activeDiscount");
            
                var api = this.api()
                var json = api.ajax.json();
                
                $("#products .editable-name").editable({
                    ajaxOptions: {
                        type: "PUT"
                    },
                    params: {
                        type: "name"
                    },
                });
                
                $("#products .editable-slug").editable({
                    ajaxOptions: {
                        type: "PUT"
                    },
                    params: {
                        type: "slug"
                    },
                });
                
                $("#products .editable-status").editable({
                    showbuttons:false,
                    ajaxOptions: {
                        type: "PUT"
                    },
                    params: {
                        type: "status",
                    },
                    source: json.status
                });   
                $("#products .editable-sales").editable({
                    ajaxOptions: {
                        type: "PUT"
                    },
                    params: {
                        type: "min_sales_count",
                    }, 
                });    
                 $("#products .editable-activeDiscount").editable({
                    ajaxOptions: {
                        type: "PUT"
                    },
                    params: {
                        type: "discount_active",
                    }, 
                });      
            }',
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'Products_' . date('YmdHis');
    }

    public function editSales()
    {

    }
}

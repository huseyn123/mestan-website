<?php

namespace App\DataTables;

use App\Models\Album;
use Yajra\DataTables\Services\DataTable;

class AlbumDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('name', function($row) {
                return '<a href="'.route('gallery.index', $row->id).'">'.$row->name.'</a>';
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => 'albums', 'forceDelete' => true])->render();
            })
            ->rawColumns(['action', 'name']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Album $model)
    {
        $query = $model->newQuery();

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'albums.id', 'title' => 'ID'],
            ['data' => 'name', 'name' => 'albums.name', 'title' => 'Albom adı', 'orderable' => false],
            ['data' => 'created_at', 'name' => 'albums.created_at', 'title' => 'Yaradıldı', 'orderable' => false, 'searchable' => false, 'exportable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'albums.updated_at', 'title' => 'Yenilənib', 'orderable' => false, 'searchable' => false, 'exportable' => false, 'class' => 'none'],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25,50],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'Albums_' . date('YmdHis');
    }


}

<?php

namespace App\DataTables;

use App\Models\UserBirthday;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class UserBirthdayDataTable extends DataTable
{

    public function dataTable($query)
    {

        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('name', function ($row){
                return $row->name.' '.$row->surname;
            })
            ->editColumn('b_day', function ($row){
                return filterDate($row->b_day,false,'birthday');
            })
            ->editColumn('image', function($row) {
                if($row->getFirstMedia()){
                    return '<div align="center"><a href="'.asset($row->getFirstMedia()->getUrl()).'" target="_blank" style="display:block"> Şəkilə bax</a><img style="max-height:300px;max-width:300px;z-index:0" src="'.asset($row->getFirstMedia()->getUrl()).'"></div>';
                }
            })
             ->editColumn('status', function($row) {
                return '<span class="text-'.config("config.alert.$row->status").' text-bold">'.config("config.status.$row->status").'</span>';
            })
            ->rawColumns(['image','status','action'])
             ->addColumn('action', function($row) {
                return view( 'widgets.action-subscriber', ['route' => 'user_birthdays','status' => $row->status,'id' =>$row->id])->render();
            });
    }


    public function query(UserBirthday $model)
    {
        $query = $model->newQuery()->with('media');

        // if($this->request()->get('type') && $this->request()->get('type') !=0){
        //     $query->where('orders.status',$this->request()->get('type'));
        // }


        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '50px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'user_birthdays.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'image', 'name' => 'media.image','title' => 'Şəkil', 'searchable' => false, 'orderable' => false],
            ['data' => 'name', 'name' => 'user_birthdays.name','title' => 'Ad Soyad', 'orderable' => false, 'searchable' => true],
            ['data' => 'b_day', 'name' => 'user_birthdays.b_day','title' => 'Ad günü', 'searchable' => true],
            ['data' => 'status', 'name' => 'user_birthdays.status', 'title' => 'Status', 'searchable' => false],
        ];
    }

   protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }



    protected function filename()
    {
        return 'user_birthdaysdatatable_' . time();
    }

}

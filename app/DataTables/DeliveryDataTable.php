<?php

namespace App\DataTables;

use App\Models\DeliveryPrice;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class DeliveryDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('price', function($row) {
                    return $row->price.' ₼';
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => 'delivery', 'forceDelete' => true])->render();
            })
            ->rawColumns(['action']);
    }


    public function query(DeliveryPrice $model)
    {
        $query = $model->newQuery();

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '100px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'delivery_prices.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'address_az', 'name' => 'delivery_prices.address_az', 'title' => 'Ünvan Az', 'orderable' => false],
            ['data' => 'price', 'name' => 'delivery_prices.price', 'title' => 'Qiymət ₼'],
            ['data' => 'created_at', 'name' => 'delivery_prices.created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'delivery_prices.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false, 'class' => 'none'],
        ];
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'deliverydatatable_' . time();
    }
}

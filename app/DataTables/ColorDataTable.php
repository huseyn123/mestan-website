<?php

namespace App\DataTables;

use App\Models\Color;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ColorDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => 'colors', 'forceDelete' => true])->render();
            })
            ->rawColumns(['color_code','action']);
    }

    public function query(Color $model)
    {
        $query = $model->newQuery();

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '100px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'colors.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'name_az', 'name' => 'colors.name_az','title' => 'Rəng', 'orderable' => true],
            ['data' => 'created_at', 'name' => 'colors.created_at','title' => 'Yaradıldı', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'colors.updated_at', 'title' => 'Yenilənib', 'searchable' => false],
        ];
    }

    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'colorsdatatable_' . time();
    }

}

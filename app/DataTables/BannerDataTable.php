<?php

namespace App\DataTables;

use App\Models\Banner;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
class BannerDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            /*->editColumn('device', function($banner) {
                return config("config.banner-devices.$banner->device");
            })*/
            ->editColumn('status', function($banner) {
                return '<label class="label label-'.config("config.label.$banner->status").'">'.config("config.status.$banner->status").'</label>';
            })
            ->editColumn('type', function($banner) {
                return config("config.banner-type.$banner->type");
            })
            ->editColumn('banner_url', function($banner) {
                return '<img src="'.asset("storage/".$banner->banner_url).'" class="img-responsive" style="max-height:400px">';
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['show' => url('/admin/banners/'.$row->id), 'route' => 'banners', 'row' => $row, 'forceDelete' => true])->render();
            })
            ->rawColumns(['banner_url', 'status', 'action']);
    }


    public function query(Banner $model)
    {
        $query = $model->newQuery();

        if($this->request()->get('type') != 0)
        {
            $query->where('type', $this->request()->get('type'));
        }

        /*if($this->request()->get('device') != 0)
        {
            $query->where('device', $this->request()->get('device'));
        }*/

        /*if($this->request()->get('status') < 2)
        {
            $query->where('status', $this->request()->get('status'));
        }*/

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '90px', 'class' => 'all', 'title' => 'Əməliyyat'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'title' => 'ID', 'visible' => false ,'searchable' => false],
            ['data' => 'title', 'title' => 'Başlıq', 'orderable' => false],
            //['data' => 'device', 'title' => 'Device','searchable' => false],
            ['data' => 'type', 'title' => 'Növü','searchable' => false],
            //['data' => 'width', 'title' => 'Uzunluq' ,'searchable' => false],
            //['data' => 'height', 'title' => 'Hündürlük' ,'searchable' => false],
            //['data' => 'viewed', 'title' => 'Trafik','searchable' => false],
            ['data' => 'public_viewed', 'title' => 'Görülüb','searchable' => false],
            ['data' => 'status', 'title' => 'Status','searchable' => false],
            ['data' => 'created_at', 'title' => 'Yaradılıb','orderable' => false, 'class' => 'none' ,'searchable' => false],
            ['data' => 'updated_at', 'title' => 'Yenilənib','orderable' => false, 'class' => 'none' ,'searchable' => false],
            ['data' => 'forward_url', 'title' => 'Link','orderable' => false, 'class' => 'none' ,'searchable' => false],
            ['data' => 'banner_url', 'title' => 'Şəkil','orderable' => false, 'class' => 'none' ,'searchable' => false],

        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'order' => [ [0,'asc'] ],
            'lengthChange' => false,
        ];
    }


    protected function filename()
    {
        return 'bannerdatatable_' . time();
    }
}

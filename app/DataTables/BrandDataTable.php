<?php

namespace App\DataTables;

use App\Models\Brand;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class BrandDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('image', function($brand) {
                if($brand->getFirstMedia()){
                    return '<img style="max-height:150px;max-width:70px;" src="'.asset($brand->getFirstMedia()->getUrl()).'">';
                }
            })
            ->editColumn('letter_id', function($post) {
                return config("config.letter_id.$post->letter_id");
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => 'brands', 'forceDelete' => true])->render();
            })
            ->rawColumns(['action','image']);
    }

    public function query(Brand $model)
    {
        $query = $model->newQuery()->with('media');

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '100px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'brands.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'image', 'name' => 'media.image','title' => 'Şəkil', 'searchable' => false, 'orderable' => false],
            ['data' => 'title', 'name' => 'brands.title','title' => 'Ad', 'searchable' => true],
            ['data' => 'letter_id', 'name' => 'brands.letter_id','title' => 'Hərf', 'searchable' => false],
            ['data' => 'created_at', 'name' => 'brands.created_at','title' => 'Yaradıldı', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'brands.updated_at', 'title' => 'Yenilənib', 'searchable' => false],
        ];
    }

    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => false,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'brandsdatatable_' . time();
    }

}

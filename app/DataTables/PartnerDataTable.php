<?php

namespace App\DataTables;

use App\Models\Partner;
use Yajra\DataTables\Services\DataTable;
use App\Logic\userAction;
use Yajra\DataTables\EloquentDataTable;

class PartnerDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('image', function($partner) {
                if($partner->getFirstMedia()){
                    return '<img style="max-height:70px;max-width:140px;" src="'.asset($partner->getFirstMedia()->getUrl()).'">';
                }
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['route' => 'partners', 'row' => $row, 'forceDelete' => true])->render();
            })
            ->rawColumns(['image', 'action']);
    }


    public function query(Partner $model)
    {
        $query = $model->newQuery()->with('media');

        if($this->request()->has('trashed') && $this->request()->get('trashed') == 1){
            $query->onlyTrashed();
        }

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '100px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'partners.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'image', 'name' => 'media.image','title' => 'Şəkil', 'searchable' => false, 'orderable' => false],
            ['data' => 'name', 'name' => 'partners.name','title' => 'Ad', 'searchable' => false],
            ['data' => 'created_at', 'name' => 'partners.created_at','title' => 'Yaradıldı', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'partners.updated_at', 'title' => 'Yenilənib', 'searchable' => false],
            ['data' => 'site_url', 'name' => 'partners.site_url','title' => 'URL', 'searchable' => false, 'class' => 'none'],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => false,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }



    protected function filename()
    {
        return 'partnerdatatable_' . time();
    }
}

<?php

namespace App\DataTables;

use App\Models\ProductFilterValue;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ProductFilterValueDataTable extends DataTable
{



    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('status', function($row) {
                return '<span class="text-'.config("config.alert.$row->status").' text-bold">'.config("config.status.$row->status").'</span>';
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => 'product_filter_value', 'forceDelete' => true])->render();
            })
            ->rawColumns(['action','status']);
    }


    public function query(ProductFilterValue $model)
    {
        $query = $model
            ->join('product_filters as pf', 'pf.id', '=', 'product_filter_values.filter_id')
            ->select('product_filter_values.*','pf.title_az as filter_name')
            ->newQuery();

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '100px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'product_filter_values.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'title_az', 'name' => 'product_filter_values.title_az', 'title' => 'Value', 'orderable' => false],
            ['data' => 'filter_name', 'name' => 'product_filter_values.filter_name', 'title' => 'Filter', 'orderable' => false],
            ['data' => 'status', 'name' => 'product_filter_values.status', 'title' => 'Status', 'searchable' => false],
            ['data' => 'created_at', 'name' => 'product_filter_values.created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'product_filter_values.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false, 'class' => 'none'],
        ];
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'productFilterValuedatatable_' . time();
    }
}

<?php

namespace App\DataTables;

use App\Models\Select;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class SelectDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => 'select', 'forceDelete' => true])->render();
            })
            ->rawColumns(['action']);
    }


    public function query(Select $model)
    {
        $query = $model->newQuery();

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '100px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'selects.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'title_az', 'name' => 'selects.title_az', 'title' => 'Select Az', 'orderable' => false],
            ['data' => 'title_en', 'name' => 'selects.title_en', 'title' => 'Select En', 'orderable' => false],
            ['data' => 'title_en', 'name' => 'selects.title_en', 'title' => 'Select Ru', 'orderable' => false],
            ['data' => 'created_at', 'name' => 'selects.created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'selects.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false, 'class' => 'none'],
        ];
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'selectdatatable_' . time();
    }
}

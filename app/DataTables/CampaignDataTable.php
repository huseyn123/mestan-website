<?php

namespace App\DataTables;

use App\Models\Campaign;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class CampaignDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('image', function($row) {
                return asset("storage/".$row->image);
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => 'campaigns', 'forceDelete' => true])->render();
            })
            ->rawColumns(['image', 'action']);
    }


    public function query(Campaign $model)
    {
        $query = $model->newQuery();

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '100px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'campaigns.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'image', 'name' => 'campaigns.image', 'title' => 'Şəkil', 'searchable' => false, 'orderable' => false],
            ['data' => 'url', 'name' => 'campaigns.url', 'title' => 'Link', 'orderable' => false],
            ['data' => 'created_at', 'name' => 'videos.created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'videos.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false, 'class' => 'none'],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => false,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'campaignsdatatable_' . time();
    }
}

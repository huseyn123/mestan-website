<?php

namespace App\DataTables;

use App\Models\Cart;
use Yajra\DataTables\Services\DataTable;
use DB;
use Form;

class CartDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('image', function($row) {
                return '<img src="'.asset("storage/thumb/$row->image").'" style="max-width:150px">';
            })
            ->editColumn('price', function($row) {
                return '<span class="price">'.$row->price.' <span class="azn">M</span></span>';
            })
            ->editColumn('amount', function($row) {
                return '<span class="price">'.$row->amount.' <span class="azn">M</span></span>';
            })
            ->editColumn('count', function($row) {
                $quantity = '<div class="quantity quantity-dt"><input type="text" name="quantity" value="'.$row->count.'" data-id="'.$row->id.'" data-max="'.$row->on_hand.'"></div>';

                return $quantity;
            })
            ->addColumn('action', function($row) {
                return view( 'web.elements.action-cart', ['id' => $row->id])->render();
            })
            ->rawColumns(['image', 'price', 'count', 'amount', 'action'])
            ->with('sum', $query->sum(DB::raw('carts.price * carts.count')));
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Cart $model)
    {
        $query = $model->newQuery()
            ->join('products as p', 'p.id', '=', 'carts.product_id')
            ->join('product_translations as pt', 'pt.product_id', '=', 'p.id')
            ->where('pt.lang', app()->getLocale())
            ->whereCondition()
            ->select('pt.name', 'carts.id', 'carts.price', 'carts.count', 'carts.created_at', 'p.image', 'p.on_hand', DB::raw('carts.count * carts.price as amount'));

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '30px', 'title' => 'Sil'])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'created_at', 'name' => 'carts.created_at', 'title' => '', 'visible' => false],
            ['data' => 'image', 'name' => 'p.image', 'title' => 'Məhsulun şəkli', 'orderable' => false],
            ['data' => 'name', 'name' => 'pt.name', 'title' => 'Məhsulun adı', 'orderable' => false, 'width' => '200px'],
            ['data' => 'price', 'name' => 'carts.price', 'title' => 'Qiymət'],
            ['data' => 'count', 'name' => 'carts.count', 'title' => 'Say'],
            ['data' => 'amount', 'name' => 'carts.price', 'title' => 'Məbləğ ']
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'filter' => false,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [5],
            'lengthChange' => false,
            'info' => false,
            'pagingType' => 'numbers',
            'language' => [
                'url' => url('lang.json'),
            ],
            'drawCallback' => 'function(settings) {
                var pagination = $(this).closest(".dataTables_wrapper").find(".dataTables_paginate");
                pagination.toggle(this.api().page.info().pages > 1);
                
                var api = this.api();
                var json = api.ajax.json();
                var sum = parseFloat(json.sum).toFixed(2);
                
                $("#total").html("");
                $("#total").append(sum);
                $("#total_price").show();
            }',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Cart_' . date('YmdHis');
    }
}

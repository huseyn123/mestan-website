<?php

namespace App\DataTables;

use App\Models\PromoCode;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class PromoCodeDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        return $dataTable 
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['route' => 'promo_code', 'row' => $row, 'softDelete' => true, 'forceDelete' => false])->render();
            })
            ->editColumn('end_date', function($row) {
                if((date('Y-m-d') > $row->end_date) || ($row->start_date >  $row->end_date)){
                    return '<p style="color:red;"><span style="text-decoration-line: line-through">'.$row->end_date.'</span></br><span>Promo kodun vaxtı bitib</span></p>';
                }elseif(date('Y-m-d') === $row->end_date){
                    return '<p style="color:#e0925c;"><span>'.$row->end_date.'</span></br><span>Promo kodun vaxtı bu gün bitir</span></p>';
                }
                else{
                    return $row->end_date;
                } 
            })
            ->rawColumns(['image', 'action','end_date']);    }



    public function query(PromoCode $model)
    {
        $query = $model->newQuery();
        if($this->request()->get('type') == 0){
            $query->withTrashed();
        }
        elseif($this->request()->get('type') == 2){
            $query->onlyTrashed();
        }

        if($this->request()->get('date') == 0){
            $query->where('end_date','<',date('Y-m-d'));
        }
        elseif($this->request()->get('date') == 1){
            $query->where('end_date','>=',date('Y-m-d'));
        }

        return $query;
    }



    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '90px', 'orderable' => false, 'searchable' => false, 'title' => ' '])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'name', 'name' => 'promo_codes.name', 'title' => 'Ad'],
            ['data' => 'promo_code', 'name' => 'promo_codes.promo_code', 'title' => 'Promo kod', 'orderable' => false],
            ['data' => 'percent', 'name' => 'promo_codes.percent', 'title' => 'Faiz','orderable' => false],
            ['data' => 'start_date', 'name' => 'promo_codes.start_date', 'title' => 'Başlama tarixi','orderable' => false],
            ['data' => 'end_date', 'name' => 'promo_codes.end_date', 'title' => 'Bitmə tarixi','orderable' => false],
            ['data' => 'created_at', 'name' => 'promo_codes.created_at', 'title' => 'Yaradıldı','searchable' => false,'orderable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'promo_codes.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false, 'class' => 'none'],
        ];
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'rowReorder' => [
                'dataSrc' => 'order',
                'update' => false
            ],
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [5,10],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'promo_codesdatatable_' . time();
    }
}

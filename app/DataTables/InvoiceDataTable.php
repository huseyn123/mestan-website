<?php

namespace App\DataTables;
 
use App\Models\Order;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class InvoiceDataTable extends DataTable
{

    protected $route,$type;

    public function route($route) {
        $this->route = $route;
        return $this;
    }


    public function dataTable($query)
    {

        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('name', function ($row){
                $name  = $row->name ?? $row->user_name;

                $user_type = $row->user_id === null ? '<i class="fa-lg fa fa-user-secret text-danger" aria-hidden="true"></i>' : ''; 

                return $user_type.$name;
            })
            ->editColumn('promo_id', function ($row){
                return $row->promo_name ? $row->promo_name : '-------';
            })
            ->editColumn('delivery_price', function ($row){
                return $row->delivery_price.' AZN';
            })
            ->editColumn('special_percent', function ($row){
                $text = $row->birthday_status ? '  <i class="fa fa-birthday-cake" aria-hidden="true"></i>' : ' ';

                return $row->special_percent ? $row->special_percent.'%'.$text : '-----';
            })
            ->editColumn('cart_type', function ($row){

                return (( $row->total_amount ? $row->total_amount :  ($row->amount + $row->delivery_price) - ($row->special_percent ? number_format(($row->amount * $row->special_percent/100),2) : 0 ) )).' AZN';


            })
            ->editColumn('status', function($row) {
                $status = config("config.order_status.$row->status");

               return '<a style="color:'.config("config.order_status_element.$row->status.1").'" href="#" class="editable-status" data-url="'.route('invoices.invoiceUpdate', $row->id).'" data-type="select" data-value="'.$row->status.'" data-pk="'.$row->id.'" data-token="'.csrf_token().'"><i class="fa '.config("config.order_status_element.$row->status.0").'"></i> '.$status.'</a>';
//                return '<span class="text-'.config("config.alert.$row->status").' text-bold">'.config("config.status.$row->status").'</span>';
            })
            ->with('status', encode(config("config.order_status")))
            ->rawColumns(['name','status','action','special_percent'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => 'invoices', 'softDelete' => false, 'forceDelete' => true, 'edit' => false, 'show' => route('invoices.show', $row->id)])->render();
            });

    }


    public function query(Order $model)
    {
        $this->type =  $this->route == 'cor_invoices' ? 2 : 1;
        $query = $model->newQuery()
            ->leftJoin('users', 'users.id', '=', 'orders.user_id')
            ->leftJoin('promo_codes', 'promo_codes.id', '=', 'orders.promo_id')
            ->where('orders.code', '<>', 0)
            ->where('orders.show', 1)
            ->where('orders.user_type',$this->type)
            ->select('orders.*', 'users.name','promo_codes.name as promo_name');

        if($this->request()->get('type') && $this->request()->get('type') !=0){
            $query->where('orders.status',$this->request()->get('type'));
        }


        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '50px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'orders.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'name', 'name' => 'users.name','title' => 'User', 'orderable' => false, 'searchable' => true],
            ['data' => 'user_phone', 'name' => 'users.user_phone','title' => 'Telefon', 'orderable' => false, 'searchable' => false],
            ['data' => 'amount', 'name' => 'orders.amount','title' => 'Məbləğ', 'searchable' => true],
            ['data' => 'special_percent', 'name' => 'orders.special_percent','title' => 'Endirim', 'searchable' => true],
            ['data' => 'promo_id', 'name' => 'orders.promo_id','title' => 'Promo kod', 'searchable' => true],
            ['data' => 'delivery_price', 'name' => 'orders.delivery_price','title' => 'Ç/qiyməti', 'searchable' => true],
            ['data' => 'cart_type', 'name' => 'orders.cart_type','title' => 'Toplam məbləğ', 'searchable' => false],
            ['data' => 'address', 'name' => 'orders.address','title' => 'Ünvan', 'searchable' => true, 'class' => 'none'],
            ['data' => 'status', 'name' => 'orders.status', 'title' => 'Status', 'searchable' => false],
            ['data' => 'created_at', 'name' => 'orders.created_at','title' => 'Tarix', 'searchable' => false],
        ];
    }

    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [25,50,100],
            'drawCallback' => 'function(settings) {
            
                var api = this.api()
                var json = api.ajax.json();

                
            $("#invoices .editable-status").editable({
                    showbuttons:false,
                    ajaxOptions: {
                        type: "PUT"
                    },
                    params: {
                        type: "status",
                    },
                    source: json.status
                });              
            }',
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'invoicesdatatable_' . time();
    }

}

<?php

namespace App\DataTables;

use App\Models\PostTerminal;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class PostTerminalDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable

            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => 'post-terminal', 'forceDelete' => false])->render();
            })
            ->rawColumns(['action']);
    }

    public function query(PostTerminal $model)
    {
        $query = $model->newQuery();

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '50px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'post_terminals.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'name', 'name' => 'post_terminals.name','title' => 'Ad', 'searchable' => true],
            ['data' => 'created_at', 'name' => 'post_terminals.created_at','title' => 'Yaradıldı', 'searchable' => false],
        ];
    }

    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => false,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'lengthChange' => false,
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'brandsdatatable_' . time();
    }

}

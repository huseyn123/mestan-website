<?php

namespace App\DataTables;

use App\Models\Tag;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class TagDataTable extends DataTable
{

    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable

        ->addColumn('action', function($row) {
            return view( 'widgets.action-dt', ['row' => $row, 'route' => 'tag', 'forceDelete' => true])->render();
        });

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Tag $model)
    {
        $query = $model->newQuery();

        if ($this->request()->has('lang') && $this->request()->get('lang') != 'all') {
            $query->where('lang', $this->request()->get('lang'));
        }

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '90px', 'class' => 'all', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'tags.id', 'title' => 'ID', 'searchable' => false, 'visible' => false],
            ['data' => 'keyword', 'name' => 'tags.keyword','title' => 'Açar söz'],
            ['data' => 'name', 'name' => 'tags.name','title' => 'Teq'],
            ['data' => 'lang', 'name' => 'tags.lang','title' => 'Dil'],
            ['data' => 'created_at', 'name' => 'tags.created_at', 'title' => 'Yaradılıb', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'tags.updated_at', 'title' => 'Yenilənib', 'searchable' => false]
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [1,'asc'] ],
            'lengthMenu' => [15,25,50],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'tagdatatable_' . time();
    }
}

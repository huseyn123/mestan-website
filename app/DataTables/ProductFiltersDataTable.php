<?php

namespace App\DataTables;

use App\Models\ProductFilters;
use App\Models\Page;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ProductFiltersDataTable extends DataTable
{



    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('status', function($row) {
                return '<span class="text-'.config("config.alert.$row->status").' text-bold">'.config("config.status.$row->status").'</span>';
            })

            ->editColumn('cat_ids', function($row) {
                $element = '';
                $categories =  Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
                    ->where('pt.lang', 'az')
                    ->whereNull('pt.deleted_at')
                    ->whereIn('pages.id',$row->cat_ids)
                    ->select('pt.name')
                    ->orderBy('pages.id','desc')
                    ->get();

                foreach($categories as $category){
                    $element .= '<span class="text-success">'.$category->name.'<span>;   ';
                }    

                return $element;
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => 'product_filters', 'forceDelete' => true])->render();
            })
            ->rawColumns(['action','status','cat_ids']);
    }


    public function query(ProductFilters $model)
    {
        $query = $model->newQuery();

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '100px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'product_filters.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'title_az', 'name' => 'product_filters.title_az', 'title' => 'Filter Adı', 'orderable' => false],
            ['data' => 'cat_ids', 'name' => 'product_filters.cat_ids', 'title' => 'Kateqoriyalar', 'orderable' => false],
            ['data' => 'filter_column', 'name' => 'product_filters.filter_column', 'title' => 'Database Adı', 'orderable' => false],
            ['data' => 'status', 'name' => 'product_filters.status', 'title' => 'Status', 'searchable' => false],
            ['data' => 'created_at', 'name' => 'product_filters.created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'product_filters.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false, 'class' => 'none'],
        ];
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'productFiltersdatatable_' . time();
    }
}

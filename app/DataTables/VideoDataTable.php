<?php

namespace App\DataTables;

use App\Models\Video;
use Yajra\DataTables\Services\DataTable;
use App\Logic\userAction;
use Yajra\DataTables\EloquentDataTable;

class VideoDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('image', function($video) {
                return '<a href="//youtube.com/watch?v='.$video->youtube_link.'" target="_blank"><img src="'.asset("storage/".$video->image).'" style="max-width:200px"></a>';
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => 'videos', 'forceDelete' => true])->render();
            })
            ->rawColumns(['image', 'action']);
    }



    public function query(Video $model)
    {
        $query = $model->newQuery()
            ->leftJoin('doctors', 'doctors.id', '=', 'videos.doctor_id')
            ->select('videos.*', 'doctors.name');

        return $query;
    }



    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '100px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'videos.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'image', 'name' => 'videos.image', 'title' => 'Şəkil', 'searchable' => false, 'orderable' => false],
            ['data' => 'name', 'name' => 'doctors.name', 'title' => 'Həkim'],
            ['data' => 'created_at', 'name' => 'videos.created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'videos.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false, 'class' => 'none'],
        ];
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => false,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'videosdatatable_' . time();
    }
}

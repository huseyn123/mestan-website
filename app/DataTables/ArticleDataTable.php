<?php

namespace App\DataTables;

use App\Models\Article;
use App\Models\ArticleTranslation;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\Logic\userAction;

class ArticleDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('published_at', function($row) {
                return filterDate($row->published_at);
            })
            ->editColumn('title', function($row) {
                return str_limit($row->title, $limit = 50, $end = '...');
            })
            ->editColumn('page_name', function($row) {
                if(is_null($row->page_id)){
                    return '<span class="text text-danger">Silinib və ya mövcud deyil!</span>';
                }
                elseif(is_null($row->deleted_page)){
                    return $row->page_name;
                }
                else{
                    return '<span class="text text-danger" style="text-decoration: line-through">'.$row->page_name.'</span>';
                }
            })
            ->editColumn('image', function($row) {
                return '<img src='.asset("storage/thumb/".$row->image).'>';
            })
            ->editColumn('status', function($row) {
                return '<span class="text-'.config("config.alert.$row->status").' text-bold">'.config("config.status.$row->status").'</span>';
            })
            ->rawColumns(['status', 'image', 'page_name', 'action'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-page', ['route' => 'articles', 'row' => $row, 'softDelete' => true, 'show' => url("/$row->page_slug/$row->slug"), 'largeModal' => true, 'album' => true])->render();
            });
    }


    public function query(ArticleTranslation $model)
    {
        $query = $model->newQuery()
            ->join('articles as a', 'article_translations.article_id', '=', 'a.id')
            ->leftJoin('page_translations as pt', 'pt.id', '=', 'article_translations.page_id')
            ->select('a.image', 'a.status', 'a.album_id', 'a.published_at',  'a.published_by', 'a.image', 'pt.name as page_name', 'pt.lang', 'pt.slug as page_slug', 'pt.deleted_at as deleted_page', 'article_translations.*');

        if ($this->request()->has('lang') && $this->request()->get('lang') != 'all') {
            $query->where('article_translations.lang', $this->request()->get('lang'));
        }

        if($this->request()->get('type') == 0){
            $query->withTrashed();
        }
        elseif($this->request()->get('type') == 2){
            $query->onlyTrashed();
        }

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'title' => 'Əməliyyat'])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'article_translations.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'published_at', 'name' => 'a.published_at', 'title' => 'Tarix'],
            ['data' => 'name', 'name' => 'article_translations.name', 'title' => 'Başlıq', 'orderable' => false],
            ['data' => 'page_name', 'name' => 'pt.name', 'title' => 'Kateqoriya'],
            ['data' => 'status', 'name' => 'a.status', 'title' => 'Status', 'searchable' => false],
            ['data' => 'lang', 'name' => 'article_translations.lang', 'title' => 'Dil', 'searchable' => false],
            ['data' => 'slug', 'name' => 'article_translations.slug', 'title' => 'Slug', 'orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'published_by', 'name' => 'a.published_by', 'title' => 'Müəllif','orderable' => false, 'class' => 'none', 'searchable' => false],
            ['data' => 'image', 'name' => 'a.image', 'title' => 'Şəkil', 'orderable' => false, 'class' => 'none', 'searchable' => false],
            ['data' => 'created_at', 'name' => 'article_translations.created_at', 'title' => 'Yaradıldı','orderable' => false, 'class' => 'none', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'article_translations.updated_at', 'title' => 'Yenilənib','orderable' => false, 'class' => 'none', 'searchable' => false],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'order' => [ [1,'desc'] ],
            'lengthMenu' => [10,25,50],
            'language' => [
                'url' => url('lang.json'),
            ],
            'drawCallback' => 'function() {
                $("#articles_length").prependTo($("#dataTables_length_box"));
            }',
        ];
    }
}

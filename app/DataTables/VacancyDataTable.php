<?php

namespace App\DataTables;

use App\Models\Vacancy;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class VacancyDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable

            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => 'vacancies', 'forceDelete' => true, 'largeModal' => true])->render();
            })
            ->rawColumns(['image', 'action']);
    }

    public function query(Vacancy $model)
    {
        $query = $model->newQuery();

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '100px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'vacancies.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'title', 'name' => 'vacancies.title','title' => 'Ad', 'searchable' => true],
            ['data' => 'work_experience', 'name' => 'vacancies.work_experience','title' => 'İŞ TƏCRÜBƏSİ', 'searchable' => true],
            ['data' => 'salary', 'name' => 'vacancies.salary','title' => 'Əmək Haqqı', 'searchable' => true],
            ['data' => 'end_date', 'name' => 'vacancies.end_date','title' => 'Bitmə Tarixi', 'searchable' => true],
            ['data' => 'lang', 'name' => 'vacancies.lang','title' => 'Dil', 'searchable' => false],
            ['data' => 'work_description', 'name' => 'vacancies.work_description','title' => 'Məlumat', 'searchable' => false],
            ['data' => 'created_at', 'name' => 'vacancies.created_at','title' => 'Yaradıldı', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'vacancies.updated_at', 'title' => 'Yenilənib', 'searchable' => false],
        ];
    }

    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => false,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'vacanciesdatatable_' . time();
    }

}

<?php

namespace App\DataTables;

use App\User;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class RegUsersDataTable extends DataTable
{
    protected $actions = ['excel', 'print'],$type=1;


    public function passData($type) {
        $this->type = $type;
        return $this;
    }

    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', function($row) {
                if($this->type ==2){
                    return view( 'widgets.action-user', ['route' => 'corporative_users', 'row' => $row])->render();
                }
            })
            ->editColumn('phone', function ($row){
                return "+994 ".$row->phone;
            });

    }

    public function query(User $model)
    {
        $query = $model->where('users.type',$this->type)->newQuery();

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '50px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
           ['data' => 'id', 'name' => 'users.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
           ['data' => 'email', 'name' => 'users.email','title' => 'Email', 'orderable' => false, 'searchable' => true],
           ['data' => 'name', 'name' => 'users.name','title' => 'Ad,Soyad', 'orderable' => false, 'searchable' => true],
            ['data' => 'phone', 'name' => 'users.phone','title' => 'Nömrə', 'searchable' => false],
        ];
    }

    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [25,50,100],
            'dom' => '<"col-md-6 hidden-xs pull-left"<"row"<"pull-left"l><"pull-left"<"col-md-12"B>>>><"pull-right col-md-6 custom-filter"f>rt<"col-md-3 col-sm-3"<"row"i>><"col-md-9"<"row"p>>',

            'buttons' => ['excel', 'print'],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'regUsersdatatable_' . time();
    }

}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OtherForm extends Mailable
{
    use Queueable, SerializesModels;

    public $form, $files, $subject,$blade;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($form,$blade, $files, $subject)
    {
        $this->form = $form;
        $this->files = $files;
        $this->subject = $subject;
        $this->blade = $blade;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if(isset($this->content['email'])){
            $replyTo = $this->content['email'];
        }
        else{
            $replyTo = config('mail.username');
        }

        $attach =  $this->replyTo($replyTo)
            ->subject($this->subject)
            ->markdown("emails.$this->blade")
            ->with('content',$this->form);

        if(!empty($this->files)){

            foreach ($this->files as $file){
                $attach->attach($file, [
                    'as' => $file->getClientOriginalExtension(),
                    'mime' => $file->getMimeType()
                ]);
            }
        }

        return $attach;


    }
}

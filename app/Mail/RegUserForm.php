<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegUserForm extends Mailable
{
    use Queueable, SerializesModels;

    public $content, $blade, $subject,$multi_user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content, $blade, $subject,$multi_user = null)
    {
        $this->content = $content;
        $this->blade = $blade;
        $this->subject = $subject;
        $this->multi_user = $multi_user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $replyTo = $this->content['email_test'];

        // if(isset($this->content['email'])){
        //     $replyTo = $this->content['email'];
        // }
        // else{
        //     $replyTo = config('mail.username');
        // }


        if($this->multi_user){
            return $this->subject($this->subject)
                ->bcc($this->multi_user)
                ->markdown("emails.$this->blade")
                ->with('content',$this->content);
        }else{
           return $this->subject($this->subject)
            ->markdown("emails.$this->blade")
            ->with('content',$this->content);
        }
      
    }
}

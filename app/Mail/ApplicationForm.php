<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApplicationForm extends Mailable
{
    use Queueable, SerializesModels;

    public $pdf, $form, $subject;

    public function __construct($pdf, $form,$files, $subject)
    {
        $this->pdf = $pdf;
        $this->form = $form;
        $this->files = $files;
        $this->subject = $subject;
    }


    public function build()
    {
        $replyTo = $this->form['email'];

        $attach = $this->replyTo($replyTo)
            ->subject($this->subject)
            ->markdown("emails.contact")
            ->attachData($this->pdf->output(), "application.pdf", ['mime' => 'application/pdf'] );



        if(!empty($this->files)){

            foreach ($this->files as $file){
                $attach->attach($file, [
                    'as' => $file->getClientOriginalExtension(),
                    'mime' => $file->getMimeType()
                ]);
            }
        }
        return $attach;

    }
}

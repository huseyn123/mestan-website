<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 10/5/18
 * Time: 19:35
 */

namespace App\Exceptions;

use Illuminate\Contracts\Support\Responsable;
use Throwable;

class WebServiceException extends \Exception implements Responsable
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function toResponse($request)
    {
        if($this->code != 200){
            return response()->json(['code' => $this->code, 'msg' => $this->message], $this->code, [], JSON_UNESCAPED_UNICODE);
        }
    }
}
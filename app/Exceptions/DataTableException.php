<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 10/5/18
 * Time: 19:35
 */

namespace App\Exceptions;

use Illuminate\Contracts\Support\Responsable;
use Throwable;

class DataTableException extends \Exception implements Responsable
{
    public function __construct($message = "", $code = 400, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function toResponse($request)
    {
        return response()->json(['code' => $this->code, 'msg' => $this->message], $this->code);
    }
}
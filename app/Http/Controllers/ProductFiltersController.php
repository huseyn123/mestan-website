<?php

namespace App\Http\Controllers;

use App\Crud\ProductFiltersCrud;
use App\DataTables\ProductFiltersDataTable;
use App\Exceptions\DataTableException;
use Illuminate\Http\Request;
use App\Models\ProductFilters;
use DB;
use Illuminate\Support\Facades\Validator;
use App\Logic\NoCrud;


class ProductFiltersController extends Controller
{
    use NoCrud;

    private $crud,$requests,$title,$route,$create_name,$columns =[];

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->title = "Məhsul Filtrləri";
        $this->create_name = "Yeni Filtr";
        $this->route = "product_filters";
        $this->view = "nocrud";
        $this->model = 'App\Models\ProductFilters';
        $this->requests = $request->except('_token', '_method');

        $this->columns = ['title'];

        $this->crud = new ProductFiltersCrud();
    }


    public function index(ProductFiltersDataTable $dataTable)
    {
        return $dataTable->render('admin.no_crud', ['title' => $this->title,'route' =>$this->route]);
    }


    public function create()
    {
        $fields = $this->crud->fields('create');

        return view('admin.no_crud.create', [ 'fields' => $fields, 'route' => $this->route.'.store','title' => $this->title,'create_name' =>$this->create_name,'colums' =>$this->columns]);
    }

    public function edit($id)
    {
        $data = $this->model::findOrFail($id);

        $fields = $this->crud->fields('edit', $data);

        return view('admin.no_crud.edit', ['title' => 'Düzəliş et', 'fields' => $fields,'data'=>$data,'route' => [$this->route.'.update', $id],'colums' =>$this->columns]);
    }

    public function update(Request $request, $id)
    {
        $data = $this->model::findOrFail($id);

        // check Validation
        $validation = Validator::make($request->all(),  $this->model::rules(),$this->model::$messages);

        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }


        foreach($this->requests as $key => $put){
            $data->$key = $put;
        }

        $data->save();

        return $this->responseSuccess();
    }


}



<?php

namespace App\Http\Controllers;

use App\DataTables\VacancyDataTable;
use App\Exceptions\DataTableException;
use Illuminate\Http\Request;
use App\Models\Vacancy;
use App\DataTables\CampaignDataTable;
use App\Crud\VacancyCrud;
use Illuminate\Support\Facades\Validator;
use DB;

class VacancyController extends Controller
{
    private $crud, $requests, $title;

    public function __construct(Request $request, VacancyCrud $crud)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->crud = $crud;
        $this->title = "Vakansiyalar";
        $this->requests = $request->except('_token', '_method');

    }


    public function index(VacancyDataTable $dataTable)
    {
        return $dataTable->render('admin.vacancies', ['title' => $this->title]);
    }


    public function create()
    {
        $fields = $this->crud->fields(null,'create');
        return view('admin.dt.create', ['title' => 'Yeni Vakansiya', 'fields' => $fields, 'route' => 'vacancies.store', 'editor' => true ]);
    }



    public function store(Request $request)
    {
        $validation = Validator::make($this->requests, Vacancy::rules(), Vacancy::$messages);

        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        Vacancy::create($this->requests);

        return $this->responseSuccess();
    }


    public function edit($id)
    {
        $data = Vacancy::findOrFail($id);

        $fields = $this->crud->fields(null,'edit', $data);

        return view('admin.dt.edit', ['title' => 'Düzəliş et', 'fields' => $fields, 'data' => $data, 'route' => ['vacancies.update', $id], 'editor' => true ]);
    }


    public function update(Request $request, $id)
    {
        $data = Vacancy::findOrFail($id);

        $validation = Validator::make($this->requests, Vacancy::rules(), Vacancy::$messages);

        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        foreach($this->requests as $key => $put){
            $data->$key = $put;
        }

        $data->save();

        return $this->responseSuccess();
    }



    public function destroy($id)
    {
        $data = Vacancy::findOrFail($id);
        $data->delete();

        return $this->responseSuccess("", "#vacancies", "#modal-confirm");
    }


    private function responseSuccess($msg = "", $draw = "#vacancies", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }
}

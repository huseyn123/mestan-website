<?php

namespace App\Http\Controllers;

use App\Crud\DeliveryCrud;
use App\DataTables\DeliveryDataTable;
use App\Exceptions\DataTableException;
use Illuminate\Http\Request;
use App\Models\DeliveryPrice;
use DB;
use Illuminate\Support\Facades\Validator;
use App\Logic\NoCrud;


class DeliveryController extends Controller
{
    use NoCrud;

    private $crud,$requests,$title,$route,$create_name,$columns =[];

    public function __construct(Request $request, DeliveryCrud $crud)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->crud = $crud;
        $this->model = 'App\Models\DeliveryPrice';
        $this->title = "Çatdırılma Qıyməti";
        $this->requests = $request->except('_token', '_method');
        $this->route = "delivery";

    }


    public function index(DeliveryDataTable $dataTable)
    {
        return $dataTable->render('admin.no_crud', ['title' => $this->title,'route' =>$this->route]);
    }


    public function create()
    {
        $fields = $this->crud->fields('create');

        return view('admin.delivery.create', [ 'fields' => $fields, 'route' => $this->route.'.store','title' => $this->title,'create_name' =>$this->create_name]);
    }

    public function edit($id)
    {
        $data = $this->model::findOrFail($id);

        $fields = $this->crud->fields('edit', $data);

        return view('admin.delivery.edit', ['title' => 'Düzəliş et', 'fields' => $fields,'data'=>$data,'route' => [$this->route.'.update', $id],'colums' =>$this->columns]);
    }

    public function update(Request $request, $id)
    {
        $data = $this->model::findOrFail($id);

        $this->requests['select'] = $request->select;


        // check Validation
        $validation = Validator::make($request->all(),  $this->model::rules(),$this->model::$messages);

        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }


        foreach($this->requests as $key => $put){
            $data->$key = $put;
        }

        $data->save();

        return $this->responseSuccess();
    }
}



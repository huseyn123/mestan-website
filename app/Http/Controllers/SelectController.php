<?php

namespace App\Http\Controllers;

use App\Crud\SelectCrud;
use App\DataTables\SelectDataTable;
use App\Exceptions\DataTableException;
use Illuminate\Http\Request;
use App\Models\Select;
use DB;
use Illuminate\Support\Facades\Validator;
use App\Logic\NoCrud;


class SelectController extends Controller
{
    use NoCrud;

    private $crud,$requests,$title,$route,$create_name,$columns =[];

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->title = "Select";
        $this->create_name = "Yeni Select";
        $this->route = "select";
        $this->view = "nocrud";
        $this->model = 'App\Models\Select';
        $this->requests = $request->except('_token', '_method');

        $this->columns = ['title'];

        $this->crud = new SelectCrud();
    }


    public function index(SelectDataTable $dataTable)
    {
        return $dataTable->render('admin.no_crud', ['title' => $this->title,'route' =>$this->route]);
    }


    public function create()
    {
        $fields = $this->crud->fields('create');

        return view('admin.select.create', [ 'fields' => $fields, 'route' => $this->route.'.store','title' => $this->title,'create_name' =>$this->create_name,'colums' =>$this->columns]);
    }

    public function edit($id)
    {
        $data = $this->model::findOrFail($id);

        $fields = $this->crud->fields('edit', $data);

        return view('admin.select.edit', ['title' => 'Düzəliş et', 'fields' => $fields,'data'=>$data,'route' => [$this->route.'.update', $id],'colums' =>$this->columns]);
    }

    public function update(Request $request, $id)
    {
        $data = $this->model::findOrFail($id);

        // check Validation
        $validation = Validator::make($request->all(),  $this->model::rules(),$this->model::$messages);

        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        $this->requests['form_id'] =$request->form_id;
        $this->requests['product_id'] =$request->product_id;

        foreach($this->requests as $key => $put){
            $data->$key = $put;
        }

        $data->save();

        return $this->responseSuccess();
    }


}



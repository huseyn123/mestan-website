<?php

namespace App\Http\Controllers;

use App\Logic\Share;
use App\Models\Config;
use App\Models\WhishList;
use App\Models\Product;
use Illuminate\Http\Request;
use Auth;
use DB;
use Validator;
use App\Logic\ProductsList;


class WhishListController extends Controller
{
    public $dActive;
    private $guard;

    use Share, ProductsList;

    public function __construct(Request $request)
    {
        $this->dActive=Config::where('key','discount_active')->first();
        $this->middleware(function ($request, $next) {
            view()->share('carts', $this->carts()->get());
            return $next($request);
        });
        $this->middleware('auth:web');
        $this->loadData();
        $this->guard = Auth::guard('web');
    }


    public function carts(){
        $carts = Product::join('product_translations as ptr','ptr.product_id','products.id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->join('carts', 'carts.product_id', '=', 'products.id')
            ->whereCondition()
            ->whereNull('ptr.deleted_at')
            ->whereNull('pt.deleted_at')
            ->where('ptr.lang',$this->lang)
            ->where('products.status',1)
            ->whereIn('pages.template_id',[4,5,6])
            ->orderBy('carts.id','desc')
            ->select('ptr.*','pt.slug as page_slug'
                ,'pt.id as tid','products.id'
                ,'products.old_price'
                ,'products.product_type'
                ,'products.bar_code'
                , 'carts.id as cart_id', 'carts.price as cart_prise','carts.product_id as cart_product_id', 'carts.count', DB::raw('carts.count * carts.price as amount'))
            ->with(['media']);
        if($this->dActive->value == 1){
            $carts->addSelect('products.price');
        }
        return $carts;
    }


    public function index()
    {
        if($this->guard->check()){
            $best_seller = $this->productsList()
                ->where('products.best_seller',1)
                ->get();

            $products = $this->productsList()->join('whish_lists as wt', 'wt.product_id', '=', 'products.id')->where('wt.user_id',$this->guard->user()->id)->get();

            return view('web.whish_list',['products'=> $products,'best_seller' =>$best_seller]);


        }else{
            return redirect(route('home'));
        }
    }


    public function store(Request $request)
    {
        if($this->guard->check()){
            $userId = $this->guard->user()->id;

            $validator = Validator::make($request->all(), WhishList::$rules);

            if ($validator->fails()) {
                return response()->json(['result'=>0,'msg' => $validator->errors()->first()]);
            }

            $product = Product::where('status', 1)->find($request->product_id);

            if(is_null($product)){
                if (request()->ajax()) {
                    return response()->json(['result'=>0,'msg' => 'Uyğun məhsul tapılmadı']);
                }
                return redirect()->back();
            }

            //eger mehsul artiq sebetdedirse
            $check = WhishList::where('user_id', $userId)
                ->where('product_id', $product->id)
                ->count();

            if($check > 0){
                if (request()->ajax()) {
                    WhishList::where('user_id', $userId)
                        ->where('product_id', $product->id)->delete();
                    return response()->json(['result'=>2,'msg' => 'Məhsul secilmislerden silindi']);
                }
                return redirect()->back();
            }

            $fields = [
                'user_id' => $userId,
                'product_id' => $product->id,
            ];


            DB::beginTransaction();

            $create = WhishList::create($fields);


            DB::commit();

            if (request()->ajax()) {
                return response()->json(['result'=>1,'msg' => 'Məhsul seçilmişlərə əlavə edildi']);
            }

        }else{
            if (request()->ajax()) {
                return response()->json(['result'=>0,'msg' => 'none']);
            }
            return redirect()->back();
        }



        if (request()->ajax()) {
            return response()->json(['result'=>0,'msg' => 'none']);
        }
        return redirect()->back();

    }

}

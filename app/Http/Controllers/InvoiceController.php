<?php

namespace App\Http\Controllers;

use App\DataTables\InvoiceDataTable;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Logic\Share;
use App\Notifications\newUserOrder;
use Illuminate\Support\Facades\Notification;
use DB;


class InvoiceController extends Controller
{
    private $route;
    public $title;
    use Share;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin')->except(['pdf']);
        $this->route = $request->segment(2);
        $this->title = $this->route == 'cor_invoices' ? 'Xisusi Sifarişlər' : "Sifarişlər";
        $this->loadData();

    }


    public function index(InvoiceDataTable $dataTable)
    {
        return $dataTable->route($this->route)->render('admin.invoices', ['title' => $this->title]);
    }

    public function show($id)
    {
        $invoice = Order::findOrFail($id);
        $products =  Product::join('product_translations as ptr','ptr.product_id','products.id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->join('carts', 'carts.product_id', '=', 'products.id')
            ->join('orders','carts.order_id','orders.id')
//            ->whereNull('ptr.deleted_at')
//            ->whereNull('pt.deleted_at')
            ->where('ptr.lang',$this->lang)
//            ->where('products.status',1)
            ->whereIn('pages.template_id',[4,5,6])
            ->where('orders.id',$invoice->id)
            ->where('carts.user_id',$invoice->user_id)
            ->orderBy('carts.id','desc')
            ->select('ptr.name','pt.id as tid',
                'products.id','products.bar_code'
                , 'carts.id as cart_id', 'carts.price as cart_prise','carts.product_id as cart_product_id', 'carts.count', DB::raw('carts.count * carts.price as amount'))
            ->with(['media'])
            ->get();

        $total_price = ($invoice->amount + $invoice->delivery_price) - ($invoice->special_percent ? (number_format(($invoice->amount * $invoice->special_percent/100),2)) : 0);        

        return view('admin.invoice-detail', ['invoice' => $invoice, 'title' => 'Sifariş #'.$invoice->id,'products' =>$products,'total_price' => $total_price]);
    }


    public function pdf($id)
    {
        $invoice = Order::findOrFail($id);

        $pdfForm = \PDF::loadView('pdf.invoice-detail-pdf', ['invoice' => $invoice, 'title' => 'Sifariş #'.$invoice->id]);
        return $pdfForm->stream('dsffds.pdf');
//        return $pdfForm->setPaper('a4', 'landscape')->download('Invoice#'.$invoice->id.'.pdf');
    }


    public function destroy($id)
    {

        $data = Order::findOrFail($id);


        $data->show = 0;
        $data->save();

        return $this->responseSuccess("", "#invoices", "#modal-confirm");
    }



    public function invoiceUpdate($id, Request $request)
    {


        $value = $request->value;
        $type = $request->type;



        $data = Order::findOrFail($id);
//        if($type == 2 && $data->email){
//            Notification::route('mail',$data->email)->notify(new newUserOrder($order, $user,$this->dictionary[$order->payment_type] ?? config('config.paymentMethod_text.'.$order->payment_type)));
//
////            Notification::route('mail', $data->email)->notify(new newOrder($order,$this->dictionary[$order->payment_type] ?? config('config.paymentMethod_text.'.$order->payment_type)));
//        }

        $data->status = $value;
        $data->save();

        return $this->responseSuccess( $value, "#invoices", ".editable-status");

    }

    private function responseSuccess($msg = "", $draw = "#invoices", $close = "#myModal")
    {

        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }
    
}

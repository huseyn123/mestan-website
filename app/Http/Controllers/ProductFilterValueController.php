<?php

namespace App\Http\Controllers;

use App\Crud\ProductFilterValueCrud;
use App\DataTables\ProductFilterValueDataTable;
use App\Exceptions\DataTableException;
use Illuminate\Http\Request;
use App\Models\ProductFilterValue;
use DB;
use Illuminate\Support\Facades\Validator;
use App\Logic\NoCrud;


class ProductFilterValueController extends Controller
{
    use NoCrud;

    private $crud,$requests,$title,$route,$create_name,$columns =[];

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->title = "Filtr Value";
        $this->create_name = "Yeni Value";
        $this->route = "product_filter_value";
        $this->view = "nocrud";
        $this->model = 'App\Models\ProductFilterValue';
        $this->requests = $request->except('_token', '_method');

        $this->columns = ['title'];

        $this->crud = new ProductFilterValueCrud();
    }


    public function index(ProductFilterValueDataTable $dataTable)
    {
        return $dataTable->render('admin.no_crud', ['title' => $this->title,'route' =>$this->route]);
    }


    public function create()
    {
        $fields = $this->crud->fields('create');

        return view('admin.no_crud.create', [ 'fields' => $fields, 'route' => $this->route.'.store','title' => $this->title,'create_name' =>$this->create_name,'colums' =>$this->columns]);
    }

    public function edit($id)
    {
        $data = $this->model::findOrFail($id);

        $fields = $this->crud->fields('edit', $data);

        return view('admin.no_crud.edit', ['title' => 'Düzəliş et', 'fields' => $fields,'data'=>$data,'route' => [$this->route.'.update', $id],'colums' =>$this->columns]);
    }

}



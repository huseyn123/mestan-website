<?php

namespace App\Http\Controllers\Auth;

use App\Providers\RouteServiceProvider;
use App\Rules\Cellphone;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::USER;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'name' => ['required','string','max:40'],
            'email' => ['required','email','unique:users'],
            'password' => ['required','string','min:8','confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $newUser =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
//            'phone' => $data['phone_prefix'].$data['phone'],
//            'sex' => $data['sex'],
            'password' => Hash::make($data['password']),
        ]);

        return $newUser;
    }


    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $request->session()->flash('auth_error', true);

            if(request()->ajax())
            {
                return response()->json(['result' => 0,'error'=>$validator->errors()->first()]);
            }

            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);


        if(request()->ajax())
        {
            return response()->json(['result' => 1,'route' => $this->redirectPath()]);
        }

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }


    public function showRegistrationForm()  //this will be deleted
    {
        return view('web.index');
    }
}

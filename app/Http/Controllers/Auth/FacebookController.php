<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use Exception;
use Auth;

class FacebookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->fields([
            'name', 'email', 'gender'
        ])->scopes([
            'email'
        ])->redirect();
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleFacebookCallback()
    {
        try {
            $user = Socialite::driver('facebook')->fields([
                'name', 'email', 'gender'
            ])->user();

            //$sex = config('config.sex.'.$user->user['gender']);

            $create['name'] = $user->getName();
            $create['email'] = $user->getEmail();
            $create['facebook_id'] = $user->getId();

            $userModel = new User;
            $createdUser = $userModel->registerByFacebook($create);
            Auth::loginUsingId($createdUser->id);

            return redirect()->route('user.profile');

        } catch (Exception $e) {
            return redirect()->route('home')->withErrors(['error' => 'Unexpected Error!']);
        }
    }
}
<?php

namespace App\Http\Controllers;

use App\Exceptions\DataTableException;
use Illuminate\Http\Request;
use App\Models\PromoCode;
use App\DataTables\PromoCodeDataTable;
use App\Crud\PromoCodeCrud;
use DB;
use Illuminate\Support\Facades\Validator;

class PromoCodeController extends Controller
{
    private $crud, $route, $requests;
    public $title;

    public function __construct(Request $request, PromoCodeCrud $crud)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->crud = $crud;
        $this->requests = $request->except('_token', '_method');
        $this->title = "Promo Kod";

    }
 

    public function index(PromoCodeDataTable $dataTable)
    {
        return $dataTable->render("admin.promo_code", ['title' => $this->title]);
    }


    public function create()
    {
        $fields = $this->crud->fields('create');

        return view("admin.dt.create",  ['fields' => $fields, 'title' => 'Yeni Promo kod', 'route' => 'promo_code.store']);
    }


    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), PromoCode::rules($request->id), PromoCode::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        $promo_code =PromoCode::create($this->requests);

        return $this->responseSuccess();
    }



    public function edit($id)
    {
        $data = PromoCode::findOrFail($id);

        $fields = $this->crud->fields('edit', $data);

        return view('admin.dt.edit', ['title' => 'Düzəliş et', 'fields' => $fields, 'data' => $data, 'route' => ['promo_code.update', $id]]);
    }


    public function update(Request $request, $id)
    {
        $data = PromoCode::findOrFail($id);

        $validation = Validator::make($request->all(), PromoCode::rules($data->id), PromoCode::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }


        foreach($this->requests as $key => $put){
            $data->$key = $put;
        }

        $data->save();

        return $this->responseSuccess();
    }

    public function trash($id)
    {
        $data = PromoCode::findOrFail($id);
        $data->delete();

        return $this->responseSuccess("", "#promo_codes", "#modal-confirm");

    }


    public function restore($id)
    {
        $member = PromoCode::onlyTrashed()->findOrFail($id);

        $member->restore();

        return $this->responseSuccess("", "#promo_codes", "#modal-confirm");
    }


    public function destroy($id)
    {
        $data = PromoCode::onlyTrashed()->findOrFail($id);

        $data->forceDelete();

        return $this->responseSuccess("", "#promo_codes", "#modal-confirm");
    }


    private function responseSuccess($msg = "", $draw = "#promo_codes", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }
}



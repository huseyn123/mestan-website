<?php

namespace App\Http\Controllers;

use App\Logic\ImageRepo;
use Illuminate\Http\Request;
use App\Models\Video;
use App\DataTables\VideoDataTable;
use App\Crud\VideoCrud;
use DB;
use Illuminate\Support\Facades\Validator;

class VideoController extends Controller
{
    private $crud, $route, $image, $requests;
    public $title;

    public function __construct(Request $request, ImageRepo $imageUpload, VideoCrud $crud)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->image = $imageUpload;
        $this->crud = $crud;
        $this->requests = $request->except('_token', '_method');
        $this->title = "Videolar";

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('mainVideo', false);
        }

    }


    public function index(VideoDataTable $dataTable)
    {
        return $dataTable->render("admin.videos", ['title' => $this->title]);
    }


    public function create()
    {
        $fields = $this->crud->fields('create');

        return view("admin.dt.create",  ['fields' => $fields, 'title' => 'Yeni video', 'route' => 'videos.store']);
    }


    public function store(Request $request)
    {
        $response = $this->validation();

        if($response['error'] == 1){
            return $this->responseJson($response);
        }

        $upload = $this->image->store($request->image, $this->resize());
        $this->requests['image'] = $upload;

        Video::create($this->requests);

        return $this->responseJson($response);
    }


    public function edit($id)
    {
        $video = Video::findOrFail($id);

        $fields = $this->crud->fields('edit', $video);

        return view('admin.dt.edit', ['title' => 'Düzəliş et', 'fields' => $fields, 'data' => $video, 'route' => ['videos.update', $id]]);
    }


    public function update(Request $request, $id)
    {
        $video = Video::findOrFail($id);

        $response = $this->validation(null);

        if($response['error'] == 1){
            return $this->responseJson($response);
        }

        $selected = $request->input('selected', 0);
        $this->requests['selected'] = $selected;

        if($request->hasFile('image')){
            $upload = $this->image->store($request->image, $this->resize());
            $this->image->deleteFile($video->image); //delete old file
            $this->requests['image'] = $upload;
        }

        foreach($this->requests as $key => $put){
            $video->$key = $put;
        }

        $video->save();

        return $this->responseJson($response);
    }


    public function destroy($id)
    {
        $data = Video::findOrFail($id);

        $data->delete();
        $this->image->deleteFile($data->image);

        $response = $this->responseDataTable(0,"", "#videos", "#modal-confirm");
        return $this->responseJson($response);

    }

    protected function resize()
    {
        $resizeImage = ['resize' => ['fit' => true, 'size' => [450, 270]], 'thumb' => null ];

        return $resizeImage;
    }

    private function validation($required = 'required|')
    {
        $validation = Validator::make($this->requests, Video::rules($required), Video::$messages);

        $response = $this->responseDataTable(0,"", "#videos", '#myModal');

        if($validation->fails()){
            $response = $this->responseDataTable(1, $validation->errors()->first());
        }

        return $response;
    }
}



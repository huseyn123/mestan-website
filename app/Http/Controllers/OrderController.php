<?php

namespace App\Http\Controllers;

use App\Logic\EposApi;
use App\Logic\ProductsList;
use App\Logic\Share;
use App\Logic\WebService;
use App\Models\Cart;
use App\Models\Config;
use App\Models\DeliveryPrice;
use App\Models\Order;
use App\Models\PostTerminal;
use App\Models\Product;
use App\Models\PromoCode;
use App\Models\UserPromo;
use App\Notifications\newOrder;
use App\Notifications\newUserOrder;
use App\Rules\Cellphone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Spatie\ArrayToXml\ArrayToXml;
use Auth;
use DB;
use Cookie;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    public $dActive;
    use Share, ProductsList;

    private $guard;

    public function __construct()
    {
        $this->dActive=Config::where('key','discount_active')->first();
        $this->middleware(function ($request, $next) {
            view()->share('carts', $this->carts()->get());
            return $next($request);
        });

        //$this->middleware('auth:web');
        $this->loadData();
        $this->guard = Auth::guard('web');
    }

    public function carts(){
        $carts = Product::join('product_translations as ptr','ptr.product_id','products.id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->join('carts', 'carts.product_id', '=', 'products.id')
            ->whereCondition()
            ->whereNull('ptr.deleted_at')
            ->whereNull('pt.deleted_at')
            ->where('ptr.lang',$this->lang)
            ->where('products.status',1)
            ->whereIn('pages.template_id',[4,5,6])
            ->orderBy('carts.id','desc')
            ->select('ptr.*','pt.slug as page_slug'
                ,'pt.id as tid','products.id'
                ,'products.old_price'
                ,'products.product_type'
                ,'products.bar_code'
                , 'carts.id as cart_id', 'carts.price as cart_prise','carts.product_id as cart_product_id', 'carts.count', DB::raw('carts.count * carts.price as amount'))
            ->with(['media']);
        if($this->dActive->value == 1){
            $carts->addSelect('products.price');
        }
        return $carts;
    }

    public function index(Request $request)
    {
        if(!Auth::guard('web')->check()){
            return redirect()->route('home');
        }
        else{
          $user = $this->guard->user();
          $orders = Order::where('user_id',$user->id)->whereNotNull('payment_key')->orderBy('created_at','desc')->get();

          $best_seller = $this->productsList()
            ->where('products.best_seller',1)
            ->get();

          return view("web.order.list", ['user' => $user, 'orders' => $orders,'best_seller' =>$best_seller]);

        }
    }

    public function post(Request $request)
    {
        $user = auth()->guard('web')->user();
        $promo_code = $request->promo_code ?? null;
        $discount_amount = 0;
        $special_percent = null;


        $validator = Validator::make($request->all(), Order::$rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors()->first());
        }

        if(is_null($request->phone) || trim($request->phone) == '' || is_null($request->address)){
            $message = null;
            if((is_null($request->phone) || trim($request->phone) == '') && is_null($request->address)){
                $message = 'Sifarişi rəsmiləşdirmək üçün əlaqə nömrənizi və ünvanı  qeyd edin.';
            }elseif ($request->phone && is_null($request->address)){
                $message = 'Sifarişi rəsmiləşdirmək üçün  ünvanı  qeyd edin.';
            }elseif ($request->address && (is_null($request->phone) || trim($request->phone) == '')){
                $message = 'Sifarişi rəsmiləşdirmək üçün əlaqə nömrənizi qeyd edin.';
            }
            return redirect()->back()->withErrors($message);
        }


        $products = Cart::whereCondition();
        $total = $products->sum(DB::raw('price*count'));


        // check user birthday
        if($user && $user->birthday){
            if($user->birthday->status == 1 && checkBirthdayPrice($user->birthday->b_day)){
                $special_percent = config('config.birthday_percent');
                $birthday_status = 1;
                 $new_sum = $this->carts()->sum(DB::raw('carts.count * carts.price'));
                 $discount_amount =number_format(($new_sum * $special_percent/100),2);
            }
        }


        // if($total > 0 && $total <= 100){
        //     $special_percent = 15;

        // }elseif($total > 100 && $total <= 200){
        //     $special_percent = 20;
        // }else{
        //     $special_percent = 25;
        // }


        if($user && $promo_code && $special_percent == null){
            $check_promo = PromoCode::where('promo_code',$promo_code)
            ->where('end_date','>=',date('Y-m-d'))
            ->where('start_date','<=',date('Y-m-d'))
            ->with(['user_promo' => function($q) use($user){
                $q->where('user_id',$user->id);
            }])->first();

            if($check_promo){
                if($check_promo->user_promo){
                    $promo_message ='Bu endirim kodu istifadə olunub';
                    return redirect('/cart/checkout')
                        ->withErrors($promo_message)
                        ->withInput();
                }else{
                    UserPromo::create([
                        'user_id' => $user->id,
                        'promo_id' => $check_promo->id
                    ]);
                    $special_percent = $check_promo->percent;
                    $new_sum = $this->carts()->sum(DB::raw('carts.count * carts.price'));
                    $discount_amount =number_format(($new_sum * $special_percent/100),2);
                }
            }else{
                $promo_message ='Bu endirim kodu mövcud deyil';
                return redirect('/cart/checkout')
                    ->withErrors($promo_message)
                    ->withInput();            }
        }


        $this_price = DeliveryPrice::where('price',$request->delivery_price)->select('id','price','select')->first();
        $discounted_price = (isset($this_price) && $total > 30 && $this_price->select == 1) ? 0 : $request->delivery_price;
        $total_amount = ($total + $discounted_price) - $discount_amount;


//        $a = $row->amount + $row->delivery_price) - ($row->special_percent ? number_format(($row->amount * $row->special_percent/100),2) : 0 ) ;


        if(count($products->pluck('id')) == 0){
            return redirect()->back();
        }

        DB::beginTransaction();

        $order = Order::create([
            'user_id' => $user->id ?? null,
            'user_type' => $user->type ?? 1,
            'email' => $user->email ?? null,
            'user_name' => $request->fullname,
            'user_phone' => $request->phone,
            'address' => $request->address,
            'special_percent' => $special_percent,
            'promo_id' => $check_promo->id ?? null,
            'amount' => $total,
            'total_amount' => $total_amount,
            'birthday_status' => $birthday_status ?? 0,
            'delivery_price' =>$discounted_price,
            'payment_type' => $request->paymentMethod,
            'desc' => $request->desc,
        ]);

        $paymentKey = md5(now().$order->id.$order->address.$order->user_phone);

        Cart::whereIn('id', $products->pluck('id'))->update(['user_id' => $user->id ?? null, 'order_id' => $order->id,'purchased' => 1]);

        $order->code = 200;
        $order->payment_key = $paymentKey;
        $order->save();

        $end_price = $order->total_amount ? $order->total_amount : $total - number_format(($total * $special_percent/100),2) + $discounted_price;


        $email = Config::whereIn('key', ['order_email','order_email2'])->pluck('value');

        DB::commit();

        try {

    // //        if(env('APP_ENV') == 'production'){
             // Notification::route('mail',$email)->notify(new newOrder($order,$this->dictionary[$order->payment_type] ?? config('config.paymentMethod_text.'.$order->payment_type), $end_price));


            if(!is_null($user)){
                Notification::route('mail', $user->email)->notify(new newUserOrder($order, $user,$this->dictionary[$order->payment_type] ?? config('config.paymentMethod_text.'.$order->payment_type), $end_price));
            }
        }
         catch (\Exception $e) {dd('a');
            return redirect()->back()->withErrors($e->getMessage());
    }

        return redirect()->route('order.complete',[$order->payment_key])->withCookie(Cookie::forget('cartCookie'));
    }

    public function completeOrder($payment_key)
    {
        $user = $this->guard->user();

        // if(!$user){
        //     return redirect()->route('home');
        // }

        $order = Order::where('payment_key',$payment_key)->first();
        return view("web.order.complate", ['order' => $order,'user' => $user]);

    }

    public function list($key)
    {


        $user = $this->guard->user();

        $user = $this->guard->user();

        if(!$user){
            return redirect()->route('home');
        }

        $order =Order::where('user_id',$user->id)->where('payment_key',$key)->where('code', 200)->first();

        $products =  Product::join('product_translations as ptr','ptr.product_id','products.id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->join('carts', 'carts.product_id', '=', 'products.id')
            ->join('orders','carts.order_id','orders.id')
//            ->whereNull('ptr.deleted_at')
//            ->whereNull('pt.deleted_at')
            ->where('ptr.lang',$this->lang)
//            ->where('products.status',1)
            ->whereIn('pages.template_id',[4,5,6])
            ->where('orders.id',$order->id)
            ->where('carts.user_id',$user->id)
            ->orderBy('carts.id','desc')
            ->select('ptr.name','pt.id as tid',
                'products.id','products.bar_code'
                , 'carts.id as cart_id', 'carts.price as cart_prise','carts.product_id as cart_product_id', 'carts.count', DB::raw('carts.count * carts.price as amount'))
            ->with(['media'])
            ->get();


        return view("web.order.order_list", ['key' => $key,'user' =>$user,'products' =>$products,'order' => $order]);
    }


    public function invoicePdf($key)
    {
        $type = request()->get('type', 'download');

        $invoice = Order::where('payment_key', $key)->where('code', 200)->first();

        $pdfForm = \PDF::loadView('pdf.invoice-detail-pdf', ['invoice' => $invoice, 'title' => 'Sifariş #'.$invoice->id]);

        if($type == 'stream'){
            return $pdfForm->setPaper('a4', 'landscape')->stream('Invoice#'.$invoice->id.'.pdf');
        }
        else{
            return $pdfForm->setPaper('a4', 'landscape')->download('Invoice#'.$invoice->id.'.pdf');
        }
    }
}

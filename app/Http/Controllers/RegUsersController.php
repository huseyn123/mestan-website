<?php

namespace App\Http\Controllers;

use App\Crud\RegUserEmailCrud;
use App\DataTables\RegUsersDataTable;
use App\Models\Dictionary;
use App\User;
use App\Crud\CorUserCrud;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\DataTableException;
use Illuminate\Support\Facades\Hash;
use App\Mail\RegUserForm;
use App\Models\Config;
use Mail;
use DB;
use Batch;

class RegUsersController extends Controller
{

    public $title;

    public function __construct(CorUserCrud $crud,RegUserEmailCrud $emailcrud)
    {
        $this->crud = $crud;
        $this->email_crud = $emailcrud;
        $this->middleware('auth:admin')->except(['pdf']);
        $this->title = "Users";
    }


    public function index(RegUsersDataTable $dataTable)
    {
        return $dataTable->render('admin.reg_users', ['title' => $this->title]);
    }

    public function corporative_users(RegUsersDataTable $dataTable)
    {
        return $dataTable->passData(2)->render('admin.cor_users', ['title' => $this->title]);
    }

    public function create()
    {
        $fields = $this->crud->fields('create');

        return view("admin.dt.create",  ['fields' => $fields, 'title' => 'Yeni User', 'route' => 'cor.post']);
    }

    public function emailCreate()
    {
        $fields = $this->email_crud->fields('create');

        return view("admin.dt.create",  ['fields' => $fields, 'title' => 'Yeni Email', 'route' => 'reg_email.post','editor'=>true]);
    }


    public function sendEmail(Request $request)
    {

        $validation =Validator::make($request->all(), [
            'subject' => ['required'],
            'content' => ['required'],
            ]);

        $content = $request['content'];


        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }
        $subject = $request->subject;



// $a = ['hhuseynhuseynli@gmail.com','mailofhuseyn@gmail.com','huseyn.h@code.edu.az','noreply.n123@gmail.com'];

// Mail::to('mailofhuseyn@gmail.com')->send(new RegUserForm($request->all(),'reg_user',$subject));

        if($request->email_test){
            try {
                Mail::to([$request->email_test])->send(new RegUserForm($request->all(),'reg_user',$subject));

            } catch (\Exception $e) {
                throw new DataTableException($e->getMessage());
            }
            $response = ["code" => 200, "msg" => 'Email Gonderildi', "draw" => ' ', "close" => '#myModal'];
            return response()->json($response, $response['code']);

        }else{
            $check_sending_email = Config::where('key','email_start')->select('value')->first();

            if($check_sending_email->value == 0){
                $limit =50;
                $users= User::where('users.type',1)->orderBy('id','DESC')->skip(0)->take($limit)->pluck('email');

                try {
                    Mail::to('info@mestan.az')->send(new RegUserForm($request->all(),'reg_user',$subject,$users));
                } catch (\Exception $e) {
                    throw new DataTableException($e->getMessage());
                }

                $config_model = new Config;
                $all_data = [];
                array_push($all_data,    [
                    'key' => 'email_start',
                    'value' => $limit,
                    'config_path' => $subject
                ]);
                $index = 'key';
                Batch::update($config_model, $all_data, $index);
                //-------------------------------------------------------------


                Dictionary::where([['keyword','multi_email_content'],['lang_id','az']])->update(['content' => $content]);

                Artisan::call('cache:clear');
                Artisan::call('config:clear');

                $response = ["code" => 200, "msg" => 'Email Gonderildi', "draw" => ' ', "close" => '#myModal'];

            }else{
                throw new DataTableException('Evvelki Email Gonderilib qurtarmayib');
            }

            return response()->json($response, $response['code']);
        }
    }


    public function store(Request $request)
    {
        $validation =Validator::make($request->all(), [
            'name' => ['required','string','max:40'],
            'email' => ['required','email','unique:users'],
            'password' => ['required','string','min:6','confirmed'],
        ]);


        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }



        $newUser =  User::create([
            'name' => $request->name,
            'email' =>  $request->email,
            'type' =>  $request->type,
//            'phone' => $data['phone_prefix'].$data['phone'],
//            'sex' => $data['sex'],
            'password' => Hash::make($request->password),
        ]);

        return $this->responseSuccess();
    }

    public function destroy($id)
    {
        $data = User::findOrFail($id);
        $data->forceDelete();

        return $this->responseSuccess("", "#reg_users", "#modal-confirm");
    }

    private function responseSuccess($msg = "", $draw = "#reg_users", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }

}
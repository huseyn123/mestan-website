<?php

namespace App\Http\Controllers;

use App\Crud\AdminCrud;
use App\DataTables\AdminsDataTable;
use App\Admin;
use App\Exceptions\DataTableException;
use App\Logic\ImageRepo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Hash;

class AdminController extends Controller
{
    private $requests, $crud, $title;

    public function __construct(Request $request, AdminCrud $crud)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index', 'profile', 'updatePhoto']);

        $this->requests = $request->except('_token', '_method', 'type');
        $this->crud = $crud;
        $this->title = 'Adminlər';
    }


    public function index(AdminsDataTable $dataTable)
    {
        return $dataTable->render('admin.admins', ['title' => $this->title]);
    }

    public function profile()
    {
        $user = auth()->guard('admin')->user();

        $fields = $this->crud->fields('profile', $user, false);

        return view('admin.profile', ['fields' => $fields, 'title' => 'Profil', 'disabled' => false]);
    }


    public function create()
    {
        $fields = $this->crud->fields('create');

        return view('admin.dt.create', ['fields' => $fields, 'title' => 'Yeni admin', 'route' => 'admins.store']);
    }


    public function store(Request $request)
    {
        $validation = Validator::make($this->requests, Admin::rules(), Admin::$messages);

        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        $this->requests['password'] =  Hash::make($request->password);

        Admin::create($this->requests);

        return $this->responseSuccess();
    }


    public function edit($id)
    {
        $data = Admin::findOrFail($id);

        $fields = $this->crud->fields('edit', $data);

        return view('admin.dt.edit', ['data' => $data, 'fields' => $fields, 'route' => ['admins.update', $id] ]);
    }


    public function update(Request $request, $id)
    {
        $data = Admin::findOrFail($id);

        $type = $request->input('type', 1);

        $validation = Validator::make($this->requests, Admin::rules($id), Admin::$messages);

        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        foreach($this->requests as $key => $put){
            $data->$key = $put;
        }

        $data->save();

        if($type == 1){
            return $this->responseSuccess();
        }
        else{
            return $this->responseSuccess("Dəyişikliklər qeydə alındı", false, false);

        }
    }


    public function updatePassword(Request $request)
    {
        $user = auth()->guard('admin')->user();

        $inputs = [
            'current_password' => 'required|current_password_match',
            'password'     => 'required|min:6|confirmed',
        ];

        $validation = Validator::make($request->all(), $inputs);

        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        $user->password = Hash::make($inputs['password']);
        $user->save();

        return $this->responseSuccess("Şifrəniz yeniləndi", false, false);
    }


    public function updatePhoto(Request $request)
    {
        $image = new ImageRepo;
        $resizeImage = ['resize' => ['fit' => true, 'size' => [215, 215]], 'thumb' => null ];
        $user = auth()->guard('admin')->user();

        $inputs = [
            'photo' => 'required|mimes:png,gif,jpeg,jpg,bmp|max:10000'
        ];

        $request->validate($inputs);

        $upload = $image->store($request->photo, $resizeImage);
        $image->deleteFile($user->image); //delete old file

        $user->image = $upload;
        $user->save();

        return redirect()->back();
    }


    public function trash($id)
    {
        $data = Admin::findOrFail($id);

        $admins = Admin::count();

        if($admins == 1){
            throw new DataTableException("Başqa admin olmadığı üçün əməliyyat baş tutmadı");
        }

        $data->delete();

        return $this->responseSuccess();
    }


    public function restore($id)
    {
        $data = Admin::onlyTrashed()->findOrFail($id);

        $data->restore();

        return $this->responseSuccess();
    }


    /*public function destroy($id)
    {
        $data = Admin::onlyTrashed()->findOrFail($id);

        $data->forceDelete();

        return $this->responseSuccess("");

    }*/

    private function responseSuccess($msg = "", $draw = "#admins", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }
}

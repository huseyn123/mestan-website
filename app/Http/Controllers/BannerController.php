<?php

namespace App\Http\Controllers;
use App\Crud\BannerCrud;
use App\Exceptions\DataTableException;
use App\Logic\ImageRepo;
use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\DataTables\BannerDataTable;
use DB;

class BannerController extends Controller
{
    protected $input, $requestInputs, $image, $type, $crud;
    public $title;

    public function __construct(Request $request, ImageRepo $image, BannerCrud $crud)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->crud = $crud;
        $this->requestInputs = $request->except(['_method', '_token']);
        $this->image = $image;
        $this->title = 'Bannerlər';
    }


    public function index(BannerDataTable $dataTable)
    {
        return $dataTable->render('admin.banners', ['title' => $this->title, "type" => $this->type]);
    }


    public function show($id)
    {
        $banner = Banner::findOrFail($id);

        $html = '<img src="'.asset($banner->banner_url).'" style="max-width:100%">';

        return $html;
    }


    public function create()
    {
        $fields = $this->crud->fields('create');

        return view('admin.dt.create', ['title' => 'Yeni banner', 'fields' => $fields, 'route' => 'banners.store' ]);
    }


    public function edit($id)
    {
        $info = Banner::findOrFail($id);

        $fields = $this->crud->fields('edit', $info);

        return view('admin.dt.edit', ["data" => $info, 'fields' => $fields, 'title' => ' Düzəliş et', 'route' => ['banners.update', $id] ]);
    }



    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Banner::$rules);

        if ($validator->fails()) {
            throw new DataTableException($validator->errors()->first());
        }

        $max = Banner::where('type', $request->type)->status()->where('device', $request->device)->max('viewed');

        DB::beginTransaction();

        try{
            $this->requestInputs['viewed'] = $max ?? 0;

            $upload = $this->image->store($request->banner_url, $this->resize());
            $this->requestInputs['banner_url'] = $upload;

            Banner::create($this->requestInputs);
        }
        catch(\Exception $e){
            DB::rollback();
            throw new DataTableException($e->getMessage());
        }

        DB::commit();

        return $this->responseSuccess();
    }


    public function update(Request $request, $id)
    {
        $find = Banner::findOrFail($id);

        $validator = Validator::make($request->all(), Banner::$rules);

        if ($validator->fails()) {
            throw new DataTableException($validator->errors()->first());
        }

        $max = Banner::where('type', $request->type)->status()->where('device', $request->device)->max('viewed');


        if($find->getOriginal('status') == 0 && $request->status == 1)
        {
            $this->requestInputs['viewed'] = $max ?? $find->viewed;
        }

        if($request->hasFile('banner_url')){
            $upload = $this->image->store($request->banner_url, $this->resize());
            $this->image->deleteFile($find->banner_url); //delete old file
            $this->requestInputs['banner_url'] = $upload;
        }

        DB::beginTransaction();

        try{
            foreach($this->requestInputs as $key => $input){
                $find -> $key = $input;
            }
        }
        catch(\Exception $e){
            DB::rollBack();
            throw new DataTableException($e->getMessage());
        }


        $find -> save();

        DB::commit();

        return $this->responseSuccess();
    }


    public function destroy($id)
    {
        $data = Banner::findOrFail($id);

        $data->delete();

        $this->image->deleteFile($data->image); //delete old file

        return $this->responseSuccess("", "#banners", "#modal-confirm");
    }


    private function resize()
    {
        $resizeImage = ['resize' => ['fit' => false, 'size' => [300, null]], 'thumb' => null ];

        return $resizeImage;
    }


    private function responseSuccess($msg = "", $draw = "#banners", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }
}

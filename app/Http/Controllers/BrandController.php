<?php

namespace App\Http\Controllers;

use App\Exceptions\DataTableException;
use Illuminate\Http\Request;
use App\Models\Brand;
use App\DataTables\BrandDataTable;
use App\Crud\BrandCrud;
use Illuminate\Support\Facades\Validator;
use DB;

class BrandController extends Controller
{
    private $crud, $requests, $title;

    public function __construct(Request $request, BrandCrud $crud)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->crud = $crud;
        $this->title = "Brendlər";
        $this->requests = $request->except('_token', '_method','image');

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('brand', false);
        }
    }


    public function index(BrandDataTable $dataTable)
    {
        return $dataTable->render('admin.brands', ['title' => $this->title]);
    }


    public function create()
    {
        $fields = $this->crud->fields(null,'create');
        return view('admin.dt.create', ['title' => 'Yeni Brend', 'fields' => $fields, 'route' => 'brands.store']);
    }



    public function store(Request $request)
    {
        // check Validation
        $validation = Validator::make($request->all(), Brand::rules(), Brand::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        $brand = Brand::create($this->requests);

        $brand
            ->addMedia($request->image)
            ->toMediaCollection();


        return $this->responseSuccess();
    }


    public function edit($id)
    {
        $data = Brand::findOrFail($id);

        $fields = $this->crud->fields(null,'edit', $data);

        return view('admin.dt.edit', ['title' => 'Düzəliş et', 'fields' => $fields, 'data' => $data, 'route' => ['brands.update', $id]]);

    }


    public function update(Request $request, $id)
    {
        $data = Brand::findOrFail($id);

        // check Validation
        $validation = Validator::make($request->all(), Brand::rules('sometimes'), Brand::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        foreach($this->requests as $key => $put){
            $data->$key = $put;
        }

        if($request->hasFile('image')){

            $data->clearMediaCollection();  //delete old file
            $data->addMedia($request->image)
                ->toMediaCollection();
        }


        $data->save();

        return $this->responseSuccess();
    }



    public function destroy($id)
    {
        $data = Brand::findOrFail($id);
        $data->delete();

        return $this->responseSuccess("", "#brands", "#modal-confirm");
    }


    private function responseSuccess($msg = "", $draw = "#brands", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }
}

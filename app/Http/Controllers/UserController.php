<?php

namespace App\Http\Controllers;

use App\DataTables\CartDataTable;
use App\Logic\ProductsList;
use App\Logic\Share;
use App\Models\Config;
use App\Rules\Cellphone;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\UserBirthday;
use App\Notifications\NewUserBirthday;
use Illuminate\Support\Facades\Notification;
use DB;
use Validator;
use Hash;

class UserController extends Controller
{
    public $dActive;
    use Share, ProductsList;

    public function __construct(Request $request)
    {
        $this->dActive=Config::where('key','discount_active')->first();

        $this->middleware(function ($request, $next) {
            view()->share('carts', $this->carts()->get());
            return $next($request);
        });
        $this->middleware('auth:web');
        $this->loadData();
        $this->requests = $request->except('_token', '_method','image');
    }

    public function carts(){
        $carts = Product::join('product_translations as ptr','ptr.product_id','products.id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->join('carts', 'carts.product_id', '=', 'products.id')
            ->whereCondition()
            ->whereNull('ptr.deleted_at')
            ->whereNull('pt.deleted_at')
            ->where('ptr.lang',$this->lang)
            ->where('products.status',1)
            ->whereIn('pages.template_id',[4,5,6])
            ->orderBy('carts.id','desc')
            ->select('ptr.*','pt.slug as page_slug'
                ,'pt.id as tid','products.id'
                ,'products.old_price'
                ,'products.product_type'
                ,'products.bar_code'
                , 'carts.id as cart_id', 'carts.price as cart_prise','carts.product_id as cart_product_id', 'carts.count', DB::raw('carts.count * carts.price as amount'))
            ->with(['media']);
        if($this->dActive->value == 1){
            $carts->addSelect('products.price');
        }
        return $carts;
    }

    public function show()
    {
        $user = auth()->guard('web')->user();
        $best_seller = $this->productsList()
            ->where('products.best_seller',1)
            ->get();

        return view('web.user.profile', ['user' => $user,'best_seller' =>$best_seller]);
    }

    public function birthday()
    {
        $user = auth()->guard('web')->user();
        $best_seller = $this->productsList()
            ->where('products.best_seller',1)
            ->get();

        $user_birthday = UserBirthday::where('user_id',auth()->guard('web')->user()->id)->first();
        return view('web.user.birthday', ['user' => $user,'best_seller' =>$best_seller,'user_birthday' => $user_birthday]);
    }

    public function PostBirthday(Request $request)
    {

        $check_birthday = UserBirthday::where('user_id',auth()->guard('web')->user()->id)->first();

        if($check_birthday){
            if($check_birthday->status == 1){
                return response()->json(['result'=>1,'msg' => 'Siz artıq ad günü məlumatlarınızı dəyişdirə bilməzsiniz!','data'=> $check_birthday]);
            }
            $birthday_id = $check_birthday->id;
        }else{
            $birthday_id = null;
        }


        $validator = Validator::make($request->all(), UserBirthday::rules($birthday_id), UserBirthday::$messages);

        if ($validator->fails()) {
            return response()->json(['result'=>0,'msg' => $validator->errors()->first()]);
        }elseif(!auth()->guard('web')->user()){
            return response()->json(['result'=>0,'msg' => 'Uyğun istifadəçi tapılmadı']);
        }


        if (request()->ajax()) {
            if($check_birthday){
                DB::beginTransaction();

                foreach($this->requests as $key => $value){
                    $check_birthday->$key = $value;
                }

                $check_birthday->save();

                if($request->hasFile('image')){
                    try{
                            $check_birthday->clearMediaCollection();  //delete old file
                            $check_birthday->addMedia($request->image)
                                ->toMediaCollection();
                        }
                        catch(\Exception $e){
                            DB::rollback();
                    }
                }
                DB::commit();

                return response()->json(['result'=>1,'msg' => 'Dəyişikliklər qeydə alındı','data'=> $check_birthday]);

            }else{
                DB::beginTransaction();

                $user_birthday = UserBirthday::create(
                    [
                        'user_id' => auth()->guard('web')->user()->id,
                        'name' => $request->name,
                        'surname' => $request->surname,
                        'b_day' => $request->b_day,
                    ]
                );

                try {
                    $user_birthday
                    ->addMedia($request->image)
                    ->toMediaCollection();
                } catch (\Exception $e) {
                    DB::rollback();
                }

                DB::commit();

                $email = Config::whereIn('key', ['order_email','order_email2'])->pluck('value');


                try {

                    Notification::route('mail',$email)->notify(new NewUserBirthday(auth()->guard('web')->user()));
                }
                     catch (\Exception $e) {
                    return response()->json(['result' => 0,'msg' =>$e->getMessage()]);
                }
                return response()->json(['result'=>1,'msg' => 'Müraciətiniz barədə sizə ən qısa vaxt ərzində məlumat veriləcək ','data'=> $user_birthday]);


            }
        }        

        
    }


    public function update(Request $request)
    {

        $user = auth()->guard('web')->user();

        $phone = str_replace([' ', '+(994', ')'], ['', '', ''], $request->phone);
        $prefix = (int) substr($phone, 0, 2);
        $request['phone'] = $phone;

        $inputs = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $phone,
            'address' => $request->address,
            'password' => $request->password,
            'password_confirmation' => $request->password_confirmation,
        ];

        $rules = [
            'name' => 'required|min:3|max:40',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'address' => 'required|min:10|max:255',
            'phone' => ['required', 'numeric', new Cellphone($prefix)],
        ];


        if($request->password || $request->password_confirmation){

            $rules = [
                'password'=> 'required|min:6|confirmed',
            ];
        }

        $validator = Validator::make($inputs, $rules);


        if ($validator->fails()) {
            return response()->json(['result'=>0,'msg' => $validator->errors()->first()]);
        }



        if($request->password !== null && $request->password_confirmation !== null) {
            $inputs['password'] = Hash::make($inputs['password']);
        }else{
            unset($inputs['password']);
        }
        unset($inputs['password_confirmation']);


        $user->where('id', $user->id)->update($inputs);

        if (request()->ajax()) {
            return response()->json(['result'=>1,'msg' => 'Dəyişikliklər qeydə alındı']);
        }

        return redirect()->back();
    }

    public function updatePassword(Request $request)
    {
        $user = auth()->user();

        $inputs = [
            'current_password' => 'required|current_password_match',
            'password'     => 'required|min:6|confirmed',
        ];

        $request->validate($inputs);

        $updated = $user->update([ 'password' => Hash::make($inputs['password']) ]);

        if ($updated) {
            request()->session()->flash('message', "Şifrəniz yeniləndi");
        }
        else{
            request()->session()->flash('error', 'Daxili xəta baş verdi');
        }

        return redirect()->back();
    }

    public function cart(CartDataTable $dataTable)
    {
        $sum = $this->carts()->sum(DB::raw('carts.count * carts.price'));

        $user = auth()->user();

        return $dataTable->render('web.user.cart', ['user' => $user, 'basketCount' => basketCount(),'sum'=>$sum]);
    }
}

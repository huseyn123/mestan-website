<?php

namespace App\Http\Controllers;

use App\Exceptions\DataTableException;
use App\DataTables\CartDataTable;
use App\Logic\Share;
use App\Models\Cart;
use App\Models\Config;
use App\Models\DeliveryPrice;
use App\Models\Order;
use App\Models\PromoCode;
use App\Models\Product;
use Illuminate\Http\Request;
use Hash;
use Auth;
use Cookie;
use DB;
use Validator;

 
class CartController extends Controller
{
    private $guard;

    use Share;

    public function __construct(Request $request)
    {
        $this->middleware('web');
        $this->loadData();
        $this->guard = Auth::guard('web');
    }

    public function cart_view($cookie = null){
        $carts = Product::join('product_translations as ptr','ptr.product_id','products.id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->join('carts', 'carts.product_id', '=', 'products.id')
            ->whereCondition($cookie)
            ->whereNull('ptr.deleted_at')
            ->whereNull('pt.deleted_at')
            ->where('ptr.lang',$this->lang)
            ->where('products.status',1)
            ->whereIn('pages.template_id',[4,5,6])
            ->orderBy('carts.id','desc')
            ->select('ptr.*','pt.slug as page_slug'
                ,'pt.id as tid','products.id'
                ,'products.price','products.old_price'
                ,'products.product_type'
                ,'products.count as stock'
                ,'products.bar_code'
                , 'carts.id as cart_id', 'carts.price as cart_price', 'carts.old_price as cart_old_price','carts.product_id as cart_product_id', 'carts.count', DB::raw('carts.count * carts.price as amount'))
            ->with(['media']);
        return $carts;
    }

    public function index(CartDataTable $dataTable)
    {
        $sum = $this->cart_view()->sum(DB::raw('carts.count * carts.price'));
        return view('web.elements.cart',['carts' =>$this->cart_view()->get(),'sum' =>$sum]);
    }

    public function store(Request $request)
    {

        $user_type = auth()->guard('web')->user() ? auth()->guard('web')->user()->type  : null;


        $validator = Validator::make($request->all(), Cart::$rules);

        if ($validator->fails()) {
            return response()->json(['result'=>0,'msg' => $validator->errors()->first()]);
        }


        if($request->cookie('cartCookie') == null){
            $cookie = str_random(60);
            Cookie::queue('cartCookie', $cookie, 30);
        }
        else{
            $cookie = $request->cookie('cartCookie');
        }

        //eger mehsulun statusu aktivdirse ve stokda sayi minimal satish sayindan az deyilse:
        $product = Product::where('status', 1)->find($request->product);

        if(is_null($product)){
            if (request()->ajax()) {
                return response()->json(['result'=>0,'msg' => 'Uyğun məhsul tapılmadı']);
            }
            return redirect()->back();
        }
//
        if($request->count > $product->count){
            if (request()->ajax()) {
                return response()->json(['result'=>0,'msg' => 'Bu məhsuldan '.$product->count.' ədəd qalıb']);
            }
            return redirect()->back();
        }

        //eger mehsul artiq sebetdedirse
        $checkBasket = Cart::where('product_id', $product->id)->whereCondition()->count();

        if($checkBasket > 0){
            if (request()->ajax()) {
                return response()->json(['result'=>0,'msg' => 'Məhsul artıq səbətdədir']);
            }
            return redirect()->back();
        }

        //userin login etmezden evvel sebetinde mehsullar varsa, onlari tapiriq. Daha sonra bu mehsullara userin id-si daxil olunacaq
        if($this->guard->check()){
            $userId = $this->guard->user()->id;

            $products = Cart::getProducts()->pluck('id')->toArray();
        }
        else{
            $userId = null;
            $products = 0;
        }

        if($user_type ==2){
            $price =$product->wholesale ?? $product->old_price;
        }else{
            $dActive=Config::where('key','discount_active')->first();
            if($dActive->value == 1 && $product->price > 0){
                $price =$product->price ?? $product->old_price;
            }else{
                $price =$product->old_price ?? $product->price;
                $old_price = $product->old_price;
            }
        }


        $fields = [
            'user_id' => $userId,
            'product_id' => $product->id,
            'bar_code' => $product->bar_code,
            'order_id' => $request->session()->get('order', null),
            'cookie' => $cookie,
            'count' => $request->count,
            'price' => $price,
            'old_price' => $old_price ?? 0
        ];


        DB::beginTransaction();

        $create = Cart::create($fields);

        if(is_array($products) && count($products) > 0){
            Cart::whereIn('id', $products)->update(['user_id' => $userId]);
        }

        DB::commit();

        if (request()->ajax()) {
            $view = view('web.elements.cart_list',['carts' =>$this->cart_view($cookie)->get()])->render();

            return response()->json(['result'=>1,'msg' => 'Səbətə '.$request->count.' ədəd  əlavə edildi','html' =>$view,'cookie' =>$cookie]);
        }

        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        $cart = Cart::whereCondition()->where('product_id',$id)->first();
        $product = Product::findOrFail($cart->product_id);

        if($request->quantity){
            $cart->count = $request->quantity;
            $cart->save();

            $sum = $this->cart_view()->sum(DB::raw('carts.count * carts.price'));
            $cart_list_header = view('web.elements.cart_list',['carts' =>$this->cart_view()->get()])->render();
            $cart_list_page = view('web.elements.cart_html',['carts' =>$this->cart_view()->get(),'sum' =>$sum])->render();

            return response()->json(['result'=>1,'msg' => 'məhsul Yeniləndi','cart_list_header' =>$cart_list_header,'cart_list_page'=>$cart_list_page]);
        }
        return response()->json(['result'=>0,'msg' => '#Error']);
    }

    public function destroy($id)
    {
        $cart = Cart::whereCondition()->where('product_id',$id)->first();

        $cart->delete();

        $sum = $this->cart_view()->sum(DB::raw('carts.count * carts.price'));

        if (request()->ajax()) {
            $cart_list_header = view('web.elements.cart_list',['carts' =>$this->cart_view()->get()])->render();
            $cart_list_page = null;
            if(request()->get('type') == 1){
                $cart_list_page = view('web.elements.cart_html',['carts' =>$this->cart_view()->get(),'sum' =>$sum])->render();
            }
            return response()->json(['result'=>1,'msg' => 'məhsul Silindi','cart_list_header' =>$cart_list_header,'cart_list_page'=>$cart_list_page]);
        }
    }

    public function checkout(Request $request)  //only for authenticated users
    {

        $special_price = 0;
        $user = auth()->guard('web')->user();

        $products = Cart::whereCondition();

        $special_percent = null;
        $birthday_status = false;

//      Promo Code parametrs
        $show_promo = null;
        $promo_price = 0;
        $promo_percent = null;
        $promo_message =null;
        $promo_code = $request->promo_code ?? null;
//       --------------------

        $delivery_value = $request->delivery_value ?? null;


        $deliveryPrice =DeliveryPrice::whereNotNull('address_az')->select('id','address_'.$this->lang.' as adress','price','select')->get();
        $this_price = DeliveryPrice::where('price',$delivery_value)->select('id','price','select')->first();

        if(count($products->pluck('id')) == 0){
            return redirect()->back();
        }

        $sum = $this->cart_view()->sum(DB::raw('carts.count * carts.price'));
        $discounted_price = (isset($this_price) && $sum > 30 && $this_price->select == 1) ? 0 : $delivery_value;


        $check_product = Cart::whereCondition()->where([['old_price','>',0],['price','>','old_price']]);


        //      check user birthday
        if($user && $user->birthday){
            if($user->birthday->status == 1 && checkBirthdayPrice($user->birthday->b_day)){
                $special_percent = config('config.birthday_percent');
                $birthday_status = true;
            }
        }


        if($special_percent !== null){
            $new_sum = $this->cart_view()->sum(DB::raw('carts.count * carts.price'));
            $special_price = number_format(($new_sum * $special_percent/100),2);
        }

    
        //order_check
        // old condition $check_product->count() != $products->count() && $special_percent === null && !$birthday_status
        if($special_percent === null && !$birthday_status){
            $show_promo = true;
        }


        // check promo_code
        if($promo_code && $show_promo == true){
            $check_promo = PromoCode::where('promo_code',$promo_code)
            ->where('end_date','>=',date('Y-m-d'))
            ->where('start_date','<=',date('Y-m-d'))
            ->with(['user_promo' => function($q) use($user){
                $q->where('user_id',$user->id);
            }])->first();

            if($check_promo ){
                if($check_promo->user_promo){
                    $promo_message ='Bu endirim kodu istifadə olunub';
                }else{
                    $new_sum = $this->cart_view()->sum(DB::raw('carts.count * carts.price'));
                    $promo_percent = $check_promo->percent;
                    $promo_price  =  number_format(($new_sum * $promo_percent/100),2);
                }
            }else{
                $promo_message ='Bu endirim kodu mövcud deyil';
            }
        }

        $total = ($sum - $special_price - $promo_price)+ $discounted_price;

        //ajax
        if (request()->ajax()) {
            $view = view('web.elements.ajax_checkout_html',
                [
                    'carts' =>$this->cart_view()->get(),
                    'sum' =>$sum,'user' =>$user,
                    'deliveryPrice'=>$deliveryPrice,
                    'delivery_value'=>$delivery_value,
                    'total' => $total,
                    'data' =>$request->all(),
                    'discounted_price' => $discounted_price,
                    'special_price' => $special_price,
                    'special_percent' => $special_percent,
                    'promo_code' => $promo_code,
                    'show_promo' => $show_promo,
                    'promo_percent' => $promo_percent,
                    'promo_price' => $promo_price,
                    'birthday_status' => $birthday_status
                ])->render();
            return response()->json(['result'=>1,'msg' => 'ok','html' =>$view,'delivery_value'=>$delivery_value,
                'discounted_price'=>$discounted_price,
                'promo_message' =>$promo_message,
                'promo_code' =>$promo_code

            ]);
        }


//        't ajax
        return view('web.elements.checkout',[
            'carts' =>$this->cart_view()->get(),
            'sum' =>$sum,
            'user' =>$user,
            'deliveryPrice'=>$deliveryPrice,
            'delivery_value'=>$delivery_value,
            'total' => $total,
            'special_price' => $special_price,
            'special_percent' => $special_percent,
            'show_promo' => $show_promo,
            'promo_percent' => $promo_percent,
            'promo_price' => $promo_price,
            'birthday_status' => $birthday_status
        ]);
    }


    public function checkoutGuest(Request $request)  //only for authenticated users
    {

        $products = Cart::whereCondition()->pluck('id')->toArray();

        if(count($products) == 0){
            return redirect()->back();
        }

        DB::beginTransaction();


        if($request->session()->has('order')){
            $orderId = $request->session()->get('order');
        }
        else{
            $order = Order::create([
                'user_id' => null,
                'address' => ''
            ]);

            $orderId = $order->id;
        }


        Cart::whereIn('id', $products)->update(['order_id' => $orderId]);

        $request->session()->put('order', $orderId);

        DB::commit();

        return redirect()->route('order.post');
    }
}

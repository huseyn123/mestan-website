<?php

namespace App\Http\Controllers;
use App\Exceptions\DataTableException;
use App\Logic\Order;
use App\Models\Article;
use App\Models\ArticleTranslation;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Logic\Slug;
use DB;

class ArticleTranslationController extends Controller
{
    private $requests;

    public function __construct(Request $request, Order $order)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax');
        $this->requests = $request->except('_token', '_method');

    }


    public function store(Request $request)
    {
        // check Validation
        $validation = Validator::make($this->requests, ArticleTranslation::rules( null), ArticleTranslation::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'articleTranslation', $request->lang );

        ArticleTranslation::create($this->requests);

        return $this->responseSuccess();
    }


    public function update(Request $request, $id)
    {
        $page = ArticleTranslation::findOrFail($id);

        // check Validation
        $validation = Validator::make($this->requests, ArticleTranslation::rules( $id), ArticleTranslation::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'articleTranslation', $request->lang );

        foreach($this->requests as $key => $put){
            $page->$key = $put;
        }

        $page->save();

        return $this->responseSuccess();
    }


    public function trash($id)
    {
        $article = ArticleTranslation::findOrFail($id);
        $article->delete();

        return $this->responseSuccess("", "#articles", "#modal-confirm");

    }


    public function restore($id)
    {
        $article = ArticleTranslation::onlyTrashed()->findOrFail($id);
        $article->restore();

        return $this->responseSuccess("", "#articles", "#modal-confirm");
    }


    public function destroy($id)
    {
        $article = ArticleTranslation::onlyTrashed()->findOrFail($id);

        $relatedPages = ArticleTranslation::where('article_id', $article->article_id)->withTrashed()->count();

        if($relatedPages == 1){
            Article::where('id', $article->article_id)->forceDelete();
        }
        else{
            $article->forceDelete();
        }

        return $this->responseSuccess("", "#articles", "#modal-confirm");
    }


    private function responseSuccess($msg = "", $draw = "#articles", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }
}

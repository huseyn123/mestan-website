<?php

namespace App\Http\Controllers;

use App\Exceptions\DataTableException;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;
use App\Notifications\UserBirthdaySucces;
use App\User;
use Illuminate\Http\Request;
use App\Models\UserBirthday;
use App\DataTables\UserBirthdayDataTable;
use DB;


class UserBirthdayController extends Controller
{
    private $route, $requests;
    public $title;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->requests = $request->except('_token', '_method');
        $this->title = "Ad Günü";

    }
 

    public function index(UserBirthdayDataTable $dataTable)
    {
        return $dataTable->render("admin.user_birthdays", ['title' => $this->title]);
    }

    public function destroy($id)
    {
        $data = UserBirthday::findOrFail($id);

        $data->delete(); 

        return $this->responseSuccess("", "#user_birthdays", "#modal-confirm");
    }

    public function update($id)
    {
        $userBirthday = UserBirthday::findOrFail($id);
        $userBirthday->status = 1;

        $user = User::findOrFail($userBirthday->user_id);

        if(isset($user->email)){
            try {
                $birthday = with(new Carbon($userBirthday->b_day));
                Notification::route('mail',$user->email)->notify(new UserBirthdaySucces(userBirthdayDate($birthday), $user));
                $userBirthday->send_mail = 1;
            }
            catch (\Exception $e) {
                return response()->json(['result' => 0,'msg' =>$e->getMessage()]);
            }

        }

        $userBirthday->save();

        return $this->responseSuccess("Qeydə Alındı", "#user_birthdays", "#modal-confirm");

    }


    private function responseSuccess($msg = "", $draw = "#user_birthdays", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }
}



<?php

namespace App\Http\Controllers;
use App\Exceptions\DataTableException;
use App\Logic\Order;
use App\Models\PageTranslation;
use Illuminate\Support\Facades\Validator;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Logic\Slug;
use DB;

class PageTranslationController extends Controller
{
    private $order, $requests;

    public function __construct(Request $request, Order $order)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['order']);

        $this->requests = $request->except('_token', '_method', 'file');
        $this->order = $order;

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('pages');
        }
    }


    public function store(Request $request)
    {
        // check Validation
        $validation = Validator::make($this->requests, PageTranslation::rules($request->lang, null), PageTranslation::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        $this->requests['order'] = PageTranslation::max('order') + 1;
        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'pageTranslation', $request->lang );

        PageTranslation::create($this->requests);

        return $this->responseSuccess();
    }


    public function update(Request $request, $id)
    {
        $page = PageTranslation::findOrFail($id);

        // check Validation
        $validation = Validator::make($request->all(), PageTranslation::rules($request->lang, $id), PageTranslation::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'pageTranslation', $request->lang );

        if(!$request->meta_keywords) {
            $this->requests['meta_keywords'] = null;
        }

        foreach($this->requests as $key => $put){
            $page->$key = $put;
        }

        $page->save();

        return $this->responseSuccess();
    }


    public function trash($id)
    {
        $page = PageTranslation::findOrFail($id);

        DB::beginTransaction();


        try{
            $page->delete();
        }
        catch(\Exception $e){
            DB::rollback();
            throw new DataTableException($e->getMessage());
        }


        try{
            $page->children()->delete();
        }
        catch(\Exception $e){
            DB::rollback();
            throw new DataTableException($e->getMessage());
        }


        DB::commit();

        return $this->responseSuccess("", "#pages", "#modal-confirm");
    }


    public function restore($id)
    {
        $page = PageTranslation::onlyTrashed()->findOrFail($id);

        DB::beginTransaction();


        try{
            $page->restore();
        }
        catch(\Exception $e){
            DB::rollback();
            throw new DataTableException($e->getMessage());
        }


        try{
            $page->children()->restore();
        }
        catch(\Exception $e){
            DB::rollback();
            throw new DataTableException($e->getMessage());
        }


        DB::commit();

        return $this->responseSuccess("", "#pages", "#modal-confirm");
    }


    public function destroy($id)
    {
        $page = PageTranslation::onlyTrashed()->findOrFail($id);

        $relatedPages = PageTranslation::where('page_id', $page->page_id)->withTrashed()->count();

        if($relatedPages == 1){
            Page::where('id', $page->page_id)->forceDelete();
        }
        else{
            $page->forceDelete();
        }

        return $this->responseSuccess("", "#pages", "#modal-confirm");
    }


    public function order()
    {
        $lang = request()->get('lang', 'az');

        return $this->order->get('pageTranslation', "sex", 'name', $lang, 5);
    }


    public function postOrder(Request $request)
    {
        return $this->order->post($request, 'pageTranslation', true);

    }


    private function responseSuccess($msg = "", $draw = "#pages", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }
}

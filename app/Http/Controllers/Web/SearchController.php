<?php

namespace App\Http\Controllers\Web;

use App\Models\Config;
use App\Logic\ProductsList;
use Illuminate\Http\Request;
use App\Logic\Share;
use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Brand;
use App\Models\PageTranslation;
use App\Models\Product;
use DB;

class SearchController extends Controller
{
    use Share, ProductsList;
    public $dActive;

    public function __construct()
    {
        $this->dActive=Config::where('key','discount_active')->first();

        $this->middleware('web');
        $this->loadData();
    }

    public function search($key)
    {
        $keyword = trim(strip_tags($key));


        $products = $this->productsList()
            ->leftJoin('colors', 'colors.id', '=', 'products.color_id')
            ->leftJoin('brands', 'brands.id', '=', 'products.brand_id')
            ->where('prt.name', 'like', "%$keyword%")
            ->orWhere('products.bar_code', 'like', "%$keyword%")
            ->orWhere('brands.title', 'like', "%$keyword%")
            ->orWhere("colors.name_$this->lang", 'like', "%$keyword%")
            ->orWhereHas('productTagsSearch', function($query) use($keyword) {
                $query->join('product_translations as pt2', 'pt2.product_id', '=', 'product_tags.product_id')
                    ->whereNull('pt2.deleted_at')
                    ->where('pt2.lang', $this->lang)
                    ->where('tags.name', 'like', "%$keyword%")
                    ->where('products.status', 1)
                    ->where('products.price', '>', 0);
            })
            ->bycategory($category)
            ->paginate(15);


        $products = $this->productsList()
            ->leftJoin('brands', 'brands.id', '=', 'products.brand_id')
            ->orderBy('ptr.order','asc')
            ->where('prt.name', 'like', "%$keyword%")
            ->orWhere('brands.title', 'like', "%$keyword%")
            ->orWhereHas('productTagsSearch', function($query) use($keyword) {
                $query->join('product_translations as pt2', 'pt2.product_id', '=', 'product_tags.product_id')
                    ->whereNull('pt2.deleted_at')
                    ->where('pt2.lang', $this->lang)
                    ->where('tags.name', 'like', "%$keyword%")
                    ->where('products.status', 1)
                    ->where('products.price', '>', 0);
            })
            ->GroupBy('products.id')
            ->limit(10)
            ->get();


        return view("web.search", ['keyword' => $keyword, 'products' => $products, 'mostSearchedProducts' => $mostSearchedProducts]);
    }


    public function productSearch(Request $request)
    {

        if(request()->ajax())
        {
            $key = $request->key;
            if($key){
                $brands = Brand::where('title', 'like', "$key%")->select('id','title')->limit(5)->get(); 

                $products =$this->productsList()
                    ->where('ptr.name', 'like', "%$key%")
                    ->orWhereHas('productTagsSearch', function($query) use($key) {
                        $query->join('product_translations as pt2', 'pt2.product_id', '=', 'product_tags.product_id')
                            ->where('products.status',1)
                            ->whereNull('pt2.deleted_at')
                            ->where('pt2.lang', $this->lang)
                            ->where('tags.name','like', "%$key%")
                            ->where('tags.lang',$this->lang);
                    })
                    ->orderBy('ptr.order','asc')
                    ->GroupBy('products.id')
                    ->limit(10)

                    ->get();

            }else{
                $products = null;
            }

            $view = view('web.elements.product_search_html',['search_products' => $products,'brands' => $brands])->render();
            return response()->json(['html'=>$view,'result' =>1,'lang'=>$this->lang ]);
        }else{
            return redirect()->route('home');
        }

    }

}

<?php

namespace App\Http\Controllers\Web;

use App\Exceptions\DataTableException;
use App\Http\Controllers\Controller;
use App\Logic\ProductsList;
use App\Logic\Share;
use App\Models\Banner;
use App\Models\Brand;
use App\Models\Config;
use App\Models\Page;
use App\Logic\Pages;
use App\Models\Product;
use App\Models\ProductFilters;
use App\Models\ProductForm;
use App\Models\Select;
use App\Models\Slider;
use App\Models\Tag;
use App\Models\Vacancy;
use App\User;
use DB;
use Illuminate\Http\Request;
use App\Models\Dictionary;
use Mail;
use App\Mail\ContactForm;
use App\Models\Cart;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Notification;
use App\Notifications\UserBirthdaySucces;

class PageController extends Controller
{
    public $dActive;
    use Share, Pages;

    public function __construct()
    {
        $this->dActive=Config::where('key','discount_active')->first();

        $this->middleware(function ($request, $next) {
            view()->share('carts', $this->carts()->get());
            return $next($request);
        });

        $this->middleware('web');
        $this->loadData();
    }

    public function carts(){
        $carts = Product::join('product_translations as ptr','ptr.product_id','products.id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->join('carts', 'carts.product_id', '=', 'products.id')
            ->whereCondition()
            ->whereNull('ptr.deleted_at')
            ->whereNull('pt.deleted_at')
            ->where('ptr.lang',$this->lang)
            ->where('products.status',1)
            ->whereIn('pages.template_id',[4,5,6])
            ->orderBy('carts.id','desc')
            ->select('ptr.*','pt.slug as page_slug'
                ,'pt.id as tid','products.id'
                ,'products.old_price'
                ,'products.product_type'
                ,'products.bar_code'
                , 'carts.id as cart_id', 'carts.price as cart_prise','carts.product_id as cart_product_id', 'carts.count', DB::raw('carts.count * carts.price as amount'))
            ->with(['media']);

        if($this->dActive->value == 1){
            $carts->addSelect('products.price');
        }
        return $carts;
    }

    public function index()
    {
        $index_disc_slide=Slider::where([['sliders.template_id', 1],['sliders.lang',$this->lang]])
            ->join('products','products.id','sliders.page_id')
            ->select('products.old_price','products.price','sliders.id','sliders.title','sliders.link')
            ->with('media')->first();

        $user_type = auth()->guard('web')->user() ? auth()->guard('web')->user()->type  : null;

        $product_pages = Page::join('page_translations as pt', 'pages.id', '=', 'pt.page_id')
            ->where('pt.lang', $this->lang)
            ->whereNull('pt.deleted_at')
            ->where('template_id',10)
            ->whereIn('pages.visible', [8,9])
            ->orderBy('pt.order', 'asc')
            ->select('pages.id','pages.template_id','pages.visible','pages.target','pt.id as tid','pt.slug','pt.name','pt.lang')
            ->with('media')
            ->get();

        $products =  $this->productsList()
            ->whereIn('ptr.cat_id', $product_pages->pluck('tid'))
            ->where('products.show_index', 1)
            ->get();

        $discounted_products =($this->dActive->value == 1) ? $this->productsList()->where('products.price','>',0)->limit(12)->get() : array();

        return view('web.index',['products' =>$products,'user_type' =>$user_type,
            'product_pages' => $product_pages,'discounted_products' => $discounted_products,
            'dActive' => $this->dActive->value,
            'index_disc_slide' =>$index_disc_slide]);
    }


    public function showPage($slug1, $slug2 = null, $slug3 = null)
    {
        $slugs = array_filter([$slug1, $slug2, $slug3]);

        $checkPages = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
            ->whereIn('pt.slug', $slugs)
            ->where('pt.lang', $this->lang)
            ->whereNull('pt.deleted_at')
            ->orderByRaw('FIELD(pt.slug, "'.end($slugs).'")')
            ->select('pages.template_id', 'pages.visible', 'pt.*', 'pt.id as tid', 'pages.id')
            ->get();

        if(count($slugs) != $checkPages->count()){
            if((!is_null($slug2) && $checkPages->count() > 0) && ( in_array($checkPages->first()->template_id,[4,5,6,22]) || in_array($checkPages->last()->template_id,[3] ))){   //maybe it is an article
                return $this->productSingle($checkPages, end($slugs));
            }

            else{
//                return view('errors.404');
                return redirect()->route('home');

            }
        }



        $getPage = $checkPages->last();

        $relatedPages = $this->menu->relatedPages($getPage);

        if (!is_null($getPage->forward_url)) {
            return redirect()->away($getPage->forward_url);
        }

        elseif ($getPage->template_id == 4) {
            return $this->products($getPage, $relatedPages,'toys');
        }
        elseif ($getPage->template_id == 5) {
            return $this->products($getPage, $relatedPages,'books');
        }
        elseif ($getPage->template_id == 6) {
            return $this->products($getPage, $relatedPages,'school_accessories');
        }
        elseif ($getPage->template_id == 7) {
            return $this->about($getPage, $relatedPages);
        }
        elseif ($getPage->template_id == 8) {
            return $this->partners($getPage, $relatedPages);
        }
        elseif ($getPage->template_id == 9) {
            return $this->brands($getPage, $relatedPages);
        }
        elseif ($getPage->template_id == 10) {
            return $this->index_category($getPage, $relatedPages);
        }
        elseif ($getPage->template_id == 11) {
            return $this->contact($getPage, $relatedPages);
        }
        elseif ($getPage->template_id == 12) {
            return $this->privace($getPage, $relatedPages);
        }
        elseif ($getPage->template_id == 13) {
            return $this->users_rule($getPage, $relatedPages);
        }
         elseif ($getPage->template_id == 14) {
            return $this->store_page($getPage, $relatedPages);
        }
        elseif ($getPage->template_id == 22) {
            return $this->products($getPage, $relatedPages,'food');
        }
        else{
            return redirect()->route('home');
//            view('errors.404');
        }
    }


    public function discount(){

        $products = $this->productsList()
            ->where('products.price', '<', DB::raw('products.old_price'))
            ->paginate(15);

        $mostSearchedProducts = $this->productsList(true)->limit(8)->get();

        return view("web.discount", ['products' => $products, 'mostSearchedProducts' => $mostSearchedProducts]);
    }

    public function brand($id){
        $selected_brands = request()->get('brand') ?? [$id];
        $paginate = request()->get('page') ?? config('config.filtr_default_other_parametr.page');
        $sort = request()->get('sort') ?? config('config.filtr_default_other_parametr.sort');
        $limit = request()->get('limit') ?? config('config.filtr_default_other_parametr.limit');
        $min_price = request()->get('min') ?? config('config.filtr_default_price.min');
        $max_price = request()->get('max') ?? config('config.filtr_default_price.max');

        $brands = $this->productBrands();

       $category_filters = ProductFilters::where('status',1)
        ->select('id','title_'.$this->lang.' as title','filter_column','cat_ids')
         ->with(['filter_value' => function ($query) {
            $query->select('id','title_'.$this->lang.' as title','filter_id','status');
        }])->get();

        $product_form = $this->getProductForm($category_filters->pluck('filter_column'));

        $selects = Select::select('id','title_'.$this->lang.' as title','form_id','product_id')->get();
        $this_brand = Brand::where('id',$id)->select('id','title')->first();

        $products = $this->productsList()
            ->BySort($sort)
            ->ByPrice($min_price,$max_price)
            ->ByBrands($selected_brands)
            ->ByForm(array_filter($product_form))
            // ->GroupBy('products.id')
            ->paginate($limit);

        if(request()->ajax())
        {
            header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            $view = view('web.elements.filtr_product_ajax',['products' => $products,'product_form' =>$product_form])->render();
            return response()->json(['html'=>$view]);
        }

        return view("web.brand_products", ['this_brand' => $this_brand,'products' => $products,'category_filters'=>$category_filters, 'product_form' =>$product_form,'selects' => $selects,'brands' =>$brands,'selected_brands' => $selected_brands,'min_price'=>$min_price,'max_price'=>$max_price,'paginate' => $paginate,'sort' =>$sort,'limit'=> $limit]);
    }


    public function showResume($slug, $id){

        $page = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')->where('pt.slug', $slug)->first();

        $vacancy = Vacancy::findOrFail($id);

        return view("web.vacancy-detail", ['page' => $page, 'vacancy' => $vacancy]);
    }


    public function applyResume($slug, $id){

        $page = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')->where('pt.slug', $slug)->first();

        $vacancy = Vacancy::findOrFail($id);

        return view("web.vacancy-apply", ['page' => $page, 'vacancy' => $vacancy]);
    }


    public function postContact(Request $request)
    {
        $dictionary = Dictionary::where('keyword', 'email_sent')->where('lang_id', $this->lang)->first();
        $email = Config::where('key', 'email')->firstOrFail();

        $inputs = [
            'full_name' => 'required|min:2',
            'email' => 'required|email',
            'subject' => 'required|min:3',
            'text' => 'required|min:20'
        ];

        $request->validate($inputs);

        try {
            Mail::to($email->value)->send(new ContactForm($request->all(), 'contact', 'Əlaqə bölməsindən göndərilən məktub'));

            $response = ["code" => 200, "msg" => $dictionary->content];

            return response()->json($response, $response['code']);

        } catch (\Exception $e) {
            throw new DataTableException($e->getMessage());
        }
    }
}
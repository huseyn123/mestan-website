<?php

namespace App\Http\Controllers;
use App\Crud\ProductCrud;
use App\Crud\ProductParameterCrud;
use App\DataTables\ProductDataTable;
use App\Exceptions\DataTableException;
use App\Exceptions\WebServiceException;
use App\Models\Color;
use App\Models\Config;
use App\Models\Page;
use App\Models\ProductFilters;
use App\Models\Cart;
use App\Models\PageTranslation;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductForm;
use App\Models\ProductTag;
use App\Models\ProductTranslation;
use App\Models\Tag;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Logic\Slug;
use App\Logic\Order;
use DB;
use App\Logic\WebService;

class ProductController extends Controller
{
    private $crud,$route,$requests, $title,$create_title, $order,$new_array;
    const TRANSLATES = ['name','page_id','product_id','lang', 'order','index_order','slug', 'summary', 'content','cat_id'];

    public function __construct(Request $request, Order $order,WebService $webService)
    {
        $this->order = $order;

        $this->middleware('auth:admin')->except('updateNeededProducts');
        $this->middleware('ajax')->except(['index', 'updateAllProducts', 'updateNeededProducts', 'getProductDetail','searchByApi','']);
        

        $except_data = ['_token', '_method', 'tag','image','order','age','sale','filter_list'];

       
        if($request->filter_list){
            $except_filter_list = json_decode($request->filter_list[0], false);
            $except_data = array_merge($except_data,$except_filter_list);
        }


        $this->requests = $request->except($except_data);

        $this->route = $request->segment(2);
        $this->crud = new ProductCrud($this->route);
        $this->paramCrud = new ProductParameterCrud();
        $this->webService = $webService;

        $this->title = config('config.product_type.'.$this->route);
        $this->create_title =config('config.create_title.'.$this->route);

    }

    public function index(ProductDataTable $dataTable)
    {
        $template_id = config('config.product_template.'.$this->route);
        $d_active = Config::where('key','discount_active')->first();
        $categories = Page::join('page_translations as pt', 'pages.id', '=', 'pt.page_id')
           ->where('pt.lang', 'az')
           ->whereNull('pt.deleted_at')
           ->whereNotIn('template_id',[0])
           ->whereNotIn('pages.visible', [0])
           ->where('pages.template_id',$template_id)
           ->orderBy('pt.order', 'asc')
           ->pluck('pt.name','pt.id');

        return $dataTable->route($this->route)->render('admin.products', ['title' => $this->title,'route' => $this->route,'categories' => $categories,'d_active' =>$d_active]);
    }

    public function create()
    {
        $fields = $this->crud->fields(null, 'create',null);

        return view('admin.page.create', ['route' => 'product.store', 'fields' => $fields, 'title' => $this->create_title]);
    }

    public function store(Request $request)
    {
        $data = null;
        if (count(config('app.locales')) == 1) {
            $lang = app()->getLocale();
        } else {
            $lang = $request->lang;
        }

        $tags = $request->tag;

        $translates = self::TRANSLATES;

        // check Validation
        $validation = Validator::make($request->all(), Product::rules( null), Product::$messages);

        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

//        get product detail from web service
         $productDetail = $this->webService->getDataByBarCode($request->bar_code);
          if(!is_array($productDetail)){
             throw new WebServiceException('Baza proqramında bu barkoda uyğun məhsul tapılmadı!', 400);
          }
//

        $parent = PageTranslation::findOrFail($request->page_id);
        $lang = $parent->lang;


        // apply
        $this->requests['lang'] = $lang;
        $this->requests['count'] = $productDetail['qaliq'];
        $this->requests['order'] = ProductTranslation::max('order') + 1;
        $this->requests['index_order'] = ProductTranslation::max('index_order') + 1;
        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'productTranslation', $lang);
        if($request->percent && $request->percent != 0){
            $this->requests['price'] = number_format($request->old_price - ($request->old_price * $request->percent/100),2);
        }


        // Product Form

        $filter_list = json_decode($request->filter_list[0], false);
        foreach ($filter_list as $value){
            if($request->$value){
                $data[$value] =$request->$value;
            }else{
                $data[$value] =[];
            }
        }

//        $data = json_encode($data);
        $this->requests['form'] = $data;



        //inputs for models
        $productInputs = array_diff_key($this->requests, array_flip($translates));
        $translationInputs = array_diff_key($this->requests, $productInputs);

        //store
        DB::beginTransaction();


        try {
            $product = Product::create($productInputs);
        } catch (\Exception $e) {
            DB::rollback();
            throw new DataTableException($e->getMessage());
        }



        try {
            $translationInputs['product_id'] = $product->id;
            $product_translation = ProductTranslation::create($translationInputs);
        } catch (\Exception $e) {
            DB::rollback();
            throw new DataTableException($e->getMessage());
        }



        if(!empty($tags))
        {

            $selectedTags = Tag::whereIn('id', $tags)->pluck('id')->toArray();

            foreach (array_diff($tags, $selectedTags) as $newTag){
                $newTag = Tag::create([
                    'keyword' => $newTag,
                    'name' => $newTag,
                    'lang' => $lang
                ]);

                $selectedTags[] = $newTag->id;
            }

            foreach($selectedTags as $list)
            {
                try{
                    ProductTag::create(['product_id' => $product->id, 'tag_id' => $list]);
                }
                catch(\Exception $e){
                    DB::rollback();
                    throw new DataTableException($e->getMessage());
                }
            }
        }



        if ($request->has('image')){

            try {
                $product
                    ->addMedia($request->image)
                    ->toMediaCollection();
            } catch (\Exception $e) {
                DB::rollback();
                throw new DataTableException($e->getMessage());
            }
        }

        DB::commit();


        return $this->responseSuccess('Məhsul əlavə olundu');
    }

    public function edit($id)
    {
        $parameterCrud = new ProductParameterCrud();
        $fields = [];
        $langs = [];
        $keys = [];

        $product = Product::join('product_translations as prt', 'prt.product_id', '=', 'products.id')
            ->where('prt.id', $id)
            ->whereNull('prt.deleted_at')
            ->select(
                'prt.*',
                'prt.id as tid',
                'products.id',
                'products.price',
                'products.old_price',
                'products.percent',
                'products.wholesale',
                'products.bar_code',
                'products.brand_id',
                'products.index_form',
                'products.index_banner',
                'products.user_type',
                'products.best_seller',
                'products.form',
                'products.status',
                'products.show_index',
                'products.youtube_link'
            )
            ->with('media')
            ->firstOrFail();

        if(count(config('app.locales')) == 1){
            return $this->editSingle($product);
        }

        $relatedPage = $product->relatedPages;
        $parameters = $parameterCrud->fields('edit', $product,$this->route);

        foreach ($relatedPage as $rel){
            $fields[$rel->lang] = $this->crud->fields($rel->lang, 'edit', $rel);
            $langs[$rel->lang] = $rel->lang;
            $keys[$rel->lang] = array_filter(explode(",", $rel->meta_keywords));
        }


        foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale){
            $fields[$key] = $this->crud->fields( $key, 'edit', null);
            $keys[$key] = [];
        }

        return view('admin.page.edit-withlocale', ['info' => $product, 'langs' => $langs, 'parameters' => $parameters,'fields' => $fields, 'relatedPage' => $relatedPage,'model' => 'product', 'route' => 'productTranslation',  'keys' => $keys ]);

    }

    private function editSingle($page)
    {
        $fields = $this->crud->fields(null,'edit', $page);

        return view('admin.page.edit', ['info' => $page, 'fields' => $fields, 'model' => 'product']);
    }

    public function update(Request $request, $id)
    {
        $tags = $request->tag;

        $product =  Product::findOrFail($id);



        // oin('product_translations as ptr','ptr.product_id','products.id')
        //     ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
        //     ->join('pages', 'pages.id', '=', 'pt.page_id')
        //     ->where('products.id',$id)->first();

        $this->requests['index_form'] = $request->index_form;
        $this->requests['index_banner'] = $request->index_banner;
        $this->requests['user_type'] = $request->user_type;
        $this->requests['show_index'] = $request->show_index;
        $this->requests['best_seller'] = $request->best_seller;
        if($request->percent &&  $request->percent != 0){
            $this->requests['price'] = number_format($request->old_price - ($request->old_price * $request->percent/100),2);
        }


        /////////////////////////////???????????????????????????????
        $data = null;


        $filter_list = json_decode($request->filter_list[0], false);
        if($request->filter_list){  
                foreach ($filter_list as $value){
                    if($request->$value){     
                        $data[$value] =$request->$value;
                    }else{
                        $data[$value] =[];
                    }
            }
        }

        $this->requests['form'] = $data;

        $currentTags = $product->tags('id')->pluck('tag_id')->toArray();

        $validation = Validator::make($request->all(), Product::parameterRules($id), Product::$messages);
        if($validation->errors()->first()){
            throw new DataTableException($validation->errors()->first());
        }

        // get product detail from web service
        $productDetail = $this->webService->getDataByBarCode($request->bar_code);
        if(!is_array($productDetail)){
            throw new WebServiceException('Baza proqramında bu barkoda uyğun məhsul tapılmadı!', 400);
        }

        $this->requests['count'] = $productDetail['qaliq'];

        if($request->hasFile('image')){
            try{

                $product->clearMediaCollection();  //delete old file
                $product->addMedia($request->image)
                    ->toMediaCollection();
            }
            catch(\Exception $e){
                DB::rollback();
                throw new DataTableException($e->getMessage());
            }
        }


        DB::beginTransaction();
        foreach($this->requests as $key => $put){
            $product->$key = $put;
        }

        $product->save();


        if($tags != $currentTags){

            try{
                $product->tags('id')->delete();
            }
            catch(\Exception $e){
                DB::rollback();
                throw new DataTableException($e->getMessage());
            }

            if(!empty($tags))
            {

                $selectedTags = Tag::whereIn('id', $tags)->pluck('id')->toArray();

                foreach (array_diff($tags, $selectedTags) as $newTag){
                    $newTag = Tag::create([
                        'keyword' => $newTag,
                        'name' => $newTag,
                        'lang' => 'az'
                    ]);

                    $selectedTags[] = $newTag->id;
                }

                foreach($selectedTags as $list)
                {
                    try{
                        ProductTag::create(['product_id' => $product->id, 'tag_id' => $list]);
                    }
                    catch(\Exception $e){
                        DB::rollback();
                        throw new DataTableException($e->getMessage());
                    }
                }

            }
        }

        DB::commit();

        return $this->responseSuccess();
    }

    public function updateSingle(Request $request, $id)
    {
        $productTranslation = ProductTranslation::findOrFail($id);
        $product = Product::findOrFail($productTranslation->product_id);

        $translates = ['name', 'page_id', 'slug', 'summary', 'content', 'lang'];

        // check Validation
        $validation = Validator::make($this->requests, Product::rules($id), Product::$messages);
        if($validation->errors()->first()){
            throw new DataTableException($validation->errors()->first());
        }

        $page = PageTranslation::findOrFail($request->page_id);


        // apply
        $this->requests['lang'] = $page->lang;
        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'productTranslation', $page->lang);

        if($request->hasFile('image')) {
            $image = $this->image->store($request->image, $this->resize());
            $this->image->deleteFile($product->image); //delete old file
            $this->requests['image'] = $image;
        }

        //inputs for models
        $productInputs = array_diff_key($this->requests, array_flip($translates));
        $translationInputs = array_diff_key($this->requests, $productInputs);


        DB::beginTransaction();

        try{
            foreach($productInputs as $key => $put){
                $product->$key = $put;
            }

            $product->save();
        }
        catch(\Exception $e){
            DB::rollback();
            throw new DataTableException($e->getMessage());
        }

        try{
            foreach($translationInputs as $key => $put){
                $productTranslation->$key = $put;
            }
            $productTranslation->save();
        }
        catch(\Exception $e){
            DB::rollback();
            throw new DataTableException($e->getMessage());
        }


        DB::commit();

        return $this->responseSuccess();
    }

    public function fastUpdate($id, Request $request)
    {
        $value = $request->value;
        $type = $request->type;

        $rules = [
            'name' =>       ['value' => "required"],
            'slug' =>       ['value' => "required|unique:product_translations,slug,'.$id"],
            'status' =>     ['value' => "required|boolean"],
        ];

        $validation = Validator::make($request->all(), $rules[$type],Product::$messages);

        if($validation->errors()->first()){
            throw new DataTableException($validation->errors()->first());
        }



        if($type == 'name' || $type == 'slug'){
            $data = ProductTranslation::findOrFail($id);
        }
        else{
            $data = Product::findOrFail($id);
        }

        $data->{$type} = $value;
        $data->save();

        return $this->responseSuccess();
    }

    public function duplicate($id)
    {
        $product = Product::join('product_translations as prt', 'prt.product_id', '=', 'products.id')
            ->where('prt.id', $id)
            ->whereNull('prt.deleted_at')
            ->select(
                'prt.name',
                'prt.content',
                'prt.id as tid',
                'products.price',
                'products.id',
                'products.product_type',
                'products.old_price',
                'products.wholesale',
                'products.brand_id'
            )
            ->with('media')
            ->firstOrFail();

        $fields = $this->crud->fields(null,'create', $product,$product->product_type,'dublicate');

        return view('admin.page.duplicate', ['info' => $product, 'fields' => $fields, 'model' => 'product']);
    }

    public function duplicatePost(Request $request)
    {
        return $this->store($request);
    }

    public function updateMinSalesCount(Request $request)
    {
        // price is not prise|| its percent
        $price = (int) $request->price;
        $rules = [
            'price' => 'required|numeric|min:0',
        ];

        $validation = Validator::make($request->all(), $rules);

        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }


        Product::where('old_price', '>',0)->update(['price' =>  $price == 0 ? null : DB::raw('old_price - old_price*'.$price.'/100'),'percent' => $price]);

        return $this->responseSuccess($request->price, "#products", "#edit-sales");
    }


    public function activeDiscount(Request $request)
    {
        $value = (int) $request->discount_active;
        $rules = [
            'discount_active' => 'numeric|min:0',
        ];

        $validation = Validator::make($request->all(), $rules);

        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }


        $check_da = Config::where('key','discount_active')->first()->value;

        if($check_da == 1 && !$request->discount_active){
            $cart = Cart::where('purchased',0)->delete();
        }

        Config::where('key','discount_active')->update(['value' => $value]);

        return $this->responseSuccess("", "#products", "#edit-activeDiscount");
    }

    private function responseSuccess($msg = "", $draw = "#products", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }



    public function searchByApi($barcode)
    {
         $productDetail = $this->webService->getDataByBarCode($barcode);
          if(!is_array($productDetail)){
             throw new WebServiceException('Baza proqramında bu barkoda uyğun məhsul tapılmadı!', 400);
          }

        die(print_r($productDetail));
      
    }


    public function forms($id)
    {

        $page_id  = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
            ->whereNull('pt.deleted_at')
            ->where('pt.id',$id)
            ->select("pages.id")
            ->orderBy("pages.id", "asc")
            ->first()->id; 

        settype($page_id, "string");

        $form_data = ProductFilters::where('status',1)
            ->with('filter_value')
            ->select('filter_column')
            ->whereJsonContains('cat_ids', $page_id)
            ->pluck('filter_column');

        return $form_data;

    }


    public function product_category_filter(Request $request){
        if($request->route && $request->page_id){
            $route = $request->route;
            $page_id = $request->page_id;
            $template_id = config('config.product_template.'.$route);     

            $page  = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
                    ->where('pt.id',$page_id)
                    ->whereNull('pt.deleted_at')
                    ->with('parent')
                    ->first();  

                    
            $page_id = isset($page->parent) ? $page->parent->page_id : null;


            settype($page_id, "string");
             $form = ProductFilters::where('status',1)
                    ->whereJsonContains('cat_ids', $page_id)
                    ->select('id','title_az as title','filter_column')
                    ->with('filter_value')
                    ->get();  

            $view = view('admin.elements.product_category_filters', ['form' => $form] )->render();
            return response()->json(['page_id'=>$request->page_id,'parent_id'=>$page,'html'=>$view,'data'=>$form,'filter_list' => $form->pluck('filter_column')]);
        }
    }
}

<?php

namespace App\Http\Controllers;
use App\Crud\PageParameterCrud;
use App\Crud\PageSubCrud;
use App\Exceptions\DataTableException;
use App\Logic\ImageRepo;
use App\Logic\Order;
use App\Models\PageTranslation;
use Illuminate\Support\Facades\Validator;
use App\Crud\PageCrud;
use App\Models\Page;
use Illuminate\Http\Request;
use App\DataTables\PageDataTable;
use App\Logic\Slug;
use DB;

class PageController extends Controller
{
    private $crud, $image, $requests, $title, $order,$page_type;

    public function __construct(ImageRepo $coverImage, Request $request, Order $order)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index', 'order']);

        $this->requests = $request->except('_token', '_method', 'file','image','cover');
        $this->page_type  = $request->get('page_type') ?? null;

        $this->crud  = new PageCrud();
        $this->image = $coverImage;
        $this->order = $order;
        $this->title = $this->page_type ?  'Alt Kateqoriyalar' : "Səhifələr";

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('pages');
        }

    }


    public function index()
    {
        $dataTable = new PageDataTable();

        return $dataTable->passData($this->page_type)->render('admin.pages', ['title' => $this->title]);
    }

    public function create()
    {
        $fields = $this->crud->fields(null, 'create');

        return view('admin.page.create', ['route' => 'page.store', 'fields' => $fields, 'title' => 'Yeni səhifə']);
    }


    public function store(Request $request)
    {
        if(count(config('app.locales')) == 1){
            $lang = app()->getLocale();
        }
        else{
            $lang = $request->lang;
        }

        $translates = ['name', 'parent_id', 'lang', 'slug', 'order', 'content', 'forward_url', 'meta_description', 'meta_keywords'];

        // check Validation
        $validation = Validator::make($request->all(), Page::rules($lang, null,$request->template_id), Page::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        if($request->parent_id > 0)
        {
            $parent = PageTranslation::findOrFail($request->parent_id);
            $lang = $parent->lang;
        }

        // apply
        $this->requests['lang'] = $lang;
        $this->requests['order'] = PageTranslation::max('order') + 1;
        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'pageTranslation', $lang );

        //inputs for models
        $pageInputs = array_diff_key($this->requests, array_flip($translates));
        $translationInputs = array_diff_key($this->requests, $pageInputs);

        //store
        DB::beginTransaction();


        try{
            $page = Page::create($pageInputs);
        }
        catch(\Exception $e){
            DB::rollback();
            throw new DataTableException($e->getMessage());
        }

        try{
            $translationInputs['page_id'] = $page->id;
            PageTranslation::create($translationInputs);
        }
        catch(\Exception $e){
            DB::rollback();
            throw new DataTableException($e->getMessage());
        }

        if ($request->has('cover')){
            Page::$width =  config('config.page_size.'.$page->template_id.'.width') ?? '1920';
            Page::$height = config('config.page_size.'.$page->template_id.'.height') ?? '300';

            //thumb_size
            Page::$thumb_width =  config('config.thumb_page_size.'.$page->template_id.'.width') ?? '300';
            Page::$thumb_height = config('config.thumb_page_size.'.$page->template_id.'.height') ?? '100';


            Page::$template_id = $page->template_id;

            try {
                $page
                    ->addMedia($request->cover)
                    ->toMediaCollection('cover');
            } catch (\Exception $e) {
                DB::rollback();
                throw new DataTableException($e->getMessage());
            }
        }


        DB::commit();

        return $this->responseSuccess();
    }


    public function edit($id)
    {
        $parameterCrud = new PageParameterCrud();
        $fields = [];
        $langs = [];
        $keys = [];

//        $page = PageTranslation::join('pages', 'page_translations.page_id', '=', 'pages.id')
//            ->select(
//                'page_translations.*',
//                'page_translations.id as tid',
//                'pages.id',
//                'pages.template_id',
//                'pages.visible',
//                'pages.target'
//            )
//            ->findOrFail($id);

        $page = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
            ->where('pt.id', $id)
            ->whereNull('pt.deleted_at')
            ->select(
                'pt.*',
                'pt.id as tid',
                'pages.id',
                'pages.template_id',
                'pages.visible',
                'pages.target'
            )
            ->firstOrFail();


        if(count(config('app.locales')) == 1){
            return $this->singleEdit($page);
        }

        $relatedPage = $page->relatedAdminPages;
        $parameters = $parameterCrud->fields('edit', $page);

        foreach ($relatedPage as $rel){
            $fields[$rel->lang] = $this->crud->fields($rel->lang, 'edit', $rel);
            $langs[$rel->lang] = $rel->lang;
            $keys[$rel->lang] = array_filter(explode(",", $rel->meta_keywords));
        }

        foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale){
            $fields[$key] = $this->crud->fields( $key, 'edit', null);

            $keys[$key] = [];
        }

        return view('admin.page.edit-withlocale', ['info' => $page, 'langs' => $langs, 'parameters' => $parameters, 'pageId' => $page->page_id, 'fields' => $fields, 'relatedPage' => $relatedPage, 'keys' => $keys, 'model' => 'page', 'route' => 'pageTranslation' ]);
    }


    private function singleEdit($page)
    {
        $fields = $this->crud->fields(null,'edit', $page);

        $keys = array_filter(explode(",", $page->meta_keywords));

        return view('admin.page.edit', ['info' => $page, 'fields' => $fields, 'keys' => $keys, 'model' => 'page']);
    }


    public function update(Request $request, $id)
    {
        $page = Page::findOrFail($id);
        Page::$template_id = $page->template_id;

        Page::$width =  config('config.page_size.'.$page->template_id.'.width') ?? '1920';
        Page::$height = config('config.page_size.'.$page->template_id.'.height') ?? '300';

        //thumb size
        Page::$thumb_width = config('config.thumb_page_size.'.$page->template_id.'.width') ?? 300;
        Page::$thumb_height = config('config.thumb_page_size.'.$page->template_id.'.height') ?? 100;

        $validation = Validator::make($request->all(), Page::parameterRules(), Page::$messages);

        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        foreach($this->requests as $key => $put){
            $page->$key = $put;
        }

        $page->save();

        if($request->hasFile('cover')){
            try{

                $page->clearMediaCollection('cover');  //delete old file
                $page->addMedia($request->cover)
                    ->toMediaCollection('cover');
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        return $this->responseSuccess();
    }


    public function updateSingle(Request $request, $id)
    {
        $pageTranslation = PageTranslation::findOrFail($id);

        $translates = ['name', 'parent_id', 'lang', 'slug', 'order', 'content', 'forward_url', 'meta_description', 'meta_keywords'];

        // check Validation
        $validation = Validator::make($this->requests, Page::rules($pageTranslation->lang, $id), Page::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        if($request->parent_id > 0)
        {
            PageTranslation::findOrFail($request->parent_id);
        }

        // apply
        $this->requests['lang'] = $pageTranslation->lang;
        $this->requests['order'] = PageTranslation::max('order') + 1;
        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'pageTranslation', $pageTranslation->lang );

        //inputs for models
        $pageInputs = array_diff_key($this->requests, array_flip($translates));
        $translationInputs = array_diff_key($this->requests, $pageInputs);

        //store
        DB::beginTransaction();


        try{
            Page::where('id', $pageTranslation->page_id)->update($pageInputs);
        }
        catch(\Exception $e){
            DB::rollback();
            throw new DataTableException($e->getMessage());
        }

        try{
            foreach($translationInputs as $key => $put){
                $pageTranslation->$key = $put;
            }
            $pageTranslation->save();
        }
        catch(\Exception $e){
            DB::rollback();
            throw new DataTableException($e->getMessage());
        }

        DB::commit();

        return $this->responseSuccess();
    }



    private function responseSuccess($msg = "", $draw = "#pages", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }


    private function resize($templateId)
    {
        if($templateId == 11){
            $resizeImage = ['resize' => ['fit' => false, 'size' => [720, null]], 'thumb' => null ];
        }
        else{
            $resizeImage = ['resize' => ['fit' => false, 'size' => [1920, null]], 'thumb' => null ];
        }

        return $resizeImage;
    }


    private function errorDt($msg)
    {
        return $this->responseDataTable(1, $msg);
    }
}

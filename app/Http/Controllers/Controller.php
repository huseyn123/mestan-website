<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;

    protected function responseDataTable($code, $msg, $draw=false, $close=false, $data = false)
    {
        return array("code" => $code, "msg" => $msg, "draw" => $draw, "close" => $close, "data" => $data);
    }
}

<?php

namespace App\Http\Controllers;

use App\Crud\AlbumCrud;
use App\DataTables\AlbumDataTable;
use App\Exceptions\DataTableException;
use App\Logic\Dropzone;
use App\Models\Album;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;

class AlbumController extends Controller
{
    private $requests, $crud, $title, $dropzone;

    public function __construct(Request $request, AlbumCrud $crud, Dropzone $dropzone)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->requests = $request->except('_token', '_method');
        $this->crud = $crud;
        $this->dropzone = $dropzone;
        $this->title = 'Albomlar';
    }


    public function index(AlbumDataTable $dataTable)
    {
        return $dataTable->render('admin.albums', ['title' => $this->title]);
    }


    public function create()
    {
        $fields = $this->crud->fields('create');

        return view('admin.dt.create', ['fields' => $fields, 'title' => 'Yeni albom', 'route' => 'albums.store']);
    }


    public function store(Request $request)
    {
        $validation = Validator::make($this->requests, Album::$rules, Album::$messages);

        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        Album::create($this->requests);

        return $this->responseSuccess();
    }


    public function edit($id)
    {
        $data = Album::findOrFail($id);

        $fields = $this->crud->fields('edit', $data);

        return view('admin.dt.edit', ['data' => $data, 'fields' => $fields, 'route' => ['albums.update', $id] ]);
    }


    public function update($id)
    {
        $services = Album::findOrFail($id);

        $validation = Validator::make($this->requests, Album::$rules, Album::$messages);

        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        foreach($this->requests as $key => $put){
            $services->$key = $put;
        }

        $services->save();

        return $this->responseSuccess();
    }


    public function destroy($id)
    {
        $data = Album::findOrFail($id);

        $images = $data->gallery;

        $this->dropzone->deleteAll($images);    //deleting all album images

        $data->delete();

        return $this->responseSuccess("");
    }


    private function responseSuccess($msg = "", $draw = "#albums", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }
}

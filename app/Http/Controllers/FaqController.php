<?php

namespace App\Http\Controllers;

use App\Exceptions\DataTableException;
use Illuminate\Http\Request;
use App\Models\Question;
use App\DataTables\FaqDataTable;
use App\Crud\FaqCrud;
use DB;
use Illuminate\Support\Facades\Validator;

class FaqController extends Controller
{
    private $crud, $requests, $title;

    public function __construct(Request $request, FaqCrud $crud)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->crud = $crud;
        $this->requests = $request->except('_token', '_method');
        $this->title = "FAQ";
    }


    public function index(FaqDataTable $dataTable)
    {
        return $dataTable->render("admin.faq", ['title' => $this->title]);
    }


    public function create()
    {
        $fields = $this->crud->fields('create');

        return view("admin.dt.create",  ['fields' => $fields, 'title' => 'Əlavə et', 'route' => 'faq.store']);
    }


    public function store()
    {
        // check Validation
        $validation = Validator::make($this->requests, Question::$rules, Question::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        Question::create($this->requests);

        return $this->responseSuccess();
    }


    public function edit($id)
    {
        $data = Question::findOrFail($id);

        $fields = $this->crud->fields('edit', $data);

        return view('admin.dt.edit', ['title' => 'Düzəliş et', 'fields' => $fields, 'data' => $data, 'route' => ['faq.update', $id]]);
    }


    public function update(Request $request, $id)
    {
        $data = Question::findOrFail($id);

        // check Validation
        $validation = Validator::make($this->requests, Question::$rules, Question::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        foreach($this->requests as $key => $put){
            $data->$key = $put;
        }

        $data->save();

        return $this->responseSuccess();
    }


    public function destroy($id)
    {
        $data = Question::findOrFail($id);
        $data->delete();

        return $this->responseSuccess("", "#questions", "#modal-confirm");
    }


    private function responseSuccess($msg = "", $draw = "#questions", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }
}



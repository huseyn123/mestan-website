<?php

namespace App\Http\Controllers;

use App\Exceptions\DataTableException;
use App\Logic\ImageRepo;
use Illuminate\Http\Request;
use App\Models\Slider;
use App\DataTables\SliderDataTable;
use App\Crud\SliderCrud;
use DB;
use Illuminate\Support\Facades\Validator;

class SliderController extends Controller
{
    private $crud, $route, $image, $requests;
    public $title;

    public function __construct(Request $request, ImageRepo $imageUpload, SliderCrud $crud)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->image = $imageUpload;
        $this->crud = $crud;
        $this->requests = $request->except('_token', '_method','image');
        $this->title = "Slider";

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('slider', true);
        }
    }


    public function index(SliderDataTable $dataTable)
    {
        return $dataTable->render("admin.sliders", ['title' => $this->title]);
    }


    public function create()
    {
        $fields = $this->crud->fields('create');

        return view("admin.dt.create",  ['fields' => $fields, 'title' => 'Yeni slider', 'route' => 'slider.store']);
    }


    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), Slider::rules( 'required|',$request->template_id), Slider::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }
        $this->requests['order'] = Slider::withTrashed()->max('order') + 1;

        $slider =Slider::create($this->requests);

        if($request->template_id == 1){Slider::$width = 390;Slider::$height = 260;}

        $slider
            ->addMedia($request->image)
            //   ->withResponsiveImages()
            ->toMediaCollection();

        return $this->responseSuccess();
    }



    public function edit($id)
    {
        $data = Slider::findOrFail($id);

        $fields = $this->crud->fields('edit', $data);

        return view('admin.dt.edit', ['title' => 'Düzəliş et', 'fields' => $fields, 'data' => $data, 'route' => ['slider.update', $id]]);
    }


    public function update(Request $request, $id)
    {
        $img = Slider::findOrFail($id);

        $validation = Validator::make($request->all(), Slider::rules( null,$request->template_id), Slider::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }


        foreach($this->requests as $key => $put){
            $img->$key = $put;
        }


        if($request->hasFile('image')){
            if($request->template_id == 1){Slider::$width = 390;Slider::$height = 260;}

            try{

                $img->clearMediaCollection();  //delete old file
                $img->addMedia($request->image)
                    ->toMediaCollection();
            }
            catch(\Exception $e){
                DB::rollback();
                throw new DataTableException($e->getMessage());
            }
        }

        $img->save();

        return $this->responseSuccess();
    }

    public function trash($id)
    {
        $data = Slider::findOrFail($id);
        $data->delete();

        return $this->responseSuccess("", "#sliders", "#modal-confirm");

    }


    public function restore($id)
    {
        $member = Slider::onlyTrashed()->findOrFail($id);

        $member->restore();

        return $this->responseSuccess("", "#sliders", "#modal-confirm");
    }


    public function destroy($id)
    {
        $slider = Slider::onlyTrashed()->findOrFail($id);

        $slider->forceDelete();

        $this->image->deleteFile($slider->image); //delete image

        return $this->responseSuccess("", "#sliders", "#modal-confirm");
    }


    public function reorder(Request $request)
    {
        $changes = array_pluck($request->all(), 'newData', 'oldData');

        $options_to_change = Slider::whereIn('order', array_keys($changes))->withTrashed()->get();

        foreach ($options_to_change as $option) {
            $option->update(['order' => $changes[$option->order]]);
        };

        return;
    }


    private function responseSuccess($msg = "", $draw = "#sliders", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }
}



<?php

namespace App\Http\Controllers;

use App\Exceptions\DataTableException;
use Illuminate\Http\Request;
use App\Models\Block;
use App\DataTables\BlockDataTable;
use App\Crud\BlockCrud;
use DB;
use Illuminate\Support\Facades\Validator;

class BlockController extends Controller
{
    private $crud, $requests, $title;

    public function __construct(Request $request, BlockCrud $crud)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->crud = $crud;
        $this->requests = $request->except('_token', '_method');
        $this->title = "Bloklar";
    }


    public function index(BlockDataTable $dataTable)
    {
        return $dataTable->render("admin.blocks", ['title' => $this->title]);
    }


    public function create()
    {
        $fields = $this->crud->fields('create');

        return view("admin.dt.create",  ['fields' => $fields, 'title' => 'Əlavə et', 'route' => 'block.store']);
    }


    public function store()
    {
        // check Validation
        $validation = Validator::make($this->requests, Block::$rules, Block::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        Block::create($this->requests);

        return $this->responseSuccess();
    }


    public function edit($id)
    {
        $data = Block::findOrFail($id);

        $fields = $this->crud->fields('edit', $data);

        return view('admin.dt.edit', ['title' => 'Düzəliş et', 'fields' => $fields, 'data' => $data, 'route' => ['block.update', $id]]);
    }


    public function update(Request $request, $id)
    {
        $data = Block::findOrFail($id);

        // check Validation
        $validation = Validator::make($this->requests, Block::$rules, Block::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        foreach($this->requests as $key => $put){
            $data->$key = $put;
        }

        $data->save();

        return $this->responseSuccess();
    }


    public function destroy($id)
    {
        $data = Block::findOrFail($id);
        $data->delete();

        return $this->responseSuccess("", "#blocks", "#modal-confirm");
    }


    private function responseSuccess($msg = "", $draw = "#blocks", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }
}



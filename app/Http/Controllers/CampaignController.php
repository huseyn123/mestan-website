<?php

namespace App\Http\Controllers;

use App\Exceptions\DataTableException;
use App\Logic\ImageRepo;
use App\Models\Campaign;
use Illuminate\Http\Request;
use App\DataTables\CampaignDataTable;
use App\Crud\CampaignCrud;
use DB;
use Illuminate\Support\Facades\Validator;

class CampaignController extends Controller
{
    private $title, $crud, $image, $requests;

    public function __construct(Request $request, ImageRepo $imageUpload, CampaignCrud $crud)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->image = $imageUpload;
        $this->crud = $crud;
        $this->requests = $request->except('_token', '_method');
        $this->title = "Kampaniyalar";
    }


    public function index(CampaignDataTable $dataTable)
    {
        return $dataTable->render("admin.campaigns", ['title' => $this->title]);
    }


    public function create()
    {
        $fields = $this->crud->fields('create');

        return view("admin.dt.create",  ['fields' => $fields, 'title' => 'Yeni kampaniya', 'route' => 'campaigns.store']);
    }


    public function store(Request $request)
    {
        $validation = Validator::make($this->requests, Campaign::rules( 'required|'), Campaign::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        $upload = $this->image->store($request->image, $this->resize());

        $this->requests['image'] = $upload;

        Campaign::create($this->requests);

        return $this->responseSuccess();
    }


    public function edit($id)
    {
        $data = Campaign::findOrFail($id);

        $fields = $this->crud->fields('edit', $data);

        return view('admin.dt.edit', ['title' => 'Düzəliş et', 'fields' => $fields, 'data' => $data, 'route' => ['campaigns.update', $id]]);
    }


    public function update(Request $request, $id)
    {
        $data = Campaign::findOrFail($id);

        $validation = Validator::make($this->requests, Campaign::rules( null), Campaign::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        if($request->hasFile('image')){
            $upload = $this->image->store($request->image, $this->resize());
            $this->image->deleteFile($data->image); //delete old file
            $this->requests['image'] = $upload;
        }

        foreach($this->requests as $key => $put){
            $data->$key = $put;
        }

        $data->save();

        return $this->responseSuccess();
    }


    public function destroy($id)
    {
        $data = Campaign::findOrFail($id);

        $data->delete();
        $this->image->deleteFile($data->image); //delete image

        return $this->responseSuccess("", "#campaigns", "#modal-confirm");
    }

    protected function resize()
    {
        $resizeImage = ['resize' => ['fit' => true, 'size' => [345, 330]], 'thumb' => null ];

        return $resizeImage;
    }

    private function responseSuccess($msg = "", $draw = "#campaigns", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }
}



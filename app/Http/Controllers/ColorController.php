<?php

namespace App\Http\Controllers;

use App\Exceptions\DataTableException;
use Illuminate\Http\Request;
use App\Models\Color;
use App\DataTables\ColorDataTable;
use App\Crud\ColorCrud;
use Illuminate\Support\Facades\Validator;
use DB;

class ColorController extends Controller
{
    private $crud, $requests, $title;

    public function __construct(Request $request, ColorCrud $crud)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->crud = $crud;
        $this->title = "Rənglər";
        $this->requests = $request->except('_token', '_method');

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('color', false);
        }

    }


    public function index(ColorDataTable $dataTable)
    {
        return $dataTable->render('admin.colors', ['title' => $this->title]);
    }


    public function create()
    {
        return view('admin.color.create', ['title' => 'Yeni Rəng', 'route' => 'colors.store']);
    }



    public function store(Request $request)
    {
        // check Validation
        $validation = Validator::make($this->requests, Color::rules(), Color::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        Color::create($this->requests);

        return $this->responseSuccess();
    }


    public function edit($id)
    {
        $data = Color::findOrFail($id);

        return view('admin.color.edit', ['title' => 'Düzəliş et', 'data' => $data, 'route' => ['colors.update', $id]]);
    }


    public function update(Request $request, $id)
    {
        $data = Color::findOrFail($id);

        // check Validation
        $validation = Validator::make($this->requests, Color::rules(), Color::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        foreach($this->requests as $key => $put){
            $data->$key = $put;
        }

        $data->save();

        return $this->responseSuccess();
    }



    public function destroy($id)
    {
        $data = Color::findOrFail($id);
        $data->delete();

        return $this->responseSuccess("", "#colors", "#modal-confirm");
    }


    private function responseSuccess($msg = "", $draw = "#colors", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }
}

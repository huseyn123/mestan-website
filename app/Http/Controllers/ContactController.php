<?php

namespace App\Http\Controllers;

use App\Logic\Share;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Config;
use App\Mail\ContactForm;
use App\Mail\ApplicationForm;
use Mail;
use DB;

class ContactController extends Controller
{
    use Share;

//    private $lang;

    public function __construct()
    {
        $this->loadData();
    }

    private function sendMail($pdf, $form,$files, $email)
    {
        Mail::to($email)->send(new ApplicationForm($pdf, $form, array_filter($files),'Onlayn Müraciət'));

        request()->session()->flash('success', true);

        return redirect()->back();

    }



    public function store(Request $request)
    {
        $method = $request->input('method');

        $rules = [
            'full_name' => 'required',
            'phone' => 'required',
        ];

        return $this->$method($request,$method,$rules);
    }


    public function callme($request,$post_page,$mainRules)
    {

        $config = Config::findOrFail(1);

        $email = Config::where('key', 'email')->firstOrFail();

        return $this->senformdMail($request->all(),$mainRules,[], $post_page);

    }


    private function senformdMail($form,$rules,$files, $title,$send_mail = null)
    {

        $validation = Validator::make($form , $rules);

        if($validation->fails()){
            return response()->json(['result' => 'error','msg' => $validation->messages()->first()]);
        }

        $email = Config::where('key', 'contact_email')->firstOrFail();

        $subject = $this->dictionary[$title.'_subject'] ?? 'AAAAAAA';


        try {
            if($send_mail){
                Mail::to($send_mail)->send(new ContactForm($form,'contact',$subject));
            }else{
                Mail::to($email->value)->send(new ContactForm($form,'contact',$subject));
            }

            return response()->json(['result' => 1,'msg' =>$subject,'a'=>$title.'_subject']);

        } catch (\Exception $e) {
            return response()->json(['result' => 0,'msg' =>$e->getMessage()]);
        }


    }

}

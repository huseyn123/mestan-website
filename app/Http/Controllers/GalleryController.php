<?php

namespace App\Http\Controllers;

use App\Logic\Dropzone;
use App\Models\Article;
use Illuminate\Http\Request;
use App\DataTables\GalleryDataTable;
use Spatie\MediaLibrary\Models\Media;
use Illuminate\Support\Facades\Response;

class GalleryController extends Controller
{
    private $title, $dropzone;

    public function __construct(Request $request, Dropzone $dropzone)
    {
        $this->middleware('auth:admin');

        $this->requests = $request->except('_token', '_method');
        $this->title = 'Qalereya';
        $this->dropzone = $dropzone;
    }


    public function index(GalleryDataTable $dataTable, $route, $id)
    {
        $model = config('config.model.'.$route);
        $data = $model::findOrFail($id);

        $gallery_type = 'gallery';

        if($route == 'page'){
            $width = config('config.gallery_size.'.$data->template_id.'.width') ?? '5000000';
            $height =config('config.gallery_size.'.$data->template_id.'.height') ?? '5000000';
        }else{
            $width = $model::$gallery_width;
            $height = $model::$gallery_height;
        }


        return $dataTable->data($data,$gallery_type)->render('admin.galleries', ['title' => $this->title, 'data' => $data, 'route' => $route, 'width' => $width, 'height' => $height,'gallery_type' => $gallery_type]);
    }


    public function store(Request $request, $route, $id)
    {
        $model = config('config.model.'.$route);
        $data = $model::findOrFail($id);
        $gallery_type ='gallery';

        if($route == 'projects'){
            $data = Article::findOrFail($id);
        }


        if($route == 'page'){
            $width = config('config.gallery_size.'.$data->template_id.'.width') ?? '5000000';
            $height =config('config.gallery_size.'.$data->template_id.'.height') ?? '5000000';
        }else{
            $width = $model::$gallery_width;
            $height = $model::$gallery_height;
        }

        $model::$gallery_width = $width;
        $model::$gallery_height = $height;


        $upload = $this->dropzone->upload($data, $request->file,$width,$height,$route,$gallery_type);

        return $upload;
    }


    public function destroy($id)
    {
        $media = Media::findOrFail($id);

        $media->delete();

        $response = ["code" => 200, "msg" => "", "draw" => "#galleries", "close" => "#modal-confirm"];
        return response()->json($response, $response['code']);
    }
}

<?php

namespace App\Http\Controllers;

use App\Exceptions\DataTableException;
use Illuminate\Http\Request;
use App\Models\Config;
use App\DataTables\ConfigDataTable;
use App\Crud\ConfigCrud;
use DB;
use Illuminate\Support\Facades\Validator;

class ConfigController extends Controller
{
    private $crud, $requests;
    public $title;

    public function __construct(Request $request, ConfigCrud $crud)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->crud = $crud;
        $this->requests = $request->except('_token', '_method');
        $this->title = "Konfiqurasiya";

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('config', false);
        }
    }


    public function index(ConfigDataTable $dataTable)
    {
        return $dataTable->render("admin.configs", ['title' => $this->title]);
    }


    public function edit($id)
    {
        $data = Config::findOrFail($id);

        $fields = $this->crud->fields('edit', $data);

        return view('admin.dt.edit', ['title' => 'Düzəliş et', 'fields' => $fields, 'data' => $data, 'route' => ['config.update', $id]]);
    }


    public function update(Request $request, $id)
    {
        $data = Config::findOrFail($id);

        // check Validation
        $validation = Validator::make($this->requests, Config::$rules, Config::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        foreach($this->requests as $key => $put){
            $data->$key = $put;
        }

        $data->save();

        return $this->responseSuccess();
    }


    private function responseSuccess($msg = "", $draw = "#configs", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }
}



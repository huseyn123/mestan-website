<?php

namespace App\Http\Controllers;
use App\Crud\ArticleParameterCrud;
use App\DataTables\ArticleDataTable;
use App\Exceptions\DataTableException;
use App\Logic\ImageRepo;
use App\Models\Album;
use App\Models\Article;
use App\Models\ArticleTranslation;
use App\Models\PageTranslation;
use Illuminate\Support\Facades\Validator;
use App\Crud\ArticleCrud;
use Illuminate\Http\Request;
use App\Logic\Slug;
use DB;

class ArticleController extends Controller
{
    private $crud, $image, $requests, $title;

    public function __construct(ArticleCrud $crud, ImageRepo $image, Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->requests = $request->except('_token', '_method');
        $this->crud = $crud;
        $this->image = $image;
        $this->title = "Xəbərlər";
    }


    public function index(ArticleDataTable $dataTable)
    {
        return $dataTable->render('admin.articles', ['title' => $this->title]);
    }

    public function create()
    {
        $fields = $this->crud->fields(null, 'create');

        return view('admin.page.create', ['route' => 'articles.store', 'fields' => $fields, 'title' => 'Yeni xəbər']);
    }


    public function store(Request $request)
    {
        $lang = $request->lang;
        $translates = ['name', 'page_id', 'slug', 'summary', 'content'];

        // check Validation
        $validation = Validator::make($this->requests, Article::rules( null), Article::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        $parent = PageTranslation::findOrFail($request->page_id);
        $lang = $parent->lang;

        // apply
        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'articleTranslation', $lang );
        $image = $this->image->store($request->image, $this->resize());
        $this->requests['image'] = $image;

        //inputs for models
        $articleInputs = array_diff_key($this->requests, array_flip($translates));
        $translationInputs = array_diff_key($this->requests, $articleInputs);

        //store
        DB::beginTransaction();


        //create automatically album
        try{
            $album = Album::create(['name' => $request->name, 'type' => 1]);
        }
        catch(\Exception $e){
            DB::rollback();
            throw new DataTableException($e->getMessage());
        }
        //end creating album

        try{
            $articleInputs['album_id'] = $album->id;
            $article = Article::create($articleInputs);
        }
        catch(\Exception $e){
            DB::rollback();
            throw new DataTableException($e->getMessage());
        }

        try{
            $translationInputs['article_id'] = $article->id;
            $translationInputs['lang'] = $lang;

            ArticleTranslation::create($translationInputs);
        }
        catch(\Exception $e){
            DB::rollback();
            throw new DataTableException($e->getMessage());
        }

        DB::commit();

        return $this->responseSuccess();
    }


    public function edit($id)
    {
        $parameterCrud = new ArticleParameterCrud();
        $fields = [];
        $langs = [];

        $article = ArticleTranslation::join('articles', 'article_translations.article_id', '=', 'articles.id')
            ->select(
                'article_translations.*',
                'articles.album_id',
                'articles.status',
                'articles.published_at',
                'articles.image'
            )
            ->findOrFail($id);

        if(count(config('app.locales')) == 1){
            return $this->editSingle($article);
        }

        $relatedPage = $article->relatedPages;
        $parameters = $parameterCrud->fields('edit', $article);

        foreach ($relatedPage as $rel){
            $fields[$rel->lang] = $this->crud->fields($rel->lang, 'edit', $rel);
            $langs[$rel->lang] = $rel->lang;
        }

        foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale){
            $fields[$key] = $this->crud->fields( $key, 'edit', null);
        }

        return view('admin.page.edit-withlocale', ['info' => $article, 'langs' => $langs, 'parameters' => $parameters, 'pageId' => $article->article_id, 'fields' => $fields, 'relatedPage' => $relatedPage, 'model' => 'articles', 'route' => 'articleTranslation' ]);
    }


    private function editSingle($page)
    {
        $fields = $this->crud->fields(null,'edit', $page);

        return view('admin.page.edit', ['info' => $page, 'fields' => $fields, 'model' => 'article']);
    }


    public function update(Request $request, $id)
    {
        $article = Article::findOrFail($id);

        $validation = Validator::make($this->requests, Article::$parameterRules, Article::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        if($request->hasFile('image')) {
            $image = $this->image->store($request->image, $this->resize());
            $this->image->deleteFile($article->image); //delete old file
            $this->requests['image'] = $image;
        }

        foreach($this->requests as $key => $put){
            $article->$key = $put;
        }

        $article->save();

        return $this->responseSuccess();
    }


    public function updateSingle(Request $request, $id)
    {
        $articleTranslation = ArticleTranslation::findOrFail($id);

        $translates = ['name', 'page_id', 'slug', 'summary', 'content', 'lang'];

        // check Validation
        $validation = Validator::make($this->requests, Article::rules( $id), Article::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        $page = PageTranslation::findOrFail($request->page_id);


        // apply
        $this->requests['lang'] = $page->lang;
        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'articleTranslation', $page->lang);

        //inputs for models
        $articleInputs = array_diff_key($this->requests, array_flip($translates));
        $translationInputs = array_diff_key($this->requests, $articleInputs);

        //store
        DB::beginTransaction();


        try{
            Article::where('id', $articleTranslation->article_id)->update($articleInputs);
        }
        catch(\Exception $e){
            DB::rollback();
            throw new DataTableException($e->getMessage());
        }

        try{
            foreach($translationInputs as $key => $put){
                $articleTranslation->$key = $put;
            }
            $articleTranslation->save();
        }
        catch(\Exception $e){
            DB::rollback();
            throw new DataTableException($e->getMessage());
        }

        DB::commit();

        return $this->responseSuccess();
    }


    private function responseSuccess($msg = "", $draw = "#articles", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }


    private function resize()
    {
        $resizeImage = ['resize' => ['fit' => false, 'size' => [570, 400]], 'thumb' => ['fit' => true, 'size' => [360, 253]] ];

        return $resizeImage;
    }
}

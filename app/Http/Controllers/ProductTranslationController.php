<?php

namespace App\Http\Controllers;
use App\Exceptions\DataTableException;
use App\Models\Page;
use App\Models\Product;
use App\Models\ProductTranslation;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Logic\Slug;
use App\Logic\Order;
use DB;

class ProductTranslationController extends Controller
{
    private $requests;

    public function __construct(Request $request, Order $order)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['order','index_order']);;
        $this->order = $order;
        $this->requests = $request->except('_token', '_method');

    }


    public function store(Request $request)
    {
        // check Validation
        $validation = Validator::make($this->requests, ProductTranslation::rules( null), ProductTranslation::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }


        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'productTranslation', $request->lang );

        $this->requests['order'] = ProductTranslation::max('order') + 1;



        $this->requests['index_order'] = ProductTranslation::max('index_order') + 1;

        ProductTranslation::create($this->requests);

        return $this->responseSuccess();
    }


    public function update(Request $request, $id)
    {
        $product = ProductTranslation::findOrFail($id);

        // check Validation
        $validation = Validator::make($this->requests, ProductTranslation::rules($id), ProductTranslation::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }

        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'productTranslation', $request->lang, $product->id);

        if(!$request->meta_keywords) {
            $this->requests['meta_keywords'] = null;
        }

        foreach($this->requests as $key => $put){
            $product->$key = $put;
        }

        $product->save();

        return $this->responseSuccess();
    }


    public function trash($id)
    {
        $product = ProductTranslation::findOrFail($id);
        $product->delete();

        return $this->responseSuccess("", "#products", "#modal-confirm");

    }


    public function restore($id)
    {
        $product = ProductTranslation::onlyTrashed()->findOrFail($id);
        $product->restore();

        return $this->responseSuccess("", "#products", "#modal-confirm");
    }

    public function destroy($id)
    {
        $product =  ProductTranslation::onlyTrashed()->findOrFail($id);

        $relatedPages = ProductTranslation::where('product_id', $product->product_id)->withTrashed()->count();

        if($relatedPages == 1){
            Product::where('id', $product->product_id)->forceDelete();
        }
        else{
            $product->forceDelete();
        }

        return $this->responseSuccess("", "#products", "#modal-confirm");
    }

    public function index_order()
    {
        $lang = request()->get('lang', 'az');

        $data =  $products = Product::join('product_translations as ptr','ptr.product_id','products.id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->whereNull('ptr.deleted_at')
            ->whereNull('pt.deleted_at')
            ->where('ptr.lang',$lang)
            ->where('products.status',1)
            ->whereNotNull('products.index_form')
            ->whereIn('pages.template_id',[4,5,6])
            ->select('pages.template_id','ptr.name','ptr.id','products.index_form','ptr.lang','ptr.index_order')
            ->orderBy('ptr.index_order')
            ->get();

        return $this->order->getProduct('IndexProductTranslation',$data,$lang,$type = 'index');
    }


    public function order()
    {
        $lang = request()->get('lang', 'az');
            $data = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
                ->where('pt.lang', $lang)
                ->whereIn('pages.template_id', [4,5,6])
                ->whereNull('pt.deleted_at')
                ->select('pt.slug', 'pt.name', 'pt.id')
                ->with('products')
                ->get();

        return $this->order->getProduct('productTranslation',$data,$lang);
    }


    public function postOrder(Request $request)
    {
        return $this->order->post($request, 'productTranslation', false);

    }

    public function postIndexOrder(Request $request)
    {
        return $this->order->post($request, 'productTranslation', false,true);

    }

    private function responseSuccess($msg = "", $draw = "#products", $close = "#myModal")
    {
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }
}

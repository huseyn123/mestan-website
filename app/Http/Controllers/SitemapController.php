<?php

namespace App\Http\Controllers;

use App\Models\Config;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use DB;

class SitemapController extends Controller
{
    public function index()
    {
        $date = Config::where('key', 'sitemap')->first();

        return view('admin.sitemap', ['title' => 'Sitemap', 'date' => $date]);
    }


    public function store()
    {
        $config = Config::where('key', 'sitemap')->first();

        // create new sitemap object
        $sitemap = App::make('sitemap');

        // set cache key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean)
        // by default cache is disabled
        $sitemap->setCache('laravel.sitemap', 60);
        $lang = 'az';

        // check if there is cached sitemap and build new only if is not
        if (!$sitemap->isCached()) {

            //home page
            $translations = [];

            foreach(config("app.locales") as $locale){
                if($locale != $lang){
                    $translations[] = ['language' => $locale, 'url' => url('/'.$locale)];
                }
            }

            $sitemap->add(url()->to('/'), Carbon::now(), '0.5', 'monthly', [], null, $translations);


            //pages
            $translations = [];

            $pages = DB::table('pages')
                ->join('page_translations as pt', 'pages.id', '=', 'pt.page_id')
                ->whereNull('pt.deleted_at')
                ->select('pt.slug', 'pt.name', 'pt.updated_at', 'pt.lang', 'pages.id', 'pt.page_id')
                ->orderBy('pages.id', 'desc')
                ->get();


            foreach ($pages as $page){

                if($page->lang == $lang){

                    foreach($pages as $transPages){
                        if($page->id == $transPages->page_id && $transPages->lang != $lang){
                            $translations[$page->id][] = ['language' => $transPages->lang, 'url' => url()->to($transPages->lang.'/'.$transPages->slug)];
                        }
                    }

                    $sitemap->add(url()->to($page->slug), $page->updated_at, '0.5', 'monthly', [], $page->name, $translations[$page->id] ?? []);
                }
            }


            //articles
            $translations = [];

            $articles = DB::table('articles')
                ->join('article_translations as at', 'articles.id', '=', 'at.article_id')
                ->join('page_translations as pt', 'at.page_id', '=', 'pt.id')
                ->whereNull('at.deleted_at')
                ->where('articles.status', 1)
                ->select('at.slug', 'at.name', 'at.updated_at', 'at.lang', 'at.summary', 'articles.id', 'at.article_id', 'pt.slug as parent', 'articles.image')
                ->orderBy('articles.id', 'desc')
                ->get();


            foreach ($articles as $article){

                $images = [
                    ['url' => asset("storage/$article->image"), 'title' => $article->name, 'caption' => $article->summary],
                ];

                if($article->lang == $lang){

                    foreach($articles as $transArticles){
                        if($article->id == $transArticles->article_id && $transArticles->lang != $lang){
                            $translations[$article->id][] = ['language' => $transArticles->lang, 'url' => url()->to($transArticles->lang.'/'.$transArticles->parent.'/'.$transArticles->slug)];
                        }
                    }

                    $sitemap->add(url()->to($article->parent.'/'.$article->slug), $article->updated_at, '0.5', 'monthly', $images, $article->name, $translations[$article->id] ?? []);
                }
            }


            //products

            $translations = [];

            $products = Product::join('product_translations as prt', 'products.id', '=', 'prt.product_id')
                ->join('product_categories as pc', 'pc.product_id', '=', 'products.id')
                ->join('page_translations as pt', 'pt.page_id', '=', 'pc.page_id')
                ->where('products.status', 1)
                ->where('products.price', '>', 0)
                ->whereNull('pt.deleted_at')
                ->whereNull('prt.deleted_at')
                ->select('prt.slug', 'prt.name', 'prt.updated_at', 'pt.lang', 'prt.lang as lang2', 'prt.summary', 'products.id', 'prt.product_id', 'pt.slug as parent', 'products.image', 'products.album_id')
                ->with('gallery')
                ->orderBy('products.id', 'desc')
                ->get();


            foreach ($products as $product){

                $images = [
                    ['url' => asset("storage/$product->image"), 'title' => $product->name, 'caption' => $product->summary],
                ];

                foreach ($product->gallery as $image) {
                    $images[] = array(
                        'url' => asset("storage/$image->filename"),
                        'title' => $image->name,
                        'caption' => $image->summary
                    );
                }

                if($product->lang == $lang && $product->lang2 == $lang){

                    foreach($products as $transProducts){
                        if($product->id == $transProducts->product_id && $transProducts->lang != $lang && $transProducts->lang2 != $lang && $transProducts->lang == $transProducts->lang2){
                            $translations[$product->id][] = ['language' => $transProducts->lang, 'url' => url()->to($transProducts->lang.'/'.$transProducts->parent.'/'.$transProducts->slug)];
                        }
                    }

                    $sitemap->add(url()->to($product->parent.'/'.$product->slug), convertDate($product->updated_at, true), '0.5', 'monthly', $images, $product->name, $translations[$product->id] ?? []);
                }
            }
        }

        $sitemap->store('xml', 'storage/sitemap');

        $config->value = Carbon::now();
        $config->save();

        return redirect()->back();
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Mockery\Exception;

class ajax
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->ajax()){
            dd('Only ajax');
        }


        return $next($request);


    }
}

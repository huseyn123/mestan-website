<?php
/**
 * Created by PhpStorm.
 * User: Riko
 * Date: 8/16/2016
 * Time: 5:12 PM
 */

use Carbon\Carbon;
use Jenssegers\Date\Date;
use App\Logic\WebCache;

function getConfig()
{
    return WebCache::config()->pluck('value', 'key');
}

function activeUrl($url, $output = 'active')
{
    if(request()->url() == $url){
        return $output;
    }
}

function activeQuery($url, $output = 'active')
{
    if(request()->url().'?'.request()->getQueryString() == $url){
        return $output;
    }
}

function loading(){
    return "<i class='fa fa-circle-o-notch fa-spin'></i>";
}

function icon($icon, $title = null){
    return "<i class='fa fa-$icon'></i>";
}

function filterDate($date, $tz = true, $time = 'day')
{
    $date = with(new Carbon($date));

    if($tz == true){
        $date = $date->timezone('Asia/Baku');
    }

    if($time == 'eFull'){
        $date = $date->format('d/m/Y, H:i');
    }
    elseif($time == 'full'){
        $date = $date->format('Y-m-d H:i');
    }
    elseif($time == 'day'){
        $date = $date->format('Y-m-d');
    }
    elseif($time == 'birthday'){
        $date = $date->format('d.m.Y');
    }
    elseif($time == 'eDay'){
        $date = $date->format('d/m/Y');
    }
    elseif($time == 'time'){
        $date = $date->format('H:i');
    }
    elseif($time == 'xeditableDate'){
        $date = $date->format('d/m/Y H:i'); //vergul yoxdu
    }
    elseif($time == 'soapDate'){
        $date = $date->format('Y-m-d\TH:i:s');
    }
    return $date;
}


function userBirthdayDate($date, $tz = true)
{
    $b_day = with(new Carbon($date));

    if($tz == true){
        $b_day = $b_day->timezone('Asia/Baku');
    }

    $subDay = $b_day->subDay(2)->format('d.m');

    return $subDay.' - '.$date->format('d.m');
}


function checkBirthdayPrice($date, $tz = true)
{
    $this_day = Carbon::now('Asia/Baku')->format('d.m');
    $b_day = with(new Carbon($date))->format('d.m');
    $addDay1 = with(new Carbon($date))->addDays(1)->format('d.m');
    $addDay2 = with(new Carbon($date))->addDays(2)->format('d.m');

    if($this_day == $b_day || $this_day == $addDay1 || $this_day == $addDay2){
        return true;
    }

    return false;
}



function albumLimit()
{
    return 15;
}

function convertDate($date, $time = false, $symbol = '/')
{
    if($time == false){
        list($d, $m, $y) = explode($symbol, $date);
        return "$y-$m-$d";
    }
    else{
        list($d1, $d2) = explode(' ', $date);
        list($d, $m, $y) = explode($symbol, $d1);

        return "$y-$m-$d $d2";
    }
}

function checkStatus()
{
    return 1;
}

function blogDate($date)
{
    Date::setLocale(app()->getLocale());
    $date =  Date::parse($date)->format('d F Y');

    return $date;
}


function getYoutubeId($url)
{
    preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
    return $match[1];
}

function g_icon($class, $title = null, $spinning = null){
    return "<i class='fa fa-circle-o-notch fa-spin'></i>";
}

function pageName($name)
{
    $remove = explode(' ', $name)[0];

    $replace = '<strong>'.$remove.'</strong>';

    $newName = str_replace($remove, $replace, $name);

    return $newName;
}

function clearCache($cache, $lang = true)
{
    if($lang == true){
        foreach(config('app.locales') as $key => $locale){
            Cache::forget($cache.'_'.$key);
        }
    }
    else{
        Cache::forget($cache);
    }
}

function azn()
{
    return ' AZN';
}


function basketCount()
{
    return \App\Models\Cart::whereCondition()->count();
}

function sprint($int, $value)
{
    return sprintf("%".$int."d", $value);
}

function encode($data)
{
    $newData = [];

    foreach($data as $key => $d){
        $newData[] = ['value' => $key, 'text' => $d];
    }

    return json_encode($newData);
}


function check($product_form,$checked_form)
{
    $data = true;
    if(!$product_form){
        $data = false;
    }else{
        foreach ($checked_form as $key=>$value){
            if(count(array_intersect($value, $product_form[$key]))){
                $data = true;
            }else{
                $data = false;
                break;
            }
        }
    }

    return $data;
}



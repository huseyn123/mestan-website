<?php

namespace App\Crud;
use App\Logic\MultiLanguageSelect;

class ColorCrud extends RenderCrud
{


    public function fields($lang, $action, $data = false)
    {
        $fields = [
            [
                "label" => 'Rəng Adı',
                "db" => "title",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ]
        ];

       return $this->render($fields, $action, $data);
    }
}



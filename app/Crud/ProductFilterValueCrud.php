<?php

namespace App\Crud;
use App\Models\ProductFilters;


class ProductFilterValueCrud extends RenderCrud
{

    private function selectFilter($data)
    {
        if($data != false){
             $categories =  Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
                ->where('pt.lang', 'az')
                ->whereNull('pt.deleted_at')
                ->whereIn('pages.id',$data->cat_ids);

                return $categories->pluck('pages.id')->toArray();        }
        else{
            return null;
        }
    }

    private function Filters()
    {
        return ProductFilters::orderBy('id','DESC')->pluck('title_az', 'id');
    }


    public function fields($action, $data = false)
    {
        $fields = [
           [
            "label" => "Filter",
                "db" => "filter_id",
                "type" => "select",
                "data" => $this->Filters(),
                "selected" => null,
                "attr" => ['class'=>'form-control','required']
            ],
            [
                "label" => "Status",
                "db" => "status",
                "type" => "select",
                "data" => config('config.article-status'),
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
        ];

        return $this->render($fields, $action, $data);
    }
}



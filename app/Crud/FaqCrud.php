<?php

namespace App\Crud;

class FaqCrud extends RenderCrud
{
    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => "Sual",
                "db" => "title",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'required'],
            ],
            [
                "label" => "Cavab",
                "db" => "text",
                "type" => "textarea",
                "attr" => ['class'=>'form-control', 'rows' => 5, 'required'],
            ],
            [
                "label" => 'Tip',
                "db" => "type",
                "type" => 'select',
                "attr" => ['class'=>'form-control'],
                "data" => ['FAQ', 'Qaydalar'],
                "selected" => 0,
            ],
        ];

        return $this->render($fields, $action, $data);
    }

}



<?php

namespace App\Crud;


class RegUserEmailCrud extends RenderCrud
{
    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => "Başlıq",
                "db" => "subject",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'required'],
            ],
            [
                "label" => "Məzmun",
                "db" => "content",
                "type" => "textarea",
                "hide" => [1,2,4,5,6,8,9,10],
                "attr" => ['class'=>'form-control ckeditor', 'id' => 'editor', 'required']
            ],
            [
                "label" => "Test Email",
                "db" => "email_test",
                "type" => "text",
                "attr" => ['class'=>'form-control','title'=>"Test email doldurularsa email ancaq test email ünvanına göndəriləcəy (Bu istifadəçilərə email göndərilməzdən qabağ test üçün nəzərdə tutulub)"],
            ],
//            [
//                "label" => " ",
//                "db" => "type",
//                "type" => "hidden",
//                "value" => 2,
//            ],
        ];
        return $this->render($fields, $action, $data);
    }
}



<?php

namespace App\Crud;

use App\Models\Page;
use App\Models\Product;

class ProductFormCrud extends RenderCrud
{

    private function form($route = 'toys'){

        $t_form = config('config.product_form.'.$route);


    }


    public function fields($action,$data = false,$route,$dublicate = false)
    {
        $fields = [
            [
                "label" => "Status",
                "db" => "status",
                "type" => "select",
                "data" => config('config.article-status'),
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
        ];

        return $this->render($fields, $action, $data);
    }
}



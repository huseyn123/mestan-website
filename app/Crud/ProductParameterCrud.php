<?php

namespace App\Crud;

use App\Logic\WebCache;
use App\Models\Color;
use App\Models\Page;
use App\Models\Product;
use App\Models\ProductForm;
use App\Models\Select;
use App\Models\ProductFilters;
use App\Models\Tag;

class ProductParameterCrud extends RenderCrud
{

    private $dublicate;

    private function brands()
    {
        return WebCache::getBrand();
    }

    private function colors()
    {
        return WebCache::getColor('az');

    }

    private function tag()
    {
        return Tag::groupBy('keyword')->pluck('keyword', 'id');
    }

    private function productTags($data)
    {
        if($data != false){
            return $data->tags->pluck('tag_id')->toArray();
        }
        else{
            return null;
        }
    }



    private function form($route, $data)
    {
        if($data != false){
            $page  = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
                ->where('pt.id',$data->page_id)
                ->whereNull('pt.deleted_at')
                ->with('parent')
                ->first();        
        }else{
            $template_id = config('config.product_template.'.$route);

            $page = Page::join('page_translations', 'page_translations.page_id', '=', 'pages.id')
                    ->select('page_translations.id', 'page_translations.parent_id')
                    ->where('page_translations.lang', 'az')
                    ->whereIn('pages.template_id',[$template_id])
                    ->whereNull('page_translations.deleted_at')
                    ->orderBy('page_translations.id', 'asc')
                    ->with('parent')
                    ->first();
        }    

        $page_id = isset($page->parent) ? $page->parent->page_id : null;
        settype($page_id, "string");
 
        $form = ProductFilters::where('status',1)
                ->whereJsonContains('cat_ids', $page_id)
                ->select('id','title_az as title','filter_column')
                ->with('filter_value')
                ->get();

        return $form;

    }


    private function productForm($data,$key)
    {
        $product_form = [];
        if($data != false){
            $product_form = $data->form[$key] ?? [];
        }

        return $product_form;
    }

    private function select($data)
    {

        $selects = $data->pluck('title_az','id');
        return $selects;
    }


    public function fields($action,$data = false,$route,$dublicate = false)
    {
        $this->dublicate =$dublicate;
        $fields = [
            [
                "label" => " ",
                "db" => "product_type",
                "type" => "hidden",
                "value" => $route,
                "attr" => ['id'=>"product_type"],
            ],
            [
                "label" => "Şəkil",
                "db" => "image",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){
                    if($this->dublicate ==false && $data != false && $data->getFirstMedia()){
                        if(substr($data->getFirstMedia()->getFullUrl(), -3) == 'svg'){
                            $url = $data->getFirstMedia()->getFullUrl();
                            $style = 'width:100px;height:100px';
                        }else{
                            $url = $data->getFirstMedia()->getUrl('blade');
                            $style="max-width:100%";
                        }

                        $img = '<div class="input-group"><img src="'.asset($url).'" style="'.$style.'"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="" required>
                    <div class="divImage" style="display:none">
                        <img class="showImage" src="#">
                    </div>';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                },
            ],
            [
                "label" => 'Qiymət',
                "db" => "old_price",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'min' => 1, 'required' => 'required'],
            ],
            [
                "label" => 'Faiz',
                "db" => "percent",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'min' => 0,'title'=>"Məs:15;Faiz yazılanda --Yeni qiymət--  qiymetin  faiz nisbətinə uyğun avtomatik hesablanacaq(Yeni qiymət boş buraxıldığı təqdirdə belə)"],
            ],
            [
                "label" => 'Yeni qiymət',
                "db" => "price",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'min' => 1],
            ],
            [
                "label" => 'Topdan qiymət',
                "db" => "wholesale",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'min' => 1],
            ],
            [
                "label" => 'Bar kod',
                "db" => "bar_code",
                "type" => 'number',
                "attr" => ['class'=>'form-control', 'min' => 1, 'required' => 'required'],
            ],
            [
                "label" => "Teq",
                "db" => "tag[]",
                "type" => "select",
                "data" => $this->tag(),
                "selected" => $this->productTags($data),
                "attr" => ['class'=>'form-control select-multiple', 'multiple' => 'multiple','data-create' => 1,  'title' => 'Teqləri bir birindən ayırmaq üçün Enter düyməsini sıxın.'],
            ],
            [
                "label" => "Brand",
                "db" => "brand_id",
                "type" => "select",
                "data" => $this->brands(),
                "selected" => null,
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Status",
                "db" => "status",
                "type" => "select",
                "data" => config('config.article-status'),
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => 'Index Banner',
                "db" => 'index_banner',
                "type" => "checkbox",
                "value" =>1,
                "checked" => false,
                'attr' =>[]
            ],
            [
                "label" => 'Korporativ müştərilər',
                "db" => 'user_type',
                "type" => "checkbox",
                "value" =>1,
                "checked" => false,
                'attr' =>[]
            ],
            [
                "label" => 'Ən çox Satılanlar',
                "db" => 'best_seller',
                "type" => "checkbox",
                "value" =>1,
                "checked" => false,
                'attr' =>[]
            ],
            [
                "label" => "Youtube Link",
                "db" => "youtube_link",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'title'=>"Məs: https://www.youtube.com/watch?v=nymfEQ6oQaI "],
            ],
            [
                "label" => 'Ana səhifədə görünsün',
                "db" => 'show_index',
                "type" => "checkbox",
                "value" =>1,
                "checked" => false,
                'attr' =>[]
            ],
            [
                "label" => " ",
                "db" => "filter_list[]",
                "type" => "hidden",
                'divClass' =>'filtr_list',
                "value" => $this->form($route, $data)->pluck('filter_column'),
            ],
        ];

        foreach ($this->form($route, $data) as  $value){
            array_push($fields,

                [
                    "label" => $value->title,
                    "db" => $value->filter_column.'[]',
                    "type" => "checkbox",
                    "value" =>$this->productForm($data,$value->filter_column),
                    "checked" => false,
                    "multiple" =>$this->select($value->filter_value),
                    "form" =>true,
                    'divClass' =>'product_filtrs',
                    'attr' =>['class'=> 'product_filtrs','divClass']
                ]
            );
        }

//        array_push($fields,
//            [
//                "label" => 'Ana Səhifə',
//                "db" => 'index_form[]',
//                "type" => "checkbox",
//                "value" =>$data ? $data->index_form : [],
//                "checked" => false,
//                "multiple" =>config('config.index_form'),
//                'attr' =>[]
//            ]);

        return $this->render($fields, $action, $data);
    }
}



<?php

namespace App\Crud;


class CorUserCrud extends RenderCrud
{
    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => "Ad Soyad",
                "db" => "name",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'required'],
            ],
            [
                "label" => "Email",
                "db" => "email",
                "type" => "email",
                "attr" => ['class'=>'form-control', 'required'],
            ],
            [
                "label" => "Şifrə",
                "db" => "password",
                "type" => "password",
                "attr" => ['class'=>'form-control', 'required'],
            ],
            [
                "label" => "Şifrə təkrar",
                "db" => "password_confirmation",
                "type" => "password",
                "attr" => ['class'=>'form-control', 'required'],
            ],
            [
                "label" => " ",
                "db" => "type",
                "type" => "hidden",
                "value" => 2,
            ],
        ];
        return $this->render($fields, $action, $data);
    }
}



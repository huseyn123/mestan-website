<?php

namespace App\Crud;

class CampaignCrud extends RenderCrud
{
    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => "Şəkil",
                "db" => "image",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none', 'title' => 'Ölçü: 345x330px'],
                "design" => function($input, $data, $title = ' Əlavə et'){

                    if($data != false){
                        $img = '<div class="input-group"><img src="'.asset("storage/$data->image").'" style="max-width:100%"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="">';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                }
            ],
            [
                "label" => "Link",
                "db" => "url",
                "type" => "text",
                "attr" => ['class'=>'form-control'],
            ],
        ];

        return $this->render($fields, $action, $data);
    }

}



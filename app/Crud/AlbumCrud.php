<?php

namespace App\Crud;

class AlbumCrud extends RenderCrud
{
    public function fields($action, $data = false)
    {
        $fields = [
            [
                "label" => 'Albom adı',
                "db" => "name",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
        ];

        return $this->render($fields, $action, $data);
    }
}



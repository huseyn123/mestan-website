<?php

namespace App\Crud;

class DeliveryCrud extends RenderCrud
{


    public function fields($action, $data = false)
    {
        $fields = [

            [
                "label" => 'Qiymət',
                "db" => "price",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'min' => 1, 'required' => 'required'],
            ],
            [
                "label" => 'Qiymət 30 AZN-dən  çoxdusa çadırılma qiyməti 0 AZN olsun',
                "db" => 'select',
                "type" => "checkbox",
                "value" =>1,
                "checked" => false,
                'attr' =>[]
            ],
        ];

        return $this->render($fields, $action, $data);
    }
}



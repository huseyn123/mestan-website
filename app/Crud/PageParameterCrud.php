<?php

namespace App\Crud;
use App\Logic\MultiLanguageSelect;
use App\Models\Page;

class PageParameterCrud extends RenderCrud
{

    public function fields($action, $data = false)
    {
        $fields = [
            1 => [
                "label" => "Template",
                "db" => "template_id",
                "type" => "select",
                "data" => config('config.template'),
                "selected" => 0,
                "attr" => ['class'=>'form-control'],
            ],
            10 => [
                "label" => "Şəkil",
                "db" => "cover",
                "type" => "file",
                "hide" => [1,2,4,5,6,7,8,9,11,12,13,20,14,21,22],
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){

                    if($data != false && $data->getFirstMedia('cover')){
                        if(substr($data->getFirstMedia('cover')->getFullUrl(), -3) == 'svg'){
                            $url = $data->getFirstMedia('cover')->getFullUrl();
                            $style = 'width:100px;height:100px';
                        }else{
                            $url = $data->getFirstMedia('cover')->getUrl('thumb');
                            $style="max-width:100%";
                        }
                        $img = '<div class="input-group"><img src="'.asset($url).'" style="'.$style.'"></div>';
                    }

                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="" required>
                    <div class="divImage" style="display:none">
                        <img class="showImage" src="#">
                    </div>';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                },
            ],
            7 => [
                "label" => "Görünüş",
                "db" => "visible",
                "type" => "select",
                "data" => config('config.menu-visibility'),
                "selected" => 1,
                "hide" => [14,21],
                "attr" => ['class'=>'form-control'],
            ],
            8 => [
                "label" => "Linkin quruluşu",
                "db" => "target",
                "type" => "select",
                "data" => config('config.menu-target'),
                "selected" => 1,
                "hide" => [14,21],
                "attr" => ['class'=>'form-control'],
            ]
        ];

        return $this->render($fields, $action, $data);
    }
}



<?php

namespace App\Crud;


use App\Models\Doctor;

class VideoCrud extends RenderCrud
{
    private function doctors()
    {
        $query = Doctor::orderBy("name", "asc")->pluck("name", "id")->prepend('Vias Clinic', 0);

        return $query;
    }


    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => "Həkim",
                "db" => "doctor_id",
                "type" => "select",
                "data" => $this->doctors(),
                "selected" => null,
                "attr" => ['class'=>'select-search form-control']
            ],
            [
                "label" => "Youtube Link",
                "db" => "youtube_link",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'required'],
            ],
            [
                "label" => "Şəkil",
                "db" => "image",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none', 'title' => 'Ölçü: 450x270px'],
                "design" => function($input, $data, $title = ' Əlavə et'){

                    if($data != false){
                        $img = '<div class="input-group"><img src="'.asset("storage/$data->image").'" style="max-width:100%"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="">';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                }
            ],
            [
                "label" => "Ana səhifədə görünsün",
                "db" => "selected",
                "type" => "checkbox",
                "value" => 1,
                "attr" => ['class'=>''],
                "checked" => false,
            ],
        ];

        return $this->render($fields, $action, $data);
    }

}



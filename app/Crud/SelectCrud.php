<?php

namespace App\Crud;

class SelectCrud extends RenderCrud
{

    private function selectProduct($data)
    {
        if($data != false && $data->product_id){
            $key = array_flip($data->product_id);
            $products = config('config.product_type');
            $result = array_flip(array_intersect_key($products,$key));

            return $result;
        }
        else{
            return null;
        }
    }


    private function selectForm($data)
    {
        if($data !== false){
            $key = array_flip($data->form_id);
            $forms = config('config.forms');
            $result = array_flip(array_intersect_key($forms,$key));

            return $result;
        }
        else{
            return null;
        }
    }


    public function fields($action, $data = false)
    {
        $fields = [
            [
                "label" => "Form",
                "db" => "form_id[]",
                "type" => "select",
                "data" => config('config.forms'),
                "selected" => $this->selectForm($data),
                "attr" => ['class'=>'form-control select-multiple', 'multiple' => 'multiple'],
            ],
            [
                "label" => "Məhsul",
                "db" => "product_id[]",
                "type" => "select",
                "data" => config('config.product_type'),
                "selected" => $this->selectProduct($data),
                "attr" => ['class'=>'form-control select-multiple', 'multiple' => 'multiple'],
            ]



        ];

        return $this->render($fields, $action, $data);
    }
}



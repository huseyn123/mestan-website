<?php

namespace App\Crud;

class BlockCrud extends RenderCrud
{
    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => "Başlıq",
                "db" => "title",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'required'],
            ],
            [
                "label" => "Təsvir",
                "db" => "desc",
                "type" => "textarea",
                "attr" => ['class'=>'form-control', 'rows' => 5],
            ],
        ];

        return $this->render($fields, $action, $data);
    }

}



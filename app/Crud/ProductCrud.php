<?php

namespace App\Crud;

use App\Logic\MultiLanguageSelect;
use App\Models\Page;

class ProductCrud extends RenderCrud
{
    private $route;
    public function __construct($route)
    {
        $this->route = $route;
    }

    private function category($lang)
    {
        $template_id = config('config.product_template.'.$this->route);
        if(is_null($lang )){

            $select = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
                ->whereIn('pages.template_id',[$template_id])
                ->whereNull('pt.deleted_at')
                ->select("pt.id", "pt.name", 'pt.lang')
                ->orderBy("pt.id", "asc")
                ->get();

            $query = MultiLanguageSelect::multiLang($select);

        }
        else{
            $query = Page::join('page_translations', 'page_translations.page_id', '=', 'pages.id')
                ->select('page_translations.id', 'page_translations.name', 'page_translations.lang')
                ->where('page_translations.lang', $lang)
                ->whereIn('pages.template_id',[$template_id])
                ->whereNull('page_translations.deleted_at')
                ->orderBy('page_translations.id', 'asc')
                ->pluck('page_translations.name', 'page_translations.id');
        }

        return $query;
    }

    private function index_category($lang)
    {
        if(is_null($lang )){

            $select = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
                ->whereIn('pages.template_id',[10])
                ->whereNull('pt.deleted_at')
                ->select("pt.id", "pt.name", 'pt.lang')
                ->orderBy("pt.name", "asc")
                ->get();

            $query = MultiLanguageSelect::multiLang($select, false, true);
        }
        else{
            $query = Page::join('page_translations', 'page_translations.page_id', '=', 'pages.id')
                ->select('page_translations.id', 'page_translations.name', 'page_translations.lang')
                ->where('page_translations.lang', $lang)
                ->whereIn('pages.template_id',[10])
                ->whereNull('page_translations.deleted_at')
                ->orderBy('page_translations.name', 'asc')
                ->pluck('page_translations.name', 'page_translations.id');

            $query->prepend('---', '');

        }

        return $query;
    }

    public function fields($lang, $action, $data = null,$route = null,$dublicate =false)
    {
        if($dublicate !=false && $route !=null){
            $this->route = $route;
        }

        $fields = [
            [
                "label" => 'Məhsulun adı',
                "db" => "name",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'required']
            ],
            [
                "label" => "Slug (URL)",
                "db" => "slug",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'autocomplete' => 'off', 'title'=>"Boş saxladığınız təqdirdə sluq avtomatik yaradılacaq."],
            ],
            [
                "label" => "Kateqoriya",
                "db" => "page_id",
                "type" => "select",
                "data" => $this->category($lang),
                "selected" => null,
                'divClass' =>'product_categories',
                "attr" => ['class'=>'form-control','id'=>'product_category','required']
            ],
            [
                "label" => "Ana Səhifə Kateqoriya",
                "db" => "cat_id",
                "type" => "select",
                "data" => $this->index_category($lang),
                "selected" => null,
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Qısa məzmun",
                "db" => "summary",
                "type" => "textarea",
                "attr" => ['class'=>'form-control', 'rows' => 5],
            ],
            [
                "label" => "Xüsusiyyətləri",
                "db" => "content",
                "type" => "textarea",
                "attr" => ['class'=>'form-control ckeditor', 'id' => 'editor'.$lang]
            ],
//            [
//                "label" => "Meta keywords",
//                "db" => "meta_keywords[]",
//                "type" => "select",
//                "data" => [],
//                "selected" => null,
//                "attr" => ['class'=>'form-control meta_keywords', 'id'  => 'meta_keywords'.$lang, 'multiple' => 'multiple', 'autocomplete' => 'off', 'title' => 'Keyword-ləri bir birindən ayırmaq üçün Enter düyməsini sıxın.'],
//            ],
        ];


        if(is_null($lang)){
            $paramFields = (new ProductParameterCrud())->fields('get',$data,$this->route,$dublicate);
            return $this->render(array_merge($fields, $paramFields), $action, $data);
        }
        else{
            return $this->render($fields, $action, $data);
        }
    }
}



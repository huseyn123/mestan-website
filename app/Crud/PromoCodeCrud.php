<?php

namespace App\Crud;

class PromoCodeCrud extends RenderCrud
{

    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => 'Ad',
                "db" => "name",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'required']
            ],
            [
                "label" => "Promo Kod",
                "db" => "promo_code",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'required'],
            ],
            [
                "label" => "Faiz",
                "db" => "percent",
                "type" => "number",
                "attr" => ['class'=>'form-control', 'required'],
            ],
             [
                "label" => "Başlanma tarixi",
                "db" => "start_date",
                "type" => "text",
                "attr" => ['class'=>'form-control datepicker', 'autocomplete' => 'off', 'required']
            ],
            [
                "label" => "Bitmə tarixi",
                "db" => "end_date",
                "type" => "text",
                "attr" => ['class'=>'form-control datepicker', 'autocomplete' => 'off', 'required']
            ],
        ];
        return $this->render($fields, $action, $data);
    }
}



<?php

namespace App\Crud;

class BannerCrud extends RenderCrud
{
    public function fields($action, $data = false)
    {
        $fields = [
            [
                "label" => "Başlıq",
                "db" => "title",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'required'],
            ],
            [
                "label" => "Bannerin növü",
                "db" => "type",
                "type" => "select",
                "data" => config('config.banner-type'),
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
            /*[
                "label" => "Device",
                "db" => "device",
                "type" => "select",
                "data" => config('config.banner-devices'),
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],*/
            [
                "label" => "Şəkil",
                "db" => "banner_url",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){
                    if($data != false && !is_null($data->banner_url)){
                        $img = '<div class="input-group"><img src="'.asset("storage/$data->banner_url").'" style="max-height:400px"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="" required>
                    <div class="divImage" style="display:none">
                        <img class="showImage" src="#">
                    </div>';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                },
            ],
            /*[
                "label" => "Iframe uzunluğu",
                "db" => "width",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'title' => 'px və ya %-lə yazın.'],
            ],
            [
                "label" => "Iframe hündürlüyü",
                "db" => "height",
                "type" => "number",
                "attr" => ['class'=>'form-control'],
            ],*/
            [
                "label" => "Link",
                "db" => "forward_url",
                "type" => "text",
                "attr" => ['class'=>'form-control'],
                "design" => function($input){
                    $group_btn = '<span class="input-group-addon">https://</span>';

                    return '<div class="input-group">'.$group_btn.$input.'</div>';
                }
            ],
             [
                "label" => "Linkin quruluşu",
                "db" => "target",
                "type" => "select",
                "data" => config('config.menu-target'),
                "selected" => 0,
                "attr" => ['class'=>'form-control'],
            ],
            [
                "label" => "Status",
                "db" => "status",
                "type" => "select",
                "data" => config('config.status'),
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
        ];

       return $this->render($fields, $action, $data);
    }
}



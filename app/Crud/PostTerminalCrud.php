<?php

namespace App\Crud;
use App\Logic\MultiLanguageSelect;

class PostTerminalCrud extends RenderCrud
{


    public function fields($lang, $action, $data = false)
    {
        $fields = [
            [
                "label" => 'Post Terminal',
                "db" => "name",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'required']
            ],
        ];

       return $this->render($fields, $action, $data);
    }
}



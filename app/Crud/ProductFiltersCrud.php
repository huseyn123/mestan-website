<?php

namespace App\Crud;
use App\Models\Page;

class ProductFiltersCrud extends RenderCrud
{

    private function selectCategory($data)
    {
        if($data != false){
             $categories =  Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
                ->where('pt.lang', 'az')
                ->whereNull('pt.deleted_at')
                ->whereIn('pages.id',$data->cat_ids);

                return $categories->pluck('pages.id')->toArray();        }
        else{
            return null;
        }
    }

    private function Category()
    {

        $parent_id = Page::join('page_translations as pt', 'pages.id', '=', 'pt.page_id')
            ->where('pt.lang', 'az')
            ->whereNull('pt.deleted_at')
            ->where('template_id',21)
            ->select('pt.id as tid')
            ->pluck('tid');

        $query = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
            ->whereIn('pt.parent_id',$parent_id)
            ->where('pt.lang', 'az')
            ->whereNull('pt.deleted_at')
            ->select("pages.id", "pt.name", 'pt.lang')
            ->orderBy("pt.id", "asc");

            $category = $query->pluck('pt.name', 'pages.id');


            return $category;
    }


    public function fields($action, $data = false)
    {
        $fields = [

            [
                "label" => "Kateqoriya",
                "db" => "cat_ids[]",
                "type" => "select",
                "data" =>  $this->Category(),
                "selected" => $this->selectCategory($data),
                "attr" => ['class'=>'form-control select-multiple', 'multiple' => 'multiple'],
            ],
            [
                "label" => 'Database Adı',
                "db" => "filter_column",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'required']
            ],
            [
                "label" => "Status",
                "db" => "status",
                "type" => "select",
                "data" => config('config.article-status'),
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
        ];

        return $this->render($fields, $action, $data);
    }
}



<?php

namespace App\Crud;
use App\Logic\MultiLanguageSelect;
use App\Models\Page;

class PageSubCrud extends RenderCrud
{
    private function category($lang)
    {
        dd('d');

        if(is_null($lang)){
            $select = Page::join('page_translations', 'page_translations.page_id', '=', 'pages.id')
                ->select('page_translations.id', 'page_translations.name', 'page_translations.lang')
                ->whereNull('page_translations.deleted_at')
                ->orderBy('page_translations.name', 'asc')
                ->get();

            $query = MultiLanguageSelect::multiLang($select, false, true);
        }
        else{
            $query = Page::join('page_translations', 'page_translations.page_id', '=', 'pages.id')
                ->select('page_translations.id', 'page_translations.name', 'page_translations.lang')
                ->whereNull('page_translations.deleted_at')
                ->where('page_translations.lang', $lang)
                ->orderBy('page_translations.name', 'asc')
                ->pluck('page_translations.name', 'page_translations.id');

            $query->prepend('---', '');
        }
        return $query;
    }


    public function fields($lang, $action, $data = false)
    {
        $fields = [

        ];

        if(is_null($lang)){
            $paramFields = (new PageParameterCrud())->fields('get');
            return $this->render(array_merge($fields, $paramFields), $action, $data);
        }
        else{
            return $this->render($fields, $action, $data);
        }
    }
}



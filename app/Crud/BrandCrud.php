<?php

namespace App\Crud;
use App\Logic\MultiLanguageSelect;

class BrandCrud extends RenderCrud
{


    public function fields($lang, $action, $data = false)
    {
        $fields = [
           [
                "label" => "Hərf",
                "db" => "letter_id",
                "type" => "select",
                "data" => config('config.letter_id'),
                "selected" => 0,
                "attr" => ['class'=>'form-control'],
            ],
            [
                "label" => 'Brend Adı',
                "db" => "title",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'required']
            ],
            [
                "label" => "Şəkil",
                "db" => "image",
                "type" => "file",
                "show" => true,
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){

                    if($data != false && $data->getFirstMedia()){
                        $img = '<div class="input-group"><img src="'.asset($data->getFirstMedia()->getUrl()).'"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="">';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                }
            ],
        ];

       return $this->render($fields, $action, $data);
    }
}



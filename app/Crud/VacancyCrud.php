<?php

namespace App\Crud;
use App\Logic\MultiLanguageSelect;
use App\Models\Vacancy;

class VacancyCrud extends RenderCrud
{


    public function fields($lang, $action, $data = false)
    {
        $fields = [
            [
                "label" => 'Vakansiyanın Adı',
                "db" => "title",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'required']
            ],
            [
                "label" => "Dil",
                "db" => "lang",
                "type" => "select",
                "data" => config('app.locales'),
                "selected" => array_first(config('app.locales')),
                "attr" => ['class'=>'form-control', 'required'],
                "divClass" => "language-form"
            ],
            [
                "label" => "İş Təcrübəsi",
                "db" => "work_experience",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'required'],
            ],
            [
                "label" => "Əmək Haqqı",
                "db" => "salary",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'required'],
            ],
            [
                "label" => "Bitmə Tarixi",
                "db" => "end_date",
                "type" => "text",
                "attr" => ['class'=>'form-control datepicker', 'autocomplete' => 'off', 'required']
            ],
            [
                "label" => "İş barədə məlumat",
                "db" => "content",
                "type" => "textarea",
                "attr" => ['class'=>'form-control ckeditor', 'id' => 'editor'.$lang]
            ],
        ];

       return $this->render($fields, $action, $data);
    }
}



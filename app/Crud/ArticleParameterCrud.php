<?php

namespace App\Crud;

class ArticleParameterCrud extends RenderCrud
{
    public function fields($action, $data = false)
    {
        $fields = [
            [
                "label" => "Şəkil",
                "db" => "image",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none', 'title' => 'Ölçü: 570x400'],
                "design" => function($input, $data, $title = ' Əlavə et'){
                    if($data != false){
                        $img = '<div class="input-group"><img src="'.asset("storage/thumb/$data->image").'" style="max-width:100%"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="" required>
                    <div class="divImage" style="display:none">
                        <img class="showImage" src="#">
                    </div>';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                },
            ],
            [
                "label" => "Tarix",
                "db" => "published_at",
                "type" => "text",
                "attr" => ['class'=>'form-control datepicker', 'autocomplete' => 'off', 'required']
            ],

            [
                "label" => 'Albom ID',
                "db" => "album_id",
                "type" => 'number',
                "create" => false,
                "attr" => ['class'=>'form-control', 'min' => 1],
            ],
            [
                "label" => "Status",
                "db" => "status",
                "type" => "select",
                "data" => config('config.article-status'),
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
        ];

        return $this->render($fields, $action, $data);
    }
}



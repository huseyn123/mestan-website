<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    protected $fillable = ['title', 'desc'];

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public static $rules = [
        'title' => 'required',
        'desc' => 'nullable'
    ];

    public static $messages = [
        'title.required' => 'Başlıq qeyd olunmayıb',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{

    protected $guarded = ['id'];
    protected $hidden = ['_token'];


    public static function rules()
    {
        return [
            'title' => 'required',
            'lang' => 'required',
            'work_experience' => 'required',
            'salary' => 'required',
            'end_date' => 'required',
            'content' => 'nullable',
        ];
    }

    public static $messages = [
        'title.required' => "Ad doldurulmayıb",
        'lang.required' => "Dil seçilməyib",
        'work_experience.required' => "İş Təcrübəsi doldurulmayıb",
        'salary.required' => "Əmək haqqı doldurulmayıb",
        'end_date.required' => "Bitmə tarixi seçilməyib",
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }
}

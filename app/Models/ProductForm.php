<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductForm extends Model
{
    public $timestamps = false;
    protected $guarded = ['id', '_token'];


    public static function whereInMultiple(array $columns, $values)
    {
        $values = array_map(function (array $value) {
            return "('".implode($value, "', '")."')";
        }, $values);

        return static::query()->whereRaw(
            '('.implode($columns, ', ').') in ('.implode($values, ', ').')'
        );
    }

//    public function setFormAttribute($value){
//        return json_encode($value);
//    }

//
//    public function getFormAttribute($value){
//        return json_decode($value,true);
//    }


}

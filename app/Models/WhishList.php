<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class WhishList extends Model
{
    protected $guarded = ['id',];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at'];


    public static $rules = [
        'product_id' => 'required'
    ];

    public static $messages = [

    ];

    public function scopeWhereCondition($query)
    {


        Auth::guard('web')->check() ? $userId = Auth::guard('web')->user()->id : $userId = null;

        return $query->where('whish_lists.user_id', $userId)->whereNotNull('whish_lists.user_id');

    }

}

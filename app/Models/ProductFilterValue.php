<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductFilterValue extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['_token'];


    public static function rules($required = 'required|')
    {
        return [
            'title_az' => 'required',
            'filter_id' => 'required',
        ];
    }

    public static $messages = [
        'title_az.required' => "Value  az doldurulmayıb",
        'filter_id.required' => "Filter Seçilməyib",
    ];


}

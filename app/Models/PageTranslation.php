<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PageTranslation extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden =  ['_token'];

    public static function rules($lang, $id){
        return [
            'name' => 'required|max:255',
            'parent_id' => 'nullable|exists:page_translations,id',
            'slug' => 'unique:page_translations,slug,'.$id.',id,lang,'.$lang,
            'content' => 'nullable',
            'forward_url' => 'nullable',
            'meta_description' => 'nullable|max:160',
            'meta_keywords' => 'nullable'
        ];
    }

    public static $messages = [
        'name.required' => "Ad qeyd olunmayıb.",
    ];

    public function setMetaKeywordsAttribute($value)
    {
        $keywords = null;

        if(is_array($value) && count($value) > 0)
        {
            $keywords = implode(",", $value);
        }

        $this->attributes['meta_keywords'] = $keywords;
    }

    public function relatedPages()
    {
        return $this->hasMany('App\Models\PageTranslation', 'page_id', 'page_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\PageTranslation', 'parent_id', 'id');
    }


    public function articles($paginate = false, $limit = 3)
    {
        $articles = $this->hasMany('App\Models\ArticleTranslation', 'page_id', 'id')
            ->join('articles', 'articles.id', '=', 'article_translations.article_id')
            ->select('article_translations.name', 'article_translations.slug', 'article_translations.summary', 'articles.image', 'articles.published_at')
            ->where('articles.status', 1)
            ->orderBy('articles.published_at', 'desc')
            ->orderBy('articles.id', 'desc');

        if($paginate == true){
            $articles->paginate($limit);
        }
        else{
            $articles->limit($limit);
        }

        return $articles;
    }
}

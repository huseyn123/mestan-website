<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    const limit = 30;

    protected $fillable = [
        'name', 'position', 'type'
    ];

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }
    public static $rules = [
        'name' => 'required|max:255'
    ];

    public static $messages = [
        'required' => 'Bütün xanaların doldurulması zəruridir!',
    ];


    public function gallery()
    {
        return $this->hasMany('App\Models\Gallery', 'album_id', 'id')
            ->orderBy('id', 'desc')
            ->limit(Album::limit);
    }

}

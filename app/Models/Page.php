<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\ProductTranslation;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Page extends Model implements HasMedia
{
    use HasMediaTrait,SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden =  ['_token'];
    protected $dates =   ['created_at', 'updated_at', 'deleted_at'];

    public static $width =1920;
    public static $height =300;
    public static $thumb_width =150;
    public static $thumb_height =150;

    public static $template_id =0;

    public static function rules($lang, $id,$template = null){

        self::$width =  config('config.page_size.'.$template.'.width') ?? '1920';
        self::$height =  config('config.page_size.'.$template.'.height') ?? '300';

        return [
            'template_id' => 'required|numeric',
            'name' => 'required|max:255',
            'parent_id' => 'nullable|exists:page_translations,id',
            'slug' => 'unique:page_translations,slug,'.$id.',id,lang,'.$lang,
            'cover' => 'max:10000|dimensions:min_width='.self::$width.',min_height='.self::$height,
            'content' => 'nullable',
            'forward_url' => 'nullable',
            'target' => 'boolean',
            'visible' => 'required',
            'album_id' => 'nullable|exists:albums,id',
            'meta_description' => 'nullable|max:160',
        ];
    }

    public static function parameterRules()
    {
        return [
            'template_id' => 'required|numeric',
            'cover' => 'max:10000|dimensions:min_width='.self::$width.',min_height='.self::$height,
            'target' => 'boolean',
            'visible' => 'required',
            'album_id' => 'nullable|exists:albums,id',
        ];
    }

    public static $messages = [
        'name.required' => "Ad qeyd olunmayıb.",
        'cover.dimensions' => 'Şəkilin ölçüsü :min_width x :min_height nisbətdə olmalıdır.',
    ];

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function setMetaKeywordsAttribute($value) {

        $keywords = null;

        if(!is_null($value) && is_array($value))
        {
            $keywords = implode(",", $value);
        }

        $this->attributes['meta_keywords'] = $keywords;
    }


    public function scopeVisible($query)
    {
        return $query->where('visible', '<>', 0);
    }

    public function scopeByPrice($query,$min_price,$max_price)
    {
        if($min_price > 0 && $max_price > 0 && $min_price <= $max_price){
            return $query->whereBetween('products.old_price', [$min_price, $max_price]);
        }

        return $query;
    }

    public function children()
    {
        return $this->hasMany('App\Models\PageTranslation', 'parent_id', 'tid')
            ->join('pages as p', 'p.id', '=', 'page_translations.page_id')
            ->where('p.visible', '<>', 0)
            ->whereNull('page_translations.deleted_at')
            ->select('page_translations.id', 'page_translations.name', 'page_translations.slug', 'page_translations.parent_id', 'p.template_id')
            ->orderBy('page_translations.order', 'asc');
    }


    public function menuChildren()
    {
        return $this->hasMany('App\Models\PageTranslation', 'parent_id', 'pt.id')->whereNull('page_translations.deleted_at')->where('page_translations.lang', app()->getLocale());
    }

    public function productCategories() //product categories count
    {
        return $this->belongsToMany(Product::class, 'product_categories')
            ->where('products.status', 1)
            ->where('products.price', '>', 0);
    }



    public function relatedPages()
    {
        return $this->hasMany('App\Models\PageTranslation', 'page_id', 'p_id')
            ->join('pages as p', 'p.id', '=', 'page_translations.page_id')
            ->select('p.id', 'page_translations.parent_id', 'page_translations.slug', 'page_translations.lang',  'page_translations.name', 'page_translations.id as pt_id');
    }

    public function relatedAdminPages()
    {
        return $this->hasMany('App\Models\PageTranslation', 'page_id', 'page_id');
    }


    public function parent()
    {
        return $this->hasOne('App\Models\PageTranslation', 'id', 'parent_id')
            ->whereNull('page_translations.deleted_at')
            ->select('page_translations.id','page_translations.name', 'page_translations.page_id');
    }

    public function products()
    {
        return $this->hasMany('App\Models\ProductTranslation', 'page_id', 'id')
            ->join('products as p', 'p.id', '=', 'product_translations.product_id')
            ->whereNull('product_translations.deleted_at')
            ->orderBy('order','asc')
            ->select('product_translations.name','product_translations.page_id','product_translations.product_id','product_translations.deleted_at','product_translations.id','product_translations.order');
    }

    public function gallery()
    {
        return $this->hasMany('App\Models\Gallery', 'album_id', 'album_id')
            ->orderBy('id', 'desc')
            ->limit(Album::limit);
    }


    public function articles($paginate = 9)
    {
        $articles = $this->hasMany('App\Models\ArticleTranslation', 'page_id', 'id')
            ->join('articles', 'articles.id', '=', 'article_translations.article_id')
            ->select('article_translations.name', 'article_translations.slug', 'article_translations.summary', 'articles.image', 'articles.published_at')
            ->where('articles.status', 1)
            ->orderBy('articles.published_at', 'desc')
            ->orderBy('articles.id', 'desc')
            ->paginate($paginate);

        return $articles;
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit('stretch',self::$thumb_width,self::$thumb_height)
            ->performOnCollections('cover')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',self::$width,self::$height)
            ->performOnCollections('cover')
            ->keepOriginalImageFormat();

    }

}
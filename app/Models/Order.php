<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at'];

    public static $rules = [
        'fullname' => 'required|max:40',
        'phone' => 'required',
        'delivery_price' => 'required',
        'address' => 'required|min:3',
        'paymentMethod' => 'required',
    ];

    public static $message = [
        'fullname.required' => 'Ad,Soyad qeyd olunmayıb',
        'fullname.max' => 'Ad,Soyad simvol uzunluğu 40 simvoldan artıq olmamalıdır',
        'delivery_price.required' => "Çatdırılma qiymətini seçin",
    ];



    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function carts()
    {
        return $this->hasMany(Cart::class, 'order_id', 'id');
    }

    public function customer()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function invoices()
    {
        return $this->hasMany(Cart::class, 'order_id', 'id')
            ->join('products', 'products.id', '=', 'carts.product_id')
            ->leftJoin('product_translations as pt', 'pt.product_id', '=', 'products.id')
            ->where('carts.purchased', 1)
            ->where('pt.lang', app()->getLocale())
            ->select('pt.name', 'carts.*', 'products.image');

        //return $this->hasMany(Cart::class, 'order_id', 'id')->where('purchased', 1);
    }

    public function postTerminal()
    {
        return $this->hasOne(PostTerminal::class, 'id', 'post_terminal_id');
    }


    public static $messages = [
        'user_name.required' => 'Xanalar boş saxlanılmamalıdır',
        'user_phone.required' => 'Xanalar boş saxlanılmamalıdır',
        'user_phone.numeric' => 'Xanalar boş saxlanılmamalıdır',

    ];
}

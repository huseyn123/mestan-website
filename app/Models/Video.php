<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at'];

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function setYoutubeLinkAttribute($value)
    {
        $this->attributes['youtube_link'] = getYoutubeId($value);
    }


    public function getYoutubeLinkAttribute($value)
    {
        return 'https://youtube.com/watch?v='.$value;
    }

    public static function rules($required){
        return [
            'youtube_link' => 'required',
            'image' => $required.'mimes:jpeg,jpg,png|max:10000|dimensions:min_width=450,min_height=270',
            'selected' => 'boolean'
        ];
    }

    public static $messages = [
        'youtube_link.required' => "Youtube link daxil edilməyib.",
        'image.required' => "Şəkil əlavə edilməyib.",
    ];


    public function image()
    {
        return $this->morphOne('App\Models\Gallery', 'gallery', 'gallery_type', 'gallery_id', 'relation')->where('main', 1);
    }

    public function relations()
    {
        return $this->morphMany('App\Models\Gallery', 'gallery');
    }


}

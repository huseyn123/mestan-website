<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArticleTranslation extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden =  ['_token'];

    public static function rules($id){
        return [
            'name' => 'required|max:255',
            'page_id' => 'required',
            'slug' => 'unique:article_translations,slug,'.$id.',id',
            'summary' => 'nullable',
            'content' => 'nullable',
        ];
    }

    public static $messages = [
        'name.required' => "Ad qeyd olunmayıb.",
        'page_id.required' => 'Kateqoriya seçilməyib',
    ];


    public function relatedPages()
    {
        return $this->hasMany('App\Models\ArticleTranslation', 'article_id', 'article_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Partner extends Model implements HasMedia

{
    use HasMediaTrait, SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden = ['_token'];


    public static function rules($required = 'required|')
    {
        return [
            'name' => 'nullable',
            'image' => $required . '|mimes:jpeg,jpg,png|max:10000',
            'site_url' => 'nullable',
        ];
    }

    public static $messages = [
        'image.required' => "Loqotip əlavə olunmayıb."
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }
}

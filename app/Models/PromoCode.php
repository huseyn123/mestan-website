<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PromoCode extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    public static function rules($id){
        return [
            'name' => "required",
            'promo_code' => 'required|unique:promo_codes,promo_code,'.$id,
            'percent' => "required|numeric",
            'start_date' => "required",
            'end_date' => "required",
        ];
    }

    public static $messages = [
        'name.required' => "Ad əlavə olunmayıb",
        'promo_code.required' => "Promo kod əlavə edilməyib",
        'promo_code.unique' => "Promo kod istifadə edilib",
        'percent.required' => "Faiz əlavə edilməyib",
        'percent.numeric' => "Faiz rəqəm olmalıdır",
        'start_date.required' => "Başlama tarixi əlavə edilməyib",
        'end_date.required' => "Bitmə tarixi əlavə edilməyib",
    ];

    public function user_promo()
    {
        return $this->hasOne('App\Models\UserPromo', 'promo_id', 'id');
    }


}

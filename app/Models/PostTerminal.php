<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostTerminal extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['_token'];


    public static $rules = [
        'name' => 'required'
    ];

    public static $messages = [
        'name.required' => "Ad qeyd olunmayıb",
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden = ['_token'];

    public static function rules($id){

        is_null($id) ? $img = 'required' : $img = 'sometimes';
        return [
            'name' => "required",
            'slug' => 'unique:article_translations,slug,'.$id,
            'image' => $img.'|mimes:jpeg,jpg,png|max:10000|dimensions:min_width=570,min_height=400',
            'page_id' => 'required|exists:page_translations,id',
            'published_at' => 'required|date',
            'status' => 'boolean',
            'featured' => 'boolean',
            'summary' => 'nullable',
            'content' => 'nullable',
        ];
    }

    public static $parameterRules = [
        'published_at' => 'required|date',
        'status' => 'boolean',
        'featured' => 'boolean',
        'album_id' => 'nullable|exists:albums,id',
        'image' => 'sometimes|mimes:jpeg,jpg,png|max:10000|dimensions:min_width=570,min_height=400'
    ];

    public static $messages = [
        'image.required_without' => "Şəkil əlavə olunmayıb",
        'page_id.required' => 'Kateqoriya seçilməyib'
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function scopeCondition($query, $keyword)
    {
        if($keyword == 1){
            return $query->where(function ($query) use ($keyword){
                $query->orWhere('articles.main_category_id', $keyword)
                    ->orWhere('articles.category_id', $keyword);
            });
        }
    }


    public function gallery()
    {
        return $this->hasMany('App\Models\Gallery', 'album_id', 'album_id')
            ->orderBy('id', 'desc')
            ->limit(Album::limit);
    }


    public function relatedPages()
    {
        return $this->hasMany('App\Models\ArticleTranslation', 'article_id', 'article_id');
    }
}


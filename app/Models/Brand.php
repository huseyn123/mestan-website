<?php

namespace App\Models;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model implements HasMedia
{
    use HasMediaTrait;
    protected $guarded = ['id'];
    protected $hidden = ['_token'];


    public static function rules($required = 'required|')
    {
        return [
            'title' => 'required',
            'image' => $required . '|mimes:jpeg,jpg,png|max:10000',
            'letter_id' => 'required',
        ];
    }



    public static $messages = [
        'title.required' => "Brend adı doldurulmayıb",
        'image.required' => "Loqotip əlavə olunmayıb.",
         'letter_id.required' => "Hərf seçilməyib."
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function scopeByProductType($query, $product_type)
    {
        if($product_type){
            return $query->where('products.product_type',$product_type);
        }

        return $query;
    }

    public function scopeByChildrenId($query, $childrenId)
    {
        if($childrenId){
            return $query->whereIn('prt.page_id',$childrenId);
            }

        return $query;
}


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function products()
    {
        return $this->hasMany(Product::class);
    }

}

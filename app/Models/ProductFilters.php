<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductFilters extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['_token'];

    protected $casts = [
        'cat_ids' => 'array',
    ];

    public static function rules($required = 'required|')
    {
        return [
            'title_az' => 'required',
            'cat_ids' => 'required',
            'filter_column' => 'required',
        ];
    }

    public static $messages = [
        'title_az.required' => "Filter Ad az doldurulmayıb",
        'cat_ids.required' => "Kateqoriya Seçilməyib",
        'filter_column.required' => "Databasa adı doldurulmayıb",
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }



    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function filter_value()
    {
        return $this->hasMany(ProductFilterValue::class, 'filter_id', 'id')->where('product_filter_values.status',1);
    }


}
 
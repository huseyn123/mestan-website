<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Auth;
use Db;

class Product extends Model implements HasMedia
{
    use HasMediaTrait, SoftDeletes;
    protected $casts = [
        'form' => 'json',
    ];

    protected $guarded = ['id'];
    protected $hidden = ['_token'];

    public static $width =220;
    public static $height =220;

    public static $gallery_width =500;
    public static $gallery_height =400;

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getBarCodeAttribute($value)
    {
        if(!empty($value)){
            return sprintf("%013d", $value);
        }
        else{
            return $value;
        }
    }

    public function getIndexFormAttribute($value)
    {
        $data = [];

        if(!is_null($value))
        {
            $data = explode(",", $value);
        }

        return $data;
    }

    public function setIndexFormAttribute($value) {

        $index_form = null;

        if($value && is_array($value))
        {
            $index_form = implode(",", $value);
        }

        $this->attributes['index_form'] = $index_form;
    }

    public static function rules($id){

        is_null($id) ? $img = 'required' : $img = 'sometimes';
        return [
            'name' => "required",
            'slug' => 'unique:product_translations,slug,'.$id,
            'page_id' => 'required',
            'image' => $img.'|mimes:jpeg,jpg,png|max:10000|dimensions:min_width='.self::$width.',min_height='.self::$height,
            'old_price' => 'required|numeric',
            'price' => 'nullable|numeric',
            'wholesale' => 'nullable|numeric',
            'product_type' => 'required',
            'bar_code' => 'required|numeric',
            'brand_id' => 'nullable|exists:brands,id',
            'status' => 'boolean',
        ];
    }

    public static function parameterRules($id){

        return [
            'status' => 'boolean',
            'bar_code' => 'required|numeric',
            'image' => 'sometimes|mimes:jpeg,jpg,png|max:10000|dimensions:min_width='.self::$width.',min_height='.self::$height,
            'old_price' => 'required|numeric',
            'price' => 'nullable|numeric',
            'wholesale' => 'nullable|numeric',
            'product_type' => 'required',
        ];
    }

    public static $messages = [
        'name.required' => 'Ad qeyd olunmayıb',
        'image.required' => "Şəkil əlavə olunmayıb",
        'image.required_without' => "Şəkil əlavə olunmayıb",
        'page_id.required' => 'Kateqoriya seçilməyib',
        'bar_code.required' => 'Barkod qeyd olunmayıb',
        'bar_code.unique' => 'Barkod başqa məhsulda istifadə olunub',
        'old_price.required' => 'Qiymət qeyd olunmayıb',
        'old_price.numeric' => 'Qiymət rəqəm olmalıdır',
        'price.numeric' => 'Yeni qiymət rəqəm olmalıdır',
        'wholesale.numeric' => 'Topdan qiymət rəqəm olmalıdır',
    ];

    public function scopeBySort($query,$sort)
    {
        $sort_type = config('config.product_filtr_order_type')[$sort] ?? 0;
        if($sort_type && $sort_type !==0){
            $query->orderBy('products.old_price', $sort_type);
        }else{
            $query->orderBy('ptr.created_at','desc');
        }
    }


    public function scopeByForm($query, $product_form)
    {
        if($product_form){
           foreach ($product_form as $key=>$array){
               $sum = 0;
               if($array){
                       $query->where(function ($query) use($array,$key,$sum){
                       foreach ($array as $key2=>$value2){
                           $sum ++;
                           if($sum == 1){
                               $query->whereJsonContains('products.form', [$key => $value2]);
                           }else{
                               $query->OrwhereJsonContains('products.form', [$key => $value2]);
                           }
                       }

                   });

               }
      

           }
       }
    }

    public function scopeByForm2($query)
    {
        $query->OrwhereJsonContains('products.form', ['age' => "1"]);
        $query->OrwhereJsonContains('products.form', ['age' => "4"]);
    }

    public function scopeByCategory($query, $value)
    {
        if(!is_null($value)){
            if(is_array($value)){
                return $query->whereIn('pc.page_id', $value);
            }
            else{
                return $query->where('pc.page_id', $value);
            }
        }

        return $query;
    }


    public function scopeByColor($query, $value)
    {
        if($value > 0){
            return $query->where('products.color_id', $value);
        }

        return $query;
    }


    public function scopeByBrands($query, $brand_id)
    {
        if(isset($brand_id) && count($brand_id)){
            return $query->whereIn('products.brand_id', $brand_id);
        }

        return $query;
    }


    public function scopeByPrice($query, $minPrice, $maxPrice)
    {
        if($minPrice >= 0 && $maxPrice >= 0 & $minPrice <= $maxPrice){
            return $query->whereBetween('products.old_price', [$minPrice, $maxPrice]);
        }
    }


    /*public function productColors() //for datatable
    {
        return $this->belongsToMany(Color::class, 'product_colors', 'product_id', 'color_id', 'p_id');
    }*/

    public function productTags() //for datatable
    {
        return $this->belongsToMany(Tag::class, 'product_tags', 'product_id', 'tag_id', 'id');
    }

    public function productTagsSearch()
    {
        return $this->belongsToMany(Tag::class, 'product_tags', 'product_id', 'tag_id', 'id');
    }


    public function tags($id = 'id')
    {
        return $this->hasMany(ProductTag::class, 'product_id', $id);
    }

    public function categories($id = 'p_id')
    {
        return $this->hasMany(ProductCategory::class, 'product_id', $id);
    }

    public function relatedPages()
    {
        return $this->hasMany('App\Models\ProductTranslation', 'product_id', 'product_id');
    }


    public function basket()
    {
        return $this->hasOne('App\Models\Cart', 'product_id', 'id')->whereCondition()->select('carts.product_id','carts.count');
    }

    public function whish_list()
    {
        return $this->hasOne('App\Models\WhishList', 'product_id', 'id')->whereCondition()->select('whish_lists.product_id');
    }



    public function scopeWhereCondition($query,$cookie = null)
    {

        $cookie = request()->cookie('cartCookie') ?? $cookie;

        Auth::guard('web')->check() ? $userId = Auth::guard('web')->user()->id : $userId = null;

        if(is_null($userId)){
            return $query->where('carts.cookie', $cookie)->whereNotNull('carts.cookie')->where('carts.purchased', 0);
        }
        else{
            return $query->where('carts.purchased', 0)
                ->where(function ($query) use($userId, $cookie) {
                    $query->where('carts.user_id', $userId)
                        ->orWhere('carts.cookie', $cookie);
                });
        }
    }



    public function gallery()
    {
        return $this->hasMany('App\Models\Gallery', 'album_id', 'album_id')
            ->orderBy('id', 'asc')
            ->limit(Album::limit);
    }

    public function registerMediaConversions(Media $media = null)
    {

        $this->addMediaConversion('search')
            ->fit('stretch',40,40)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('basket')
            ->fit('stretch',64,64)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',self::$width,self::$height)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('thumb')
            ->fit('stretch',250,200)
            ->performOnCollections('gallery')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',self::$gallery_width,self::$gallery_height)
            ->performOnCollections('gallery')
            ->keepOriginalImageFormat();

    }

}

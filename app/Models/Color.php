<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['_token'];


    public static function rules($required = 'required|')
    {
        return [
            'name_az' => 'required',
        ];
    }

    public static $messages = [
        'name_az.required' => "Rəng adı doldurulmayıb",
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }
}

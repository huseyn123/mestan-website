<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $guarded =    ['id'];
    protected $hidden =     ['_token'];
    protected $dates = ['created_at', 'updated_at'];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public static $rules = [
        'file' => 'required|mimes:png,gif,jpeg,jpg,bmp|max:10000'
    ];

    public static $messages = [
        'file.required' =>  "Şəkil əlavə olunmayıb",
        'file.mimes' => 'Şəkil :values formatda yüklənməlidir',
    ];
}

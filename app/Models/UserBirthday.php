<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class UserBirthday extends Model implements HasMedia
{
	use HasMediaTrait;
	
    public $timestamps = false;
    protected $guarded = ['id', '_token'];


    public static function rules($id =null){
        is_null($id) ? $img = 'required' : $img = 'sometimes';

        return [
            'name' => "required",
            'surname' => "required",
            'b_day' => "required|date",
            'image' => $img.'|mimes:jpeg,jpg,png|max:5120|dimensions:max_width=3000,max_height=3000',
        ];
    }

    public static $messages = [
        'surname.required' => 'Soyad boş saxlanılmamalıdır',
        'b_day.required' => 'Tarix boş saxlanılmamalıdır',
        'b_day.date' => 'Tarix düz deyil',
        'image.required' => 'Şəkil əlavə edilməyib',
        'image.mimes' => 'Şəkil jpeg,jpg,png formatında yüklənməlidir',
        'image.dimensions' => 'Şəkil olçüsü :max_widthx:max_height nisbətindən böyük olmamalıdır',
        'image.max' => 'Şəkilin həcmi 5 meqabaytdan artıq olmamalıdır.',
    ];

}
 
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['title', 'text', 'type'];

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public static $rules = [
        'title' => 'required',
        'text' => 'required'
    ];

    public static $messages = [
        'required' => 'Bütün xanaların doldurulması zəruridir!',
    ];
}

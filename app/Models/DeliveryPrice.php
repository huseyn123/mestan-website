<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryPrice extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['_token'];


    public static function rules($required = 'required|')
    {
        return [
            'address_az' => 'required',
            'price' => 'required|numeric',
        ];
    }

    public static $messages = [
        'address_az.required' => "Ünvan az doldurulmayıb",
        'price.required' => "Qiymət  doldurulmayıb",
        'price.numeric' => 'Qiymət rəqəm olmalıdır',
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }



    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Banner extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at'];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public static $rules = [
        'title' => 'required',
        'banner_url' => 'sometimes|mimes:jpeg,jpg,png,gif|max:10000|dimensions:min_width=220',
        //'device' => 'required',
        'link' => 'nullable',
        'width' => 'nullable',
        'height' => 'nullable',
        'status' => 'boolean',
        'type' => 'required'
    ];


    public function scopeStatus($query)
    {
        return $query->where('status', 1);
    }


    public function scopeSelectData($query)
    {
        return $query->select('id', 'banner_url', 'type', 'device', 'width', 'height', 'forward_url','target');
    }


    public function scopeUnions($query, $group)
    {
        foreach ($group as $gr){
            $query->unionAll($gr);
        }

        return $query;
    }


    public static function get($bannersArray = null)
    {

        if(is_null($bannersArray)){
            $bannersArray = [
                1 =>  [1, 2, 3],
                2 =>  [1, 2, 3],
            ];
        }

        $bannerQueries = [];
        $bannerData = [];
        $bannerId = [];

        $bannerDevice = 1;

        foreach ($bannersArray[$bannerDevice]  as $bannerType){
            $bannerQueries[] = Banner::where('type', $bannerType)
                ->where('device', $bannerDevice)
                ->status()
                ->orderBy('viewed', 'asc')
                ->selectData()
                ->limit(1);
        }

        $banners = Banner::where('type', 1)
            ->where('device', $bannerDevice)
            ->status()
            ->selectData()
            ->orderBy('viewed', 'asc')
            ->limit(1)
            ->unions($bannerQueries)
            ->get()
            ->toArray();


        foreach($banners as $banner)
        {
            $bannerData[$banner['type']] = $banner;
            $bannerId[] = $banner['id'];
        }

        if(!empty($bannerId)){
            Banner::whereIn('id', array_unique($bannerId))
                ->update(['viewed' => DB::raw('viewed + 1'), 'public_viewed' => DB::raw('public_viewed + 1')]);
        }

        return $bannerData;
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductTranslation extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden =  ['_token'];

    public static function rules($id){
        return [
            'name' => 'required|max:255',
            'slug' => 'unique:product_translations,slug,'.$id,
            'summary' => 'nullable',
            'content' => 'nullable',
            'meta_description' => 'nullable',
            'meta_keywords' => 'nullable',
        ];
    }

    public static $messages = [
        'name.required' => "Ad qeyd olunmayıb.",
    ];

    public function setMetaKeywordsAttribute($value)
    {
        $keywords = null;

        if(is_array($value) && count($value) > 0)
        {
            $keywords = implode(",", $value);
        }

        $this->attributes['meta_keywords'] = $keywords;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Cart extends Model
{
    protected $guarded = ['id', 'purchased'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at'];


    public static $rules = [
        'count' => 'required|numeric|min:1',
        'product' => 'required'
    ];

    public static $messages = [

    ];


    public function getBarCodeAttribute($value)
    {
        if(!empty($value)){
            return sprintf("%013d", $value);
        }
        else{
            return $value;
        }
    }


    public function scopeWhereCondition($query)
    {

        $cookie = request()->cookie('cartCookie');

        Auth::guard('web')->check() ? $userId = Auth::guard('web')->user()->id : $userId = null;

        if(is_null($userId)){
            return $query->where('carts.cookie', $cookie)->whereNotNull('carts.cookie')->where('carts.purchased', 0);
        }
        else{
            return $query->where('carts.purchased', 0)
                ->where(function ($query) use($userId, $cookie) {
                    $query->where('carts.user_id', $userId)
                        ->orWhere('carts.cookie', $cookie);
                });
        }
    }


    public function scopeGetProducts($query)  //get products by only cookie, if user authenticated
    {
        return $query->where('carts.purchased', 0)->where('carts.cookie', request()->cookie('cartCookie'))->whereNotNull('carts.cookie');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Slider extends Model implements HasMedia
{
    use HasMediaTrait, SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    public static $width =1920;
    public static $height =430;

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public static function rules($required,$template_id){
        if($template_id == 1){self::$width = 390;self::$height = 260;}
        return [
            'title' => 'nullable',
            'image' => $required.'mimes:jpeg,jpg,png|max:10000|dimensions:min_width='.self::$width.',min_height='.self::$height,
        ];
    }

    public static $messages = [
        'image.required' => "Şəkil əlavə edilməyib.",
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit('stretch',300,68)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',self::$width,self::$height)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();

    }
}

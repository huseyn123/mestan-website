<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Select extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['_token'];


    public static function rules($required = 'required|')
    {
        return [
            'title_az' => 'required',
            'title_en' => 'required',
            'title_ru' => 'required',
        ];
    }

    public static $messages = [
        'title_az.required' => "Ad az doldurulmayıb",
        'title_en.required' => "Ad en doldurulmayıb",
        'title_ru.required' => "Ad ru doldurulmayıb",
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }



    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function setProductIdAttribute($value) {

        $products = null;

        if(!is_null($value) && is_array($value))
        {
            $products = implode(",", $value);
        }

        $this->attributes['product_id'] = $products;
    }

    public function setFormIdAttribute($value) {

        $forms = null;

        if(!is_null($value) && is_array($value))
        {
            $forms = implode(",", $value);
        }

        $this->attributes['form_id'] = $forms;
    }
    
    public function getFormIdAttribute($value)
    {
        $data = [];

        if(!is_null($value))
        {
            $data = explode(",", $value);
        }

        return $data;
    }

    public function getProductIdAttribute($value)
    {
        $data = [];

        if(!is_null($value))
        {
            $data = explode(",", $value);
        }

        return $data;
    }




}

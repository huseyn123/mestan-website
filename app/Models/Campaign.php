<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $fillable = ['image', 'url'];
    protected $hidden = ['_token'];

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public static function rules($required){
        return [
            'image' => $required.'mimes:jpeg,jpg,png|max:10000|dimensions:min_width=345,min_height=330',
            'url' => 'nullable'
        ];
    }

    public static $messages = [
        'image.required' => "Şəkil əlavə edilməyib."
    ];
}

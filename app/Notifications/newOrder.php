<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class newOrder extends Notification
{
    use Queueable;

    protected $order,$payment_type;

    public function __construct($order,$payment_type,$end_price)
    {
        $this->order = $order;
        $this->payment_type = $payment_type;
        $this->end_price = $end_price;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $text = "Sifariş № {$this->order->id}<br>";

        $text.= "Transaction ID: {$this->order->payment_key}<br>";

        $text.= "Müştəri: {$this->order->user_name}<br>";
        $text.= "Əlaqə nömrəsi: {$this->order->user_phone}<br>";
        $text.= "Çatdırılma ünvanı: {$this->order->address}<br>";
        $text.= "Məbləğ: {$this->order->amount} AZN<br>";
        $text.= "Çatdırılma qiyməti: {$this->order->delivery_price} AZN<br>";
        if(isset($this->order->special_percent)){
          $text.= "Endirim : {$this->order->special_percent} %<br>";
        }
        $text.= "Cəmi: {$this->end_price} AZN<br>";
        $text.= "Ödəniş Növü: {$this->payment_type} <br>";
        $text.= $this->order->user_type == 2 ? 'Status:Xisusi İstifadəçi' : '';



        return (new MailMessage)
            ->subject('Yeni sifariş daxil oldu')
            ->greeting('Yeni sifariş daxil oldu')
            ->line($text)
            ->action('Sifarişə bax', url('admin/invoices', $this->order->id))
            ->salutation('');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

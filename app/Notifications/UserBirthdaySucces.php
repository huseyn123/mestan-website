<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserBirthdaySucces extends Notification
{
    use Queueable;

    public $beetwen;
    protected $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($beetwen, $user)
    {
        $this->beetwen = $beetwen;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $text = "Artıq siz Mestan.az saytından 5 il müddətində hər il ".$this->beetwen." tarix aralığındakı bütün sifarişlərdə 15% endirim əldə edəcəksiniz";

        return (new MailMessage)
            ->subject('Mestan.az saytından ad günü müraciətiniz qeydə alındı')
            ->greeting('Salam Mestan.az saytından ad günü müraciətiniz qeydə alındı')
            ->line($text)
            ->salutation(' ');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

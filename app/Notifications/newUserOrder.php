<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class newUserOrder extends Notification
{
    use Queueable;

    protected $order;
    protected $user;
    protected $payment_type;

    public function __construct($order, $user,$payment_type,$end_price)
    {
        $this->order = $order;
        $this->user = $user;
        $this->payment_type = $payment_type;
        $this->end_price = $end_price;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $text = "Sifariş № {$this->order->id}<br>";

        $text.= "Transaction ID: {$this->order->payment_key}<br>";

        $text.= "Əlaqə nömrəsi: {$this->order->phone}<br>";
        $text.= "Çatdırılma ünvanı: {$this->order->address}<br>";
        $text.= "Məbləğ: {$this->order->amount} AZN<br>";
        if(isset($this->order->special_percent)){
          $text.= "Endirim : {$this->order->special_percent} %<br>";
        }
        $text.= "Çatdırılma qiyməti: {$this->order->delivery_price} AZN<br>";
        $text.= "Cəmi: {$this->end_price} AZN<br>";
        $text.= "Ödəniş Növü: {$this->payment_type} <br>";


        return (new MailMessage)
            ->subject('Mestan.az saytından sifarişiniz qeydə alındı')
            ->greeting('Salam '.$this->user->name.', <br>Mestan.az saytından alış-veriş etdiyiniz üçün təşəkkür edirik! Aşağıda sifarişiniz haqqındada ətraflı məlumat əldə edə bilərsiniz.')
            ->line($text)
            ->action('Sifarişə bax', route('order.list', $this->order->payment_key))
            ->salutation('Yenidən görüşmək ümidi ilə...');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

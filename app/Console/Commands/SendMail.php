<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Dictionary;
use App\User;
use App\Mail\RegUserForm;
use App\Models\Config;
use App\Exceptions\DataTableException;
use Mail;
use DB;
use Batch;
use Illuminate\Support\Facades\Artisan;



class SendMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Əməliyyat uğurla tamamlandı';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $check_sending_email = Config::where('key','email_start')->select('value')->first()->value;
        $users= User::where('users.type',1)->orderBy('id','DESC');
        if($check_sending_email != 0){
            if($users->count() <= $check_sending_email){

                //Update config gmail setup && add product email end count------------------
                $config_model = new Config;
                $all_data = [];
              
                array_push($all_data,    [
                    'key' => 'email_start',
                    'value' => 0,
                ]);
                $index = 'key';
                Batch::update($config_model, $all_data, $index);
                //-------------------------------------------------------------

                Artisan::call('cache:clear');
                Artisan::call('config:clear');

            }else{
                $subject = Config::where('key','email_start')->select('config_path')->first()->config_path;
                $content = Dictionary::where([['keyword','multi_email_content'],['lang_id','az']])->select('content')->first()->content;
                $limit = $check_sending_email + 50;
                $users= User::where('users.type',1)->orderBy('id','DESC')->skip($check_sending_email)->take($limit)->pluck('email');

                try {
                    Mail::to('info@mestan.az')->send(new RegUserForm(['content' =>$content],'reg_user',$subject,$users));
                } catch (\Exception $e) {
                    throw new DataTableException($e->getMessage());
                }
                Config::where('key','email_start')->update(['value' => $limit]);
            }

        }
//        else{
//
////               Dictionary::where([['keyword','multi_email_content'],['lang_id','en']])->update(['content' => 'Qaqalar']);
//
//
//        }

    }
}

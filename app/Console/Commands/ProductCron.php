<?php

namespace App\Console\Commands;

use App\Models\Config;
use Illuminate\Console\Command;
use App\Models\Product;
use Batch;
use SoapClient;

class ProductCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'All Products update order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $page_c = Config::where('key','count_page')->first();


        $key='#ROVA45@+Z=+f6Zc-gFvcMqm!3*9drVr5KgW+egRP!?xNxMWPt';
        $soapclient = new \SoapClient('http://api.3333047.az/MainWebService.asmx?wsdl',array(
            'trace' => 1,
            'exceptions' => true,
            'cache_wsdl' => WSDL_CACHE_NONE,
            /*'proxy_host' => "35.197.216.59", // Not working on hostgator withour proxy
            'proxy_port' => "80"*/
        ));


        // Count total
        $param_c = array('Key' => $key);
        $response_c = $soapclient->Get_NovAllCount($param_c);
        $total_xml = new \SimpleXMLElement($response_c->Get_NovAllCountResult);
        $total_d = json_decode(json_encode($total_xml), TRUE);
        $total = ceil(($total_d['row']['say'] + 0)/1000);

        $offset = 1;
        $limit = 1000;
        if ($page_c->value) {
            $offset = $page_c->value;
            if ($offset > $total) {
                $offset = 1;
            }
        }

    try {

        $params = array('offset' => $offset,'limit' => $limit,'Key' => $key);
        $response = $soapclient->Get_NovAll($params);

        $xml = new \SimpleXMLElement($response->Get_NovAllResult);
        $results = json_decode(json_encode($xml), TRUE);
        $all_data = [];

        foreach ($results['row'] as $key => $r) {
            array_push($all_data,    [
                'bar_code' => $r['seri'],
                'count' => $r['qaliq'],
            ]);}


        $product_model = new Product;
        $index = 'bar_code';
        Batch::update($product_model, $all_data, $index);
        Config::where('key','count_page')->update(['value'=>$offset+1]);

        echo '1000 product update successfully';
    } catch (\Exception $e) {
    echo $e->getMessage();
    exit;
    }



}
}

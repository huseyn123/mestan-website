<?php

namespace App\Logic;

use App\Models\Brand;
use App\Models\Color;
use App\Models\Config;
use App\Models\Page;
use App\Models\Product;

trait ProductsList
{

    public function productsList($orderType = 0)
    {
        $user_type = auth()->guard('web')->user() ? auth()->guard('web')->user()->type  : null;

        $products = Product::join('product_translations as ptr','ptr.product_id','products.id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->leftJoin('brands', 'brands.id', '=', 'products.brand_id')
            ->whereNull('ptr.deleted_at')
            ->whereNull('pt.deleted_at')
            ->where('ptr.lang',$this->lang)
            ->where('products.status',1)
            ->whereIn('pages.template_id',[4,5,6,22]);

            if($user_type ==2){
                $products->where('products.user_type',1)
                 ->select('pages.template_id','ptr.*','pt.slug as page_slug'
                    ,'pt.id as tid','products.id'
                     ,'products.brand_id'
                     ,'brands.title as brand_title'
                     ,'products.wholesale as price',
                     'products.old_price'
                     ,'products.percent'
                     , 'products.count'
                    ,'products.product_type'
                    ,'products.index_banner'
                    ,'products.youtube_link'
                    ,'products.bar_code','products.index_form','products.form','products.user_type');
            }else{
                $products->select('pages.template_id','ptr.*','pt.slug as page_slug'
                    ,'pt.id as tid','products.id'
                    ,'products.brand_id'
                    ,'brands.title as brand_title'
                    ,'products.old_price'
                    ,'products.percent'
                    ,'products.product_type'
                    , 'products.count'
                    ,'products.index_banner'
                    ,'products.youtube_link'
                    ,'products.bar_code','products.index_form','products.form');

                if($this->dActive->value == 1){
                    $products->addSelect('products.price');
                }

            }



        $products->with(['basket','media','whish_list']);

//        if($orderType == 1){
//            $products->orderBy('products.search_count', 'desc');
//        }
//        elseif($orderType == 2){
//            $products->orderBy('products.id', 'asc');
//        }
//        elseif(request()->has('sort')){
//            if(request()->get('sort') == 2){
//                $products->orderBy('products.id', 'asc');
//            }
//            elseif(request()->get('sort') == 3){
//                $products->orderBy('products.price', 'asc');
//            }
//            elseif(request()->get('sort') == 4){
//                $products->orderBy('products.price', 'desc');
//            }
//            else{
//                $products->orderBy('products.id', 'desc');
//            }
//        }
//        else{
//            $products->orderBy('products.id', 'desc');
//        }


        return $products;
    }

    public function productCategory($data)
    {
        return Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
            ->whereIn('pt.parent_id', $data)
            ->whereNull('pt.deleted_at')
            ->select('pt.name', 'pt.slug', 'pages.id')
            ->withCount('productCategories')
            ->orderBy('pt.name', 'asc')
            ->get();
    }


    public function productColors($page)
    {
        return Color::join('products', 'products.color_id', '=', 'colors.id')
            ->join('product_translations as prt', 'products.id', '=', 'prt.product_id')
            ->join('product_categories as pc', 'pc.product_id', '=', 'products.id')
            ->join('page_translations as pt', 'pt.page_id', '=', 'pc.page_id')
            ->whereIn('pc.page_id', $page)
            ->whereNull('pt.deleted_at')
            ->whereNull('prt.deleted_at')
            ->where('products.status', 1)
            ->where('products.price', '>', 0)
            ->pluck('colors.name_'.$this->lang.' as name', 'colors.id')
            ->prepend('---', '');
    }


    public function productBrands($product_type = null,$childrenId  = null)
    {
        return Brand::join('products', 'products.brand_id', '=', 'brands.id')
            ->join('product_translations as prt', 'products.id', '=', 'prt.product_id')
            ->join('page_translations as pt', 'pt.id', '=', 'prt.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->whereNull('prt.deleted_at')
            ->whereNull('pt.deleted_at')
            ->where('prt.lang',$this->lang)
            ->where('products.status',1)
            ->whereIn('pages.template_id',[4,5,6,22])
            ->ByProductType($product_type)
            ->ByChildrenId($childrenId)
            ->pluck('brands.title', 'brands.id');
    }

}

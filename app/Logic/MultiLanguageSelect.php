<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/28/17
 * Time: 22:20
 */

namespace App\Logic;

class MultiLanguageSelect
{
    public static function multiLang($query, $empty = false, $prepend = false)
    {

        if($prepend == true){
            $optgroup = [' ' => '---'];
        }
        else{
            $optgroup = [];
        }

        foreach($query as $data)
        {
            $lang = config("app.localesFull.".$data->lang);
            $optgroup[$lang][$data->id] = $data->name;
        }

        return $optgroup;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/28/17
 * Time: 22:20
 */

namespace App\Logic;

use App\Models\Brand;
use App\Models\Color;
use App\Models\Config;
use App\Models\Dictionary;
use App\Models\Slider;
use App\Models\Partner;
use App\Models\Product;
use DB;
use Cache;

class WebCache
{
    use ProductsList;

    public function getDictionary($lang)
    {
        $collect = [];

        $dictionary = Cache::rememberForever("dictionary_$lang", function () use($collect, $lang)
        {
            return Dictionary::where('lang_id', $lang)->select('keyword', 'content')->orderBy('keyword', 'asc')->pluck('content', 'keyword')->toArray();
        });

        return $dictionary;
    }



    public function getSlider($lang)
    {
        $slider = Cache::rememberForever("slider_$lang", function () use($lang){
            return Slider::with('media')->where('lang',$lang)->where('template_id',0)->orderBy('order', 'desc')->limit(10)->get();
        });

        return $slider;
    }


    public static function getBrand()
    {
        $brand = Cache::rememberForever("brand", function () {
            return Brand::orderBy('title', 'asc')->pluck('title', 'id')->prepend('---', '');
        });

        return $brand;
    }


    public static function getColor($lang)
    {
        $color = Cache::rememberForever("color", function () use($lang) {
            return Color::select("name_$lang as name", 'id')->orderBy("name_$lang", 'asc')->pluck('name', 'id')->prepend('---', '');
        });

        return $color;
    }

    public function getPartner()
    {
        $partners = Cache::rememberForever("partners", function () {
            return Partner::with('media')->orderBy('order', 'desc')->limit(40)->get();
        });

        return $partners;
    }

    public static function config()
    {
        $config = Cache::rememberForever('config', function () {
               return Config::all();
        });

        return $config;
    }
}
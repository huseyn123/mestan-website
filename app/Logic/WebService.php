<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/28/17
 * Time: 22:20
 */

namespace App\Logic;

use SoapClient;

class WebService
{
    public function getDataByBarCode($barcode)
    {
        try {
            $client = $this->soap();

            $parameters = [
                'seri' => $barcode,
                'Key'  => '#ROVA45@+Z=+f6Zc-gFvcMqm!3*9drVr5KgW+egRP!?xNxMWPt'
            ];

            $result = $client->Get_NovSeri($parameters);

            $xml = new \SimpleXMLElement($result->Get_NovSeriResult);

            $decode = json_decode(json_encode($xml), TRUE);

            if(isset($decode['row']) && !empty($decode['row'])){
                return $decode['row'];
            }
            else{
                return $decode['ErrorMessage'];
            }

        }

        catch(\Exception $e) {
            return $e->getMessage();
        }
    }



    public function updateAllProducts()
    {
        try {
            $client = $this->soap();

            $result = $client->YenilenmeFullMehsullarXML();

            $decode = json_decode(json_encode($result->YenilenmeFullMehsullarXMLResult), TRUE);

            if(!empty($decode['ItemsList'])){
                return $decode['ItemsList']['uitems'];
            }
            else{
                return $decode['ErrorMessage'];
            }
        }

        catch(\Exception $e) {
            return $e->getMessage();
        }
    }


    public function updateNeededProducts()
    {
        try {
            $client = $this->soap();

            $result = $client->YenilenmeliMehsullarXML();

            $decode = json_decode(json_encode($result->YenilenmeliMehsullarXMLResult), TRUE);

            if(!empty($decode['ItemsList'])){
                return $decode['ItemsList']['uitems'];
            }
            else{
                return $decode['ErrorMessage'];
            }
        }

        catch(\Exception $e) {
            return $e->getMessage();
        }
    }


    public function newOrder($data)
    {
        try {
            $client = $this->soap();

            return $client->YeniSifaris($data);
        }

        catch(\Exception $e) {
            return $e->getMessage();
        }
    }


    private function soap()
    {
     
        $opts = array(
            'http' => array(
            'user_agent' => 'PHPSoapClient'
            )
        );
        $context = stream_context_create($opts);
        $wsdlUrl = 'http://api.3333047.az/MainWebService.asmx?wsdl';
        $soapClientOptions = array(
            'stream_context' => $context,
            'cache_wsdl' => WSDL_CACHE_NONE
        );
        return new SoapClient($wsdlUrl, $soapClientOptions);
    }


    /*private function parse($data)
    {
        $newData = [];
        $newKeys = ['Logicalref' => 'logical_ref', 'Code' => 'bar_code', 'ItemStatus' => 'status', 'UnitCode' => 'unit_code', 'Price' => 'price', 'OldPrice' => 'old_price', 'Onhand' => 'on_hand'];

        foreach ($data as $k => $d){

            if(is_array($d)){
                foreach ($d as $k2 => $d2){
                    if(array_key_exists($k2, $newKeys)){
                        $newData[$k][$newKeys[$k2]] = $d2;
                    }
                }
            }
            else{
                if(array_key_exists($k, $newKeys)){
                    $newData[$newKeys[$k]] = $d;
                }
            }

        }

        return $newData;
    }*/


}
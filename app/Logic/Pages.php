<?php

namespace App\Logic;
use App\Models\Article;
use App\Models\Banner;
use App\Models\Block;
use App\Models\Brand;
use App\Models\Cart;
use App\Models\Page;
use App\Models\PageTranslation;
use App\Models\Product;
use App\Models\ProductFilters;
use App\Models\Question;
use App\Models\Select;
use App\Models\Vacancy;
use DB;

trait Pages
{
    use ProductsList;

    protected function about($getPage, $relatedPages)
    {
        $best_seller = $this->productsList()
            ->where('products.best_seller',1)
            ->get();

        return view("web.about", ['page' => $getPage, 'pageTitle' => $getPage->name,'best_seller' =>$best_seller]);
    }



    protected function store_page($getPage, $relatedPages)
    {
        $best_seller = $this->productsList()
            ->where('products.best_seller',1)
            ->get();

        return view("web.store_page", ['page' => $getPage, 'pageTitle' => $getPage->name,'best_seller' =>$best_seller]);
    }

    protected function partners($getPage, $relatedPages)
    {
        $best_seller = $this->productsList()
            ->where('products.best_seller',1)
            ->get();

        return view("web.partners", ['page' => $getPage, 'pageTitle' => $getPage->name,'best_seller' =>$best_seller]);
    }

    protected function brands($getPage, $relatedPages)
    {
        $best_seller = $this->productsList()
            ->where('products.best_seller',1)
            ->get();

        $brands =Brand::withcount('products')->with('media')->orderBy('letter_id','asc')->get();
        return view("web.brands", ['page' => $getPage, 'pageTitle' => $getPage->name,'brands' =>$brands,'best_seller' =>$best_seller]);
    }

    protected function staticPage($getPage, $relatedPages)
    {
        $cat = $this->menu->getData($getPage, 0);

        $page = $this->getChildrenPage($getPage);

        return view("web.static", ['page' => $page, 'cat' => $cat, 'children' => $cat->children, 'relatedPages' => $relatedPages, 'banner' => Banner::get()]);
    }

    protected function news($getPage, $relatedPages)
    {
        $cat = $this->menu->getData($getPage, 0);

        $articles = $getPage->articles();

        if (request()->ajax()) {

            $view = view('web.elements.news-grid', ['page' => $getPage, 'articles' => $articles] )->render();
            return response()->json(['html'=>$view]);
        }

        return view("web.news", ['articles' => $articles, 'page' => $getPage, 'cat' => $cat]);
    }

    protected function articleSingle($checkPages, $slug)
    {
        $category = $checkPages->first();
        $cat = $this->menu->getData($category, 0);

        $getArticle = Article::join('article_translations as at', 'articles.id', '=', 'at.article_id')
            ->where('at.slug', $slug)
            ->whereNull('at.deleted_at')
            ->firstOrFail();

        $topArticles = $this->lastArticles($getArticle->id);

        $relatedPages = $this->menu->relatedPages($checkPages->last(), $getArticle);

        return view('wesib.article-single', ['article' => $getArticle, 'cat' => $cat, 'page' => $category, 'topArticles' => $topArticles, 'relatedPages' => $relatedPages]);
    }

    protected function products($getPage, $relatedPages,$product_types)
    {
        $cat = $this->menu->getData($getPage, 0);
        $selected_brands = request()->get('brand');
        $paginate = request()->get('page') ?? config('config.filtr_default_other_parametr.page');
        $sort = request()->get('sort') ?? config('config.filtr_default_other_parametr.sort');
        $limit = request()->get('limit') ?? config('config.filtr_default_other_parametr.limit');


        $brands = $this->productBrands($product_types,$this->getChildrensId($getPage));
        
        $page_type = array_search($getPage->template_id, config('config.product_template'));

        if($getPage->children->count() > 0){
            $cat_id = $getPage->id;
        }else{
            $cat_id = $getPage->parent->page_id ?? null;
        }

        // Product Filter
     
        settype($cat_id, "string");

        $category_filters = ProductFilters::where('status',1)
            ->whereJsonContains('cat_ids', $cat_id)
            ->select('id','title_'.$this->lang.' as title','filter_column','cat_ids')
             ->with(['filter_value' => function ($query) {
                $query->select('id','title_'.$this->lang.' as title','filter_id','status');
            }])->get();



        $product_form = $this->getProductForm($category_filters->pluck('filter_column'));

        // filtr_parametr
        $min_price = request()->get('min') ?? config('config.filtr_default_price.min');
        $max_price = request()->get('max') ?? config('config.filtr_default_price.max');
        $select_brands = request()->get('brand') ?? [];

        $products = $this->productsList()
            ->where('products.product_type',$product_types)
            ->whereIn('ptr.page_id', $this->getChildrensId($getPage))
            ->BySort($sort)
            ->ByPrice($min_price,$max_price)
            ->ByBrands($selected_brands)
            ->ByForm($product_form)
            // ->GroupBy('products.id')
            // ->orderBy('ptr.created_at','desc')
            ->paginate($limit);

        if(request()->ajax())
        {
            header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            $view = view('web.elements.filtr_product_ajax',['products' => $products])->render();
            return response()->json(['html'=>$view]);
        }


        return view("web.products", ['products' => $products, 'page' => $getPage, 'cat' => $cat, 'relatedPages' => $relatedPages,'page_type' => $page_type,'product_form' =>$product_form,'category_filters'=>$category_filters,'brands' =>$brands,'selected_brands' => $selected_brands,'min_price'=>$min_price,'max_price'=>$max_price,'paginate' => $paginate,'sort' =>$sort,'limit'=> $limit]);
    }


    protected function index_category($getPage, $relatedPages)
    {
        $cat = $this->menu->getData($getPage, 0);
        $selected_brands = request()->get('brand');
        $paginate = request()->get('page') ?? config('config.filtr_default_other_parametr.page');
        $sort = request()->get('sort') ?? config('config.filtr_default_other_parametr.sort');
        $limit = request()->get('limit') ?? config('config.filtr_default_other_parametr.limit');

        $brands = $this->productBrands();

        // Product Filter
         $category_filters = ProductFilters::where('status',1)
        ->select('id','title_'.$this->lang.' as title','filter_column','cat_ids')
         ->with(['filter_value' => function ($query) {
            $query->select('id','title_'.$this->lang.' as title','filter_id','status');
        }])->get();


        $product_form = $this->getProductForm($category_filters->pluck('filter_column'));
        

        // filtr_parametr
        $min_price = request()->get('min') ?? config('config.filtr_default_price.min');
        $max_price = request()->get('max') ?? config('config.filtr_default_price.max');
        $select_brands = request()->get('brand') ?? [];

        $selects = Select::select('id','title_'.$this->lang.' as title','form_id','product_id')->get();

        $products = $this->productsList()
            ->where('ptr.cat_id',$getPage->tid)
            ->BySort($sort)
            ->ByPrice($min_price,$max_price)
            ->ByBrands($selected_brands)
            ->ByForm(array_filter($product_form))
            // ->GroupBy('products.id')
            ->paginate($limit);


        if(request()->ajax())
        {
            header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            $view = view('web.elements.filtr_product_ajax',['products' => $products,'product_form' =>$product_form])->render();
            return response()->json(['html'=>$view]);
        }


        return view("web.products", ['products' => $products, 'page' => $getPage, 'cat' => $cat, 'relatedPages' => $relatedPages,'product_form' =>$product_form,'category_filters'=>$category_filters,'selects' => $selects,'brands' =>$brands,'selected_brands' => $selected_brands,'min_price'=>$min_price,'max_price'=>$max_price,'paginate' => $paginate,'sort' =>$sort,'limit'=> $limit]);
    }

    protected function discountedProducts()
    {
        $selected_brands = request()->get('brand');
        $paginate = request()->get('page') ?? config('config.filtr_default_other_parametr.page');
        $sort = request()->get('sort') ?? config('config.filtr_default_other_parametr.sort');
        $limit = request()->get('limit') ?? config('config.filtr_default_other_parametr.limit');

        $brands = Brand::join('products', 'products.brand_id', '=', 'brands.id')
            ->join('product_translations as prt', 'products.id', '=', 'prt.product_id')
            ->join('page_translations as pt', 'pt.id', '=', 'prt.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->whereNull('prt.deleted_at')
            ->whereNull('pt.deleted_at')
            ->where('prt.lang',$this->lang)
            ->where('products.status',1)
            ->whereIn('pages.template_id',[4,5,6])
            ->pluck('brands.title', 'brands.id');



       $category_filters = ProductFilters::where('status',1)
        ->select('id','title_'.$this->lang.' as title','filter_column','cat_ids')
         ->with(['filter_value' => function ($query) {
            $query->select('id','title_'.$this->lang.' as title','filter_id','status');
        }])->get();

        $product_form = $this->getProductForm($category_filters->pluck('filter_column'));
        

        $min_price = request()->get('min') ?? config('config.filtr_default_price.min');
        $max_price = request()->get('max') ?? config('config.filtr_default_price.max');
        $select_brands = request()->get('brand') ?? [];

        $selects = Select::select('id','title_'.$this->lang.' as title','form_id','product_id')->get();

        if($this->dActive->value == 1){
            $products = $this->productsList()
                ->where('products.price','>',0)
                ->BySort($sort)
                ->ByPrice($min_price,$max_price)
                ->ByBrands($selected_brands)
                ->ByForm(array_filter($product_form))
                // ->GroupBy('products.id')
                ->paginate($limit);
        }else{
            $products =$this->productsList()
                ->where('products.price','<',0)
                ->paginate(1);
        }

        if(request()->ajax())
        {
            header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            $view = view('web.elements.filtr_product_ajax',['products' => $products,'product_form' =>$product_form])->render();
            return response()->json(['html'=>$view]);
        }


        return view("web.discounted_web", ['products' => $products,'category_filters'=>$category_filters,'product_form' =>$product_form,'selects' => $selects,'brands' =>$brands,'selected_brands' => $selected_brands,'min_price'=>$min_price,'max_price'=>$max_price,'paginate' => $paginate,'sort' =>$sort,'limit'=> $limit]);
    }

    protected function productSingle($checkPages, $slug)
    {
        $category = $checkPages->first();
        $getProduct =$this->productsList()->where('ptr.slug',$slug)->firstOrFail();

        $similarProducts = $this->productsList()
            ->where('products.id', '<>', $getProduct->id)
            ->where('pages.template_id',$getProduct->template_id)
            ->where('ptr.page_id',$getProduct->page_id)
            ->inRandomOrder()
            ->get();

        $relatedPages = $this->menu->relatedPages($checkPages->last(), $getProduct);

        return view('web.product-single', ['product' => $getProduct,'page' => $category, 'similarProducts' => $similarProducts,'relatedPages' => $relatedPages]);
    }

    protected function giftCards($getPage, $relatedPages)
    {
        $cat = $this->menu->getData($getPage, 0);

        $children = PageTranslation::where('parent_id', $getPage->id)->orWhere('id', $getPage->id)->pluck('page_id')->toArray();
        $cards = $this->productsList()->whereIn('pc.page_id', $children)->paginate(15);

        return view("web.gift-cards", ['cards' => $cards, 'page' => $getPage, 'cat' => $cat, 'relatedPages' => $relatedPages]);
    }

    protected function contact($getPage, $relatedPages)
    {
        $best_seller = $this->productsList()
            ->where('products.best_seller',1)
            ->get();

        return view("web.contact", ['page' => $getPage, 'pageTitle' => $getPage->name,'best_seller' =>$best_seller]);
    }

    protected function privace($getPage, $relatedPages)
    {
        $best_seller = $this->productsList()
            ->where('products.best_seller',1)
            ->get();

        return view("web.privace", ['page' => $getPage, 'pageTitle' => $getPage->name,'best_seller' =>$best_seller]);
    }

    protected function users_rule($getPage, $relatedPages)
    {
        $best_seller = $this->productsList()
            ->where('products.best_seller',1)
            ->get();

        return view("web.users_rule", ['page' => $getPage, 'pageTitle' => $getPage->name,'best_seller' =>$best_seller]);
    }


    protected function getProductList($type){

        if (request()->ajax()) {
            $lastProductsArray = [];

            $lastProducts = $this->productsList()
                ->paginate(8);

            foreach ($lastProducts as $lp){
                $lastProductsArray[] = $lp->id;
            }

            $anotherProducts = $this->productsList(2)
                ->whereNotIn('products.id', $lastProductsArray)
                ->paginate(8);


            if($type == 'anotherProducts'){
               $view = view('web.elements.anotherProducts', ['anotherProducts' => $anotherProducts] )->render();
               $main_products = $anotherProducts;
            }else{
                $view = view('web.elements.lastProducts', ['lastProducts' => $lastProducts] )->render();
                 $main_products = $lastProducts;
            }

            return response()->json(['html'=>$view,'data' => $main_products]);
        }
    }

    protected function faq($getPage, $relatedPages, $type)
    {
        $cat = $this->menu->getData($getPage, 0);

        $faq = Question::where('type', $type)->orderBy('id', 'desc')->limit(50)->get();

        return view("web.faq", ['page' => $getPage, 'faq' => $faq, 'cat' => $cat]);
    }

    protected function rules($getPage, $relatedPages, $type)
    {
        $cat = $this->menu->getData($getPage, 0);

        $faq = Question::where('type', $type)->orderBy('id', 'desc')->limit(50)->get();

        return view("web.rules", ['page' => $getPage, 'faq' => $faq, 'cat' => $cat]);
    }

    protected function getPageByTemplateId($templateId)
    {
        $page = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
            ->where('pt.lang', $this->lang)
            ->where('pages.template_id', $templateId)
            ->whereNull('pt.deleted_at')
            ->select('pt.slug', 'pt.name', 'pt.id')
            ->first();

        return $page;
    }

    private function getChildrensId($page)
    {
        $id = [];

        if(!$page->children->count() > 0){
            array_push($id,$page->tid);
        }else{
            $childrens = $page->children;
            foreach ($childrens as $children){
                array_push($id,$children->id);
            }
        }
        return $id;
    }

    private function lastArticles($id = 0)
    {
        $articles = Article::join('article_translations as at', 'articles.id', '=', 'at.article_id')
            ->join('page_translations as pt', 'pt.id', '=', 'at.page_id')
            ->select('at.name', 'at.slug as article_slug', 'pt.slug')
            ->where('articles.id', '<>', $id)
            ->where('at.lang', $this->lang)
            ->where('articles.status', 1)
            ->whereNull('pt.deleted_at')
            ->whereNull('at.deleted_at')
            ->orderBy('articles.published_at', 'desc')
            ->orderBy('articles.id', 'desc')
            ->limit(6)
            ->get();

        return $articles;
    }

    private function getProductForm($filtrs){
        $data = [];
        foreach ($filtrs as $value){
            if(request()->get($value)){
                $data[$value] = request()->get($value);
            }else{
                $data[$value] =null;
            }
        }
        return $data;
    }
}

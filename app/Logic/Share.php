<?php

namespace App\Logic;
use App\Models\Product;
use DB;


trait Share
{
    public $lang, $config, $menu;

    protected function loadData()
    {
        $webCache = new WebCache;
        $menu = new Menu;

        $this->lang = app()->getLocale();
        $this->dictionary = $webCache->getDictionary($this->lang);

        $this->menu = $menu;
        $this->config = getConfig();
        view()->share('menu', $menu->all());
        view()->share('footerMenu', []);
        view()->share('config', $this->config);
        view()->share('dictionary', $webCache->getDictionary($this->lang));
        view()->share('dictionary',$this->dictionary);
        view()->share('sliders', $webCache->getSlider($this->lang));
        view()->share('partners', $webCache->getPartner());
        view()->share('lang', $this->lang);
    }
}

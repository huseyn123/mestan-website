<?php

namespace App\Logic;

use App\Models\Faq;
use App\Exceptions\DataTableException;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

trait NoCrud
{

    public function create()
    {
        return view('admin.'.$this->view.'.create', ['route' => $this->route.'.store','title' => $this->title,'create_name' =>$this->create_name,'colums' =>$this->columns]);

    }

    public function store(Request $request)
    {

        // check Validation
        $validation = Validator::make($request->all(),  $this->model::rules(),$this->model::$messages);
        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }



        DB::beginTransaction();

        try{
            $this->model::create($this->requests);
        }
        catch(\Exception $e){
            DB::rollback();
            throw new DataTableException($e->getMessage());
        }

        DB::commit();


        return $this->responseSuccess();
    }

    public function edit($id)
    {
        $data = $this->model::findOrFail($id);

        return view('admin.'.$this->view.'.edit', ['title' => 'Düzəliş et','data'=>$data,'route' => [$this->route.'.update', $id],'colums' =>$this->columns]);
    }

    public function update(Request $request, $id)
    {
        $data = $this->model::findOrFail($id);

        // check Validation
        $validation = Validator::make($request->all(),  $this->model::rules(),$this->model::$messages);

        if($validation->fails()){
            throw new DataTableException($validation->errors()->first());
        }


        foreach($this->requests as $key => $put){
            $data->$key = $put;
        }

        $data->save();

        return $this->responseSuccess();
    }

    public function destroy($id)
    {
        $data = $this->model::findOrFail($id);
        $data->forceDelete();

        return $this->responseSuccess("", "", "#modal-confirm");
    }


    private function responseSuccess($msg = "", $draw = "#xxx", $close = "#myModal")
    {
        $draw ='#'.$this->route;
        $response = ["code" => 200, "msg" => $msg, "draw" => $draw, "close" => $close];

        return response()->json($response, $response['code']);
    }


}

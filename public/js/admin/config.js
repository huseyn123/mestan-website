var loadingIcon = "<i class='fa fa-circle-o-notch fa-spin text-center'></i>";
var requestSent = false;

$(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.datepicker').datetimepicker({
        format: 'YYYY-MM-DD',
        useCurrent:false
    });

    $('.datetimepicker').datetimepicker({
        format: 'DD/MM/YYYY HH:mm',
        stepping : 30,
        minDate: moment().add(1, 'days'),
        locale: 'az',
        showClose: true
    });

    $('#startDate').datetimepicker({
        locale: 'az',
        format: 'DD/MM/YYYY',
        maxDate: moment().subtract(7, 'days'),
        showClose: true
    });

    $('#endDate').datetimepicker({
        defaultDate: moment(),
        locale: 'az',
        format: 'DD/MM/YYYY',
        maxDate: moment(),
        showClose: true
    });


    $('#startDate').datetimepicker().on('dp.change', function (e) {
        $('#endDate').data('DateTimePicker').minDate(moment(new Date(e.date)));
        $(this).data("DateTimePicker").hide();
    });

    $('#endDate').datetimepicker().on('dp.change', function (e) {
        if($('#endDate').val() != ''){
            $('#startDate').data('DateTimePicker').maxDate(moment(new Date(e.date)));
        }
        $(this).data("DateTimePicker").hide();
    });

    $('.select-search').select2();

    $(".ajax-select").each(function(){
        var $this = $(this);

        $this.select2({
            placeholder: $this.data('placeholder'),
            minimumInputLength: $this.data('mininput'),
            allowClear:$this.data('clear'),
            ajax: {
                url: $this.data('url'),
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            }
        });
    }).on(
        'select2:close',
        function () {
            $(this).focus();
        }
    );

    $(".select-multiple").each(function(){
        var $this = $(this);

        $this.select2({
            placeholder: $this.data('placeholder'),
            tags: true,
            maximumSelectionLength: 1000,

            createTag: function(params) {
                if($this.data('create') && $this.data('create') == 1){
                    return {
                        id: params.term,
                        text: params.term,
                        isNew : true
                    };
                }
                else{
                    return undefined;
                }
            }
        });
    }).on("change", function(e) {
        var isNew = $(this).find('[data-select2-tag="true"]');
        if(isNew.length){
            isNew.replaceWith('<option selected value="'+isNew.val()+'">'+isNew.val()+'</option>');
        }
    }).on(
        'select2:close',
        function () {
            $(this).focus();
        }
    );
});


$(document).ready( function() {

    $("#template_id").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            if(optionValue){
                $(".form-class").not(".hh" + optionValue).show();
                $(".hh" + optionValue).hide();
            } else{
                $(".form-class").hide();
            }
        });
    }).change();


    $("#parent_id").change(function(){

        if($('#parent_id option:selected').val() == 0){
            $(".language-form").show();
            //$(".cover_photo").show();
        } else{
            $(".language-form").hide();
           //$(".cover_photo").hide();
        }

    }).change();


    $(':file').on('fileselect', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if( input.length ) {
            input.val(log);
        }
    });


    $(':file').on('change',function(e){

        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);

    });
});


$.fn.select2.defaults.set( "theme", "bootstrap" );

$('#modal-confirm').on('show.bs.modal', function(e) {
    $(this).find('.warning-modal').attr('action', $(e.relatedTarget).attr('data-action'));
});


$('#myModal').on('hide.bs.modal', function (e) {
    if (typeof(CKEDITOR) != "undefined"){

        for ( instance in CKEDITOR.instances ) {
            CKEDITOR.instances[instance].destroy();
        }
    }
});



function datatableAction(form){

    $.ajax({
        url:  form.action,
        type: 'PUT',
        timeout: 20000,
        headers: { 'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') },
        dataType: "json",

        beforeSend: function(response) {
            $("#loadingButton").button('loading');
        }
    })
    .done(function(response) {
        requestSent = false;
        $("#loadingButton").button('reset');

        if(typeof response.draw =='object') {
            $(response.draw[0]).DataTable().draw( false );
            $(response.draw[1]).DataTable().draw( false );
        }
        else{
            $(response.draw).DataTable().draw( false );
        }

        toastr.success(response.msg || 'Əməliyyat uğurlu alındı.');
    })
    .fail(function(xhr, ajaxOptions, thrownError) {
        requestSent = false;

        var json = JSON.parse(xhr.responseText);
        $(".loadingButton").button('reset');
        toastr.error(json.msg);
    });
}


$(document).ready(function(){

    //modal dialog
    $("body").on('click', '.open-modal-dialog', function (e){
        e.preventDefault();
        if(requestSent) {return;}
        modal($(this), $(this).attr('data-link'), "#modal-body");
    });

    function modal(data, route, body) {
        requestSent = true;

        $('#myModal').modal('show');

        if(data.attr('data-large') == true){
            $('.modal-dialog').addClass("modal-lg");
        }
        else{
            $('.modal-dialog').removeClass( "modal-lg" );
        }

        $.ajax({
            url: route,
            type: "GET",
            timeout: 20000,
            beforeSend: function() {
                $(body).html(loadingIcon);
            }
        })
        .done(function(data) {
            requestSent = false;
            $(body).html(data);
        })
        .fail(function(xhr, ajaxOptions, thrownError) {
            requestSent = false;
            var json = JSON.parse(xhr.responseText);
            $(".loadingButton").button('reset');
            $('#myModal').modal("toggle");
            toastr.error(json.msg);
        });
    }

    //for pages
    $(".actionPage").on("submit",function (event){

        event.preventDefault();
        actionPage($(this));
    });

    function actionPage(action){

        for ( instance in CKEDITOR.instances ) {
            CKEDITOR.instances[instance].updateElement();
        }

        $.ajax({
            url:  action.attr('action'),
            type: action.attr('method'),
            timeout: 20000,
            headers: { 'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') },
            //data: action.serialize(),
            data: new FormData(action[0]),
            dataType: "json",
            processData: false,
            contentType: false,

            beforeSend: function() {
                $(".loadingButton").button('loading');
            }
        })
        .done(function(response) {
            requestSent = false;
            $(".loadingButton").button('reset');

            $(response.draw).DataTable().draw( false );

            if(response.close)
            {
                $(response.close).modal("toggle");
            }

            toastr.success(response.msg || 'Əməliyyat uğurlu alındı');
        })
        .fail(function(xhr, ajaxOptions, thrownError, response) {
            requestSent = false;

            var json = JSON.parse(xhr.responseText);
            $(".loadingButton").button('reset');
            toastr.error(json.msg);
        });
    }


    $("#dtForm").on("submit",function (event){

        var loading = '.loadingButton';

        event.preventDefault();
        if(requestSent) {return;}
        datatableForm($(this), loading);
    });

    $(".dtForm").on("submit",function (event){
        var loading = '.loadingButton';

        event.preventDefault();
        if(requestSent) {return;}
        datatableForm($(this), loading);
    });

    function datatableForm(action, loading){

        requestSent = true;

        if (typeof(CKEDITOR) != "undefined"){

            for ( instance in CKEDITOR.instances ) {
                CKEDITOR.instances[instance].updateElement();
            }
        }

        $.ajax({
            url:  action.attr('action'),
            type: action.attr('method'),
            timeout: 20000,
            headers: { 'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') },
            //data: action.serialize(),
            data: new FormData(action[0]),
            dataType: "json",
            processData: false,
            contentType: false,

            beforeSend: function(response) {
                $(loading).button('loading');
            }
        })
        .done(function(response) {
            requestSent = false;
            if(response.draw) {

                if(typeof response.draw =='object') {
                    $(response.draw[0]).DataTable().draw( false );
                    $(response.draw[1]).DataTable().draw( false );
                }
                else{
                    $(response.draw).DataTable().draw( false );
                }
            }

            if(response.close)
            {
                if(typeof response.close =='object') {

                    $(response.close[0]).modal("toggle");
                    $(response.close[1]).modal("toggle");
                }
                else{
                    $(response.close).modal("toggle");
                }
            }

            $(".loadingButton").button('reset');

            toastr.success(response.msg || 'Əməliyyat uğurlu alındı');
        })
        .fail(function(xhr, ajaxOptions, thrownError) {
            requestSent = false;

            var json = JSON.parse(xhr.responseText);
            $(".loadingButton").button('reset');
            toastr.error(json.msg);
        });
    }

    // Product Category Change
    $("body").on("change", "#product_category", function() {
        var obj = $(this)
        $p_type = $('#product_type').val();
        $page_id = $(this).val();

        $.ajax({
            url: '/admin/product/category/filter',
            type: 'POST',
            dataType: 'json',
            data: {
                route: $p_type,
                page_id: $page_id
            },
            error: function(e) {
                // $(".box-suggestions").removeClass('on-progress');
            },
            success: function(resp) {
                var parent = $('.product_categories').parent();
                $( ".product_filtrs" ).remove();
                var html = resp.html;
                parent.append(html);
                $('.filtr_list').find('input[type=hidden]').val(JSON.stringify(resp.filter_list));
            }
        });


    });

});
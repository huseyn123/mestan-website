(function ($, DataTable) {
    "use strict";

    DataTable.ext.buttons.activeDiscount = {
        className: 'activeDiscount-modal',
        text: function (dt) {
            return '<i class="fa fa-certificate"></i> ' + dt.i18n('buttons', 'Endirimi Aktiv et');
        }

        /*action: function (e, dt, button, config) {
            window.location = window.location.href.replace(/\/+$/, "") + '/create';
        }*/
    };
})(jQuery, jQuery.fn.dataTable);
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('brand_id')->nullable();
            $table->boolean('status')->index()->unsigned();
            $table->string('image', 255);
            $table->unsignedBigInteger('bar_code')->unique();
            $table->unsignedInteger('search_count')->default(0);
            $table->float('price', 6,2)->nullable();
            $table->float('old_price', 6,2)->default(0);
            $table->float('wholesale', 6,2)->nullable();
            $table->string('published_by', 40);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

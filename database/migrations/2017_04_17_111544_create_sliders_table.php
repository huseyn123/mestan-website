<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->increments('id');
             $table->unsignedInteger('template_id')->nullable()->default(0);
            $table->unsignedInteger('page_id')->nullable();
            $table->string('title', 255)->nullable();
            $table->string('title2', 255)->nullable();
            $table->string('summary', 255)->nullable();
            $table->string('lang', 2);
            $table->unsignedInteger('order')->default(1);
            $table->text('link')->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
    }
} 

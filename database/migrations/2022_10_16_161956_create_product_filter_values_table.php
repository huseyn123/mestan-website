<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductFilterValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */ 
    public function up()
    {
        Schema::create('product_filter_values', function (Blueprint $table) {
            $table->id();
            $table->string('title_az',50);
            $table->string('title_en',50)->nullable();
            $table->string('title_ru',50)->nullable();
            $table->unsignedInteger('filter_id');
            $table->boolean('status')->index()->unsigned();
            $table->timestamps();


            $table->foreign('filter_id')->references('id')->on('product_filters')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_filter_values');
    }
}

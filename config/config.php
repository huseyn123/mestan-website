<?php

return [
    'birthday_percent' => 15,
    'status' => ['Gizli', 'Aktiv'],
    'alert' => ['danger', 'success'],
    "slider-type" => [1 => "Slider", "Kompaniya"],
    "article-status" => ["Qaralama", "Dərc olunub"],
    "label" => ["danger", "success"],

    "paymentMethod" => ['card' => "card_text",'cash'=> "order_type_2"],
    "paymentMethod_text" => ['card' => "Card",'cash'=> "Nağd ödəniş"],

    //product filtr default min && max price
    "filtr_default_price" => ['min' => 0,"max" => 1000],
    "filtr_default_other_parametr" => ['page' => 1,'sort' => 0,'limit' =>24],
    "produc_stock_count" =>['single' => 10, 'crud' => 2],

    "pages_lang" => ['az' => 'AZE','ru' => 'RUS'],

    "menu-target" => [1 => "Self", 0 => "Blank"],
    'letter_id' =>[ 1 => 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'],
    "menu-visibility" => [
        0 => "Hidden",
        1=> "Kataloq",
        2=> "Kataloq and Footer",
        3=> "Header and Footer",
        4=> "Header",
        5=> "Footer",
        6=> "Top Menu",
        7=> "Top Menu and Footer",
        8=> "Ana Səhifə Slider",
        9=> "Ana səhifə Kart",
    ],

    "menu-visibility-boolean" => [0 => "Gizli", "Aktiv"],
    "menu-visibility-label" => [0 => "danger", "success", "warning", "warning", "info"],
    "role" => [1 => "Administrator", "Editor"],
    "blog_page" => [0 => "No", "Yes"],

    "social-network" => [
        'facebook',
        'youtube',
        'instagram',
        'twitter'
    ],

    "subscriber-label" => ["0" => "warning", "1" => "success"],
    "subscriber-status" => ["0" => "pending", "1" => "active"],

    "order_status" => [1 => "Görülməyib", 2 => "Görülüb", 3 => "Çatdırılıb"],
    "user_order_status" => [1 => "Sifarişiniz qeydə alındı", 2 => "Yoldadır", 3 => "Çatdırılıb"],

    "order_status_element" => [
        1 => ["fa-eye-slash",'red'],
        2 => ["fa-cart-plus ",'#3455eb'],
        3 => ["fa-check-circle",'#28a745'],
    ],

    "yes-no" => [0 => "minus", "plus"],
    'prefix-number' => [50 => 50, 51 => 51, 55 => 55, 70 => 70, 77 => 77],
    'slug_replacement' => ['az' => 'azerbaijani', 'en' => 'azerbaijani', 'ru' => 'russian'],
    "model" => ['articles' => 'App\Models\Article','page' => 'App\Models\Page', 'pageTranslation' => 'App\Models\PageTranslation','product' => 'App\Models\Product', 'productTranslation' => 'App\Models\ProductTranslation', 'articleTranslation' => 'App\Models\ArticleTranslation'],

    "rating" => [0, '0.5', 1, '1.5', 2, '2.5', 3, '3.5', 4, '4.5', 5],
    'sex' => ['male' => 1, 'female' => 0], //facebook auth
    'payment-method-class' => [1 => 'row-item card', 'row-item', 'row-item-other clearfix'],
    'filter-type-1' => ['Bütün', 'Aktiv', 'Bloklanan'],
    'filter-type-2' => ['Bütün', 'Aktiv', 'Silinən'],
    'filter-type-3' => [0=>'Bütün', 'Görülməyib', 'Görülüb','Сatdırıb'],
    'crud_another_filtr' => [
        'promo_codes' =>['date' => ['Vaxtı bitmiş','Aktiv Promo','Bütün']] 
    ],
    'sort-product' => [1 => 'Yenidən köhnəyə', 'Köhnədən Yeniyə', 'Ucuzdan Bahaya', 'Bahadan Ucuza'],
    "best-seller" => [1 => "Bəli", 0 => "Xeyr"],
    "banner-type" => [1 => 'A', 'B', 'C'],


    "template" => [
        21=>'Top Menular',
        1 => "Onlayn Oyunlar",
        2 => "İş vərəqləri",
        4=>'Oyuncağlar',
        5=>'Kitablar',
        6=>'Məktəb ləvazimatları',
        22=>'Qida / Baxım',
        7=>'Haqqimizda',
        8=>'Partnyorlar',
        9=>'Brendlər',
        10=>'Ana səhifə kateqoriya',
        11=>'Əlaqə',
        12=>'Məxfilik səhifəsi',
        13=>'İstifadəçi şərtləri və qaydaları',
        14=>'Mağazalar səhifəsi',
        20=>'Dropdown',
    ],

    "page_class" =>[
        1 => 'online-games',
        2 => 'worksheets',
        7 => 'about-link',
        8 => 'partners-link',
        9 => 'brands-link',
        20 => 'info-link',
    ],

    "m_page_class" =>[
        7 => 'about',
        8 => 'partners',
        9 => 'brands',
        11 => 'contact-link',
        12 => 'privacy-link',
        14 => 'stores-link',
        20 => 'info',
    ],

    "slider_category" => [
        0 =>'Slider',
        1 =>'Ana Səhifə Endirim',
    ],

    "slider_image_size" => [
        0 =>['width' => '1920','height'=>'430'],
        1 =>['width' => '390','height'=>'260'],
    ],

    "product_template" =>[
        'toys' => 4,
        'books' => 5,
        'school_accessories' => 6,
        'food' => 22,
    ],

    "product_type" =>[
        'books' =>'Kitablar',
        'toys' => 'Oyuncağlar',
        'school_accessories' => 'Məktəb ləvazimatları',
        'food' => 'Qida / Baxım',
    ],

    "create_title" =>[
        'toys' =>'Yeni Oyuncağ',
        'books' =>'Yeni Kitab',
        'school_accessories' =>'Yeni Məktəb ləvazimatı',
        'food' =>'Yeni Qida / Baxım'
    ],


    "forms" =>[
        'age' =>'Yaş aralığı',
        'product_lang' =>'Dil',
        'tip' =>'Tip',
        'sale' => 'Endirim',
        'material' => 'Material',
    ],

    "index_form" =>[
        'best_seller' =>'Məhsullar',
        'sale_product' => 'Endrimdə olan mallar',
    ],

    "index_form_view" =>[
        'best_seller' =>'Məhsullar',
        'advice_product' =>'Məsləhət Görülənlər',
        'new_product' =>'Yeni Məhsullar',
    ],

    "product_form" =>[
        "toys" =>['age','material','tip'],
        "books" =>['age','product_lang','tip'],
        "school_accessories" =>['tip']
    ],

// Index kateqoriyada islenenler yaddan cixarma
    "product_form_index" =>['age','material','tip','product_lang'],

    "product_filtr_paginate" => [24,36,48,60,72],
    "product_filtr_order" => [1 => 'Əvvəlcə ucuz',2=>'Əvvəlcə bahalı'],
    "product_filtr_order_type" => [1 => 'asc',2=>'desc'],


    'page_size'  =>[
        10 =>['width' => '340','height'=>'340'],
    ],

    'thumb_page_size'  =>[
        10 =>['width' => '150','height'=>'150'],
    ],

    'mailgun_account' =>[
        'username' => 'info@mestan.az',
        'password' => 'F,e?um,Z#m/xu6t'
    ],

    "mail_config" => [
        'gmail' => ['smtp_address'=>'noreply.n123@gmail.com','username' => 'noreply.n123@gmail.com','password' =>'noreply123huseyn','encryption' =>'tls','port' =>587,'host'=>'smtp.gmail.com'],
        'mailgun' => ['smtp_address'=>'no-reply@mestan.az','username' => 'no-reply@mestan.az','password' =>'15604d4acbf0613fd13da0e4e7fceb3f-b6190e87-5c0d431d','encryption' =>'tls','port' =>587,'host'=>'smtp.mailgun.org'],
    ]


];

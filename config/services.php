<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

 'mailgun' => [
        'domain' => 'sandboxd7e112f511fc4d5aa04674320cb04675.mailgun.org',
        'secret' => '6cb27be35eee2754b8ac336d8fa4da3b-e5da0167-95a02a0f',
    ],

   'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'facebook' => [
        'client_id'     => '193603458180514',
        'client_secret' => '9ba357cbe6446bbb9abc0706a3b6a60e',
        'redirect'      => env('APP_URL').'/auth/facebook/callback',
    ],
];

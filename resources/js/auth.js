var loginForm = function() {
    $("#login-form").on("submit", function() {
        var form = $(this);
        var submitable = true;

        $(":input.required", form).each(function() {
            if (!$(this).val()) {
                $(this).addClass("error");
                submitable = false;
            }
        });

        if (submitable) {
            $("body").addClass("on-progress");

            $.ajax({
                url: form.attr("action"),
                type: 'POST',
                dataType: 'jsonp',
                data: {
                    action: "login",
                    ref: $(":input[name='ref']", form).val(),
                    email: $(":input[name='email']", form).val(),
                    password: $(":input[name='password']", form).val(),
                    remember: $(":input[name='remember']", form).is(":checked") ? 1 : 0,
                    oformat: "json"
                },

                error: function() {
                    $("body").removeClass("error");
                    form.find(".form-result").text(form.find(".form-result").data("error"));
                    form.find(".form-result").removeClass("d-none");
                },

                success: function(resp) {
                    if (resp.result == 1) {
                        if (resp.go) {
                            window.location.href = resp.go;
                        } else {
                            window.location.reload(true);
                        }
                    } else {
                        $("body").removeClass("on-progress");
                        form.find(".form-result").html("");
                        for (var i = 0; i < resp.msg.length; i++) {
                            form.find(".form-result").append("<li>"+resp.msg[i]+"</li>");
                        }
                    }
                }
            });
        }

        return false;
    });
}; // Login Form

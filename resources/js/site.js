(function($) {

    'use strict';
    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    }; // is Mobile

    var responsiveMenu = function() {
        var $toggle = $('.mobile-toggle');

        $('#mobile-menu').hcOffcanvasNav({
            customToggle: $toggle,
            insertClose:false,
            insertBack:true,
            labelClose:'Close',
            labelBack:'Back',
            levelSpacing:20,
            disableBody:true,
            levelOpen:'expand'
        });
    };

    var mobileBasket = function() {
        $(document).on('click', '.header-mobile .basket-button-container', function(e) {
            $(this).children('div.dropdown-box').toggleClass('active');
            e.stopImmediatePropagation();
        });
    };


    var waveButton = function () {
        Waves.attach('.button', ['waves-button', 'waves-float']);
        Waves.init();
    };

    var slideBrand = function() {
        $(".brand-carousel").owlCarousel({
            autoplay:true,
            nav: false,
            dots: true,
            responsive: true,
            margin:30,
            loop:true,
            items:8,
            responsive:{
                0:{
                    items: 2,
                    dots: false
                },
                479:{
                    items: 2,
                    dots: false
                },
                599:{
                    items: 3,
                    dots: false
                },
                768:{
                    items: 4
                },
                991:{
                    items: 6
                },
                1200: {
                    items: 8
                }
            }
        });
    };// slide Brand

    var slideMenuBrands = function() {
        $(".menu-carousel").owlCarousel({
            autoplay:true,
            nav: false,
            dots: false,
            responsive: true,
            margin:30,
            loop:true,
            items:4,
            responsive:{
                0:{
                    items: 2
                },
                479:{
                    items: 2
                },
                599:{
                    items: 3
                },
                768:{
                    items: 4
                },
                991:{
                    items: 4
                },
                1200: {
                    items: 4
                }
            }
        });
    };// slide Menu Brands

    var mainSlider = function() {
        $('.main-slider').owlCarousel({
            autoplay:true,
            autoplayTimeout:8000,
            autoplaySpeed:1200,
            smartSpeed:700,
            nav: false,
            dots: true,
            margin:0,
            touchDrag:true,
            mouseDrag:true,
            loop:true,
            items:1,
            responsive:{
                0:{
                    dots: false
                },
                479:{
                    dots: false
                },
                599:{
                    dots: false
                },
                768:{
                    dots: true
                },
                991:{
                    dots: true
                },
                1200: {
                    dots: true
                }
            }
        });
    };// main Slider


    var slideMainPageProducts = function() {
        $(".home-carousel").owlCarousel({
            autoplay:true,
            nav: true,
            dots: true,
            responsive: true,
            margin:30,
            loop:true,
            items:5,
            responsive:{
                0:{
                    items: 2,
                    dots: false,
                    margin:10
                },
                479:{
                    items: 2,
                    dots: false
                },
                600:{
                    items: 2,
                    dots: false
                },
                768:{
                    items: 3,
                    margin:20,
                },
                991:{
                    items: 4
                },
                1200: {
                    items: 4
                }
            }
        });
    };// slide Main Page Products


    var slideSortBoxProducts = function() {
        $(".sortbox-carousel").owlCarousel({
            autoplay:true,
            nav: false,
            dots: false,
            margin:30,
            loop:true,
            items:5,
            responsive:{
                0:{
                    items: 3,
                    margin: 16,
                    stagePadding: 20
                },
                479:{
                    items: 3,
                    margin:20
                },
                600:{
                    items: 3,
                    margin:20
                },
                768:{
                    items: 3,
                    margin:20
                },
                991:{
                    items: 4
                },
                1200: {
                    items: 4
                }
            }
        });
    };// slide Sort Box Products


    var slideSimilarProducts = function() {
        $(".similar-carousel").owlCarousel({
            autoplay:true,
            nav: true,
            dots: true,
            responsive: true,
            margin:30,
            loop:true,
            items:5,
            responsive:{
                0:{
                    items: 2,
                    dots: false,
                    margin:10
                },
                479:{
                    items: 2,
                    dots: false
                },
                600:{
                    items: 2,
                    dots: false
                },
                768:{
                    items: 3,
                    margin:20,
                },
                991:{
                    items: 4
                },
                1200: {
                    items: 4
                }
            }
        });
    };// slide Most Viewer

    var tabSortproduct = function() {
        $('.wrap-imagebox').each(function() {
            $(this).find('ul.icons').children('li').first().addClass('active');
            $(this).find('.tab-product').children('.sort-box').first().show().siblings().hide();
            $(this).find('ul.icons').children('li').on('click', function(e) {
                var liActive = $(this).index();
                $(this).addClass('active').siblings().removeClass('active');
                $(this).closest('.wrap-imagebox').find('.tab-product').children('.sort-box').eq(liActive).fadeIn(1000).show().siblings().hide();
                e.preventDefault();
            });
        });
    }; // Tab Sort Product

    var scrollbarCheckbox = function() {
        if ( $().mCustomScrollbar ) {
            $(".box-checkbox").mCustomScrollbar({
                scrollInertia:400,
                theme:"light-3",
            });
        }
    }; // Scrollbar Checkbox

    var scrollbarTableCart = function() {
        $(".table-cart").mCustomScrollbar({
            axis:"x",
            advanced:{autoExpandHorizontalScroll:true},
            scrollInertia:400,
        });
    }; // Scrollbar Table Cart


    var toggleCatlist = function() {
        $('.cat-list').each(function() {
            $(this).children('li').children('ul.cat-child').hide();
            $( ".cat-list li span" ).on('click', function() {
                $(this).parent('li').toggleClass('active');
                $(this).toggleClass('active');
                $(this).parent('li').children('ul.cat-child').slideToggle(300);
            });
        })
    }; // Toggle Cat List

    var toggleWidget = function() {
        $(window).on('load resize', function() {
            if ( matchMedia( 'only screen and (max-width: 767px)' ).matches ) {
                $( "#category .sidebar .widget .dropdown-title" ).removeClass('active').parent('.widget').children('.widget-content').slideUp(300);
            }
        });


        $('#category .btn-filter').on('click', function() {
            $('#category .sidebar').slideToggle(300);
            $(this).toggleClass('active');
            $( "#category .sidebar .widget .dropdown-title" ).removeClass('active').parent('.widget').children('.widget-content').slideUp(300);
            return false;
        });

        $( "#category .sidebar .widget .dropdown-title" ).on('click', function() {
            $(this).toggleClass('active');
            $(this).parent('.widget').children('.widget-content').slideToggle(300);
        });
    }; // Toggle Dropdown


    var showSuggestions = function() {
        $( ".top-search .box-search" ).each(function() {
            $( ".box-search input" ).on('input', (function() {
                $(this).closest('.boxed').children('.overlay').css({
                    opacity: '1',
                    display: 'block'
                });
                $(this).parent('.box-search').children('.search-suggestions').css({
                    opacity: '1',
                    visibility: 'visible',
                    top: '49px'
                });
            }));
            $( ".box-search input" ).on('blur', (function() {
                $(this).closest('.boxed').children('.overlay').css({
                    opacity: '0',
                    display: 'none'
                });
                $(this).parent('.box-search').children('.search-suggestions').css({
                    opacity: '0',
                    visibility: 'hidden',
                    top: '100px'
                });
            }));
        });

        $( ".top-search .box-search" ).each(function() {
            $( ".box-search input" ).on('input', (function() {

                // var product_id = $(this).data("id");
                // if(product_id) {

                $(".box-suggestions").addClass('on-progress');

                $.ajax({
                    url: '/api/search',
                    type: 'POST',
                    dataType: 'jsonp',
                    data: {
                        // product_id: product_id
                    },

                    error: function() {
                        $(".box-suggestions").removeClass('on-progress');
                    },

                    success: function(resp) {
                        if (resp.result == 1) {

                        }

                        $(".box-suggestions").removeClass('on-progress');
                    }
                });
                // }
            }));
        });
    }; // Toggle Location

    var showSuggestions = function() {
        $( ".top-search .box-search" ).each(function() {
            $( ".box-search input" ).on('input', (function() {
                $(this).closest('.boxed').children('.overlay').css({
                    opacity: '1',
                    display: 'block'
                });
                $(this).parent('.box-search').children('.search-suggestions').css({
                    opacity: '1',
                    visibility: 'visible',
                    top: '77px'
                });
            }));
            $( ".box-search input" ).on('blur', (function() {
                $(this).closest('.boxed').children('.overlay').css({
                    opacity: '0',
                    display: 'none'
                });
                $(this).parent('.box-search').children('.search-suggestions').css({
                    opacity: '0',
                    visibility: 'hidden',
                    top: '100px'
                });
            }));
        });
    }; // Toggle Location

    var popups = function() {
        var w = getScrollbarWidth();

        $(window).on("resize", function(){
            $('#popupstyles').remove();
            if($("body").outerHeight() > $(window).height()){
                $("head").append(
                    "<style id='popupstyles'>"+
                    "html.popup-open{padding-right:"+w+"px; overflow:hidden;}"+
                    ".popup-window{overflow-y:scroll}"+
                    "</style>"
                );
            }
        }).trigger("resize");

        $("body").on("click", ".open-popup", function() {
            if ($($(this).data("popup")).length == 1) {
                $("html").addClass('popup-open');
                $($(this).data("popup")).removeClass('d-none');

                $(".dropdown.open").removeClass("open");

                return false;
            }
        });

        $("body").on("click", ".close-popup", function(){
            $("html").removeClass('popup-open');
            $(this).parents(".popup-window").addClass("d-none");
            $(this).children("form").addClass("d-none");

        });

        $("body").on("click", ".newPopupElement", function(){
            $(this).parents(".popup-window").addClass("d-none");
        });

    }; // Popups
    // var signupForm = function() {
    //     $("#signup-form").on("submit", function() {
    //         var form = $(this);
    //         var submitable = true;
    //
    //         $(":input.required", form).each(function() {
    //             if (!$(this).val()) {
    //                 $(this).addClass("error");
    //                 submitable = false;
    //             }
    //         });
    //
    //         if (submitable) {
    //             $("body").addClass("on-progress");
    //
    //             $.ajax({
    //                 url: form.attr("action"),
    //                 type: 'POST',
    //                 dataType: 'jsonp',
    //                 data: {
    //                     action: "signup",
    //                     ref: $(":input[name='ref']", form).val(),
    //                     firstname: $(":input[name='firstname']", form).val(),
    //                     lastname: $(":input[name='lastname']", form).val(),
    //                     email: $(":input[name='email']", form).val(),
    //                     password: $(":input[name='password']", form).val(),
    //                     oformat: "json"
    //                 },
    //
    //                 error: function() {
    //                     $("body").removeClass("error");
    //                     form.find(".form-result").text(form.find(".form-result").data("error"));
    //                     form.find(".form-result").removeClass("d-none");
    //                 },
    //
    //                 success: function(resp) {
    //                     if (resp.result == 1) {
    //                         if (resp.go) {
    //                             window.location.href = resp.go;
    //                         } else {
    //                             window.location.reload(true);
    //                         }
    //                     } else {
    //                         $("body").removeClass("on-progress");
    //                         form.find(".form-result").html("");
    //                         for (var i = 0; i < resp.msg.length; i++) {
    //                             form.find(".form-result").append("<li>"+resp.msg[i]+"</li>");
    //                         }
    //                     }
    //                 }
    //             });
    //         }
    //
    //         return false;
    //     });
    // }; // Signup Form

    var logReg = function() {
        $("body").on("click", ".logreg-form-switch", function() {
            var form = $("#logreg #"+$(this).data("form")+"-form");

            if (form.length == 1) {
                $("#logreg form").addClass("d-none");
                form.removeClass("d-none");

                return false;
            }
        });


        // loginForm();
        // signupForm();
    };  // LogReg toggle

    var flexProduct = function() {
        $('.flexslider').flexslider({
            animation: "slide",
            controlNav: "thumbnails"
        });
    }; // Flex Product

    var progressCircle = function() {
        $('.progress-circle').waypoint(function() {
            $('.demo').percentcircle({
                coverBg: '#e1e1e4',
                bgColor: '#e1e1e4',
                fillColor: '#f28b00'
            });
            $('.demo').children('div:not(:first-child)').css({
                display: 'none'
            });
        }, {offset: '100%'});
    }; // Progress Circle

    var detectViewport = function() {
        $('[data-waypoint-active="yes"]').waypoint(function() {
            $(this).trigger('on-appear');
        }, { offset: '100%', triggerOnce: true });
        $(window).on('load', function() {
            setTimeout(function() {
                $.waypoints('refresh');
            }, 100);
        });
    }; // Detect Viewport

    var BrandsIsotope = function() {
        if ( $().isotope ) {
            var $container = $('.brands-list');
            var selector = $(this).attr('data-filter');
            $container.imagesLoaded(function(){
                $container.isotope({
                    itemSelector: '.ipsotope',
                    transitionDuration: '1s'
                });

                $container.isotope({ filter: '*' })
            });

            $('.brands-tablist li').on('click',function() {
                var selector = $(this).attr('data-filter');
                $('.brands-tablist li').removeClass('active');
                $(this).addClass('active');
                $container.isotope({ filter: selector });
                return false;
            });
        };
    }; // Brands Isotope


    var scrollUp = function() {
        $.scrollUp({
            scrollText: '<i class=\'fa fa-angle-double-up\' aria-hidden=\'true\'></i>',
            scrollDistance: 1800,
            scrollSpeed: 500,
            animationSpeed: 500
        });
    }

    var overlay = function(){
        var megaMenu = $('#mega-menu');
        megaMenu.on('mouseover', function() {
            $(this).closest('.boxed').children('.overlay').css({
                opacity: '1',
                display: 'block',
            });
        });
        megaMenu.on('mouseleave', function() {
            $(this).closest('.boxed').children('.overlay').css({
                opacity: '0',
                display: 'none',
            });
        });
    }; // Overlay

    var zoomImage = function() {
        $(document).ready(function(){
            $('.zoom').zoom();
        });
    }; // zoom Image

    var zoomVideo = function() {
        $(document).ready(function(){
            $(".zoom-video").magnificPopup({
                disableOn: 700,
                type: "iframe",
                mainClass: "mfp-fade",
                removalDelay: 160,
                preloader: !1,
                fixedContentPos: !0,
                gallery: {
                    enabled: !1
                }
            })
        });
    }; // Zoom Video

    var paymentChecked = function() {
        $('.payment-method').on('click', function() {
            if(!$(this).hasClass('coming-soon')) {
                $('.payment-method').removeClass('active');
                $(this).addClass('active');
            }
        });
    }; // Payment Checked

    var removePreloader = function() {
        $(window).on('load', function() {
            $('.preloader').delay(1000).fadeOut(600);
        });
    }; //remove Preloader

    var imageUpload = function() {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#imageResult').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(function () {
            $('#upload').on('change', function () {
                readURL(input);
            });
        });

        var input = document.getElementById( 'upload' );
        
        if(input) {
            input.addEventListener( 'change', showFileName );
            function showFileName( event ) {
                var input = event.srcElement;
            }
        }
    };

    // Dom Ready
    $(function() {
        responsiveMenu();
        mobileBasket();
        waveButton();
        slideBrand();
        slideMenuBrands();
        mainSlider();
        slideMainPageProducts();
        slideSortBoxProducts();
        slideSimilarProducts();
        tabSortproduct();
        overlay();
        toggleWidget();
        toggleCatlist();
        showSuggestions();
        flexProduct();
        detectViewport();
        BrandsIsotope();
        scrollbarTableCart();
        scrollUp();
        popups();
        logReg();
        zoomImage();
        zoomVideo();
        paymentChecked();
        removePreloader();
        imageUpload();
    });

    /**
     * Validate email
     * @param  {string}  email
     * @return {Boolean}
     */
    function isValidEmail(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }

    /**
     * Get the width of window default scrollbar
     * @return {Integer}
     */
    function getScrollbarWidth() {
        var scrollDiv = document.createElement("div");
        scrollDiv.className = "scrollbar-measure";
        document.body.appendChild(scrollDiv);
        var w = scrollDiv.offsetWidth - scrollDiv.clientWidth;
        document.body.removeChild(scrollDiv);

        return w;
    }


})(jQuery);
var addToWihlist = function() {

    $(document).on('click', '.wishlist', function(e) {
        e.preventDefault();
        let _this = $(this);
        _this.addClass('preload');
        if (_this.hasClass('active')) {
            // removeFromWishlist(btn);
            $.ajax({
                url: '/api/wishlist/add',
                type: 'POST',
                dataType: 'jsonp',
                data: {
                    product_id: _this.data("id"),
                    quantity: 1
                },
                error: function () {
                    _this.removeClass("preload");
                },
                success: function (resp) {
                    if (resp.result == 1) {
                        _this.removeClass("active");
                    }

                    _this.removeClass("preload");
                }
            });
        } else {
            // addToWishlist(btn);
            $.ajax({
                url: '/api/wishlist/add',
                type: 'POST',
                dataType: 'jsonp',
                data: {
                    product_id: _this.data("id"),
                    quantity: 1
                },
                error: function () {
                    _this.removeClass("preload");
                },
                success: function (resp) {
                    if (resp.result == 1) {
                        _this.addClass("active");
                    }

                    _this.removeClass("preload");
                }
            });

            return false;
        }
        return false;
    });
}; // Add To Wishlist

var addToCart = function() {

    $(document).on('click', '.btn-add-cart', function(e) {
        e.preventDefault();
        let _this = $(this);
        _this.addClass('preload');
        if (_this.hasClass('active')) {
            $.ajax({
                url: '/api/shopping-cart/add',
                type: 'POST',
                dataType: 'jsonp',
                data: {
                    product_id: _this.data("id"),
                    quantity: 1
                },
                error: function () {
                    _this.removeClass("preload");
                },
                success: function (resp) {
                    if (resp.result == 1) {
                        _this.removeClass("active");

                        if (resp.total > 0) {
                            $("#basketItemCount").text(resp.total > 9 ? "9+" : resp.total);
                        } else {
                            $("#basketItemCount").text("0");
                        }
                    }

                    _this.removeClass("preload");
                }
            });
        } else {
            $.ajax({
                url: '/api/shopping-cart/add',
                type: 'POST',
                dataType: 'jsonp',
                data: {
                    product_id: _this.data("id"),
                    quantity: 1
                },
                error: function () {
                    _this.removeClass("preload");
                },
                success: function (resp) {
                    if (resp.result == 1) {
                        _this.addClass("active");

                        if (resp.total > 0) {
                            $("#basketItemCount").text(resp.total > 9 ? "9+" : resp.total);
                        } else {
                            $("#basketItemCount").text("0");
                        }
                    }

                    _this.removeClass("preload");
                }
            });

            return false;
        }
        return false;
    });
}; // Add To Cart



//MaskInput
$.mask.definitions['9'] = '';
$.mask.definitions['d'] = '[0-9]';
$(".maskphone").mask("+(994 dd) ddd dd dd");

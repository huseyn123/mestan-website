<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 5/11/18
 * Time: 19:40
 */

return [
    "config" => [
        'sex' => ['Qadın', 'Kişi'],
        "minDates" => ['Baz', 'B.e', 'Ç.a', 'Çər', 'C.a', 'Cüm', 'Şən'],
        "dates" => ['Bazar günü', 'Bazar ertəsi', 'Çərşənbə axşamı', 'Çərşənbə', 'Cümə axşamı', 'Cümə', 'Şənbə'],
        "months" => [1 => 'Yanvar', 'Fevral', 'Mart', 'Aprel', 'May', 'İyun', 'İyul', 'Avqust', 'Sentyabr', 'Oktyabr', 'Noyabr', 'Dekabr'],
        "minMonths" => ['Yan', 'Fev', 'Mar', 'Apr', 'May', 'İyn', 'İyl', 'Avq', 'Sen', 'Okt', 'Noy', 'Dek'],
        "eventDates" => ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'],
        'subCopy' => "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n". 'into your web browser: [:actionURL](:actionURL)',
        'payment-method' => [1 => 'Onlayn ödəmə', 'Ünvanda nağd ödəniş', 'Ünvanda Plastik Kartla ödəniş'],
    ],
    'product-delivery' => [1 => 'Bakı şəhəri üzrə', 'Azərbaycan üzrə', 0 => 'Yoxdur'],
    'forgot_password' => 'Şifrəni Unutdun?'
];

@extends ('layouts.web', [
'page_heading' => Cache::get('dictionary_'.app()->getLocale())['nothing_found_title'],
'menu' => \App\Logic\Menu::all(),
'config' => getConfig(),
'dictionary' => Cache::get('dictionary_'.app()->getLocale()),
'social' => Cache::get('social'),
'lang' => app()->getLocale(),
'notFound' => true
])

@section ('content')
	<!-- Error404 Begin -->
	<section class="error404">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 text-center">
					<span>404</span>
					<h3>{{ $dictionary['nothing_found_heading'] ??  Cache::get('dictionary_'.app()->getLocale())['nothing_found_heading'] }}</h3>
					<p>{{ $dictionary['nothing_found_content'] ?? Cache::get('dictionary_'.app()->getLocale())['nothing_found_content'] }}</p>
					<a href="{{ route('home') }}">{{ $dictionary['back_home_page'] ?? Cache::get('dictionary_'.app()->getLocale())['back_home_page'] }}</a>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row">
				<img src="{{ asset('images/error404.png') }}">
			</div>
		</div>
	</section>
	<!-- Error404 End -->
@endsection
@extends ('layouts.admin')
@section ('title', $title)

@section('content')
    <!-- Main content -->
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <img src="{{ asset('images/logos/logo.png') }}">
                    <small class="pull-right">Tarix: {{ $invoice->created_at }}</small>
                </h2>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <address>
                    <strong>{{ $invoice->customer->name ?? $invoice->user_name}}</strong><br>
                    {{ $invoice->address }}<br>
                    Mobil: {{ $invoice->customer->phone  ?? $invoice->user_phone}}<br>
                    Email: {{ $invoice->customer->email ?? ''}}
                </address>
            </div>

            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <b>{{ $title }}</b><br>
                @if(!is_null($invoice->payment_key))
                    <b>Transaction ID:</b> {{ $invoice->payment_key }}<br>
                @endif
                <b>Məbləğ:</b> {{ $invoice->amount }} AZN<br>


                @if($invoice->special_percent) <b>Endirim: {!! $invoice->birthday_status ? '<i class="fa fa-birthday-cake" aria-hidden="true"></i> Ad günü' : ' ' !!}</b> 



                {{ $invoice->special_percent.'%'}} <br> @endif
                <b>Çatdırılma qiyməti:</b> {{ ($invoice->delivery_price ? $invoice->delivery_price : 0).' AZN'}} <br> 
                 {!! '<b>Cəmi:</b> '.($invoice->total_amount ? $invoice->total_amount :  $total_price).' AZN <br>'   !!}

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-12">
                <p class="lead">Əlavə qeyd:</p>
                {{ $invoice->desc }}
            </div>
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Say</th>
                        <th>Məhsul</th>
                        <th>Kod #</th>
                        <th>Şəkil</th>
                        <th>qiymət</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{ $product->count }}</td>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->bar_code }}</td>
                            @if($product->getFirstMedia())
                                <td><img src="{{asset($product->getFirstMedia()->getUrl('basket'))}}"></td>
                            @endif
                            <td>{{ $product->cart_prise }} AZN</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->


        <!-- this row will not appear when printing -->
        {{--<div class="row no-print">--}}
            {{--<div class="col-xs-12">--}}
                {{--<a href="javascript:window.print();" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>--}}
                {{--<a class="btn btn-primary pull-right" style="margin-right: 5px;" href="{{ route('invoices.pdf', $invoice->id) }}">--}}
                    {{--<i class="fa fa-download"></i> PDF--}}
                {{--</a>--}}
            {{--</div>--}}
        {{--</div>--}}
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
@endsection
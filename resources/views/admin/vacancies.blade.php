@extends ('layouts.admin',['script' => true,'editor' => true])
@section ('title', $title)

@section ('content')

    @include('widgets.modal-confirm')

    @component('admin.components.dt', ['create' => 'vacancies.create', 'route' => 'vacancies', 'largeModal' => true])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'vacancies', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
    CKEDITOR.replace('editor');

    {!! $dataTable->scripts() !!}
@endpush
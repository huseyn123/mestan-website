@extends('layouts.modal', ['ajax' => '', 'script' => false])
@section('title', $title)

@section('content')
    <div class="col-md-12">
        @csrf
        <input type="file" name="import_file">
    </div>
@endsection
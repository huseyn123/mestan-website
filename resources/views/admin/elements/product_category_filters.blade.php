@foreach($form as $key=>$value)

	<div class="form-group form-class  product_filtrs hh">
		<label for="{{$value->filter_column}}[]" class="col-md-3 control-label">{{$value->title}}</label>
		<div class="col-md-8">

			@foreach($value->filter_value as $item)

				<span style="margin-right: 8px">{{$item->title_az}} 
					<input name="{{$value->filter_column}}[]" type="checkbox" value="{{$item->id}}" id="{{$value->filter_column}}[]">
				</span>
			@endforeach()
		</div>
	</div>

@endforeach



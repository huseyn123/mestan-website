@extends ('layouts.admin', ['table' => 'sliders'])
@section ('title', $title)

@section ('content')

    @include('widgets.modal-confirm')

    @component('admin.components.dt', ['create' => 'slider.create', 'route' => 'slider', 'filter' => config('config.filter-type-2'), 'locale' => true])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'sliders', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
    {!! $dataTable->scripts() !!}

    <script>
        window.LaravelDataTables["sliders"].on( 'row-reorder', function ( e, diff ) {

            $.ajax({
                url: "{{ route('slider.reorder') }}",
                type: 'POST',
                data: JSON.stringify(diff),
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}',
                    'content-type': 'application/json'
                },
                success: function (data) {
                    $('#sliders').DataTable().draw(false);
                },
                error: function (data) {
                    alert('Error !');
                }
            });
        });

    </script>
@endpush
@extends ('layouts.admin', ['table' => 'invoices'])
@section ('title', $title)

@section('content')
    @include('widgets.modal-confirm')

    @component('admin.components.dt', ['filter' => config('config.filter-type-3'),'inv_t' =>true])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'invoices', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
    <link href="{{ asset('css/admin/bootstrap-editable.css') }}" rel="stylesheet">

    <script src="{{ asset('vendor/dataTables/dataTables.buttons.edit-sale.js') }}?v=2"></script>
    <script src="{{ asset('vendor/dataTables/buttons.server-side.js') }}"></script>

    <script>

        $('#invoices').on('change', function(e) {
            $('#invoices').DataTable().draw(false);
        });


    </script>

    {!! $dataTable->scripts() !!}

@endpush
@extends('layouts.modal', ['route' => $route, 'editor' => $editor ?? false, 'script' => $script ?? true])
@section('title', $title)

@section('content')
    {!! $fields !!}
@endsection
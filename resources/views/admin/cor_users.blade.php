@extends ('layouts.admin', ['table' => 'reg_users'])
@section ('title', $title)

@section('content')
    @include('widgets.modal-confirm')

    @component('admin.components.dt', ['create' => 'cor.create', 'route' => 'cor.users'])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'reg_users', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')

    <script src="{{ asset('vendor/dataTables/buttons.server-side.js') }}"></script>


    {!! $dataTable->scripts() !!}
@endpush

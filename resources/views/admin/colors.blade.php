@extends ('layouts.admin')
@section ('title', $title)

@section ('content')

    @include('widgets.modal-confirm')

    @component('admin.components.dt', ['create' => 'colors.create', 'route' => 'colors'])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'colors', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
{!! $dataTable->scripts() !!}
@endpush
@extends('layouts.modal', ['route' => $route, 'method' => 'PUT','script' => true])
@section('title', $data->name ?? $title)

@section('content')

    <div class="col-lg-10 col-lg-offset-1">

        @foreach($colums as $column)

            @include('widgets.lang-tab', ['input' => 'text', 'info' => $data, 'name' => $column])

        @endforeach

        {!! $fields !!}

    </div>


@endsection

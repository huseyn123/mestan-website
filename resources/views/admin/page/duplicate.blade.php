@extends('layouts.modal', ['route' => ["$model.duplicate", $info->id], 'method' => 'PUT', 'editor' => true, 'ajax' => '', 'class' => 'actionPage', 'script' => $script ?? true, 'saveTxt' => 'Duplicate'] )
@section('title', $info->name)

@section('content')
    {!! $fields !!}
@endsection

@push('scripts')
    <script>
        @if(isset($keys))
        $('#meta_keywords').select2({
            data: {!! json_encode($keys) !!},
            tags: true,
            maximumSelectionLength: 10

        }).on("change", function(e) {
            var isNew = $(this).find('[data-select2-tag="true"]');
            if(isNew.length){
                isNew.replaceWith('<option selected value="'+isNew.val()+'">'+isNew.val()+'</option>');
            }
        });

        $('#meta_keywords').val({!! json_encode($keys) !!}).trigger('change');
        @endif
    </script>
@endpush
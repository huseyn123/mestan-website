@extends('layouts.modal', ['route' => $route, 'script' => true])
@section('title', $title)

@section('content')
    <ul class="nav nav-tabs">
        @foreach(config('app.locales') as $key => $locale)
            <li @if($loop->first) class="active" @endif><a href="#{{$key}}" data-toggle="tab"> {{ $key }}</a></li>
        @endforeach
    </ul>
    <br>
    <div class="tab-content">
        @foreach(config("app.locales") as $key => $locale)
            <div id="{{$key}}" class="tab-pane fade @if($loop->first) in active @endif">
                <div class="form-group">
                    <label for="name" class="col-md-3 control-label">Rəng</label>
                    <div class="col-md-8">
                        <input class="form-control" name="name_{{$key}}" type="text" @if($loop->first) required @endif>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
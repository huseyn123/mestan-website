@extends ('layouts.modal', ['script' => true])
@section ('title', $create_name)

@section('content')

    <div class="col-lg-10 col-lg-offset-1">

        @foreach($colums as $column)

            @include('widgets.lang-tab', ['input' => 'text', 'name' => $column])

        @endforeach

        {!! $fields !!}

    </div>

@endsection

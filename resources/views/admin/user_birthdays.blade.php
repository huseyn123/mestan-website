@extends ('layouts.admin', ['table' => 'user_birthdays'])
@section ('title', $title)

@section ('content')

    @include('widgets.modal-confirm')

    @component('admin.components.dt', ['route' => 'user_birthdays', 'largeModal' => true])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'user_birthdays', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection
@push('scripts')
    <script src="{{ asset('vendor/dataTables/buttons.server-side.js') }}"></script>

    {!! $dataTable->scripts() !!}
@endpush
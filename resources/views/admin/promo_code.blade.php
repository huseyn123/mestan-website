@extends ('layouts.admin', ['table' => 'promo_codes'])
@section ('title', $title)

@section ('content')

    @include('widgets.modal-confirm')

    @component('admin.components.dt', ['create' => 'promo_code.create', 'route' => 'promo_code', 'filter' => config('config.filter-type-2'),'another_filter' => config('config.crud_another_filtr')['promo_codes']])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'promo_codes', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')

    <script>
		$('#filter-date').on('change', function(e) {
            $('#promo_codes').DataTable().draw(false);
            e.preventDefault();
        });

         $("#promo_codes").on('preXhr.dt', function(e, settings, data) {
            data.date = $('#filter-date').find("option:selected").val();
        });
    </script>

    {!! $dataTable->scripts() !!}

@endpush


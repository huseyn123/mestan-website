@extends ('layouts.admin', ['table' => 'reg_users'])
@section ('title', $title)

@section('content')
    @include('widgets.modal-confirm')

	@component('admin.components.dt', ['create' => 'reg_email.create', 'route' => 'reg.users','largeModal' => true,'create_title' => 'Yeni Email'])
{{--    @component('admin.components.dt')--}}
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'reg_users', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')

    <script src="{{ asset('vendor/dataTables/buttons.server-side.js') }}"></script>


    {!! $dataTable->scripts() !!}
@endpush

@extends ('layouts.admin')
@section ('title', $title)

@section ('content')

    @include('widgets.modal-confirm')

    @component('admin.components.dt', ['create' => 'campaigns.create', 'route' => 'campaigns'])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'campaigns', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
    {!! $dataTable->scripts() !!}
@endpush
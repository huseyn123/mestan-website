<div class="login-logo">
    <a href="{{ url('/') }}">
        <img src="{{ asset('images/logos/logo.png') }}">
    </a>
</div>
@if (session()->has('status'))
    <div class="alert alert-success">
        {{ session()->get('status') }}
    </div>
@endif
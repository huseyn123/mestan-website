<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <div class="user-panel">
        <div class="pull-left image">
            @include('widgets.profile-picture', ['class' => 'img-circle'])
        </div>
        <div class="pull-left info">
            <p>{{ auth()->guard('admin')->user()->name }}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>
    <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search..." autocomplete="off">
            <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
          </span>
        </div>
    </form>
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu" data-widget="tree">
        <li {{ activeUrl(route('admin.dashboard')) }}><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard fa-fw"></i> <span>Dashboard</span></a></li>
        <li {{ activeUrl(route('admins.index')) }}><a href="{{ route('admins.index') }}"><i class="fa fa-user-plus fa-fw"></i> <span>Adminlər</span></a></li>
        <li {{ activeUrl(route('reg.users')) }}><a href="{{ route('reg.users') }}"><i class="fa fa-user fa-fw"></i> <span>Users</span></a></li>
        <li {{ activeUrl(route('cor.users')) }}><a href="{{ route('cor.users') }}"><i class="fa fa-user fa-fw"></i> <span>Xisusi İstifadəçilər</span></a></li>
        <li class="treeview">
            <a href="#"><i class="fa fa-file fa-fw"></i> <span>Səhifələr</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li {{ activeUrl(route('page.index')) }}><a href="{{ route('page.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Esas Səhifələr</span></a></li>
                <li {{ activeUrl(route('page.index')) }}><a href="{{ route('page.index') }}?page_type=sub"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Alt Kateqorialar</span></a></li>
                <li {{ activeUrl(route('pageTranslation.order')) }}><a href="{{ route('pageTranslation.order') }}?lang=az"><i class="fa fa-circle-o fa-fw"></i> <span>Ardıcıllıq</span></a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#"><i class="fa fa-tag fa-fw"></i> <span>Məhsullar</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li {{ activeUrl(route('toys.index')) }}><a href="{{ route('toys.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Oyuncağlar</span></a></li>
                <li {{ activeUrl(route('books.index')) }}><a href="{{ route('books.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Kitablar</span></a></li>
                <li {{ activeUrl(route('school_accessories.index')) }}><a href="{{ route('school_accessories.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Məktəb ləvazimatları</span></a></li>
                <li {{ activeUrl(route('food.index')) }}><a href="{{ route('food.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Qida / Baxım</span></a></li>
                <li {{ activeUrl(route('brands.index')) }}><a href="{{ route('brands.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Brendlər</span></a></li>
                <li {{ activeUrl(route('tag.index')) }}><a href="{{ route('tag.index') }}"><i class="fa fa-circle-o fa-fw"></i> <span>Teqlər</span></a></li>
                <li {{ activeUrl(route('productTranslation.order')) }}><a href="{{ route('productTranslation.order') }}?lang=az"><i class="fa fa-circle-o fa-fw"></i> <span>Ardıcıllıq</span></a></li>
                <li {{ activeUrl(route('IndexProductTranslation.order')) }}><a href="{{ route('IndexProductTranslation.order') }}?lang=az"><i class="fa fa-circle-o fa-fw"></i> <span>Ana Səhifə Ardıcıllıq</span></a></li>
            </ul>
        </li>

        <li class="treeview">
            <a href="#"><i class="fa fa-print fa-circle"></i> <span>Form</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li {{ activeUrl(route('select.index')) }}><a href="{{ route('select.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Select</span></a></li>
                <li {{ activeUrl(route('product_filters.index')) }}><a href="{{ route('product_filters.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Məhsul Filtrləri</span></a></li>
                <li {{ activeUrl(route('product_filter_value.index')) }}><a href="{{ route('product_filter_value.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Filtr Value</span></a></li>
            </ul>
        </li>

        <li {{ activeUrl(route('invoices.index')) }}><a href="{{ route('invoices.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Sifarişlər</span></a></li>
        <li {{ activeUrl(route('promo_code.index')) }}><a href="{{ route('promo_code.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Promo kod</span></a></li>
        <li {{ activeUrl(route('cor_invoices.index')) }}><a href="{{ route('cor_invoices.index') }}"><i class="fa fa-user-secret fa-fw fa-fw"></i> <span>Xisusi Sifarişlər</span></a></li>
        <li {{ activeUrl(route('user_birthdays.index')) }}><a href="{{ route('user_birthdays.index') }}"><i class="fa fa-print fa-gift"></i> <span>Ad günü</span></a></li>

        {{--<li {{ activeUrl(route('banners.index')) }}>--}}
            {{--<a href="{{ route('banners.index') }}">--}}
                {{--<i class="fa fa-image fa-fw"></i><span>Bannerlər</span>--}}
                {{--<span class="pull-right-container">--}}
                    {{--<small class="label pull-right bg-green">yeni</small>--}}
                {{--</span>--}}
            {{--</a>--}}
        {{--</li>--}}

        {{--<li {{ activeUrl(route('articles.index')) }}><a href="{{ route('articles.index') }}"><i class="fa fa-newspaper-o fa-fw"></i> <span>Xəbərlər</span></a></li>--}}
        <li {{ activeUrl(route('delivery.index')) }}><a href="{{ route('delivery.index') }}"><i class="fa fa-print fa-circle"></i> <span>Çatdırılma Qiymətləri</span></a></li>
        <li {{ activeUrl(route('slider.index')) }}><a href="{{ route('slider.index') }}"><i class="fa fa-image fa-fw"></i> <span>Slider</span></a></li>
{{--        <li {{ activeUrl(route('campaigns.index')) }}><a href="{{ route('campaigns.index') }}"><i class="fa fa-star fa-fw"></i> <span>Kampaniyalar</span></a></li>--}}
{{--        <li {{ activeUrl(route('vacancies.index')) }}><a href="{{ route('vacancies.index') }}"><i class="fa fa-newspaper-o fa-fw"></i> <span>Vakansiyalar</span></a></li>--}}

        {{--<li {{ activeQuery(route('faq.index').'?type=0') }}><a href="{{ route('faq.index') }}?type=0"><i class="fa fa-question-circle fa-fw"></i> <span>FAQ</span></a></li>--}}
{{--        <li {{ activeQuery(route('faq.index').'?type=1') }}><a href="{{ route('faq.index') }}?type=1"><i class="fa fa-question-circle fa-fw"></i> <span>Qaydalar</span></a></li>--}}
        <li {{ activeUrl(route('partners.index')) }}><a href="{{ route('partners.index') }}"><i class="fa fa-briefcase fa-fw"></i> <span>Tərəfdaşlar</span></a></li>
        {{--<li {{ activeUrl(route('block.index')) }}>--}}
            {{--<a href="{{ route('block.index') }}">--}}
                {{--<i class="fa fa-list fa-fw"></i> <span>Bloklar</span>--}}
            {{--</a>--}}
        {{--</li>--}}
{{--        <li {{ activeUrl(route('subscribers.index')) }}><a href="{{ route('subscribers.index') }}"><i class="fa fa-users fa-fw"></i> <span>İzləyicilər</span></a></li>--}}
        <li {{ activeUrl(route('dictionary.index')) }}><a href="{{ route ('dictionary.index') }}"><i class="fa fa-text-height fa-fw"></i> <span>Lüğət</span></a></li>

        <li class="treeview">
            <a href="#"><i class="fa fa-cogs fa-fw"></i> <span>Advanced</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li {{ activeUrl(url('api/filemanager')) }}><a href="{{ url('api/filemanager') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>File Manager</span></a></li>
                <li {{ activeUrl(route('config.index')) }}><a href="{{ route('config.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Konfiqurasiya</span></a></li>
                {{--<li {{ activeUrl(route('analytic.index')) }}><a href="{{ route('analytic.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Google Analytics</span></a></li>--}}
                {{--<li {{ activeUrl(route('sitemap.index')) }}><a href="{{ route('sitemap.index') }}"><i class="fa fa-circle-o fa-fw fa-fw"></i> <span>Sitemap</span></a></li>--}}
            </ul>
        </li>
    </ul>
    <!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->
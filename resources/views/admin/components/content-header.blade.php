<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>{{ $title }}</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route($homeRoute) }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">{{ $title }}</li>
    </ol>
</section>
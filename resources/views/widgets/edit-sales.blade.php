<div class="modal fade" id="edit-sales" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        {!! Form::open(['route' => 'product.updateMinSalesCount', 'method'=> 'PUT', 'class'=>'warning-modal form-inline dtForm']) !!}
            <div class="modal-content">
                <div class="modal-header">
                    {!! Form::button('&times;', ['class' => 'close', 'data-dismiss' => 'modal', 'aria-hidden' => "true"]) !!}
                    <h4 class="modal-title" id="myModalLabel">Endirim Faizi</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="number" name="price" class="form-control" placeholder="%" required min="0">
                    </div>
                    {!! Form::button('Təsdiq et', ['class' => 'btn btn-success loadingButton', 'type' => 'submit', 'data-loading-text' => loading()]) !!}
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
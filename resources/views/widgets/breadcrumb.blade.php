@if ($breadcrumbs)
    @foreach ($breadcrumbs as $breadcrumb)

        @if (!isset($breadcrumb->last))
            <li class="trail-item">
                <a href="{{ $breadcrumb->url }}" title="">{{ $breadcrumb->title }}</a>
                <span><img src="{{asset('images/icon/next.svg')}}" ></span>
            </li>
        @else
            <li class="trail-end">
                <a title="">{{ $breadcrumb->title }}</a>
            </li>
        @endif
    @endforeach
@endif
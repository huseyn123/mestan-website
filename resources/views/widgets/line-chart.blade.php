<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Ən çox baxılan səhifələr</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="chart" id="line-chart" style="position: relative; height: 300px;"></div>
                <!-- /.chart-responsive -->
            </div>
        </div>
        <!-- /.row -->
    </div>
</div>
<!-- /.box -->

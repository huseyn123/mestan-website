<div class="modal fade" id="edit-activeDiscount" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        {!! Form::open(['route' => 'product.activeDiscount', 'method'=> 'PUT', 'class'=>'warning-modal form-inline dtForm']) !!}
        <div class="modal-content">
            <div class="modal-header">
                {!! Form::button('&times;', ['class' => 'close', 'data-dismiss' => 'modal', 'aria-hidden' => "true"]) !!}
                <h4 class="modal-title" id="myModalLabel">Endirimi Aktiv Et</h4>
            </div>

            <div class="form-group form-class">
                <label for="discount_active" class="col-md-4 control-label"> Aktiv Et</label>
                <div class="col-md-4"><input name="discount_active" @if($d_active->value == 1) checked @endif type="checkbox" value="1" id="discount_active"></div>
            </div>

            {!! Form::button('Təsdiq et', ['class' => 'btn btn-success loadingButton', 'type' => 'submit', 'data-loading-text' => loading()]) !!}
        </div>
        {!! Form::close() !!}
    </div>
</div>



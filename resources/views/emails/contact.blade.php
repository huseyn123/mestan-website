@component('mail::message')

    @if(isset($content['text']))
        #{{$content['text']}}
    @endif

    İstifadəçinin məlumatları:
    @if(isset($content['full_name']))
        - Ad, soyad: {{$content['full_name']}}
    @endif

    @if(isset($content['email']))
        - Email: {{$content['email']}}
    @endif

    @if(isset($content['phone']))
        - Telefon: {{$content['phone']}}
    @endif

    @if(isset($content['subject']))
        - Mövzu: {{$content['subject']}}
    @endif

@endcomponent
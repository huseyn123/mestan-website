@extends ('layouts.web', ['page_heading' => $page->name])

@section ('content')

    @include('web.elements.breadcrumbs')

    <section class="flat-partners">
        <div class="container">
            <div class="partners-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <div class="flat-row-title">
                            <h3>{{$page->name}}</h3>
                        </div>
                    </div><!-- /.col-md-12 -->
                    <div class="col-md-12">
                        <div class="partners-list grid">
                            @foreach($partners as $partner)
                                @if($partner->getFirstMedia())

                                    <div class="partners-item">
                                        <a href="#" class="box-cat" title="">
                                            <div class="partners-image">
                                                <img src="{{asset($partner->getFirstMedia()->getUrl())}}">
                                            </div>
                                            <div class="partners-name">
                                                {{$partner->name}}
                                            </div>
                                        </a>
                                    </div><!-- /.partners-item -->

                                @endif
                            @endforeach
                        </div>
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.partners-wrap -->
        </div><!-- /.container -->
    </section><!-- /.flat-partners -->

    @include('web.elements.best_seller')

@endsection

@extends ('layouts.web', ['page_heading' => $page->name])

@section ('content')

    @include('web.elements.breadcrumbs')

    <section class="flat-about">
        <div class="container">
            <div class="about-wrap">
                <div class="row">
                    <div class="col-lg-12">

                        {!! $page->content !!}

                    </div><!-- /.col-lg-12 -->
                </div><!-- /.row -->
            </div><!-- /.about-wrap -->
        </div><!-- /.container -->
    </section><!-- /.flat-about -->

    @include('web.elements.best_seller')


@endsection

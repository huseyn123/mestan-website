@extends ('layouts.web', ['page_heading' => $page->name])

@section ('content')
    @include('web.elements.breadcrumbs')
    <!-- Faq Begin -->
    <section class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="rules">
                    @foreach($faq as $q)
                        <article>
                            <div class="head">
                                <span class="open"><i class="fa fa-plus" aria-hidden="true"></i></span>
                                <h2>{{ $q->title }}</h2>
                            </div>
                            <div class="body">
                                <p>{{ $q->text }}</p>
                            </div>
                        </article>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- Faq End -->
@endsection
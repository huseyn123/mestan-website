<div class="container">
    <form action="{{ route('order.post') }}" method="post" accept-charset="utf-8" id="checkout_post">
        @csrf

        <div class="row">
            <div class="col-lg-9">
                <div class="shop-checkout-wrap">
                    <div class="flat-row-title">
                        <h3>{{$dictionary['order_address'] ?? 'Göndəriləcək ünvanım'}}</h3>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group required">
                                <label for="street" class="control-label">{{$dictionary['address_text'] ?? 'Adress'}}</label>
                                <input id="street" type="text" class="customer-form" placeholder="{{$user ? $user->address : ''}}"  name="address" value="{{$data['adress']}}" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group required">
                                <label for="phone" class="control-label">{{$dictionary['phone'] ?? 'Telefon'}}</label>
                                <input id="phone" type="text" class="customer-form maskphone" placeholder="{{$user ? $user->phone : ''}}" name="phone" value="{{$data['phone']}}" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group required">
                                <label for="fullname" class="control-label">{{$dictionary['full_name'] ?? 'Ad Soyad'}}</label>
                                <input id="fullname" type="text" class="customer-form" placeholder="{{$user ? $user->name : ''}}" name="fullname" value="{{$data['fullname']}}" required>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group required">
                                <label for="delivery" class="control-label">Çatdırılma</label>

                                <select class="delivery-select" id="delivery" name="delivery_price" required>
                                    <option value="">Seçin...</option>
                                    @foreach($deliveryPrice as $dP)
                                        <option value="{{$dP->price}}" @if($delivery_value == $dP->price) selected @endif>{{$dP->adress}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group required">
                                <label  for="Desc" class="control-label">{{$dictionary['inv_desc'] ?? 'Qeyd'}}</label>
                                <textarea class="customer-form" name="desc" id="description">{{$data['description']}}</textarea>
                            </div>
                        </div>
                    </div><!-- /row -->
                </div><!-- /.shop-checkout-wrap -->
                <div class="shop-payment-wrap">
                    <div class="flat-row-title">
                        <h3>{{$dictionary['order_type'] ?? 'Ödəniş növünü seçin'}}</h3>
                    </div>
                    <div class="row">
                        <div class="col-12 payment-box">
                            <label for="payCard" class="payment-method coming-soon">
                                <div class="radio-input">
                                    <input id="payCard" type="radio" name="paymentMethod" disabled required  value="card">
                                    {{$dictionary['card_text'] ?? 'Card'}}
                                </div>
                                <div class="card-logos">
                                    <img src="{{asset('images/logos/payment-logo1.png')}}" >
                                    <img src="{{asset('images/logos/payment-logo2.png')}}" >
                                </div>
                            </label>
                            <label for="payCash" class="payment-method active">
                                <div class="radio-input">
                                    <input id="payCash" type="radio" name="paymentMethod" value="cash" checked required>
                                    {{$dictionary['order_type_2'] ?? 'Nağd ödəniş'}}
                                </div>
                                <img src="{{asset('images/icon/checkout-cash.png')}}" >
                            </label>
                        </div>
                    </div><!-- /row -->
                </div><!-- /.shop-checkout-wrap -->
            </div><!-- /.col-lg-9 -->
            <div class="col-lg-3">
                <div class="cart-totals">
                    <h3>Sifarişlərim ({{$carts->count()}} məhsul)</h3>
                    <table>
                        <tbody>
                        <tr>
                            <td>Toplam</td>
                            <td class="subtotal">{{$sum}} AZN</td>
                        </tr>
                        @if(isset($discounted_price))
                            <tr>
                                <td>{{$dictionary['delivery'] ?? 'Çatdırılma'}}</td>
                                <td class="delivery"><span class="d_price">{{$discounted_price}}</span> AZN</td>
                            </tr>
                        @endif
                        @if($special_percent > 0 && $special_price > 0)
                            <tr>
                                <td>{{ $birthday_status ? 'Ad günün endirimi' : ($dictionary['special_price'] ?? 'Endirim') }}</td>
                                <td class="special"><span class="s_price">{{$special_percent.'%'}}</td>
                            </tr>
                        @endif
                        @if($show_promo && $promo_price > 0 && $promo_percent)
                            <tr>
                                <td>{{$dictionary['special_price'] ?? 'Endirim'}}</td>
                                <td class="special"><span class="s_price">{{$promo_percent.'%'}}</td>
                            </tr>
                        @endif

                        <tr>
                        <td class="price-total-title">{{$dictionary['sum_p'] ?? 'Cəmi'}}</td>
                        <td class="price-total"><span class="p_t">{{$total}}</span> AZN</td>
                        </tr>

                        </tbody>
                    </table>
                    @if($user && $show_promo)
                        <div class="coupon-input">
                            <input type="text" name="promo_code" placeholder="Endrim kodunuzu yazın" value="{{$promo_code}}" id="promo_code" autocomplete=”off”>
                            <a  type="submit" class="check_promo" >{{$dictionary['promo_code_text'] ?? 'tətbiq et'}}</a>
                        </div>
                    @endif
                    {{--<ul class="box-checkbox">--}}
                    {{--<li class="check-box custom-control-right">--}}
                    {{--<input type="checkbox" id="checkpresent" name="checkpresent">--}}
                    {{--<label class="custom-control-label" for="checkpresent">Hədiyyə paketi olunsun</label>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                    <div class="btn-cart-totals">

                        <button type="submit" class="checkout">{{ $dictionary['complete_order'] ?? 'Sifarişi rəsmiləşdir' }} <img src="{{asset('images/icon/next-white.svg')}}"></button>

                  <!--       @if(auth()->guard('web')->check())
                        @else
                            <a href="JavaScript:Void(0);" class="checkout @if(!auth()->guard('web')->check()) open-popup @endif">{{ $dictionary['complete_order'] ?? 'Sifarişi rəsmiləşdir' }}<img src="{{asset('images/icon/next-white.svg')}}"></a>
                        @endif -->
                    </div>
                </div><!-- /.cart-totals -->
            </div><!-- /.col-lg-3 -->
        </div><!-- /.row -->


        <div id="checkout-popup" class="popup-window d-none">
            <div class="mask"></div>
            <div class="wrapper clearfix">
                <a href="javascript:void(0)" class="close-popup"></a>
                <img src="{{asset('images/other/note.jpg')}}" alt="">
                <!-- <h2 class="popup-title">Sifarişi təsdiqləmək istəyirsiz?</h2> -->
                <!-- <div class="actions">
                    <input type="submit" value="Təsdiqlə" class="large fluid button">
                </div> -->
            </div>
       </div><!-- /#checkout-popup -->

    </form><!-- /form -->
</div><!-- /.container -->
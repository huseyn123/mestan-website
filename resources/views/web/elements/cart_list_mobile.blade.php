<div class="icon-container">
    <i class="icon navigation-icon-basket"></i>
</div>

<div class="basket-item-count">
    <span id="basketItemCount">{{$carts->count()}}</span>
</div>

@if($carts->count())
    <div class="dropdown-box">
        <div class="basket-title">
            {!!  str_replace(":count",'<span>'.$carts->count().'</span>', $dictionary['basket_product_count']) !!}
        </div>
        <ul>
            @foreach($carts as $cart)
                <li>
                    @if($cart->getFirstMedia())
                        <div class="img-product">
                            <img src="{{ asset($cart->getFirstMedia()->getUrl('basket'))}}">
                        </div>
                    @endif
                    <div class="info-product">
                        <div class="name">
                            {{$cart->name}}
                        </div>
                        <div class="count">
                            Ədəd: <span>{{$cart->count}}</span>
                        </div>
                        <div class="price">
                            <span>  {{$cart->amount}} <span>M</span></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </li>
            @endforeach
        </ul>
        <div class="btn-cart">
            <a href="{{route('cart.index')}}" class="view-cart" title="">{{$dictionary['get_basket'] ?? 'Səbətə Get'}}</a>
            @if(auth()->guard('web')->check())
                <form action="{{ route('order.post') }}" method="post" accept-charset="utf-8">
                    {{ csrf_field() }}
                    <button type="submit" class="check-out" >{{$dictionary['order_now'] ?? 'Sifariş ver'}}</button>
                </form>

            @else
                <a href="JavaScript:Void(0);" class="check-out @if(!auth()->guard('web')->check()) open-popup @endif">{{$dictionary['order_now'] ?? 'Sifariş ver'}}</a>
            @endif
        </div>
    </div><!-- /.dropdown-box -->
@endif

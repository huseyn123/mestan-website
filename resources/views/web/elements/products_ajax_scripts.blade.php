<script>

    $(".sort select" ).change(function() {
        ProductFiltr();
    });

    $( "#slider-range" ).slider({
        range: true,
        min: 0,
        max: 1000,
        values: [{{$min_price}}, {{$max_price}}],
        slide: function( event, ui ) {
            $( "#amount" ).val( ui.values[ 0 ] + "M" + " - " + ui.values[ 1 ] + "M" );
        },
        stop: function( event, ui ) {
            $( "#amount" ).val( ui.values[ 0 ] + "M" + " - " + ui.values[ 1 ] + "M" );
            ProductFiltr();
        }
    });
    $( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) + "M" + " - " + $( "#slider-range" ).slider( "values", 1 ) + "M" );

    $(document).on('click', '.blog-pagination li.pag a', function(event){
        event.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        ProductFiltr($("#slider-range" ).slider( "values",0),$( "#slider-range").slider( "values",1),page);
    });

    function ProductFiltr(min = $("#slider-range" ).slider( "values",0),max = $( "#slider-range").slider( "values",1),page = {{$paginate}}){
        $('.preloader').show();
        var filtr = [];
        var limit = $('.sort select[name$="limit"]').val();
        var sort = $('.sort select[name$="sort"]').val();
        if(limit && limit != 24){
            filtr.push({'name':'limit','val':limit});
        }
        if(sort !== 'null' && sort != 0){
            parametr='?sort='+sort;

        }else{
            parametr='?sort';
        }
        range1 = min;
        range2 = max;



        filtr.push({'name':'min','val':range1},{'name':'max','val':range2});
        $.each($('.product_filtr'), function(){
            if($(this).is(':checked') === true){
                filtr.push({'name':$(this).attr('name')+encodeURI('[]'),'val' : $(this).val()});
            }
        });
        filtr.push({'name':'page','val':page});

        if (filtr.length > 0 && history.pushState) {
            $.each(filtr,function (key,value) {
                parametr +='&'+value.name+'='+value.val;
            });
            var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + parametr;
            window.history.pushState({path:newurl},'',newurl);

            $.ajax({
                url: newurl,
                type: 'GET',
                dataType: "json",

                beforeSend: function(response) {
                },
                success: function(response){
                    $('.main-shop .product_list').html(response.html);
                    document.body.scrollTop = 70;
                    document.documentElement.scrollTop = 70;
                    $('.preloader').hide();
                },
                error: function(res){
                    $('.preloader').hide();
                }

            });

        }
    }

    $('.product_filtr').change(function(){
        ProductFiltr();
    });


</script>
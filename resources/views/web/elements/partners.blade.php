@if($partners->count())

    <section class="flat-row flat-brand">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <ul class="brand-carousel">
                        @foreach($partners as $partner)
                            @if($partner->getFirstMedia())
                                <li>
                                    <img src="{{asset($partner->getFirstMedia()->getUrl())}}">
                                </li>
                                @endif
                        @endforeach
                    </ul><!-- /.brand-carousel -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-brand -->

@endif

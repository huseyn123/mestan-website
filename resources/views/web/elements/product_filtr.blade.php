<div class="widget">
    <div class="widget-title dropdown-title active">
        <h3>{{$dictionary['price_range'] ?? 'Qiymət aralığı'}}</h3>
    </div>
    <div class="widget-content">
        <p>{{$dictionary['price'] ?? 'Qiymət'}} </p>
        <div class="price search-filter-input">
            <div id="slider-range" class="price-slider ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">
                <div class="ui-slider-range ui-corner-all ui-widget-header"></div>
                <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
                <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
            </div>
            <p class="amount">
                <input type="text" id="amount" disabled="">
            </p>
        </div>
    </div>
</div><!-- /.widget widget-price -->


<div class="widget">
    <div class="widget-title dropdown-title active">
        <h3>{{$dictionary['brands'] ?? 'Markalar'}}</h3>
        <i class="widget-arrow fa fa-chevron-right"></i>
    </div>
    <div class="widget-content">
        <ul class="box-checkbox">

            @foreach($brands as $key=>$value)
                <li class="check-box">
                    <input type="checkbox" id="brand{{$loop->iteration}}" name="brand" class="product_filtr"  value="{{$key}}" @if(isset($selected_brands) && count($selected_brands) && in_array($key,$selected_brands)) checked @endif>
                    <label for="brand{{$loop->iteration}}">{{$value}}</label>
                </li>
            @endforeach

        </ul>
    </div>
</div><!-- /.widget widget-brands -->




@foreach($category_filters as $value)
    <div class="widget">
        <div class="widget-title dropdown-title active">
            <h3>{{ $value->title}}</h3>
            <i class="widget-arrow fa fa-chevron-right"></i>
        </div>
        @if($value->filter_value)
            <div class="widget-content">
                <ul class="box-checkbox">
                    @foreach($value->filter_value as $key=>$select)
                            <li class="check-box">
                                <input 
                                    type="checkbox" 
                                    id="{{$select->id}}" 
                                    name="{{$value->filter_column}}" 
                                    class="product_filtr" 
                                    value="{{$select->id}}"
                                    @if( isset($product_form[$value->filter_column]) && in_array($select->id, $product_form[$value->filter_column])) checked @endif>
                                <label for="{{$select->id}}">{{$select->title}}</label>
                            </li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
@endforeach
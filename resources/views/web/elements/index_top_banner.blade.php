@if($top_banners->count() && $user_type !=2)

    <section class="flat-row flat-banner-box">
        <div class="container">
            <div class="row">
                @foreach($top_banners as $top_banner)

                     <div class="col-md-6">
                        <div class="banner-box">
                            <div class="box-image">
                                @if($top_banner->getFirstMedia())
                                    <a  href="{{route('showPage',[$top_banner->page_slug,$top_banner->slug])}}" title="{{$top_banner->name}}">
                                        <img src="{{asset($top_banner->getFirstMedia()->getUrl('blade'))}}" >
                                    </a>
                                @endif
                            </div><!-- /.box-image -->
                            <div class="box-content">
                                <div class="price">
                                    @if($top_banner->price)<span class="sale-icon">{{$dictionary['sale_grid'] ?? 'SALE'}}</span>@endif
                                    <span class="regular">{{$top_banner->old_price}}  <span>m</span></span>
                                    @if($top_banner->price)<span class="sale">{{$top_banner->price}} <span>M</span></span>@endif
                                </div>
                                <div class="product-name">
                                    <a href="{{route('showPage',[$top_banner->page_slug,$top_banner->slug])}}" title="{{$top_banner->name}}">{{$top_banner->name}}</a>
                                </div>
                                <div class="product-desc">
                                    <p>{{$top_banner->summary}}</p>
                                </div>
                            </div><!-- /.box-content -->
                        </div><!-- /.banner-box -->
                     </div><!-- /.col-md-6 -->

                @endforeach

            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-banner-box -->

@endif

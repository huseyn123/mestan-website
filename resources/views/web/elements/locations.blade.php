
<div id="stores-popup" class="popup-window d-none">
    <div class="mask"></div>
    <div class="wrapper clearfix">
        <a href="javascript:void(0)" class="close-popup"></a>
        <h2 class="popup-title">{{$dictionary['shops'] ?? 'Mağazalarımız'}}</h2>
        <div class="store-list">
            <div class="store-info">
                <h3>{{$dictionary['main_shop'] ?? 'Əsas Mağaza'}}</h3>
                <p>{{$dictionary['location1'] ?? 'Nəsimi rayonu, Puşkin k. 32B, Baku, Azerbaijan AZ1130'}}</p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-9">
        <div class="shop-cart-wrap">
            <div class="flat-row-title">
                <h3>Sifarişlərim</h3>
            </div>

            @if($carts->count())

                <div class="table-cart">
                    <table>
                        <tbody>

                        @foreach($carts as $cart)

                            <tr>
                                <td>
                                    @if($cart->getFirstMedia())
                                        <div class="img-product">
                                            <img src="{{asset($cart->getFirstMedia()->getUrl('basket'))}}">
                                        </div>
                                    @endif

                                    <div class="name-product">
                                        {{$cart->name}}
                                    </div>

                                    <div class="product-id">
                                        Malın kodu: {{$cart->bar_code}}
                                    </div>
                                    <div class="clearfix"></div>
                                </td>
                                <td>
                                    <div class="quantity-box cart_count_v">
                                        <select name="quantity">
                                            @for ($i = 1; $i <= (($cart->stock > 10)  ? 10 : $cart->stock); $i++)
                                                <option @if($cart->count === $i) selected @endif value="{{$i}}" data-id="{{$cart->cart_product_id}}">{{$i}}</option>
                                                The current value is {{ $i }}
                                            @endfor
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="total">
                                        {{$cart->amount}}<span>m</span>
                                    </div>
                                    <div class="price">
                                        {{$cart->cart_price}}<span>m</span>
                                    </div>
                                </td>
                                <td>
                                    <a href="javascript:void(0)" data-id="{{$cart->cart_product_id}}" class="delete_p_cart">
                                        <img src="{{asset('images/icon/delete.svg')}}">
                                    </a>
                                </td>
                            </tr>

                        @endforeach


                        </tbody>
                    </table>
                </div>

            @endif

        </div><!-- /.shop-cart-wrap -->
    </div><!-- /.col-lg-9 -->
    @if($carts->count())
        <div class="col-lg-3">
            <div class="cart-totals">
                <h3>Sifarişlərim ({{$carts->count()}} məhsul)</h3>
                <form action="{{ route('cart.checkout') }}" method="post" accept-charset="utf-8">
                    {{ csrf_field() }}
                    <table>
                        <tbody>
                        <tr>
                            <td>Toplam</td>
                            <td class="subtotal">{{$sum}} AZN</td>
                        </tr>
                        </tbody>
                    </table>
                    {{--<ul class="box-checkbox">--}}
                    {{--<li class="check-box custom-control-right">--}}
                    {{--<input type="checkbox" id="checkpresent" name="checkpresent">--}}
                    {{--<label class="custom-control-label" for="checkpresent">Hədiyyə paketi olunsun</label>--}}
                    {{--</li>--}}
                    {{--</ul>--}}

                        <div class="btn-cart-totals">
                            @if(auth()->guard('web')->check())
                                <button type="submit" class="checkout" >{{ $dictionary['complete_order'] ?? 'Sifarişi rəsmiləşdir' }}</button>
                            @else
                                <a href="JavaScript:Void(0);" class="checkout @if(!auth()->guard('web')->check()) open-popup @endif">{{ $dictionary['complete_order'] ?? 'Sifarişi rəsmiləşdir' }}<img src="{{asset('images/icon/next-white.svg')}}"></a>
                            @endif
                        </div>

                </form><!-- /form -->

            </div><!-- /.cart-totals -->
        </div><!-- /.col-lg-3 -->
    @endif
</div><!-- /.row -->
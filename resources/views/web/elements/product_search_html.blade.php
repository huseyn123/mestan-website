<!-- @if($search_products && $search_products->count()) -->

    <div class="title">
        <!-- {{$dictionary['search_result'] ?? 'Axtarış təklifləri'}} -->
    </div>

    <ul>
        @foreach($brands as $brand)

            <li>
                <a href="{{route('brand.resume',[$brand->id])}}">
                    <div class="info-product">
                        <div class="name">
                            <p>{{$brand->title}} <span>{{$dictionary['search_brand_title'] ?? 'Brend'}}</span></p>
                        </div>
                    </div>
                </a>
            </li>

        @endforeach

        @foreach($search_products as $product)
            <li>
                <a href="{{route('showPage',[$product->page_slug,$product->slug])}}">

                    @if($product->getFirstMedia())

                        <div class="image">
                            <img src="{{asset($product->getFirstMedia()->getUrl('search'))}}">
                        </div>

                    @endif

                        <div class="info-product">
                            <div class="name">
                                {{$product->name}}
                            </div>
                            <div class="price">

                                <span class="sale">
                                    {{$product->price ? $product->price : $product->old_price}} <span>M</span>
                                </span>

                                @if($product->price)
                                    <span class="regular">
                                    {{$product->old_price}} <span>m</span>
                                    </span>
                                @endif

                            </div>
                        </div>

                </a>

            </li>
        @endforeach
    </ul>
<!-- @endif -->




<div id="checkout-popup" class="popup-window d-none">
    <div class="mask"></div>
    <div class="wrapper clearfix">
        <a href="javascript:void(0)" class="close-popup"></a>
        <form action="/checkout" id="checkout-form" method="POST" autocomplete="off">
            <h2 class="popup-title">Sifarişi təsdiqləmək istəyirsiz?</h2>
            <div class="actions">
                <input type="submit" value="Təsdiqlə" class="large fluid button">
            </div>
        </form>
    </div>
</div>


<div id="callme" class="popup-window d-none">
    <div class="mask"></div>
    <div class="wrapper clearfix">
        <a href="javascript:void(0)" class="close-popup"></a>
        {!! Form::open(['url'=>route('post.form'), 'method'=>'POST', 'id' => 'callme-form','autocomplete' => "off"]) !!}

            {{ Form::hidden('method', 'callme') }}
            <h2 class="popup-title">{{$dictionary['help'] ?? 'Kömək'}}</h2>
            <p>{{$dictionary['help_text1'] ?? 'Sizə kömək göstərməkdən məmnunluq duyarıq'}} </p>
            <p>{{$dictionary['help_text2'] ?? 'Zəhmət olmazsa adınızı, soyadınızı və əlaqə nömrənizi qeyd edin tez zamanda sizinlə əlaqə saxlanılacaq'}} </p>
            <div class="mb10">
                {!! Form::text('full_name', null, ["class" => "required","placeholder" => $dictionary['full_name'],'required' => 'required']) !!}
            </div>
            <div class="mb10">
                {!! Form::text('phone', null, ["class" => "required maskphone","placeholder" => $dictionary['phone'],'required' => 'required']) !!}
            </div>
            <ul class="form-result d-none" data-error="Xəta baş verdi! Zəhmət olmasa az sonra təkrar cəhd edin!"></ul>
            <div class="actions">
                <input type="submit" value="Göndər" class="large fluid button">
            </div>
            <div class="popup-footer clearfix">
                <p>{{$dictionary['help_text3'] ?? 'Bu məlumatlar başqa heç bir yerdə istifadə edilməyəcək!'}}</p>
            </div>
            <div class="popup-result invisible">
                <div class="no-data">
                    <i class="fa fa-thumbs-up icon-checkout"></i>
                    <p>{!! $dictionary['help_text4'] ?? 'Sorğunuz göndərildi! <br>  Tezliklə sizinlə əlaqə saxlanılacaq.' !!}</p>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>



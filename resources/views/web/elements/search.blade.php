<div class="search">
    <form action="{{ route('search') }}" method="GET">
        <div class="group">
            <select name="category">
                <option value="">{{ $dictionary['categories'] ?? 'Bütün çeşidlər' }}</option>
                @foreach($menu as $cat)
                    @if($cat->template_id == 5 && !is_null($cat->parent_id))
                        <option value="{{ $cat->id }}" @if($cat->id == request()->get('category')) selected @endif>{{ $cat->name }}</option>
                    @endif
                @endforeach
            </select>
            <input type="text" name="keyword" placeholder="{{ $dictionary['search'] or 'Axtar' }}" autocomplete="off">
        </div>
        <button type="submit"></button>
    </form>
</div>
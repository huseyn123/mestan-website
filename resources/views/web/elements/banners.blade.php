@if(array_key_exists($type, $banner))
    <div class="banner">
        <a href="@if(!is_null($banner[$type]['forward_url']))http://{{$banner[$type]['forward_url']}}@else javascript:void(0) @endif" @if($banner[$type]['target'] == 0) target="_blank" @endif>
            <img src="{{ asset('storage/'.$banner[$type]['banner_url']) }}" class="img-responsive">
        </a>
    </div>
@endif
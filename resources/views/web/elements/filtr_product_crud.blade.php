@foreach($products as $product)

    <div class="{{ isset($index_form) ? 'col-sm-6 col-lg-3' :  'col-6 col-sm-6 col-lg-4'}}">
    <div class="product-box">
        <div class="imagebox">

            @if($product->price) <span class="item-sale">{{$product->percent ? $product->percent.'%' : $dictionary['sale_grid'] ?? 'SALE'}} </span> @endif

            @if($product->getFirstMedia())
                <div class="box-image">
                    <a href="{{route('showPage',[$product->page_slug,$product->slug])}}" title="{{$product->name}}">
                        <img src="{{asset($product->getFirstMedia()->getUrl('blade'))}}" >
                    </a>
                </div><!-- /.box-image -->
            @endif
             <div class="box-info">
                @if($product->count == 0)<div class="out-of-stock"><h4>{{$dictionary['no_product'] ?? 'Stokda yoxdur'}} </h4></div>
                    @elseif($product->count > 0 && $product->count < config('config.produc_stock_count.crud'))<div class="limited-quantity"><h4>{{$dictionary['product_limited'] ?? 'Məhdud sayda'}}</h4></div>
                @endif
            </div><!-- /.box-info -->
            <div class="box-content">
                <div class="product-name">
                    <a href="{{route('showPage',[$product->page_slug,$product->slug])}}" title="{{$product->name}}">{{$product->name}}</a>
                </div>
            </div><!-- /.box-content -->
            <div class="box-bottom">
                <div class="price">
                    <span class="sale">{{$product->price ? $product->price : $product->old_price }} <span>M</span></span>
                    @if($product->price) <span class="regular">{{$product->old_price}} <span>M</span></span> @endif
                </div>
                <div class="wishlist-cart">
                    <a  href="#" class="btn wishlist @if($product->whish_list) active @endif @if(!auth()->guard('web')->check()) open-popup @endif" data-id="{{$product->id}}"></a>
                    <a href="{{ route('cart.index') }}" class="btn @if($product->count && $product->count > 0) btn-add-cart @else out-of-stock @endif   @if($product->basket && $product->basket->count) active @endif"  data-id="{{$product->id}}"></a>
                </div>
            </div><!-- /.box-bottom -->
        </div><!-- /.imagebox -->
    </div><!-- /.product-box -->
</div><!-- /.col-lg-4 col-sm-6 -->

@endforeach

<!-- Pagination Begin -->
{!! $products->appends(request()->input())->links('vendor.pagination.default') !!}
<!-- Pagination End -->
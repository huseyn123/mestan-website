<script>

    $.mask.definitions['9'] = '';
    $.mask.definitions['d'] = '[0-9]';
    $(".maskphone").mask("+(994 dd) ddd dd dd");
 

    $("#update_user").on("submit", function(e) {
        e.preventDefault();
        $('.preloader').show();

        var form = $(this);
        var submitable = true;



        if (submitable) {
            $("body").addClass("on-progress");

            $.ajax({
                url:  form.attr('action'),
                type: form.attr('method'),
                data: new FormData(form[0]),
                dataType: "json",
                processData: false,
                contentType: false,

                error: function(error) {
                    $("body").removeClass("error");
                    form.find("#update_message").addClass('text-danger');
                    form.find("#update_message").html(error);
                    $('.preloader').hide();
                },

                success: function(resp) {
                    form.find("#update_message").removeClass();
                    if (resp.result == 1) {
                        form.find("#update_message").addClass('text-success');

                    } else {
                        $("body").removeClass("on-progress");
                        form.find("#update_message").addClass('text-danger');
                    }
                    form.find("#update_message").html(resp.msg);
                    $('.preloader').hide();
                }
            });
        }

        return false;
    });

    $("#login-form").on("submit", function(e) {
        e.preventDefault();
        $('.preloader').show();
        var form = $(this);
        var submitable = true;
        $(":input.required", form).each(function() {
            if (!$(this).val()) {
                $(this).addClass("error");
                submitable = false;
            }
        });


        if (submitable) {
            $("body").addClass("on-progress");

            $.ajax({
                url:  form.attr('action'),
                type: form.attr('method'),
                data: new FormData(form[0]),
                dataType: "json",
                processData: false,
                contentType: false,

                error: function(error) {
                    $("body").removeClass("error");
                    form.find(".form-result").text(form.find(".form-result").data("error"));
                    form.find(".form-result").removeClass("d-none");
                    form.find(".form-result").html("");
                    form.find(".form-result").append("<li>"+error+"</li>");
                    $('.preloader').hide();
                },

                success: function(resp) {
                    if (resp.result == 1) {
                        if (resp.route) {
                            window.location.href = resp.route;
                        } else {
                            window.location.reload(true);
                        }
                    } else {
                        $("body").removeClass("on-progress");
                        form.find(".form-result").text(form.find(".form-result").data("error"));
                        form.find(".form-result").removeClass("d-none");
                        form.find(".form-result").html("");
                        form.find(".form-result").append("<li>"+resp.error+"</li>");
                        $('.preloader').hide();
                    }
                }
            });
        }

        return false;




    });

    $("#signup-form").on("submit", function(e) {
        e.preventDefault();
        $('.preloader').show();
        var form = $(this);
        var submitable = true;
        var data = new FormData(form[0]);

        $(":input.required", form).each(function() {
                if (!$(this).val()) {
                    $(this).addClass("error");
                    submitable = false;
                }
            });

            if (submitable) {
            //     $("body").addClass("on-progress");

                $.ajax({
                    url:  form.attr('action'),
                    type: form.attr('method'),
                    data: new FormData(form[0]),
                    dataType: "json",
                    processData: false,
                    contentType: false,

                    error: function(error) {
                        $("body").removeClass("error");
                        form.find(".form-result").text(form.find(".form-result").data("error"));
                        form.find(".form-result").removeClass("d-none");
                        form.find(".form-result").html("");
                        form.find(".form-result").append("<li>"+error+"</li>");
                        $('.preloader').hide();
                    },

                    success: function(resp) {
                        if (resp.result == 1) {
                            if (resp.route) {
                                window.location.href = resp.route;
                            } else {
                                window.location.reload(true);
                            }
                        } else {
                            $("body").removeClass("on-progress");
                            form.find(".form-result").text(form.find(".form-result").data("error"));
                            form.find(".form-result").removeClass("d-none");
                            form.find(".form-result").html("");
                            form.find(".form-result").append("<li>"+resp.error+"</li>");
                            $('.preloader').hide();
                        }
                    }
                });
            }

            return false;
        });

    $("body").on("click", ".open-popup", function() {
        if ($($(this).data("popup")).length == 1) {
            $("html").addClass('popup-open');
            $($(this).data("popup")).removeClass('d-none');

            $(".dropdown.open").removeClass("open");

            return false;
        }
    });

    function addToCart(element,product_id,$type,count = 1){
        $.ajax({
            url:  '/cart',
            type: 'POST',
            dataType: "json",
            data:{count:count,product:product_id},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },

            error: function(error) {
                if($type == 0){
                    element.removeClass('preload');

                }else{
                    $(".btn-add-bs").removeClass('on-progress');
                }
            },

            success: function(resp) {
                if(resp.result == 1){
                    $('.basket-button-container').html(resp.html);
                    if($type == 0){
                        $('a.btn-add-cart[data-id="'+product_id+'"]').addClass('active');
                        $('a.btn-add-cart[data-id="'+product_id+'"]').attr("href", '/cart');
                        element.removeClass('preload');
                    }else{
                        $(".btn-add-bs").addClass("d-none");
                        $(".btn-added-bs").removeClass("d-none");
                        $(".btn-added-bs").find('p.message').html(resp.msg);
                        $(".btn-add-bs").removeClass('on-progress');
                    }
                }else{
                    if($type == 0){}else{
                        $(".btn-add-bs").removeClass('on-progress');
                        $(".btn-add-bs").removeClass('on-progress');
                        $(document).ready( function() {
                            toastr.error(resp.msg);
                        });
                    }
                }
            }
        });
    }

    function removeFromCart(element,product_id,$type){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url:  '/cart/'+product_id,
            type: 'delete',
            dataType: "json",
            data:{id:product_id,type:$type},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            error: function(error) {
                if($type == 0){
                    $('.btn-added-bs').removeClass('on-progress');
                }else{
                    $('.preloader').hide();
                }
            },
            success: function(resp) {
                $('.basket-button-container').html(resp.cart_list_header);

                if(resp.result == 1){
                    if($type == 0){
                        $(".btn-add-bs").removeClass("d-none");
                        $(".btn-added-bs").addClass("d-none");
                        $('.btn-added-bs').removeClass('on-progress');
                    }else if($type == 1){
                        $('.cart_d_table').html(resp.cart_list_page);
                        $('.preloader').hide();
                    }
                }
            }
        });
    }

    //Add To Cart With Grid
    $('body').on("click", ".btn-add-cart",function (e) {
        var _this =$(this);
        if(!$(this).hasClass("active")){
            _this.addClass('preload');
            e.preventDefault();
            product_id = $(this).attr('data-id');
            addToCart(_this,product_id,0);
        }
    });

    //Add To Cart ProductSingle
    $("body").on("click", ".js-submit-trigger", function() {
        var _this = $(this);
        var count = $(":input[name='quantity']").val();
        product_id = $(this).attr('data-id');
        $('.btn-add-bs').addClass('on-progress');
        addToCart(_this,product_id,1,count);
    });

    //Add To WhishList
    $('body').on('click','.wishlist',function (e) {
        e.preventDefault();
        var _this = $(this);

        if ($(this).hasClass("open-popup")) {
            $("#logreg").removeClass('d-none');
        }else{
            _this.addClass('preload');
            product_id = $(this).attr('data-id');
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:  '/wishlist',
                type: 'POST',
                dataType: "json",
                data:{product_id:product_id},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                error: function(error) {
                    _this.removeClass('preload');
                },

                success: function(resp) {
                    _this.removeClass('preload');

                    if(resp.result == 1){
                        _this.addClass('active');
                    }
                    else if(resp.result == 2){
                        _this.removeClass('active');
                    }
                }
            });
        }
    });

    //Remove Cart From ProductSingle
    $("body").on("click", ".js-remove", function() {
        var _this = $(this);
        product_id = $(this).attr('data-id');
        $('.btn-added-bs').addClass('on-progress');
        removeFromCart(_this,product_id,0);
    });

    //Remove Cart From Carts
    $("body").on("click", ".delete_p_cart", function() {
        var _this = $(this);
        product_id = $(this).attr('data-id');
        $('.preloader').show();
        removeFromCart(_this,product_id,1);
    });

    function updateFromCart(value,data_id){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url:  '/cart/'+data_id,
            type: 'put',
            dataType: "json",
            data:{id:data_id,quantity:value},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            error: function(error) {
                $('.preloader').hide();
            },
            success: function(resp) {

                if(resp.result == 1){
                    $('.basket-button-container').html(resp.cart_list_header);
                    $('.cart_d_table').html(resp.cart_list_page);
                    $('.preloader').hide();
                }
            }
        });
    }

    //Update Cart from Carts
    $("body").on('change','div.cart_count_v select',function() {
        var selected = $(this).find('option:selected');
        var data_id = selected.data('id');
        var value = $(this).val();
        $('.preloader').show();
        updateFromCart(value,data_id);
    });

    $('.show_wishlist').on('click',function (e) {
        e.preventDefault();
        var url = $(this).attr('href');

        if ($(this).hasClass("open-popup")) {
            $("#logreg").removeClass('d-none');
        }else{
            window.location.href = url;
        }
    });


    $('body').on('click',".check-out,.checkout",function () {
        var _this = $(this);
        if ($(this).hasClass("open-popup")) {
            $("#startLogin").removeClass('d-none');
        }
    });


//    Search
     $( ".box-search input" ).on('keyup', (function() {

            var value = $(this).val();
            $(".box-suggestions").addClass('on-progress');
            $.ajax({
                url: '{{route('product.search')}}',
                type: 'get',
                dataType: 'json',
                data: {
                    key: value
                },

                error: function(xhr, status,error) {
                    $(".box-suggestions").removeClass('on-progress');
                },

                success: function(resp) {
                    if (resp.result == 1) {
                        $('.box-search div.box-suggestions').html(resp.html);
                        $(".box-suggestions").removeClass('on-progress');
                    }
                }
            });
        }));

//    call me

    $("#callme-form").on("submit", function(e) {
        e.preventDefault();

        var form = $(this);
        var submitable = true;

        $(":input.required", form).each(function() {
            if (!$(this).val()) {
                $(this).addClass("error");
                submitable = false;
            }
        });

        if (submitable) {
            $("body").addClass("on-progress");

            $.ajax({
                url:  form.attr('action'),
                type: form.attr('method'),
                data: new FormData(form[0]),
                dataType: "json",
                processData: false,
                contentType: false,
                error: function() {
                    $("body").removeClass("error");
                    form.find(".form-result").text(form.find(".form-result").data("error"));
                    form.find(".form-result").removeClass("d-none");
                },

                success: function(resp) {
                    $("body").removeClass("on-progress");
                    if (resp.result == 1) {
                        form.find(".popup-result").removeClass('invisible');
                    } else {
                        form.find(".form-result").text(form.find(".form-result").data("error"));
                        form.find(".form-result").removeClass("d-none");
                        form.find(".form-result").html("");
                        form.find(".form-result").append("<li>"+resp.msg+"</li>");
                    }
                }
            });
        }

        return false;
    });


    // user birthday
        $("#user_birthday").on("submit", function(e) {
        e.preventDefault();
        $('.preloader').show();

        var form = $(this);
        var submitable = true;



        if (submitable) {
            $("body").addClass("on-progress");

            $.ajax({
                url:  form.attr('action'),
                type: form.attr('method'),
                data: new FormData(form[0]),
                dataType: "json",
                processData: false,
                contentType: false,

                error: function(xhr, status, error) {
                    $("body").removeClass("error");
                    form.find("#update_message").addClass('text-danger');
                    form.find("#update_message").html(error);
                    $('.preloader').hide();
                     var err = eval("(" + xhr.responseText + ")");

                },

                success: function(resp) {
                    form.find("#update_message").removeClass();
                    if (resp.result == 1) {
                        form.find("#update_message").addClass('text-success');
                    } else {
                        $("body").removeClass("on-progress");
                        form.find("#update_message").addClass('text-danger');
                    }
                    form.find("#update_message").html(resp.msg);
                    $('.preloader').hide();

                }
            });
        }

        return false;
    });


</script>
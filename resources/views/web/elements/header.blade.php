<section id="header" class="header">
    <div class="header-top">
        <div class="container">
            <div class="row">

                <div class="col-md-3">
                <ul class="flat-stores">
                    @include('web.elements.menu', ['parent' => null,'sub' => false,'lang' => $lang,'type' => 'store_link'])
                </ul>
                </div><!-- /.col-md-2 -->

                <div class="col-md-6">
                    @include('web.elements.menu', ['parent' => null,'sub' => false,'lang' => $lang, 'hidden' => [0,1,2,3,4,5,8,9],'type' => 'top_menu'])
                </div><!-- /.col-md-7 -->
                {{--@include('web.elements.login-form')--}}
                <div class="col-md-3">
                    <ul class="flat-unstyled">
                        @if(auth()->guard('web')->check())
                            <li class="account login">
                                <a href="#" title="">{{ auth()->guard('web')->user()->name }}</a>
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                <ul class="unstyled">
                                    <li>
                                        <i class="icon icon-orders"></i>
                                        <a href="{{ route('orders.list') }}">{{$dictionary['my_orders'] ?? 'Sifarişlərim'}}</a>
                                    </li>
                                    <li>
                                        <i class="icon icon-user-info"></i>
                                        <a href="{{ route('user.profile') }}">{{$dictionary['my_info'] ?? 'Məlumatlar'}}</a>
                                    </li>
                                    <li>
                                        <i class="icon icon-logout"></i>
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" >{{ $dictionary['logout'] ?? 'Çıxış'}}</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul><!-- /.unstyled -->
                            </li>
                        @else
                            <li class="account guest">
                                <a href="#" title="">{{$dictionary['login_m_register_m'] ?? 'Giriş/Qeydiyyat'}}</a>
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                <ul class="unstyled">

                                    <li>
                                        <a class="open-popup" href="JavaScript:Void(0);"  data-popup="#logreg" data-form="login" >{{$dictionary['login_m'] ?? 'Giriş'}}</a>
                                    </li>
                                    <li>
                                        <a class="open-popup" href="JavaScript:Void(0);" data-popup="#logreg" data-form="signup" >{{$dictionary['register_m'] ?? 'Qeydiyyat'}}</a>
                                    </li>


                                </ul><!-- /.unstyled -->
                            </li>
                        @endif

                        <!-- @include('web.elements.lang') -->

                    </ul><!-- /.flat-unstyled -->
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.header-top -->

    <div class="header-middle">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div id="logo" class="logo">
                        <a href="{{route('home')}}" >
                            <img src="{{asset('images/logos/logo.png')}}">
                        </a>
                    </div><!-- /#logo -->
                </div><!-- /.col-md-3 -->
                <div class="col-md-6">

                    @include('web.elements.product_search')

                </div><!-- /.col-md-6 -->

                <div class="col-md-3">
                    <div class="user-navigation-container">
                        <ul class="user-navigation">

                            <li class="wishlist-button-container">
                                <div class="icon-container">
                                    <a href="{{ route('wishlist.index') }}" class="show_wishlist @if(!auth()->guard('web')->check()) open-popup @endif"><i class="icon navigation-icon-wishlist"></i></a>
                                </div>
                                <div class="nav-span">{{$dictionary['wishlist'] ?? 'Избранное'}}</div>
                            </li><!-- /.wishlist-button-container -->
                            <li class="basket-button-container">
                                @include('web.elements.cart_list')
                            </li>

                        </ul><!-- /.user-navigation -->
                    </div><!-- /.user-navigation-container -->
                </div><!-- /.col-md-3 -->


            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.header-middle -->



    <div class="header-bottom">
        <div class="container mega-menu-container">
            <div class="row">
                @include('web.elements.menu', ['parent' => null,'sub' => true,'lang' => $lang,'type' => 'catalog_top'])
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.header-bottom -->



    @include('web.elements.mobile-menu')

</section><!-- /#header -->
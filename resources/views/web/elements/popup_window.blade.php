<div id="logreg" class="popup-window d-none">
    <div class="mask"></div>
    <div class="wrapper clearfix">
        <a href="javascript:void(0)" class="close-popup"></a>
        <form action="/login" method="POST" id="login-form" autocomplete="off" class="">
            <input type="hidden" name="action" value="login">
            <input type="hidden" name="ref" value="">
            <h2 class="popup-title">Giriş</h2>
            <p>Zəhmət olmazsa qeydiyyat keçdiyiniz hesaba daxil olun</p>
            <div class="mb15">
                <input type="text" class="required" name="email" value="" placeholder="E-mail">
            </div>
            <div class="mb15">
                <input type="password" class="required" name="password" value="" placeholder="Şifrə">
            </div>
            <div class="actions mb15 clearfix">
                <a class="forgot-password" href="/password-recovery"><img src="images/icon/forgot-password.svg" alt=""> Şifrənizi unutmusuz?</a>
                <input type="submit" value="Daxil ol" class="large fluid button">
                <script>
                    (function(d, s, id) {
                        var js = d.createElement(s),
                            fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) {
                            return;
                        }
                        js.id = id;
                        js.src = "https://connect.facebook.net/en_US/sdk.js#version=v2.2&appId=384235118641188&status=true&xfbml=true";
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                </script>
                <ul class="form-result d-none" data-error="Xəta baş verdi! Zəhmət olmasa az sonra təkrar cəhd edin!"></ul>
                <div class="login-text">
                    <span>Digər metodla daxil olun</span>
                </div>
                <div class="login-buttons-wrapper">
                    <div class="fb-login" onclick="">
                        <img src="images/icon/fb-logo.svg" alt="">
                        <span>Facebook ilə qeydiyat</span>
                    </div>
                </div>
            </div>
            <div class="popup-footer clearfix">
                <p>Artıq hesabınız var?<a href="/signup" class="logreg-form-switch" data-form="signup">Qeydiyyatdan keçin</a></p>
            </div>
        </form>
        <form action="/signup" method="POST" id="signup-form" autocomplete="off" class="d-none">
            <input type="hidden" name="action" value="signup">
            <input type="hidden" name="ref" value="">
            <h2 class="popup-title">Qeydiyyat</h2>
            <p>Zəhmət olmazsa qeydiyyat üçün tələb olunan xanaları doldurun</p>
            <div class="mb10">
                <input type="text" class="required" name="firstname" placeholder="Ad">
            </div>
            <div class="mb10">
                <input type="text" class="required" name="lastname" placeholder="Soyad">
            </div>
            <div class="mb10">
                <input type="text" class="required" name="email" placeholder="E-mail">
            </div>
            <div class="mb20">
                <input type="password" class="required" name="password" placeholder="Şifrə">
            </div>
            <div class="actions mb10 clearfix">
                <span><a href='#' target='_blank'>Qaydalar və Şərtlərimizlə</a> razı olduğunuzu bildirmiş olursuz.</span>
                <input type="submit" value="Qeydiyyatdan keç" class="large fluid button">
                <script>
                    (function(d, s, id) {
                        var js = d.createElement(s),
                            fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) {
                            return;
                        }
                        js.id = id;
                        js.src = "https://connect.facebook.net/en_US/sdk.js#version=v2.2&appId=384235118641188&status=true&xfbml=true";
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                </script>
                <ul class="form-result d-none" data-error="Xəta baş verdi! Zəhmət olmasa az sonra təkrar cəhd edin!"></ul>
                <div class="login-text">
                    <span>Digər metodla daxil olun</span>
                </div>
                <div class="login-buttons-wrapper">
                    <div class="fb-login" onclick="">
                        <img src="images/icon/fb-logo.svg" alt="">
                        <span>Facebook ilə qeydiyat</span>
                    </div>
                </div>
            </div>
            <div class="popup-footer clearfix">
                <p>Artıq hesabınız var?<a href="/login" class="logreg-form-switch" data-form="login">Daxil olun</a></p>
            </div>
        </form>
    </div><!-- /.wrapper -->
</div><!-- /.popup-window -->
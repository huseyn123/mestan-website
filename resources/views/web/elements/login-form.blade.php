<div id="logreg" class="popup-window d-none">
    <div class="mask"></div>
    <div class="wrapper clearfix">
        <a href="javascript:void(0)" class="close-popup"></a>



            <form action="{{ route('login') }}" method="POST" id="login-form" >
                @csrf
            <h2 class="popup-title">{{$dictionary['login_m'] ?? 'Giriş'}}</h2>
            <p>{{$dictionary['login_m_text'] ?? 'Zəhmət olmazsa qeydiyyat keçdiyiniz hesaba daxil olun'}}</p>

            <div class="mb15">
                <input type="text" name="email"  value="{{ old('email') }}"  placeholder="{{ $dictionary['email'] }}" required>
            </div>

            <div class="mb15">
                <input type="password" name="password" placeholder="{{ $dictionary['password'] ?? 'ŞİFRƏ' }}" required>
            </div>

            <div class="actions mb15 clearfix">
                <a class="forgot-password" href="{{ route('password.request') }}" target="_blank"><img src="{{asset('images/icon/forgot-password.svg')}}" > {{$dictionary['forgot_password'] ?? 'Şifrənizi unutmusuz?'}}</a>
                <input type="submit" value="{{ $dictionary['login'] ?? 'Daxil ol' }}" class="large fluid button" >
                <script>
                    (function(d, s, id) {
                        var js = d.createElement(s),
                            fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) {
                            return;
                        }
                        js.id = id;
                        js.src = "https://connect.facebook.net/en_US/sdk.js#version=v2.2&appId=384235118641188&status=true&xfbml=true";
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                </script>
                <ul class="form-result d-none" data-error="Xəta baş verdi! Zəhmət olmasa az sonra təkrar cəhd edin!"></ul>

                <div class="login-text">
                    <span>{{$dictionary['login_other_method'] ?? 'Digər metodla daxil olun'}} </span>
                </div>
                <div class="login-buttons-wrapper">
                    <div class="fb-login" onclick="">
                        <img src="{{asset('images/icon/fb-logo.svg')}}">
                        {{--<a href=""><i class="fa fa-facebook-official" aria-hidden="true"></i>{{  }}</a>--}}
                        <span><a href="{{ url('auth/facebook') }}"><span>{{$dictionary['login_by_facebook'] ?? 'Facebook ilə daxil ol'}}</span></a>
                    </div>
                </div>
            </div>
            <div class="popup-footer clearfix">
                <p>Artıq hesabınız var?<a href="/signup" class="logreg-form-switch" data-form="signup">{{$dictionary['register_button'] ?? ' Qeydiyyatdan keçin'}}</a></p>
            </div>
        </form>




        <form method="POST" action="{{ route('register') }}" id="signup-form" autocomplete="off" class="d-none">
            @csrf
            <input type="hidden" name="action" value="signup">
            <input type="hidden" name="ref" value="">
            <h2 class="popup-title">{{$dictionary['register_m'] ?? ' Qeydiyyat'}} </h2>
            <p>{{$dictionary['register_m_text'] ?? ' Zəhmət olmazsa qeydiyyat üçün tələb olunan xanaları doldurun'}}</p>
            <div class="mb10">
                <input type="text" name="name" placeholder="{{ $dictionary['full_name'] ?? 'AD SOYAD' }}" class="required" required>
            </div>
            <div class="mb10">
                <input type="email"  name="email" placeholder="{{ $dictionary['email'] }}" value="{{ old('email') }}" class="required" required>
            </div>
            <div class="mb20">
                <input type="password" name="password" placeholder="{{ @$dictionary['password'] }}"  class="required" required>
            </div>
            <div class="mb20">
                <input type="password" name="password_confirmation" placeholder="{{ @$dictionary['repeat_password'] }}" class="required" required>
            </div>

            <div class="actions mb10 clearfix">
                <span>
                    {!! $dictionary['rules'] ?? "<a href='#' target='_blank'>Qaydalar və Şərtlərimizlə</a> razı olduğunuzu bildirmiş olursuz."!!}
                </span>
                <input type="submit" value="Qeydiyyatdan keç" class="large fluid button">
                <script>
                    (function(d, s, id) {
                        var js = d.createElement(s),
                            fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) {
                            return;
                        }
                        js.id = id;
                        js.src = "https://connect.facebook.net/en_US/sdk.js#version=v2.2&appId=384235118641188&status=true&xfbml=true";
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                </script>
                <ul class="form-result d-none" data-error="Xəta baş verdi! Zəhmət olmasa az sonra təkrar cəhd edin!"></ul>
                <div class="login-text">
                    <span>{{$dictionary['login_other_method'] ?? 'Digər metodla daxil olun'}}</span>
                </div>
                <div class="login-buttons-wrapper">
                    <div class="fb-login" onclick="">
                        <img src="{{asset('images/icon/fb-logo.svg')}}" >
                        <a href="{{ url('auth/facebook') }}"><span>{{ $dictionary['register_by_facebook'] ?? 'Facebook ilə qeydiyyat' }}</span></a>
                    </div>
                </div>
            </div>
            <div class="popup-footer clearfix">
                <p>{{$dictionary['have_account'] ?? ' Artıq hesabınız var?'}}<a href="/login" class="logreg-form-switch" data-form="login">{{$dictionary['login_m2'] ?? 'Daxil olun'}}</a></p>
            </div>
        </form>
    </div><!-- /.wrapper -->
</div><!-- /.popup-window -->


<!-- Costumer popup -->
<div id="startLogin" class="popup-window d-none">
    <div class="mask"></div>
    <div class="wrapper clearfix startLoginWrapper">
        <a href="javascript:void(0)" class="close-popup"></a>
        <div class="actions">
            <a class="open-popup large fluid button newPopupElement" href="/login" data-popup="#logreg" data-form="login" title="">{{ $dictionary['login'] ?? 'Daxil ol' }}</a>
            <a class="open-popup large fluid button" href="{{ route('cart.checkout') }}" >{{ $dictionary['costumer_l'] ?? 'Qonaq kimi davam et' }}</a>
        </div>
    </div>
</div>

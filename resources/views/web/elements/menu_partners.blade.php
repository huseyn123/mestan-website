@if($partners->count())
    <ul class="menu-carousel">
        @foreach($partners as $partner)
            @if($partner->getFirstMedia())
                <li>
                    <img src="{{asset($partner->getFirstMedia()->getUrl())}}">
                </li>
            @endif
        @endforeach
    </ul><!-- /.menu-carousel -->
@endif
<!-- MainSection Begin -->
<section class="main_section">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="head_title">YENi MƏHSUllar</h1>
                    </div>
                </div>
                <div class="product_list lastProducts">
                    <div class="row">
                        <!-- ProductList End -->

                    @include('web.elements.lastProducts')

                    </div>
                    <!-- LoadMore Begin -->
                    <div class="load_more">
                        <button type="button" id="lastProducts" data-id=1 data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>" autocomplete="off">{{ $dictionary['load_more'] ?? 'Daha çox' }}</button>
                    </div>
                    <!-- LoadMore End -->
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="head_title">ƏN ÇOX SATILANLAR</h1>
                    </div>
                </div>
                <div class="product_list">
                    <div class="row">
                        <!-- ProductList End -->
                        @foreach($mostSoldProducts as $ms)
                            <div class="col-md-3 col-sm-6 col-xs-6 col-mob-12">
                                @include('web.elements.product-list', ['p' => $ms])
                            </div>
                        @endforeach
                        <!-- ProductList Begin -->
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="head_title">Digər məhsullar</h1>
                    </div>
                </div>
                <div class="product_list last anotherProducts">
                    <div class="row">
                        @include('web.elements.anotherProducts')
                    </div>
                </div>
                <!-- LoadMore Begin -->
                <div class="load_more">
                    <button type="button"  id="anotherProducts" data-id=2 data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>" autocomplete="off">{{ $dictionary['load_more'] ?? 'Daha çox' }}</button>
                </div>
                <!-- LoadMore End -->
            </div>
            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                @include('web.elements.banners', ['type' => 1])
                @include('web.elements.banners', ['type' => 2])
                @include('web.elements.banners', ['type' => 3])
            </div>
        </div>
    </div>
</section>
<!-- MainSection End -->
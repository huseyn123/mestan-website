@if(count($discounted_products))

        <section class="flat-imagebox single-box">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="flat-row-title">
                            <h2>{{$dictionary['discounted_products'] ?? '   Endirimdə olanlar' }}</h2>
                            <a class="see-all" href="{{ route('discounted.resume') }}">{{ $dictionary['see_all'] ?? 'Hamısına bax' }}</a>
                        </div>
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="home-carousel">

                            @foreach ($discounted_products as $product)

                                    <div class="imagebox single">
                                        @if($product->price) <span class="item-sale">{{$product->percent ? $product->percent.'%' : $dictionary['sale_grid'] ?? 'SALE'}}</span> @endif
                                        @if($product->getFirstMedia())
                                            <div class="box-image">
                                                <a href="{{route('showPage',[$product->page_slug,$product->slug])}}" title="{{$product->name}}">
                                                    <img src="{{asset($product->getFirstMedia()->getUrl('blade'))}}" >
                                                </a>
                                            </div><!-- /.box-image -->
                                        @endif
                                         <div class="box-info">
                                            @if($product->count == 0)<div class="out-of-stock"><h4>{{$dictionary['no_product'] ?? 'Stokda yoxdur'}} </h4></div>
                                                 @elseif($product->count > 0 && $product->count < config('config.produc_stock_count.crud'))<div class="limited-quantity"><h4>{{$dictionary['product_limited'] ?? 'Məhdud sayda'}}</h4></div>
                                            @endif
                                        </div><!-- /.box-info -->
                                        <div class="box-content">
                                            <div class="product-name">
                                                <a href="{{route('showPage',[$product->page_slug,$product->slug])}}" title="{{$product->name}}">{{$product->name}}</a>
                                            </div>
                                        </div><!-- /.box-content -->
                                        <div class="box-bottom">
                                            <div class="price">
                                                <span class="sale">{{$product->price ? $product->price : $product->old_price }} <span>M</span></span>
                                                @if($product->price) <span class="regular">{{$product->old_price}} <span>M</span></span> @endif
                                            </div>
                                            <div class="wishlist-cart">
                                                <a  href="#" class="btn wishlist @if($product->whish_list) active @endif @if(!auth()->guard('web')->check()) open-popup @endif" data-id="{{$product->id}}"></a>
                                                <a href="{{ route('cart.index') }}" class="btn @if($product->count && $product->count > 0) btn-add-cart @else out-of-stock @endif   @if($product->basket && $product->basket->count) active @endif"  data-id="{{$product->id}}"></a>
                                            </div>
                                        </div><!-- /.box-bottom -->
                                    </div><!-- /.imagebox single -->
                            @endforeach

                        </div><!-- /.home-carousel -->
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.flat-imagebox single-box -->

@endif





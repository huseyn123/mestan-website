@if($sliders->count())
    <section class="flat-row flat-slider">
        <div class="slider main-slider owl-carousel owl-theme">
            @foreach($sliders as $slider)
                @if($slider->getFirstMedia())
                    <div class="slider-item d-flex align-items-end">
                        <a href="{{$slider->link ? $slider->link : 'javascript:void(0)' }}" @if($slider->link) target="_blank" @endif>
                            <img src="{{asset($slider->getFirstMedia()->getUrl('blade'))}}" class="bg-image">
                        </a>
                    </div><!-- /.slider-item -->
                @endif
            @endforeach
        </div><!-- /.slider main-slider -->
    </section><!-- /.flat-slider -->
@endif




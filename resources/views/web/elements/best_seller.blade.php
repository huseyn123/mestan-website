
@if(isset($best_seller) && $best_seller->count())
    <section class="flat-imagebox single">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="flat-row-title">
                        <h3>{{$dictionary['best_sellers'] ?? 'Ən çox satılanlar'}}</h3>
                    </div>
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="similar-carousel">


                        @foreach($best_seller as $bestSeller)

                            <div class="imagebox single">
                                @if($bestSeller->price && $bestSeller->old_price) <span class="item-sale">{{$bestSeller->percent ? $bestSeller->percent.'%' : $dictionary['sale_grid'] ?? 'SALE'}}</span> @endif
                                @if($bestSeller->getFirstMedia())
                                    <div class="box-image">
                                        <a href="{{route('showPage',[$bestSeller->page_slug,$bestSeller->slug])}}" title="{{$bestSeller->name}}">
                                            <img src="{{asset($bestSeller->getFirstMedia()->getUrl('blade'))}}" >
                                        </a>
                                    </div><!-- /.box-image -->
                                @endif
                             
                                    <div class="box-info">
                                        @if($bestSeller->count == 0)<div class="out-of-stock"><h4>{{$dictionary['no_product'] ?? 'Stokda yoxdur'}} </h4></div>
                                            @elseif($bestSeller->count > 0 && $bestSeller->count < config('config.produc_stock_count.crud'))<div class="limited-quantity"><h4>{{$dictionary['product_limited'] ?? 'Məhdud sayda'}}</h4></div>
                                        @endif
                                    </div><!-- /.box-info -->
                              
                                <div class="box-content">
                                    <div class="product-name">
                                        <a href="{{route('showPage',[$bestSeller->page_slug,$bestSeller->slug])}}" title="{{$bestSeller->name}}">{{$bestSeller->name}}</a>
                                    </div>
                                </div><!-- /.box-content -->
                                <div class="box-bottom">
                                    <div class="price">
                                        <span class="sale">{{$bestSeller->price ? $bestSeller->price : $bestSeller->old_price }} <span>M</span></span>
                                        @if($bestSeller->price) <span class="regular">{{$bestSeller->old_price}} <span>M</span></span> @endif
                                    </div>
                                    <div class="wishlist-cart">
                                        <a  href="#" class="btn wishlist @if($bestSeller->whish_list) active @endif @if(!auth()->guard('web')->check()) open-popup @endif" data-id="{{$bestSeller->id}}"></a>
                                            <a href="{{ route('cart.index') }}" class="btn @if($bestSeller->count && $bestSeller->count > 0) btn-add-cart @else out-of-stock @endif   @if($bestSeller->basket && $bestSeller->basket->count) active @endif"  data-id="{{$bestSeller->id}}"></a>
                                    </div>
                                </div><!-- /.box-bottom -->
                            </div><!-- /.imagebox single -->
                        @endforeach

                    </div><!-- /.similar-carousel -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-imagebox single -->
@endif
<article>
    <a href="{{ route('showPage', [$p->page_slug, $p->slug]) }}@if(isset($search) && $search == true)?search=on @endif" title="{{ $p->name }}">
        <figure>
            @if(env('APP_ENV') == 'production')
                <img src="{{ asset("storage/thumb/$p->image") }}" alt="{{ $p->name }}">
            @endif
        </figure>
        <h2>{{ $p->name }}</h2>
        <div class="price_block">
            @if($p->old_price > $p->price  && !isset($hideDiscount))
                <span class="old-price">{{ $p->old_price }} <span class="azn">M</span></span>
            @endif
            <span class="price">{{ $p->price }} <span class="azn">M</span></span>
        </div>
    </a>
</article>
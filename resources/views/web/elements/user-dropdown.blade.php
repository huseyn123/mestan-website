<ul class="list-unstyled">
    <li><a href="{{ route('user.profile') }}">{{ $dictionary['user_profile'] }}</a></li>
    <li>
        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('{{ $id }}').submit();" >{{ $dictionary['logout'] or 'Çıxış'}}</a>
        <form id="{{ $id }}" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </li>
</ul>
@if($type == 'header')
    @foreach($menu as $m)
        @if($m->parent_id == $parent && !in_array($m->visible, $hidden) && $m->lang == $lang && !in_array($m->template_id,[0,10]))

            @if(isset($mobile))
                <li class="{{config('config.page_class.'.$m->template_id)}}">
                    <a href="@if($m->forward_url){{$m->forward_url}}@else {{route('showPage',[$m->slug])}} @endif" @if($m->target == 0) target="_blank" @endif>{{$m->name}}</a>
                </li>
            @else

                <div class="col-md-4 col-5">
                    <div class="{{config('config.page_class.'.$m->template_id)}}">
                        <a href="@if($m->forward_url){{$m->forward_url}}@else {{route('showPage',[$m->slug])}} @endif" title="{{$m->name}}" class="btn" @if($m->target == 0) target="_blank" @endif>{{$m->name}}</a>
                    </div>
                </div>
                <!-- /.col-md-4 -->
            @endif


        @endif

    @endforeach


@elseif($type == 'top_menu')
    <ul  @if(!isset($mobile))class="flat-links"  @endif>

        @if(isset($mobile))
            @include('web.elements.menu', ['parent' => null,'sub' => false,'lang' => $lang,'mobile' => true,'type' => 'store_link'])
        @endif

        @foreach($menu as $m)
            @if($m->parent_id == $parent && !in_array($m->visible, $hidden) && $m->lang == $lang && !in_array($m->template_id,[0,10,14]))
                <li class="{{config('config.page_class.'.$m->template_id)}}">
                    <a  @if(isset($mobile) && (config('config.m_page_class.'.$m->template_id)))
                            class="mobile-icon icon-{{config('config.m_page_class.'.$m->template_id)}}"
                        @endif
                        href="@if($m->forward_url){{$m->forward_url}}
                        @elseif($m->template_id == 20)#@else {{route('showPage',[$m->slug])}}
                        @endif" @if($m->target == 0) target="_blank" @endif title="{{$m->name}}">
                        {{$m->name}}
                    </a>
                    @if($m->template_id == 20 && $m->menu_children_count > 0)
                        @if(!isset($mobile))<i class="fa fa-angle-down" aria-hidden="true"></i> @endif
                        @include('web.elements.menu', ['parent' => $m->tid,'sub' => false,'lang' => $lang, 'hidden' => [0,1,2,3,4,5,8,9],'type' => 'dropdown_content'])
                    @endif
                </li>
            @endif

        @endforeach

        <li class="callme-link">
            <a href="JavaScript:Void(0);" class="open-popup @if(isset($mobile)) mobile-icon icon-callme @endif" data-popup="#callme">{{$dictionary['help'] ?? 'Kömək'}}</a>
        </li>
    </ul>


@elseif($type == 'dropdown_content')

    <ul @if(!isset($mobile)) class="dropdown-links" @endif>
        @foreach($menu as $m)
            @if($m->parent_id == $parent && !in_array($m->visible, $hidden) && $m->lang == $lang && !in_array($m->template_id,[0,10,14,20]))
                <li @if(isset($mobile)) class="{{config('config.m_page_class.'.$m->template_id)}}" @endif>
                    <a  href="@if($m->forward_url){{$m->forward_url}}@else {{route('showPage',[$m->slug])}} @endif" @if($m->target == 0) target="_blank" @endif title="{{$m->name}}">  {{$m->name}}</a>
                </li>
            @endif
        @endforeach

    </ul>

@elseif($type == 'catalog_top')
        @foreach($menu as $m)
            @if($m->parent_id == $parent && in_array($m->template_id,[21]) && $m->lang == $lang)
                <div class="col-md-3">
                    <div class="mega-menu-wrap">
                        <div id="mega-menu">
                            <div class="btn-mega"><span></span>{{$m->name}}</div>
                            <div class="menu">
                                <div class="row">
                                    @include('web.elements.menu', 
                                    [
                                        'parent' => $m->tid,
                                        'sub' => true,
                                        'lang' => $lang,
                                        'hidden' => [0,3,4,5,6,7,8,9],
                                        'type' => 'catalog'
                                     ])
                                </div><!-- /.row -->
                            </div><!-- /.menu -->
                        </div><!-- /#mega-menu -->
                    </div><!-- /.mega-menu-wrap -->
                </div><!-- /.col-md-4 -->
            @endif
        @endforeach

@elseif($type == 'catalog_top_mobile')
    @foreach($menu as $m)
        @if($m->parent_id == $parent && in_array($m->template_id,[21]) && $m->lang == $lang)

            <li class="shortcuts big-menu-mobile-dropdown">
                <a><span>{{$m->name}}</span></a>
                @if($m->menu_children_count)

                    @include('web.elements.menu', 
                        [
                            'parent' => $m->tid,
                            'sub' => true,
                            'lang' => $lang,
                            'hidden' => [0,3,4,5,6,7,8,9],
                            'type' => 'catalog',
                            'mobile' => true
                         ])

                @endif
            </li>

        @endif
    @endforeach


@elseif($type == 'catalog')
    @foreach($menu as $m)


        @if($m->parent_id == $parent && !in_array($m->visible, $hidden) && $m->lang == $lang && !in_array($m->template_id,[0,10,14,21]))
        <!-- Check Menu Parametr -->

            <!-- Check Menu Template -->
            @if(isset($mobile))
                <ul>
                    
                    <li class="info-link">
                        <a href="{{$m->menu_children_count ? '#': route('showPage',[$m->slug])}}" @if($m->target == 0) target="_blank" @endif title="{{$m->name}}" @if($m->menu_children_count) class="mobile-icon" @endif>
                            <span>
                                {{$m->name}}
                            </span>
                        </a>
                        @if($m->menu_children_count)
                            @include('web.elements.menu', ['parent' => $m->tid,'main_slug' =>$m->slug,'main_page_name' => $m->name,'sub' => true,'lang' => $lang, 'hidden' => [0,3,4,5],'type' => 'sub_cataloq','mobile' =>1])
                        @endif    
           
                    </li>
                </ul>
            @else
            <!--End mobile menu -->

                <!-- Check menu children -->
                    <div class="col-lg-2 col-md-12">
                        <h3 class="cat-title">{{$m->name}}</h3>
                        @include('web.elements.menu', ['parent' => $m->tid,'main_slug' =>$m->slug,'sub' => true,'lang' => $lang, 'hidden' => [0,3,4,5],'type' => 'sub_cataloq','mobile' =>0])
                        <div class="show">
                            <a href="{{route('showPage',[$m->slug])}}" title="{{$m->name}}">{{$dictionary['see_all'] ?? 'Hamısına bax'}}</a>
                        </div>
                    </div><!-- /.col-lg-2 col-md-12 -->


            @endif

        @endif

    @endforeach


@elseif($type === 'sub_cataloq')
    <ul @if(!$mobile) class="menu-child" @endif>
        @foreach($menu as $sub_m)
            @if($sub_m->parent_id == $parent && !in_array($sub_m->visible, $hidden) && $sub_m->lang == $lang && !in_array($sub_m->template_id,[0,10,14]))
                <li>
                    <a href="{{$m->forward_url ? $m->forward_url : route('showPage',[$main_slug,$sub_m->slug])}}" @if($m->target == 0) target="_blank" @endif title="{{$sub_m->name}}">{{$sub_m->name}}</a>
                </li>
            @endif
        @endforeach

        @if($mobile === 1)
            <li class="show">
                <a href="{{route('showPage',[$main_slug])}}" title="{{ isset($main_page_name) ? $main_page_name : ($dictionary['see_all'] ?? 'Hamısına bax') }}">{{$dictionary['see_all'] ?? 'Hamısına bax'}}</a>
            </li>
         @endif
    </ul>

@elseif($type == 'footer')
    @foreach($menu as $m)
        @if($m->parent_id == $parent && !in_array($m->visible, $hidden) && $m->lang == $lang && !in_array($m->template_id,[0,10,14]))
            <li>
                <a href="{{$m->forward_url ? $m->forward_url : route('showPage',[$m->slug])}}" title="{{$m->name}}" @if($m->target == 0) target="_blank" @endif>{{$m->name}}</a>
            </li>
        @endif
    @endforeach

@elseif($type == 'store_link')
    @foreach($menu as $m)
        @if($m->parent_id == $parent  && $m->lang == $lang && in_array($m->template_id,[14]))
            <li @if(isset($mobile)) class="stores-link" @endif>
                <a @if(isset($mobile)) class="mobile-icon icon-stores" @endif href="{{$m->forward_url ? $m->forward_url : route('showPage',[$m->slug])}}" title="{{$m->name}}" @if($m->target == 0) target="_blank" @endif>{{$m->name}}</a>
            </li>
        @endif
    @endforeach

@endif

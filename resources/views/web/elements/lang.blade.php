@if(isset($type) && $type == 'footer_mobile' )

    <select name="lang-select" onchange="location = this.value;">
        @foreach(config("config.pages_lang") as $key => $locale)
            <option value="{{ url($relatedPages[$key] ?? $key) }}" @if($key == $lang) selected @endif>{{ $locale }}</option>
        @endforeach
    </select>
@else
    <li class="lang">
        <a href="#" title="">{{config("config.pages_lang")[$lang] ?? $lang}}<i class="fa fa-angle-down" aria-hidden="true"></i></a>
        <ul class="unstyled">
            @foreach(config("config.pages_lang") as $key => $locale)
                @if($key != $lang)
                    <li>
                        <a  href="{{ url($relatedPages[$key] ?? $key) }}" hreflang="{{$key}}" >{{ $locale }}</a>
                    </li>
                @endif

            @endforeach
        </ul>
    </li>
@endif


@if($page->gallery->count())
    <!-- PageGallery Begin -->
    <div class="page_gallery">
        <div class="row">
            <div class="col-xs-12">
                <h2>{{ $dictionary['photo_gallery'] }}</h2>
            </div>
            @foreach($page->gallery as $gallery)
                <div class="col-cmd-5 col-sm-4 col-xs-4 col-mob-6">
                    <figure>
                        <a href="{{ asset("storage/$gallery->filename") }}" class="zoom-photo"><img src="{{ asset("storage/thumb/$gallery->filename") }}"></a>
                    </figure>
                </div>
            @endforeach
        </div>
    </div>
    <!-- PageGallery End -->
@endif
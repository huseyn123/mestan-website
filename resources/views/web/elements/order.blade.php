<!-- Section Begin -->
<section class="pt50 pb80">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-push-8 col-xs-12">
                <!-- Information -->
                @if(trim($dictionary['complete_order_title']) != '')
                    <div class="information">
                        <div class="clearfix">
                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                            <h1>{{ $dictionary['complete_order_title'] ?? '' }}</h1>
                        </div>
                        <div class="text">
                            <p>{{ $dictionary['complete_order_text'] ?? '' }}</p>
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-sm-8 col-sm-pull-4 col-xs-12">
                @if (isset($errors) && $errors->any())
                    @include('widgets.alert', array('class' => 'danger', 'message' => $errors->first(), 'dismissable' => true))
                @endif
                    <!-- StepNav -->
                <div class="step_nav">
                    <div class="row">
                        <div class="col-sm-3 col-xs-3 @if($num == 1) current @elseif($num > 1) active @endif">
                            <span class="num"><a href="{{ route('order.index') }}?step=1">1</a></span>
                            <span class="txt"><a href="{{ route('order.index') }}?step=1">Şəxsi məlumat</a></span>
                        </div>
                        <div class="col-sm-3 col-xs-3 @if($num == 2) current @elseif($num > 2) active @endif">
                            <span class="num"><a href="{{ route('order.index') }}?step=2">2</a></span>
                            <span class="txt"><a href="{{ route('order.index') }}?step=2">Çatdırılma</a></span>
                        </div>
                        <div class="col-sm-3 col-xs-3 @if($num == 3) current @elseif($num > 3) active @endif">
                            <span class="num"><a href="{{ route('order.index') }}?step=3">3</a></span>
                            <span class="txt"><a href="{{ route('order.index') }}?step=3">Ödəniş</a></span>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <span class="num">4</span>
                            <span class="txt">Status</span>
                        </div>
                    </div>
                </div>
                {!! Form::open(['route' => $route ?? 'order.index', 'method' => $method]) !!}
                    <!-- StepOne -->
                    {{ $step }}
                    <input type="hidden" name="step" value="{{ $num+1 }}">
                    <!-- StepBottom -->
                    <div class="step_bottom">
                        <button type="submit" class="next">{{ $dictionary['next_step'] ?? '' }}<i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
<!-- Section End -->


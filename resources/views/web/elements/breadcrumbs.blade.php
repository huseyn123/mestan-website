<section class="flat-breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs">
                    @if(!is_null($page))
                        {!! Breadcrumbs::render('breadcrumb', $page, $dictionary['home_page'], isset($data) ? $data : false) !!}
                    @elseif(isset($p_title))
                        <li class="trail-item">
                            <a href="{{route('home')}}" title="">{{$dictionary['home_page']}}</a>
                            <span><img src="{{asset('images/icon/next.svg')}}" ></span>
                        </li>
                        <li class="trail-end">
                            <a title="">{{$p_title}}</a>
                        </li>
                    @endif
                </ul>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.flat-breadcrumb -->





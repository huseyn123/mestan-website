<footer>

    <div class="footer-main">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo logo-ft">
                        <a href="{{route('home')}}">
                            <img src="{{asset('images/logos/logo-ft.png')}}">
                        </a>
                        <p class="copyright"> {{$dictionary['copyright'] ?? '© 2019, Mestan.az All Rights Reserved.'}}</p>
                        <div class="payment-logos">
                            {{--<img src="images/logos/payment-logo1.png" alt="">--}}
                            {{--<img src="images/logos/payment-logo2.png" alt="">--}}
                        </div>
                    </div><!-- /.logo-ft -->
                </div><!-- /.col-md-6 -->
                <div class="col-md-6">
                    <div class="widget-ft widget-menu">
                        <ul>
                            @include('web.elements.menu', ['parent' => null,'sub' => true,'lang' => $lang, 'hidden' => [0,1,2,4,6,8,9],'type' => 'footer'])
                            <li>
                                <a href="/callme" class="open-popup" data-popup="#callme" title=""> {{$dictionary['help'] ?? 'Kömək'}}</a>
                            </li>
                        </ul>
                    </div>
                    <div class="widget-ft widget-social">
                        @include('web.elements.social')
                    </div>
                    <div class="widget-ft widget-about">
                        <p class="email"> {{$config['contact_email']}}</p>
                        <p class="phone"> {{$config['contact_phone']}}</p>
                    </div>
                </div><!-- /.col-md-6 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.footer-main -->



    <div class="footer-mobile d-block d-md-block d-lg-none">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="mobile-widget widget-menu">

                        <ul>
                            @include('web.elements.menu', ['parent' => null,'sub' => true,'lang' => $lang, 'hidden' => [0,1,2,4,6,8,9],'type' => 'footer'])
                            <li>
                                <a href="/callme" class="open-popup" data-popup="#callme" title=""> {{$dictionary['help'] ?? 'Kömək'}}</a>
                            </li>
                        </ul>
                    </div><!-- /.widget-menu -->
                    <div class="mobile-lang">
                        <!-- @include('web.elements.lang',['type' =>'footer_mobile']) -->
                    </div>
                    <div class="mobile-widget widget-about">
                        <p class="email"> {{$config['contact_email']}}</p>
                        <p class="phone"> {{$config['contact_phone']}}</p>
                    </div>
                    <div class="logo logo-ft-mobile">
                        <a href="{{route('home')}}">
                            <img src="{{asset('images/logos/logo-ft.png')}}">
                        </a>
                        <p class="copyright">{{$dictionary['copyright'] ?? '© 2019, Mestan.az All Rights Reserved.'}}</p>
                        <div class="payment-logos">
                            {{--<img src="images/logos/payment-logo1.png" alt="">--}}
                            {{--<img src="images/logos/payment-logo2.png" alt="">--}}
                        </div>
                    </div><!-- /.logo-ft -->
                </div><!-- /.col-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.footer-mobile -->


</footer><!-- /footer -->
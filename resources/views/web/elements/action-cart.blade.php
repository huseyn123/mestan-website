<div class="basket_bottom">
    <button class="delete" onclick="event.preventDefault(); cartAction(document.getElementById('delete-{{ $id }}'));">
        <i class="fa fa-times" aria-hidden="true"></i>
        {!! Form::open(['method' => 'DELETE', 'url' => route("cart.destroy", $id), 'id' => "delete-{$id}", 'style' => 'display:none']) !!}
        {!! Form::close() !!}
    </button>
</div>
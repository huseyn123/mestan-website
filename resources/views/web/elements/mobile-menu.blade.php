<div class="header-mobile d-block d-md-block d-lg-none">
    <div class="container">
        <div class="row">
            <div class="col-8 col-md-6">
                <a class="mobile-toggle">
                    <span></span>
                </a>
                <div id="logo" class="logo">
                    <a href="{{route('home')}}" title="">
                        <img src="{{asset('images/logos/logo.png')}}">
                    </a>
                </div><!-- /#logo -->
            </div><!-- /.col-8 col-md-6 -->
            <div class="col-4 col-md-6">
                <div class="user-navigation-container">
                    <ul class="user-navigation">
                        <li class="wishlist-button-container">
                            <div class="icon-container">
                                <a href="{{ route('wishlist.index') }}" class="show_wishlist @if(!auth()->guard('web')->check()) open-popup @endif"><i class="icon navigation-icon-wishlist"></i></a>
                            </div>
                        </li><!-- /.wishlist-button-container -->
                        <li class="basket-button-container">
                            @include('web.elements.cart_list',['mobile' =>true])
                        </li><!-- /.basket-button-container -->
                    </ul><!-- /.user-navigation -->
                </div><!-- /.user-navigation-container -->
            </div><!-- /.col-4 col-md-6 -->
        </div><!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <div class="top-search">
                    <div class="box-search">
                        <input type="text" name="search" placeholder="{{$dictionary['product_search_placeholder'] ?? 'Axtardığınız məhsulu yazın ?'}}" autocomplete="off">
                        <span class="icon-search"><img src="{{asset('images/icon/magnify.svg')}}"></span>
                        <div class="search-suggestions">
                            <div class="box-suggestions">
                            </div><!-- /.box-suggestions -->
                        </div><!-- /.search-suggestions -->
                    </div><!-- /.box-search -->
                </div><!-- /.top-search -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.header-mobile -->

<div id="mobile-menu" class="d-none">
    <ul class="mobile-user">

        @if(auth()->guard('web')->check())

            <li class="account login">
                <a href="#">{{ auth()->guard('web')->user()->name }}</a>
                <i class="fa fa-angle-down" aria-hidden="true"></i>
                <ul class="unstyled">

                    <li>
                        <a href="{{ route('orders.list') }}" class="mobile-icon icon-orders">{{$dictionary['my_orders'] ?? 'Sifarişlərim'}}</a>
                    </li>
                    <li>
                        <a href="{{ route('user.profile') }}" class="mobile-icon icon-user-info">{{$dictionary['my_info'] ?? 'Məlumatlar'}}</a>
                    </li>
                    <li>
                        <a class="mobile-icon icon-logout" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" >{{ $dictionary['logout'] ?? 'Çıxış'}}</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>

                </ul><!-- /.unstyled -->
            </li>

        @else
            <li class="account guest">
                <a class="open-popup" href="JavaScript:Void(0);" data-popup="#logreg" data-form="login">{{$dictionary['login_m_register_m'] ?? 'Giriş/Qeydiyyat'}}</a>
            </li>

        @endif
    </ul>


    <ul class="mobile-links">
        @include('web.elements.menu', ['parent' => null,'sub' => true,'lang' => $lang, 'hidden' => [0,1,2,5,6,7,8,9],'type' => 'header','mobile' =>true])

        <li class="shortcuts">
            <a href="#">{{$dictionary['about'] ?? 'Haqqımızda'}}</a>
            @include('web.elements.menu', ['parent' => null,'sub' => false,'lang' => $lang, 'hidden' => [0,1,2,3,4,5,8,9],'type' => 'top_menu','mobile' =>true])
        </li>

        @include('web.elements.menu', ['parent' => null,'sub' => true,'lang' => $lang,'type' => 'catalog_top_mobile'])

    </ul>


    @include('web.elements.social',['mobile' =>true])

</div><!-- /.mobile-menu -->
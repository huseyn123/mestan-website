@extends ('layouts.web', [ 'page_heading' => 'Sifarişlərim' ] )

@section ('content')


    @include('web.elements.breadcrumbs', ['page' => null, 'p_title' => 'Sifarişlərim'])


    <section class="flat-shop-cart">
        <div class="container cart_d_table">
            @include('web.elements.cart_html')
        </div><!-- /.container -->
    </section><!-- /.flat-shop-cart -->



@endsection

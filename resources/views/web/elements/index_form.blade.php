@foreach(config('config.index_form') as $key=>$value)
    <section class="flat-imagebox single-box">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="flat-row-title">
                    <h2>{{ $dictionary[$key] ?? $value}}</h2>
                </div>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <div class="home-carousel">
                    @foreach ($products as $product)
                        @if(in_array($key,$product->index_form))

                            <div class="imagebox single">
                                @if($product->price) <span class="item-sale">{{$dictionary['sale_grid'] ?? 'SALE'}}</span> @endif
                                @if($product->getFirstMedia())
                                    <div class="box-image">
                                        <a href="{{route('showPage',[$product->page_slug,$product->slug])}}" title="{{$product->name}}">
                                            <img src="{{asset($product->getFirstMedia()->getUrl('blade'))}}" >
                                        </a>
                                    </div><!-- /.box-image -->
                                @endif
                                <div class="box-content">
                                    <div class="product-name">
                                        <a href="{{route('showPage',[$product->page_slug,$product->slug])}}" title="{{$product->name}}">{{$product->name}}</a>
                                    </div>
                                </div><!-- /.box-content -->
                                <div class="box-bottom">
                                    <div class="price">
                                        <span class="sale">{{$product->price ? $product->price : $product->old_price }} <span>M</span></span>
                                        @if($product->price) <span class="regular">{{$product->old_price}} <span>M</span></span> @endif
                                    </div>
                                    <div class="wishlist-cart">
                                        <a  href="#" class="btn wishlist @if($product->whish_list) active @endif @if(!auth()->guard('web')->check()) open-popup @endif" data-id="{{$product->id}}"></a>
                                        @if($product->count > 0)
                                            <a href="{{ route('cart.index') }}" class="btn btn-add-cart @if($product->basket && $product->basket->count) active @endif" data-id="{{$product->id}}"></a>
                                        @endif
                                    </div>
                                </div><!-- /.box-bottom -->
                            </div><!-- /.imagebox single -->

                        @endif
                    @endforeach
                </div><!-- /.home-carousel -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.flat-imagebox single-box -->
@endforeach





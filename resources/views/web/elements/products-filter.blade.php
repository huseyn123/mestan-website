<div class="head clearfix">
    <h1>Filterləmə</h1>
    <button type="button" class="open_filter"><i class="fa fa-filter" aria-hidden="true"></i></button>
</div>
<!-- FilterBlock -->
<div class="filter_block">
    <form method="GET" action="{{ route('product.search') }}">
        <input type="hidden" name="page_id" value="{{ $cat->id }}">
        @if($productCategories->count())
            <!-- Block -->
            <div class="block">
                <h4>{{ $dictionary['filter_by_category'] }}</h4>
                <div class="search_category">
                    <ul class="list-unstyled">
                        @foreach($productCategories as $c)
                            <li>
                                <input type="checkbox" name="category[]" id="{{ $c->slug }}" class="css-checkbox" value="{{ $c->id }}" @if((isset($categories) && in_array($c->id, $categories)) || $c->id == $page->page_id) checked="checked" @endif>
                                <label for="{{ $c->slug }}" class="css-label">{{ $c->name }} <span>({{ $c->product_categories_count }})</span></label>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <!-- Block -->
        @endif
        <div class="block pb50 price">
            <h4 class="mb25">{{ $dictionary['filter_by_price'] }}</h4>
            <span class="pull-left azn" style="padding:6px 3px; font-size:14px">M</span>
            <input type="text" name="min_price" class="form-control pull-left price" style="width:33%" value="{{ request()->get('min_price') }}">
            <span class="pull-left azn" style="padding:6px 3px; font-size:14px">- M</span>
            <input type="text" name="max_price" class="form-control pull-left price" style="width:33%" value="{{ request()->get('max_price') }}">
        </div>
        <!-- Block -->
        <!-- Block -->
        <div class="block">
            <h4>{{ $dictionary['filter_by_brand'] }}</h4>
            {!! Form::select('brand_id', $brands, request()->get('brand_id')) !!}
        </div>
        <div class="block">
            <h4>{{ $dictionary['filter_by_color'] }}</h4>
            {!! Form::select('color_id', $colors, request()->get('color_id')) !!}
        </div>
        <!-- Block -->
        <input type="hidden" name="sort" value="{{ request()->get('sort', 1) }}">
        <!-- Btns -->
        <div class="btns clearfix">
            <button type="submit">{{ $dictionary['search'] }}</button>
        </div>
    </form>
</div>
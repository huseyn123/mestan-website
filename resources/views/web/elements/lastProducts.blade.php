@foreach($lastProducts as $ms)
    <div class="col-md-3 col-sm-6 col-xs-6 col-mob-12">
        @include('web.elements.product-list', ['p' => $ms])
    </div>
@endforeach
<!-- ProductList Begin -->
<div class="top-search">

    <div class="box-search">
        <input type="text" name="search" placeholder="{{$dictionary['product_search_placeholder'] ?? 'Axtardığınız məhsulu yazın ?'}}" class="search_product" autocomplete="off">
        <span class="icon-search"><img src="{{asset('images/icon/magnify.svg')}}"></span>
        <div class="search-suggestions">
            <div class="box-suggestions">
            </div><!-- /.box-suggestions -->
        </div><!-- /.search-suggestions -->
    </div><!-- /.box-search -->

</div><!-- /.top-search -->

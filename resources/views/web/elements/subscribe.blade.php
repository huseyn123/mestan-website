<div class="subscribe">
    {!! Form::open(['url'=>route('subscribe'), 'method'=>'POST', 'id' => 'subscribe-form']) !!}
        {!! Form::text('email', null, ["placeholder" => '* '.$dictionary['email'], 'required']) !!}
        {!! Form::button('', ['id' => 'loadingSubButton', 'data-loading-text' => loading(), 'type' => 'submit']) !!}
    {!! Form::close() !!}
</div>
@if($index_disc_slide)
    <section class="flat-highlights">
        <div class="container">
            <div class="row">
                <div class="col-6 col-md-6">
                    <div class="highlights-box">
                        <h3>{{$index_disc_slide->title}}</h3>
                        @if($index_disc_slide->price)<span class="highlights-sale">{{$index_disc_slide->price}} <span>M</span></span>@endif
                        <span class="highlights-regular">{{$index_disc_slide->old_price}} <span>m</span></span>
                        @if($index_disc_slide->link)
                            <div class="btn-shop">
                                <a href="{{$index_disc_slide->link}}">{{$dictionary['shop_now'] ?? 'SHOP NOW'}}</a>
                            </div>
                        @endif
                    </div>
                </div><!-- /.col-6 col-md-6 -->
                @if($index_disc_slide->getFirstMedia())
                <div class="col-6 col-md-6">
                    <div class="highlights-image">
                        <img src="{{asset($index_disc_slide->getFirstMedia()->getUrl())}}">
                    </div>
                </div><!-- /.col-6 col-md-6 -->
                @endif
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-highlights -->
@endif


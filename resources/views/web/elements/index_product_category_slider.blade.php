<section class="flat-sort-box">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sortbox-carousel">

                    @foreach($product_pages as $m)
                        @if(in_array($m->visible, [8]) && $m->lang == $lang)
                            <div class="sortbox single">
                                @if($m->getFirstMedia('cover'))
                                    <div class="sortbox-image">
                                        <a href="@if($m->forward_url){{$m->forward_url}}@else {{route('showPage',[$m->slug])}} @endif" @if($m->target == 0) target="_blank" @endif>
                                            <img src="{{asset($m->getFirstMedia('cover')->getUrl('blade'))}}">
                                        </a>
                                    </div><!-- /.sortbox-image -->
                                @endif
                                <div class="sortbox-title">
                                    <a href="@if($m->forward_url){{$m->forward_url}}@else {{route('showPage',[$m->slug])}} @endif" @if($m->target == 0) target="_blank" @endif>{{$m->name}}</a>
                                    <div class="sortbox-icon"></div>
                                </div><!-- /.sortbox-title -->
                            </div><!-- /.sortbox single -->
                        @endif
                    @endforeach


                </div>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.flat-sort-box -->

@extends ('layouts.web', [ 'page_heading' => 'Səbət' ] )

@section ('content')
    @include('web.elements.breadcrumbs', ['page' => null, 'title' => 'Səbət'])
    <!-- Section Begin -->
    <section class="ptb50">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <!-- Banner Begin -->
                    <div class="banner">
                        <h1 class="h1_title">{{ $dictionary['advert'] ?? '' }}</h1>
                        <figure>
                            <img src="{{asset('images/delete/banner.png')}}">
                        </figure>
                    </div>
                    <!-- Banner End -->
                </div>
                <div class="col-md-9 col-sm-8 col-xs-12">
                    @include('web.elements.cart')
                </div>
            </div>
        </div>
    </section>
    <!-- Section End -->
@endsection


@extends ('layouts.web', [ 'page_heading' => $page->name ] )

@section ('content')
    @include('web.elements.breadcrumbs')
    <!-- Section Begin -->

    <!-- Section Begin -->
    <section class="ptb50">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">

                    <!-- TextBanner Begin -->
                    @include('web.elements.apply')
                    <!-- TextBanner End -->

                </div>
                <div class="col-md-9 col-sm-8 col-xs-12">
                    <!-- VacancyDetail Begin -->
                    <div class="vacancy_detail">
                        <h1 class="h1_title">Şöbə müdiri</h1>
                        <!-- JobParam -->
                        <div class="job_param clearfix">
                            <div class="block">
                                <span class="lbl">İş təcrübəsi:</span>
                                <span class="val">{{ $vacancy->work_experience }}</span>
                            </div>
                            <div class="block">
                                <span class="lbl">Əmək haqqı:</span>
                                <span class="val">{{ $vacancy->salary }}</span>
                            </div>
                            <div class="block">
                                <span class="lbl">Elanın bitmə tarixi:</span>
                                <span class="val">{{ blogDate($vacancy->end_date) }}</span>
                            </div>
                            <!-- BtnSendCV -->
                            <a href="{{ route('apply.resume',[$page->slug, $vacancy->id]) }}" title="" class="btn_sendcv">CV göndər</a>
                        </div>
                        <!-- JobDescription -->
                        <div class="job_description">
                            <h4>İş barədə məlumat</h4>
                            <ul class="list-unstyled">
                                {{ $vacancy->work_description }}
                            </ul>
                        </div>
                    </div>
                    <!-- VacancyDetail End -->
                </div>
            </div>
        </div>
    </section>
    <!-- Section End -->

@endsection

@extends ('layouts.web', ['page_heading' => $page->name])

@section ('content')

    @include('web.elements.breadcrumbs')


    <section class="flat-brands">
        <div class="container">
            <div class="brands-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <div class="flat-row-title">
                            <h3>{{$page->name}}</h3>
                        </div>
                    </div><!-- /.col-md-12 -->
                    <div class="col-md-12">
                        <ul class="brands-tablist">
                            <li data-filter="*" class="active">{{$dictionary['all'] ?? 'All'}} </li>
                            @foreach(config('config.letter_id') as $key=>$brand)
                                <li data-filter=".brand{{$key}}">{{$brand}}</li>
                            @endforeach
                        </ul><!-- /.brands-tablist -->

                        <div class="brands-list grid">

                            @foreach($brands as $brand)
                                <div class="brands-item ipsotope brand{{$brand->letter_id}}">
                                    <a class="box-cat" title="">
                                        <div class="cat-image">

                                            @if($brand->getFirstMedia())
                                                <img src="{{asset($brand->getFirstMedia()->getUrl())}}">
                                            @endif

                                        </div>
                                        <div class="cat-name">
                                            {{$brand->title}}
                                        </div>
                                        <div class="numb-product">
                                            {{$brand->products_count}} {{$dictionary['product'] ?? 'Məhsul'}}
                                        </div>
                                    </a>
                                </div><!-- /.brands-item ipsotope -->
                            @endforeach

                        </div><!-- /.brands-list grid -->
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.brands-wrap -->
        </div><!-- /.container -->
    </section><!-- /.flat-brands -->

    @include('web.elements.best_seller')

@endsection

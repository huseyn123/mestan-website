@extends ('layouts.web', ['page_heading' => null ] )

@section ('content')

    @include('web.elements.slider')

    @include('web.elements.index_product_category_slider')

    @include('web.elements.discounted_products')

    @include('web.elements.index_recurring')

    @include('web.elements.partners')

    @include('web.elements.index_disc_slide')

@endsection

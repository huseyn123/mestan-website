@extends ('layouts.user-cabinet')

@section ('section')


    @if($orders->count())
        <div class="table-cart orders-main">
        <table>
            <thead>
            <tr>
                <th>Sifarişlərim</th>
                <th>Status</th>
                <th>Tarix</th>
            </tr>
            </thead>
            <tbody>

                @foreach($orders as $key=>$order)
                    <tr>

                        <td>
                            <div class="name-product">
                                Sifariş#00{{$loop->count - $key}}
                                <div class="total">{{ ($order->total_amount ? $order->total_amount : ($order->amount +  $order->delivery_price) - ($order->special_percent ? (number_format(($order->amount * $order->special_percent/100),2)) : 0)) }}<span>M</span></div>
                                <a href="{{route('order.list',[$order->payment_key])}}" class="order-details-btn">Sifarişə bax</a>
                            </div>
                        </td>
                        <td>
                            <div class="order-status">
                                <span>{{config("config.user_order_status.$order->status")}}</span>
                            </div>
                        </td>
                        <td>
                            <div class="order-time">
                                {{$order->created_at}}
                            </div>
                        </td>

                    </tr>

                @endforeach

            </tbody>
        </table>
    </div>
    @endif
@endsection


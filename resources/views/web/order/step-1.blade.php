@extends ('layouts.web', [ 'page_heading' => $dictionary['basket'] ] )

@section ('content')
    @include('web.elements.breadcrumbs', ['page' => null, 'title' => $dictionary['basket'] ])

    @component('web.elements.order', ['num' => $step, 'method' => 'POST', 'route' => 'order.post'])
        @slot('step')
            <div class="step_one">
                <!-- StepForm -->
                <div class="step_form">
                    <div class="row">
                        <div class="col-md-6 col-sm-8 col-xs-12">
                            @if(!is_null($user))
                                <div class="item">
                                    <label class="lbl">{{ $dictionary['full_name'] }}</label>
                                    <input type="text" value="{{ $user->name }}" class="ipt_style" readonly>
                                </div>
                                @if(!is_null( $user->phone))
                                    <div class="item">
                                        <label class="lbl">{{ $dictionary['phone'] }}</label>
                                        <input type="text" value="+994 {{ $user->phone }}" class="ipt_style" readonly>
                                    </div>
                                @endif
                            @else
                                <div class="item @if($errors->has('user_name')) has-error @endif">
                                    <label class="lbl">{{ $dictionary['full_name'] }}</label>
                                    <input type="text" class="ipt_style" name="user_name" value="{{ $order->user_name ?? old('user_name') }}" required>
                                    @if ($errors->has('user_name'))
                                        <label>{{ $errors->first('user_name') }}</label>
                                    @endif
                                </div>
                                <div class="item @if($errors->has('user_phone')) has-error @endif">
                                    <label class="lbl">{{ $dictionary['phone'] }}</label>
                                    <input type="text" class="maskphone ipt_style" name="user_phone" value="{{ $order->user_phone ?? old('user_phone') }}" required>
                                    @if ($errors->has('user_phone'))
                                        <label>{{ $errors->first('user_phone') }}</label>
                                    @endif
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endslot
    @endcomponent
@endsection


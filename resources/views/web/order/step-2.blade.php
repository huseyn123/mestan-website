@extends ('layouts.web', [ 'page_heading' => $dictionary['basket']  ] )

@section ('content')
    @include('web.elements.breadcrumbs', ['page' => null, 'title' => $dictionary['basket'] ])

    @component('web.elements.order', ['num' => $step, 'method' => 'POST', 'route' => 'order.post'])
        @slot('step')
            <!-- StepOne -->
            <div class="step_one">
                <!-- StepForm -->
                <div class="step_form">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="item @if($errors->has('address')) has-error @endif">
                                <label class="lbl">{{ $dictionary['shipping_address'] }}</label>
                                <textarea name="address" class="ipt_style" required>{{ trim($order->address == '') ? $user->address ?? old('address') : $order->address }}</textarea>
                                @if ($errors->has('address'))
                                    <label>{{ $errors->first('address') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="item @if($errors->has('desc')) has-error @endif">
                                <label class="lbl">{{ $dictionary['shipping_note'] }}</label>

                                <textarea name="desc" class="ipt_style">{{ $order->desc ?? old('desc') }}</textarea>

                                @if ($errors->has('desc'))
                                    <label>{{ $errors->first('desc') }}</label>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- StepBottom -->
        @endslot
    @endcomponent
@endsection


@extends ('layouts.web', [ 'page_heading' => $dictionary['basket'] ] )

@section ('content')

    @include('web.elements.breadcrumbs', ['page' => null, 'title' => ' '])


    <section class="flat-customer">
        <div class="container">
            <div class="customer-wrap order-checkout">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="no-data">
                            <i class="fa fa-thumbs-up icon-checkout"></i>
                            <h1 class="title">Sifariş qəbul edildi!</h1>
                            @if($user)
                                <p>Sifarişiniz qəbul edildi. Sifariş haqqında məlumatlara aşağıdakı keçiddən baxa bilərsiniz.</p>
                                <a href="{{route('order.list',[$order->payment_key])}}" class="order-checkout-btn">SİFARİŞƏ BAX</a>
                            @endif
                        </div>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.customer-wrap -->
        </div><!-- /.container -->
    </section><!-- /.flat-customer -->

@endsection


@extends ('layouts.user-cabinet')

@section ('section')

    @if($products->count())

        <div class="table-cart orders-details">
        <table>
            <thead>
            <tr>
                <th>Sifarişlərim<span> - Sifariş# {{$key}}</span></th>
                <th>Say</th>
                <th>Məbləğ</th>
            </tr>
            </thead>
            <tbody>

            @if($order->special_percent)
                <tr>
                    <td>
                        <div class="product-delivery">
                            <h4>Endirim</h4>
                        </div>
                        <div class="clearfix"></div>
                    </td>
                    <td></td>
                    <td>
                        <div class="total">
                            {{$order->special_percent}}<span>%</span>
                        </div>
                    </td>
                </tr>
            @endif

            @if($order->delivery_price > 0)
                    <tr>
                        <td>
                            <div class="product-delivery">
                                <i class="icon icon-delivery"></i>
                                <h4>Çatdırılma qiyməti</h4>
                            </div>
                            <div class="clearfix"></div>
                        </td>
                        <td></td>
                        <td>
                            <div class="total">
                                {{$order->delivery_price}}<span>m</span>
                            </div>
                        </td>
                    </tr>
                @endif

                <tr>
                    <td>
                        <div class="product-delivery">
                            <h4>{{$dictionary['sum_p'] ?? 'Cəmi'}}</h4>
                        </div>
                        <div class="clearfix"></div>
                    </td>
                    <td></td>
                    <td>
                        <div class="total">
                            {{ ($order->total_amount ? $order->total_amount : ($order->amount +  $order->delivery_price) - ($order->special_percent ? (number_format(($order->amount * $order->special_percent/100),2)) : 0)) }} <span>AZN</span>
                        </div>
                    </td>
                </tr>

                @foreach($products as $product)
                    <tr>

                        <td>
                            @if($product->getFirstMedia())
                                <div class="img-product">
                                    <img src="{{asset($product->getFirstMedia()->getUrl('basket'))}}" >
                                </div>
                            @endif
                            <div class="name-product">
                               {{$product->name}}
                            </div>
                            <div class="product-id">
                                Malın kodu: {{$product->bar_code}}
                            </div>
                            <div class="clearfix"></div>
                        </td>

                        <td>
                            <div class="order-quantity">
                                <span>{{$product->count}}</span>
                            </div>
                        </td>
                        <td>
                            <div class="total">
                                {{$product->amount}}<span>m</span>
                            </div>
                            <div class="price">
                                {{$product->cart_prise}}<span>m</span>
                            </div>
                        </td>

                    <tr>

                @endforeach


            </tbody>
        </table>
    </div>

    @endif

@endsection


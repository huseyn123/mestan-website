@extends ('layouts.web', [ 'page_heading' => $dictionary['basket']  ] )

@section ('content')
    @include('web.elements.breadcrumbs', ['page' => null, 'title' => $dictionary['basket'] ])

    @component('web.elements.order', ['num' => $step, 'method' => 'POST', 'route' => 'order.complete'])
        @slot('step')
            <!-- StepOne -->
            <div class="step_one">
                <div class="row">
                    <div class="col-md-8 col-sm-10 col-xs-12">
                        <!-- PaymentMethods -->
                        <div class="payment_methods">
                            <h4>{{ $dictionary['payment_type'] }}</h4>
                            @foreach(config('config.payment-method-class') as $key => $class)
                                <div class="{{ $class }}">
                                    <div class="clearfix">
                                        <input type="radio" name="payment_type" id="payment{{$key}}" class="css-checkbox" value="{{$key}}" @if($key == old('payment_type', 2)) checked="checked" @endif>
                                        <label for="payment{{$key}}" class="css-label">
                                            {{ trans("locale.config.payment-method.$key") }}
                                            @if($key == 3 && trim($dictionary['choose_post_terminal']) != '')
                                                <span>{{ $dictionary['choose_post_terminal'] ?? '' }}</span>
                                            @endif
                                        </label>
                                    </div>
                                    @if($key == 3)
                                        <div class="checkboxs">
                                            @foreach($terminals as $terminal)
                                                <input type="radio" name="post_terminal" id="{{ $terminal->name }}" class="css-checkbox" value="{{ $terminal->id }}">
                                                <label for="{{ $terminal->name }}" class="css-label">{{ $terminal->name }}</label>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
            <!-- StepBottom -->
        @endslot
    @endcomponent
@endsection


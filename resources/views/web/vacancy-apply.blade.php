@extends ('layouts.web', [ 'page_heading' => $page->name ] )

@section ('content')
    @include('web.elements.breadcrumbs')
    <!-- Section Begin -->

    <!-- Section Begin -->
    <section class="ptb50">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">

                    <!-- TextBanner Begin -->
                    @include('web.elements.apply')
                    <!-- TextBanner End -->

                </div>
                <div class="col-md-9 col-sm-8 col-xs-12">
                    <!-- VacancyForm Begin -->
                    <div class="vacancy_form">

                        <h1 class="h1_title">@if(session()->has('success')) <span class="text-success bg-success">Mesajınız gonderildi</span> @else İş üçün müraciət anketi @endif</h1>
                        <form id="form" action="{{ route('resume') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="row">

                                <div class="col-md-9 col-xs-12">

                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <div class="item">
                                                <label class="lbl">Adı</label>
                                                <div class="ipt_group">
                                                    <input type="text" name="name" placeholder="" class="ipt_style @if($errors->has('name')) error-item @endif" value="{{ old('name') }}">
                                                    <span class="error-item"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="item">
                                                <label class="lbl">Soyadı</label>
                                                <div class="ipt_group">
                                                    <input type="text" name="surname" placeholder="" class="ipt_style @if($errors->has('surname')) error-item @endif" value="{{ old('surname') }}">
                                                    <span class="error-item"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <div class="item">
                                                <label class="lbl">Doğum tarixi</label>
                                                <div class="input-group">
                                                    <input type="text" name="date" placeholder="" class="ipt_style @if($errors->has('date')) error-item @endif"  value="{{ old('date') }}">
                                                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>
                                                <span class="error-item"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="item clearfix">
                                                <label class="lbl family @if($errors->has('family_status')) text-danger @endif">Ailə vəziyyəti</label>
                                                <div class="radiobox">
                                                    <input type="radio" name="family_status" id="Evli" class="css-checkbox radio_check" value="Evli" @if( old('family_status') == 'Evli')) checked @endif>
                                                    <label for="Evli" class="css-label">Evli</label>
                                                    <input type="radio" name="family_status" id="Subay" class="css-checkbox radio_check" value="Subay"  @if( old('family_status') == 'Subay')) checked @endif>
                                                    <label for="Subay" class="css-label">Subay</label>
                                                </div>
                                            </div>
                                            <span class="error-item"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="item">
                                                <label class="lbl">Müraciət olunan vəzifə</label>
                                                <div class="ipt_group">
                                                    <input type="text" name="job" placeholder="" class="ipt_style @if($errors->has('job')) error-item @endif" value="{{ old('job') }}">
                                                    <span class="error-item"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <!-- UploadPhoto -->
                                    <div class="upload_photo">
                                        <figure>
                                            <img src="{{ asset('images/bg/photo.png') }}" alt="" id="image" >
                                        </figure>

                                        <span @if($errors->has('image')) style="color:red" @endif> 1 mb az olmaq şərti ilə şəkil yükləyin</span>
                                        <div class="fileupload">
                                            Şəkil yüklə
                                            <input type="file" name="image" class="uploadimage" accept="image/*">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 col-xs-12">
                                    <label class="lbl">Telefon nömrəsi</label>
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <div class="item">
                                                <div class="ipt_group  text-error">
                                                    <i class="fa fa-mobile" aria-hidden="true"></i>
                                                    <input type="text" name="phone1"placeholder="" class="ipt_style pl25 @if($errors->has('phone1')) error-item @endif" value="{{ old('phone1') }}" onkeypress="return event.charCode >= 43 && event.charCode <= 57">
                                                    <span class="error-item"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="item">
                                                <div class="ipt_group">
                                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                                    <input type="text" name="phone2" id="phone2"  placeholder="" class="ipt_style pl25 @if($errors->has('phone2')) error-item @endif"  value="{{ old('phone2') }}" onkeypress="return event.charCode >= 43 && event.charCode <= 57">
                                                    <span class="error-item"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5 col-xs-12">
                                    <div class="item">
                                        <label class="lbl">E-mail</label>
                                        <div class="ipt_group">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <input type="text" name="email" placeholder="" class="ipt_style pl25 @if($errors->has('email')) error-item @endif"  value="{{ old('email') }}">
                                            <span class="error-item"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="item">
                                        <label class="lbl">Təhsil</label>
                                        <div class="table-responsive">
                                            <table class="table" style="border-color: #c23934!important">
                                                <thead>
                                                <tr>
                                                    <th class="text-left">Təhsil müəssisəsinin adı</th>
                                                    <th>Daxil olduğu il</th>
                                                    <th>Bitirdiyi il</th>
                                                    <th>Fakultə (ixtisas)</th>
                                                </tr>
                                                </thead>
                                                <tbody class="edu_body">

                                                <tr>
                                                    <td>
                                                        <input type="text" name="education[]" class="text-left"  value="{{old('education')[0] ?? ''}}  " >
                                                    </td>
                                                    <td>
                                                        <input type="text" name="start_date[]" value="{{old('start_date')[0]}}">
                                                    </td>
                                                    <td>
                                                        <input type="text" name="end_date[]" value="{{old('end_date')[0]}}">
                                                    </td>
                                                    <td>
                                                        <input type="text" name="speciality[]" value="{{old('speciality')[0]}}">
                                                        <button type="button" class="add add_edu"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th class="text-left">Xarici dil</th>
                                                    <th>Kafi</th>
                                                    <th>Yaxşı</th>
                                                    <th>Əla</th>
                                                </tr>
                                                </thead>
                                                <tbody class="lang_body">
                                                <tr>
                                                    <td>
                                                        <input type="text" name="foreign_lang[]" class="text-left" value="{{old('foreign_lang')[0] ?? '' }}">
                                                    </td>

                                                    <td>
                                                        <input type="radio" name="lang[]" id="Kafi" class="css-checkbox" value="Kafi"  @if( old('lang')[0] ==  'Kafi') checked @endif>
                                                        <label for="Kafi" class="css-label"></label>
                                                    </td>
                                                    <td>
                                                        <input type="radio" name="lang[]" id="Yaxşı" class="css-checkbox" value="Yaxşı"  @if( old('lang')[0] ==  'Yaxşı') checked @endif>
                                                        <label for="Yaxşı" class="css-label"></label>
                                                    </td>
                                                    <td>
                                                        <input type="radio" name="lang[]" id="Əla" class="css-checkbox" value="Əla"  @if( old('lang')[0] ==  'Əla') checked @endif>
                                                        <label for="Əla" class="css-label"></label>
                                                        <button type="button" class="add add_lang"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th class="text-left">Kompyuter bilikləri</th>
                                                    <th>Kafi</th>
                                                    <th>Yaxşı</th>
                                                    <th>Əla</th>
                                                </tr>
                                                </thead>
                                                <tbody class="comp_body">
                                                <tr>
                                                    <td>
                                                        <input type="text" name="comp_skills[]" class="text-left" value="{{ old('comp_skills')[0] ?? ''}}">
                                                    </td>


                                                    <td>
                                                        <input type="radio" name="skils[]" id="CKafi" class="css-checkbox" value="Kafi" @if( old('skils')[0] ==  'Kafi') checked @endif>
                                                        <label for="CKafi" class="css-label"></label>
                                                    </td>
                                                    <td>
                                                        <input type="radio" name="skils[]" id="CYaxşı" class="css-checkbox" value="Yaxşı"  @if( old('skils')[0] ==  'Yaxşı') checked @endif>
                                                        <label for="CYaxşı" class="css-label"></label>
                                                    </td>

                                                    <td>
                                                        <input type="radio" name="skils[]" id="CƏla" class="css-checkbox" value="Əla" @if( old('skils')[0] ==  'Əla') checked @endif>
                                                        <label for="CƏla" class="css-label"></label>
                                                        <button type="button" class="add add_comp"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <label class="lbl">İştirak etdiyiniz təlim, məşğələ və kurslar</label>
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th class="text-left">Adı</th>
                                                    <th class="w30">Ay/İl</th>
                                                </tr>
                                                </thead>
                                                <tbody class="kurs_body">
                                                <tr>
                                                    <td>
                                                        <input type="text" name="courses_title[]" class="text-left" value="{{ old('courses_title')[0] ?? ''}}">
                                                    </td>
                                                    <td>
                                                        <input type="text" name="cources_date[]" value="{{ old('cources_date')[0] }}">
                                                        <button type="button" class="add add_kurs"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <label class="lbl">Özünüz haqqında daha ətraflı</label>
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Sizi başqa gənclərdən fərqləndirən üstün cəhətləriniz</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <textarea name="superior_features">{{ old('superior_features') }}</textarea>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Əlavə etmək istədiyiniz məlumat</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <textarea name="other_information">{{ old('other_information') }}</textarea>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <button type="submit" name="">CV göndər</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- VacancyForm End -->
                </div>
            </div>
        </div>
    </section>
    <!-- Section End -->


@endsection


@push('scripts')
<script>
    $(function(){
           $("#form").validate({
            rules: {
                name: {
                    required: true,
                },
                surname: {
                    required: true,
                },
                date: {
                    required: true,
                },
                family_status: {
                    required: true,
                },
                job: {
                    required: true,
                },
                phone1: {
                    required:function() {
                        if ($('#phone2').val() == '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                phone2: {
                    required: true,
                    numericality: {
                        onlyInteger: true,
                    }

                },
                email: {
                    required: true,
                    email: true
                },
            },
            messages: {
                name: {
                    required: 'Bu sahənin doldurulması vacibdir',
                },
                phone: {
                    required: 'Bu sahənin doldurulması vacibdir'
                },
                surname: {
                    required: 'Bu sahənin doldurulması vacibdir'
                },
                mobile: {
                    required: 'Bu sahənin doldurulması vacibdir'
                },
                email: {
                    required: 'Bu sahənin doldurulması vacibdir',
                    email: 'E-mail forması düzgün deyil'
                },
            },
            success: function(label) {
                label.removeClass('error-item');
            },
            errorPlacement: function(error, element) {
                $(element).addClass('error-item');
            },
            highlight: function ( element, errorClass, validClass ) {
                $(element).removeClass('error-item');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('error-item');
            }
        });

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

    });

    $('#form').submit(function () {
        if($("input[name='family_status']:checked").val() == undefined){
            $('.family').addClass('text-danger');
        }else{
            $('.family').removeClass('text-danger');
        }

    });

    $(".uploadimage").change(function () {

        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                if(e.target.result){
                    $('#image').attr('src', e.target.result);
                }
            }
            reader.readAsDataURL(this.files[0]);
        }
    })

</script>
@endpush

@extends ('layouts.web', [ 'page_heading' => $product->name])

@section ('content')

    @include('web.elements.breadcrumbs',['data' => $product])


    <section class="flat-product-detail">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="flexslider">

                        <ul class="slides">
                            @foreach($product->getMedia('gallery') as $gallery)
                                <li data-thumb="{{asset($gallery->getUrl('blade'))}}">
                                    <a @if($product->youtube_link && $loop->last) href="{{$product->youtube_link}}"  class="zoom-video"  @else href="#"  class="zoom" @endif>
                                        <img  src="{{asset($gallery->getUrl('blade'))}}" />
                                    </a>
                                </li>
                            @endforeach

                        </ul><!-- /.slides -->

                    </div><!-- /.flexslider -->
                </div><!-- /.col-md-6 -->
                <div class="col-md-6">
                    <div class="product-detail">
                        <div class="header-detail">
                            <h4 class="name">{{$product->name}}</h4>
                            @if($product->brand_id && $product->brand_title)
                                <div class="product-brand">
                                    <span>{{$product->brand_title}}</span><a href="{{route('brand.resume',[$product->brand_id])}}" title="{{$product->brand_title}}">{{$dictionary['brand_other_products_text'] ?? 'Bu markanın bütün məhsullarına bax'}}</a>
                                </div>
                            @endif
                            {{--<div class="reviewed">--}}
                                {{--<div class="review">--}}
                                    {{--<span>3 comment</span>--}}
                                {{--</div><!-- /.review -->--}}
                            {{--</div><!-- /.reviewed -->--}}
                            <div class="product-id">
                                Malın kodu: <span class="id">{{$product->bar_code}}</span>
                            </div>
                            <div class="price">
                                <span class="sale">
                                    {{$product->price ? $product->price : $product->old_price }}<span>M</span>
                                </span>
                                @if($product->price)
                                    <span class="regular">
                                        {{$product->old_price }}<span>m</span>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.header-detail -->
                        <div class="content-detail">

                            <input type="hidden" name="product" value="">
                            <div class="quantity-box">
                                <div class="quantity @if($product->count == 0) nostock @endif">
                                            <span class="btn-down"
                                                  onclick="this.parentNode.querySelector('input[type=number]').stepDown()"></span>
                                    <!-- <input type="number" name="quantity" value="1" min="1" max="10" readonly
                                        placeholder=""> -->
                                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                           type="number" name="quantity" value="1" min="1" max="{{$product->count > 10 ? 10 : $product->count}}" maxlength = "2" placeholder="">
                                    <span class="btn-up" onclick="this.parentNode.querySelector('input[type=number]').stepUp()"></span>
                                </div>
                            </div><!-- /.quanlity-box -->


                                <div class="btn-add-bs  @if(($product->basket && $product->basket->count) || ($product->count == 0)) d-none @endif">
                                    <a href="javascript:void(0)"  class="js-submit-trigger"  data-id="{{$product->id}}">{{$dictionary['add_to_basket'] ?? 'Səbətə əlavə et'}}<img src="{{asset('images/icon/add-cart.svg')}}" ></a>
                                    <input type="submit" class="d-none" value="Səbətə əlavə et">
                                </div>

                                <div class="btn-added-bs @if(!$product->basket) d-none @endif">
                                    <p class="message">Səbətə @if($product->basket && $product->basket->count) <span class="count">{{$product->basket->count}}</span> ədəd @endif əlavə edildi</p>
                                    <a href="javascript:void(0)" class="js-remove" data-id="{{$product->id}}">{{$dictionary['cancel'] ?? 'Ləğv et'}} </a>
                                    <span>və ya</span>
                                    <a href="{{route('cart.index')}}" class="to-cart">{{$dictionary['get_basket'] ?? 'Səbətə Get'}}</a>
                                </div>
                                @if($product->count <= 0)
                                    <div class="btn-nostock">
                                        <a href="javascript:void(0)" title="">{{$dictionary['no_stock'] ?? 'Stokda Yoxdur'}}</a>
                                        <input type="submit" class="d-none" value="{{$dictionary['no_stock'] ?? 'Stokda Yoxdur'}}">
                                    </div>        
                                @endif
                            <div class="box-wishlist">
                                <a  href="#" class="btn wishlist @if($product->whish_list) active @endif @if(!auth()->guard('web')->check()) open-popup @endif" data-id="{{$product->id}}"><i class="icon cart-icon-wishlist"></i></a>
                            </div>
                        </div><!-- /.content-detail -->

                        @if($product->count > 0 && $product->count < config('config.produc_stock_count.single'))
                            <div class="content-info">
                                <p>Bu məhsuldan stokda <span>{{$product->count}}</span> ədəd qalıb!</p>
                            </div>
                        @endif

                        <div class="footer-detail">
                            <div class="info-text">
                                <h4>{{$dictionary['product_info'] ?? 'Mal haqqında Məlumat'}}</h4>
                                {!! $product->content !!}
                            </div>
                        </div><!-- /.footer-detail -->
                    </div><!-- /.product-detail -->
                </div><!-- /.col-md-6 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-product-detail -->

    @if($similarProducts->count())
        <section class="flat-imagebox single">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="flat-row-title">
                        <h3>{{$dictionary['similar_products'] ?? 'Oxşar məhsullar'}}</h3>
                    </div>
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="similar-carousel">


                        @foreach($similarProducts as $similarProduct)

                            <div class="imagebox single">
                                @if($similarProduct->price && $similarProduct->old_price) <span class="item-sale">{{$product->percent ? $product->percent.'%' : $dictionary['sale_grid'] ?? 'SALE'}}</span> @endif
                                @if($similarProduct->getFirstMedia())
                                    <div class="box-image">
                                        <a href="{{route('showPage',[$similarProduct->page_slug,$similarProduct->slug])}}" title="{{$similarProduct->name}}">
                                            <img src="{{asset($similarProduct->getFirstMedia()->getUrl('blade'))}}" >
                                        </a>
                                    </div><!-- /.box-image -->
                                @endif
                                <div class="box-info">
                                    @if($similarProduct->count == 0)<div class="out-of-stock"><h4>{{$dictionary['no_product'] ?? 'Stokda yoxdur'}} </h4></div>
                                        @elseif($similarProduct->count > 0 && $similarProduct->count < config('config.produc_stock_count.crud'))<div class="limited-quantity"><h4>{{$dictionary['product_limited'] ?? 'Məhdud sayda'}}</h4></div>
                                    @endif
                                </div><!-- /.box-info -->
                                <div class="box-content">
                                    <div class="product-name">
                                        <a href="{{route('showPage',[$similarProduct->page_slug,$similarProduct->slug])}}" title="{{$similarProduct->name}}">{{$similarProduct->name}}</a>
                                    </div>
                                </div><!-- /.box-content -->
                                <div class="box-bottom">
                                    <div class="price">
                                        <span class="sale">{{$similarProduct->price ? $similarProduct->price : $similarProduct->old_price }} <span>M</span></span>
                                        @if($similarProduct->price) <span class="regular">{{$similarProduct->old_price}} <span>M</span></span> @endif
                                    </div>
                                    <div class="wishlist-cart">
                                        <a  href="#" class="btn wishlist @if($similarProduct->whish_list) active @endif @if(!auth()->guard('web')->check()) open-popup @endif" data-id="{{$similarProduct->id}}"></a>
                                        <a href="{{ route('cart.index') }}" class="btn @if($similarProduct->count && $similarProduct->count > 0) btn-add-cart @else out-of-stock @endif   @if($product->basket && $product->basket->count) active @endif"  data-id="{{$product->id}}"></a>
                                        @if($similarProduct->count > 0)
                                            <a href="{{ route('cart.index') }}" class="btn btn-add-cart  @if($similarProduct->basket && $similarProduct->basket->count) active @endif"  data-id="{{$similarProduct->id}}"></a>
                                        @endif    
                                    </div>
                                </div><!-- /.box-bottom -->
                            </div><!-- /.imagebox single -->
                         @endforeach

                    </div><!-- /.similar-carousel -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-imagebox single -->
    @endif

    {{--<section class="flat-product-review">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    {{--<div id="fb-root"></div>--}}
                    {{--<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v4.0"></script>--}}
                    {{--<div class="fb-comments" data-href="http://www.store.mestan.az/" data-width="100%" data-numposts="5"></div>--}}
                {{--</div><!-- /.col-md-12 -->--}}
            {{--</div><!-- /.row -->--}}
        {{--</div><!-- /.container -->--}}
    {{--</section><!-- /.flat-product-review -->--}}


@endsection
@extends ('layouts.web', ['page_heading' => $page->name])

@section ('content')

    @include('web.elements.breadcrumbs')

    <main id="category">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4 col-lg-3">
                    <div class="sidebar">

                        {{--@include('web.elements.product_filtr')--}}

                    </div><!-- /.sidebar -->
                </div><!-- /.col-lg-3 col-md-4 -->
                <div class="col-lg-9 col-md-8">
                    <div class="main-shop">
                        <div class="wrap-imagebox">
                            <div class="flat-row-title">
                                <h3>{{$page->name}}</h3>

                                <div class="sort">
                                    <div class="popularity">
                                        <select name="sort">
                                            @if($sort == 0)
                                                <option selected disabled>{{$dictionary['select_sort'] ?? 'Sıralamanı seç'}}</option>
                                            @endif
                                            @foreach(config('config.product_filtr_order') as $key=>$value)
                                                <option value="{{$key}}" @if($key == $sort) selected @endif>{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="showed">
                                        <select name="limit">
                                            @foreach(config('config.product_filtr_paginate') as $key=>$value)
                                                <option value="{{$value}}" @if($value == $limit) selected @endif >{{$dictionary['show'] ?? 'Göstər'}} {{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>


                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="tab-product">
                                <div class="row sort-box product_list">

                                    @include('web.elements.filtr_product_crud')

                                </div><!-- /.sort-box -->
                            </div><!-- /.tab-product -->
                        </div><!-- /.wrap-imagebox -->

                    </div><!-- /.main-shop -->
                </div><!-- /.col-lg-9 col-md-8 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </main><!-- /#category -->


@endsection

@push('scripts')
<script>

    $(".sort select" ).change(function() {
        ProductFiltr();
    });

    $( "#slider-range" ).slider({
        range: true,
        min: 0,
        max: 500,
        values: [{{$min_price}}, {{$max_price}}],
        slide: function( event, ui ) {
            $( "#amount" ).val( ui.values[ 0 ] + "M" + " - " + ui.values[ 1 ] + "M" );
        },
        stop: function( event, ui ) {
            $( "#amount" ).val( ui.values[ 0 ] + "M" + " - " + ui.values[ 1 ] + "M" );
            ProductFiltr();
        }
    });
    $( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) + "M" + " - " + $( "#slider-range" ).slider( "values", 1 ) + "M" );

    $(document).on('click', '.blog-pagination li.pag a', function(event){
        event.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        ProductFiltr($("#slider-range" ).slider( "values",0),$( "#slider-range").slider( "values",1),page);
    });

    function ProductFiltr(min = $("#slider-range" ).slider( "values",0),max = $( "#slider-range").slider( "values",1),page = {{$paginate}}){
        $('.preloader').show();
        var filtr = [];
        var limit = $('.sort select[name$="limit"]').val();
        var sort = $('.sort select[name$="sort"]').val();
        if(limit && limit != 24){
            filtr.push({'name':'limit','val':limit});
        }
        if(sort !== 'null' && sort != 0){
            parametr='?sort='+sort;

        }else{
            parametr='?sort';
        }
        range1 = min;
        range2 = max;



        filtr.push({'name':'min','val':range1},{'name':'max','val':range2});
        $.each($('.product_filtr'), function(){
            if($(this).is(':checked') === true){
                filtr.push({'name':$(this).attr('name')+encodeURI('[]'),'val' : $(this).val()});
            }
        });
        filtr.push({'name':'page','val':page});

        if (filtr.length > 0 && history.pushState) {
            $.each(filtr,function (key,value) {
                parametr +='&'+value.name+'='+value.val;
            });
            var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + parametr;
            window.history.pushState({path:newurl},'',newurl);

            $.ajax({
                url: newurl,
                type: 'GET',
                dataType: "json",

                beforeSend: function(response) {
                },
                success: function(response){
                    $('.main-shop .product_list').html(response.html);
                    $('.preloader').hide();
                },
                error: function(res){
                   $('.preloader').hide();
                }

            });

        }
    }

    $('.product_filtr').change(function(){
       ProductFiltr();
    });


</script>

@endpush
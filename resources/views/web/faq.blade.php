@extends ('layouts.web', ['page_heading' => $page->name])

@section ('content')
    @include('web.elements.breadcrumbs')
    <!-- Faq Begin -->
    <section class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="faq">
                    @foreach($faq as $q)
                        <article>
                            <span class="open"><i class="fa fa-plus" aria-hidden="true"></i></span>
                            <h2>{{ $q->title }}</h2>
                            <p>{{ $q->text }}</p>
                        </article>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- Faq End -->
@endsection
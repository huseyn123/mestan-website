@extends ('layouts.web', ['page_heading' => $article->name, 'page_image' => asset('storage/'.$article->image)])

@section ('content')
    @include('web.elements.breadcrumbs')
    <!-- Page Begin -->
    <section class="pt50 pb30">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8 col-xs-12">
                    <!-- NewsDetail Begin -->
                    <div class="news_detail">
                        <figure>
                            <img src="{{ asset('storage/'.$article->image) }}" alt="{{ $article->name }}">
                        </figure>
                        <div class="body">
                            <h2 class="title">{{ $article->name }}</h2>
                            <span class="date">{{ blogDate($article->published_at) }}</span>
                            <div class="text">
                                {!! $article->content !!}
                            </div>
                        </div>
                    </div>
                    <!-- NewsDetail End -->
                    @include('web.elements.page-gallery', ['page' => $article])

                </div>
                <div class="col-md-3 col-sm-4 col-xs-12">
                    @if($topArticles->count() > 0)
                        <!-- TopNews Begin -->
                        <div class="topnews">
                            <h1 class="side_title">{{ $dictionary['last_news'] ?? '' }}</h1>
                            @foreach($topArticles as $top)
                                <article>
                                    <h2><a href="{{ route('showPage', [$top->slug, $top->article_slug]) }}">{{ $top->name }}</a></h2>
                                </article>
                            @endforeach
                        </div>
                        <!-- PopularBlock End -->
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-- Page End -->
@endsection

@extends ('layouts.web', [ 'page_heading' => $page->name ] )

@section ('content')
    @include('web.elements.breadcrumbs')
    <!-- Section Begin -->
    <section class="ptb50">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                    @include('web.elements.banners', ['type' => 1])
                    @include('web.elements.banners', ['type' => 2])
                    @include('web.elements.banners', ['type' => 3])
                </div>
                <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12">
                    <!-- DesignServices Begin -->
                    <div class="design_services">
                        <!-- Text -->
                        <div class="text">
                            {!! $page->content !!}
                        </div>
                    </div>
                    <!-- DesignServices End -->
                    @include('web.elements.page-gallery')
                </div>
            </div>
        </div>
    </section>
    <!-- Section End -->
@endsection
@extends ('layouts.web', [ 'page_heading' => $page->name ] )

@section ('content')
    @include('web.elements.breadcrumbs')
    <!-- GiftCards Begin -->
    <section class="gift_card">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>{{ $page->name }}</h1>
                </div>
                @foreach($cards as $card)
                    <div class="col-md-4 col-xs-6 col-mob-12">
                        <div class="box">
                            <a href="{{ route('showPage', [$card->page_slug, $card->slug]) }}@if(isset($search) && $search == true)?search=on @endif" title="{{ $card->name }}">
                                @if(env('APP_ENV') == 'production')
                                    <img src="{{ asset("storage/thumb/$card->image") }}" alt="{{ $card->name }}">
                                @endif
                                <h2>{{ $card->name }}</h2>
                                <span>{{ $card->price }} <span class="azn">M</span></span>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- GiftCards End -->
@endsection
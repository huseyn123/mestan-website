@extends ('layouts.web', [ 'page_heading' => 'Səbət' ] )

@section ('content')


    @include('web.elements.breadcrumbs', ['page' => null, 'p_title' => 'Wishlist'])


    <section class="flat-imagebox wishlist-wrap">
        <div class="container">
            <div class="box-product">
                <div class="row">

                    @foreach ($products as $product)
                            @include('web.elements.product_crud',['index_form' => true])
                    @endforeach

                </div><!-- /.row -->
            </div><!-- /.box-product -->
        </div><!-- /.container -->
    </section><!-- /.flat-imagebox -->


    @include('web.elements.best_seller')

@endsection


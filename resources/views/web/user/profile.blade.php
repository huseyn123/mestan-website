@extends ('layouts.user-cabinet')

@section ('section')

    {!! Form::open(['route' => ['user.update', $user->id], 'method' => 'PUT','id'=>'update_user','autocomplete'=>"off"]) !!}
        <div class="block mb30">
            <div class="block-header">
                <h4>{{ $dictionary['user_profile_title'] ?? 'Şəxsi məlumatlar' }}</h4>
            </div>
            <div class="block-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group required">
                            <label for="firstname" class="control-label">{{ $dictionary['full_name'] }}</label>
                            <input type="text" name="name" value="{{ $user->name }}" class="customer-form" placeholder="{{ $dictionary['full_name'] }}">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group required">
                            <label for="phone" class="control-label">{{ $dictionary['phone'] }}</label>
                            <input type="text" name="phone" value="{{ $user->phone }}" class="customer-form maskphone" placeholder="{{ $dictionary['phone'] }}">
                        </div>
                    </div>
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group required">
                            <label for="street" class="control-label">{{ $dictionary['shipping_address'] }}</label>
                            <input type="text" name="address" value="{{ $user->address }}" class="customer-form" placeholder="{{ $dictionary['shipping_address'] }}">
                        </div>
                    </div>
                </div><!-- /.row -->
            </div>
        </div>
        <div class="block mb30">
            <div class="block-header">
                <h4>{{ $dictionary['account_information'] ?? 'Hesab məlumatlar' }}</h4>
            </div>
            <div class="block-body">

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group required">
                            <label for="email" class="control-label">{{ $dictionary['email'] }}</label>
                            <input type="email" name="email" value="{{ $user->email }}" class="customer-form" placeholder="{{ $dictionary['email'] }}">
                        </div>
                    </div>
                </div><!-- /.row -->

                @if(is_null($user->facebook_id))
                    <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group required">
                            <label for="password_1" class="control-label">{{ $dictionary['password'] }}</label>
                            <input id="password_1" type="password" name="password" class="customer-form" autocomplete="new-password">
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group required">
                            <label for="password_2" class="control-label">{{ $dictionary['repeat_password'] }}</label>
                            <input id="password_2" type="password" name="password_confirmation" class="customer-form" >
                        </div>
                        <span>{{ $dictionary['password_change_validation'] ?? 'Şifrəni dəyişmək istəmədiyiniz halda xanaları boş saxlayın' }}</span>
                    </div>
                </div><!-- /.row -->
                @endif
            </div>
        </div>
        <div class="pull-right mb50">
            <input type="submit" class="customer-submit" value="{{ $dictionary['record_changes'] ?? 'Dəyişikləri qeydə al' }}">
        </div>

        <p id="update_message"></p>
    {!! Form::close() !!}



@endsection


@extends ('layouts.user-cabinet')

@section ('section')

  {!! Form::open(['route' => ['user.post.birthday', $user->id], 'method' => 'PUT','id'=>'user_birthday','autocomplete'=>"off", 'enctype' => 'multipart/form-data']) !!}
      <div class="block mb30">
        <div class="block-header">
          <h4>{{$dictionary['birthday_page_title'] ?? 'Doğum gününə xüsusi endirim əldə etmək üçün uşağın məlumatlarını daxil edin'}}</h4>
        </div>
        <div class="block-body">
          <div class="row">
            <div class="col-sm-6">
               <div class="form-group required">
                  <label for="firstname" class="control-label">{{ $dictionary['name'] }}</label>
                  <input type="text" name="name" value="{{ $user_birthday->name ?? null}}" class="customer-form" placeholder="{{ $dictionary['name'] }}" required @if(isset($user_birthday->status) && $user_birthday->status == 1) disabled @endif>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group required">
                <label for="lastname" class="control-label">{{ $dictionary['surname'] }}</label>
                <input id="lastname" type="text" class="customer-form" placeholder="{{ $dictionary['surname'] }}" name="surname" value="{{ $user_birthday->surname ?? null}}" required @if(isset($user_birthday->status) && $user_birthday->status == 1) disabled @endif>
              </div>
            </div>
          </div><!-- /.row -->
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group required">
                <label for="bday" class="control-label">{{$dictionary['birthday'] ?? 'Doğum günü'}}</label>
                <input id="bday" type="date" class="customer-form" placeholder="2008-01-01" name="b_day" 
                value="{{ $user_birthday->b_day ?? null}}" required @if(isset($user_birthday->status) && $user_birthday->status == 1) disabled @endif>
              </div>
            </div>
            <div class="col-sm-6">
              @if(!isset($user_birthday->status) || (isset($user_birthday->status) &&  $user_birthday->status != 1))
                <div class="form-group required">
                      <label id="upload-label" for="upload" class="customer-form">{{$dictionary['id_cards_text'] ?? 'Şəxsiyyət vəsiqəsinin surəti'}}</label>
                      <input id="upload" type="file" accept="image/*" onchange="readURL(this);" class="form-control border-0" name="image">
                </div>
              @endif
              <div class="image-area mt-4">
                <img id="imageResult" @if($user_birthday && $user_birthday->getFirstMedia()) src="{{asset($user_birthday->getFirstMedia()->getUrl())}}" @else src="" @endif  class="img-fluid rounded shadow-sm mx-auto d-block">
              </div>
            </div>
          </div><!-- /.row -->
        </div>
      </div>

       @if(!isset($user_birthday->status) || (isset($user_birthday->status) &&  $user_birthday->status != 1))
          <div class="pull-right mb50">
            <input type="submit" class="customer-submit" value="{{ $dictionary['record_changes'] ?? 'Dəyişikləri qeydə al' }}">
          </div>
       @endif

      <p id="update_message"></p>
  {!! Form::close() !!}

@endsection


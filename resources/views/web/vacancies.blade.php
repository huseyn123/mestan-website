@extends ('layouts.web', [ 'page_heading' => $page->name ] )

@section ('content')
    @include('web.elements.breadcrumbs')
    <!-- Section Begin -->
    <section class="ptb50">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <!-- TextBanner Begin -->
                    @include('web.elements.apply')
                    <!-- TextBanner End -->
                </div>
                <div class="col-md-9 col-sm-8 col-xs-12">
                    <!-- Vacancy Begin -->
                    <div class="vacancy">
                        <h1 class="h1_title">Boş vakant yerləri</h1>
                        <div class="table-responsive">
                            <table class="table vacancy_table">
                                <thead>
                                <tr>
                                    <th>VAKANSİYANIN ADI</th>
                                    <th class="text-center">İŞ TƏCRÜBƏSİ</th>
                                    <th class="text-center">ƏMƏK HAQQI</th>
                                    <th class="text-center">BİTMƏ TARİXİ</th>
                                    <th width="110">&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($vacancies as $vacancy)
                                    <tr>
                                        <td>{{ $vacancy->title }}</td>
                                        <td class="text-center">{{ $vacancy->work_experience }}</td>
                                        <td class="text-center">{{ $vacancy->salary }}</td>
                                        <td class="text-center">{{ $vacancy->end_date }}</td>
                                        <td><a href="{{ route('show.resume',[$page->slug, $vacancy->id]) }}" title="" class="more"><span>Ətraflı</span></a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- Vacancy End -->
                </div>
            </div>
        </div>
    </section>
    <!-- Section End -->
@endsection

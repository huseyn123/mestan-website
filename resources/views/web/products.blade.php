@extends ('layouts.web', ['page_heading' => $page->name])

@section ('content')

    @include('web.elements.breadcrumbs')

    <main id="category">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4 col-lg-3">
                    <div class="btn-filter">{{ $dictionary['mobile_filtr_title'] ?? 'Filter' }}</div>

                    <div class="sidebar">
                        @include('web.elements.product_filtr')

                    </div><!-- /.sidebar -->
                </div><!-- /.col-lg-3 col-md-4 -->
                <div class="col-lg-9 col-md-8">
                    <div class="main-shop">
                        <div class="wrap-imagebox">
                            <div class="flat-row-title">
                                <h3>{{$page->name}}</h3>

                                <div class="sort">
                                    <div class="popularity">
                                        <select name="sort">
                                            @if($sort == 0)
                                                <option selected disabled>{{$dictionary['select_sort'] ?? 'Sıralamanı seç'}}</option>
                                            @endif
                                            @foreach(config('config.product_filtr_order') as $key=>$value)
                                                <option value="{{$key}}" @if($key == $sort) selected @endif>{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="showed">
                                        <select name="limit">
                                            @foreach(config('config.product_filtr_paginate') as $key=>$value)
                                                <option value="{{$value}}" @if($value == $limit) selected @endif >{{$dictionary['show'] ?? 'Göstər'}} {{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>


                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="tab-product">
                                <div class="row sort-box product_list">

                                    @include('web.elements.filtr_product_crud')

                                </div><!-- /.sort-box -->
                            </div><!-- /.tab-product -->
                        </div><!-- /.wrap-imagebox -->

                    </div><!-- /.main-shop -->
                </div><!-- /.col-lg-9 col-md-8 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </main><!-- /#category -->


@endsection

@push('scripts')

    @include('web.elements.products_ajax_scripts')

@endpush
@extends ('layouts.web', ['page_heading' => $page->name])

@section ('content')
    @include('web.elements.breadcrumbs')
    <!-- News Begin -->
    <section class="news_list">
        <div class="container">
            <div class="row">
                <!-- NewsList Begin -->
                @foreach($articles as $article)
                    <div class="col-sm-4 col-xs-6 col-mob-12">
                        <article>
                            <figure>
                                <img src="{{ asset('storage/thumb/'.$article->image) }}" alt="{{ $article->title }}">
                            </figure>
                            <h2>{{ $article->title }}</h2>
                            <p>{{ str_limit($article->summary, 150) }}</p>
                            <div class="clearfix">
                                <span class="date"><i class="fa fa-calendar" aria-hidden="true"></i>{{ blogDate($article->published_at) }}</span>
                                <a href="{{ route('showPage', [$page->slug, $article->slug]) }}" title="{{ $article->title }}" class="more"><span>{{ $dictionary['read_more'] }}</span></a>
                            </div>
                        </article>
                    </div>
                @endforeach
                <!-- NewsList End -->
            </div>
            <div class="row">
                <!-- Pagination Begin -->
                <nav class="pag col-xs-12">
                    {{ $articles->links() }}
                </nav>
                <!-- Pagination End -->
            </div>
        </div>
    </section>
    <!-- News End -->
@endsection

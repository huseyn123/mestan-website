@extends ('layouts.web', ['page_heading' => $dictionary['searched_result_title'] ?? 'Axtarışın nəticələri'])

@section ('content')

	<!-- Section Begin -->
	<section class="pt50 pb30">
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-xs-12">
					<!-- SearchResult Begin -->
					@if($keyword != '')
						<div class="search_result">
							{!! str_replace( [ ':keyword', ':count' ], [ '<span>'.$keyword.'</span>', '<span>'.$products->total().'</span>' ], $dictionary['search_result']) !!}
						</div>
					@endif
					<!-- SearchResult End -->
					@if($products->count() > 0)
						<!-- ProductList Begin -->
						<div class="product_list">
							<div class="row">
								@foreach($products as $product)
									<div class="col-sm-4 col-xs-6 col-mob-12">
										@include('web.elements.product-list', ['p' => $product, 'figureClass' => 'brd', 'search' => true])
									</div>
								@endforeach
							</div>
							<div class="row">
								<div class="col-xs-12">
									<!-- Pagination Begin -->
									<nav class="pag brd text-right">
										{{ $products->appends(request()->input())->links('web.elements.pagination') }}
									</nav>
									<!-- Pagination End -->
								</div>
							</div>
						</div>
					@endif
				</div>
				<div class="col-md-3 hidden-sm hidden-xs">
					<!-- LastSearch Begin -->
					<div class="lastsearch">
						<h1 class="side_title">{{ $dictionary['most_searched'] ?? 'Daha çox axtarılanlar' }}</h1>
						@foreach($mostSearchedProducts as $p)
							<article>
								<h2><a href="{{ route('showPage', [$p->page_slug, $p->slug]) }}" title="{{ $p->name }}">{{ $p->name }}</a></h2>
								<span class="price">{{ $p->price }} <span class="azn">M</span> </span>
							</article>
						@endforeach
					</div>
					<!-- LastSearch End -->
				</div>
			</div>
		</div>
	</section>
@endsection
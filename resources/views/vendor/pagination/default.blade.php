@if ($paginator->hasPages())

    <div class="blog-pagination">

        <ul class="flat-pagination">

            @if (!$paginator->onFirstPage())
                <li class="prev">
                    <a href="{{ $paginator->previousPageUrl() }}" >
                        <img src="{{asset('images/icon/left-1.svg')}}" >Əvvəlki
                    </a>
                </li>
            @endif

                {{-- Pagination Elements --}}
                @foreach ($elements as $element)

                    {{-- Array Of Links --}}
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == $paginator->currentPage())
                                <li class="active">
                                    <a class="waves-effect" title="">{{ $page }}</a>
                                </li>
                            @else
                                <li class="pag">
                                    <a href="{{ $url }}" class="waves-effect" title="">{{ $page }}</a>
                                </li>
                            @endif
                        @endforeach
                    @endif
                @endforeach


            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="next">
                    <a href="{{ $paginator->nextPageUrl() }}" title="">
                        Növbəti<img src="{{asset('images/icon/right-1.svg')}}" >
                    </a>
                </li>
            @endif

        </ul>
        <div class="clearfix"></div>

    </div><!-- /.blog-pagination -->

@endif

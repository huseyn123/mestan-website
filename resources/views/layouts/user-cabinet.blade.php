@extends ('layouts.web', ['page_heading' => $user->name ] )

@section ('content')

    <section class="flat-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs">
                        <li class="trail-item">
                            <a href="{{route('home')}}" title="">{{$dictionary['home_page']}}</a>
                            <span><img src="{{asset('images/icon/next.svg')}}"></span>
                        </li>
                        <li class="trail-end">
                            <a title="">{{$dictionary['personal_account'] ?? 'Şəxsi hesab'}} </a>
                        </li>
                    </ul><!-- /.breacrumbs -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-breadcrumb -->


    <section class="flat-customer">
        <div class="container">
            <div class="customer-wrap">

                <div class="row">
                    <div class="col-3 col-md-3 col-lg-2">
                        <div class="customer-profile">
                            {{--<img class="img-fluid rounded-circle" src="/images/product/customer/IMG_20190504_2107251.jpg" alt="">--}}
                        </div>
                    </div><!-- /.col-lg-2 -->
                    <div class="col-9 col-md-9 col-lg-10">
                        <div class="customer-title">
                            <h3>{{$user->name}}</h3>
                        </div>
                    </div><!-- /.col-lg-10 -->
                </div><!-- /.row -->


                <div class="row">
                    <div class="col-12 col-md-3 col-lg-2">
                        <div class="customer-nav-wrap">
                            <nav class="list-group customer-nav">
                                <a href="{{ route('user.profile') }}" class="{{activeUrl(route('user.profile'))}} list-group-item d-flex justify-content-between align-items-center">{{$dictionary['sidebar1'] ?? 'Məlumatlar'}}</a>
                                <a href="{{ route('orders.list') }}" class="{{activeUrl(route('orders.list'))}} list-group-item d-flex justify-content-between align-items-center">{{$dictionary['user_sidebar2'] ?? 'Sifarişlərim'}}</a>
                                <a href="{{ route('user.birthday') }}" class="{{activeUrl(route('user.birthday'))}} list-group-item d-flex justify-content-between align-items-center">{{$dictionary['user_sidebar3'] ?? 'Doğum günü'}}</a>
                            </nav>
                        </div>
                    </div><!-- /.col-lg-2 -->
                    <div class="col-12 col-md-9 col-lg-10">
                        @yield('section')
                    </div><!-- /.col-lg-10 -->
                </div><!-- /.row -->

            </div><!-- /.customer-wrap -->
        </div><!-- /.container -->
    </section><!-- /.flat-customer -->

    @include('web.elements.best_seller')


@endsection




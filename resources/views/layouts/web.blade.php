<!DOCTYPE html>
<html lang="{{$lang}}">
<head>
    @if(env('APP_ENV') == 'production')
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/{{$lang}}_{{strtoupper($lang)}}/sdk.js#xfbml=1&version=v2.9&appId={{ config('services.facebook.client_id') }}";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-5PJH4FV');</script>
        <!-- End Google Tag Manager -->
    @endif

    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#ffffff">
    <meta property="author" content="{{ $config['name'] }}">
    <meta property="og:title" content="{{ $page_heading }}">
    @if((isset($page) && $page->meta_description) || isset($meta_description))
    <meta property="og:description" content="{{ $page->meta_description ?? $meta_description }}">
    @endif
    <meta property="og:url" content="{{request()->url()}}">
    <meta property="og:image" content="{{asset('images/logos/logo.png')}}">
    <meta property="og:image:url" content="{{asset('images/logos/logo.png')}}">
    <meta name="twitter:image" content="{{asset('images/logos/logo.png')}}">
    @if((isset($page) && $page->meta_description) || isset($meta_description))
    <meta name="description" content="{{ $page->meta_description ?? $meta_description }}">
    @endif
    @if((isset($page) && $page->meta_keywords) || isset($meta_keywords))
    <meta name="keywords" content="{{ $page->meta_keywords ?? $meta_keywords }}">
    @endif
    <title>@if(!is_null($page_heading)){{ $page_heading }} | @endif{{ $config['name'] }}</title>

    <link rel="shortcut icon" href="{{ asset('images/favicon/logo.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('images/favicon/logo.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon/logo.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon/logo.png') }}">
    <link rel="mask-icon" href="{{ asset('images/favicon/logo.png') }}" color="#5bbad5">

    <link href="{{ asset(mix('css/style.css')) }}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="{{ asset('js/html5shiv.min.js') }}"></script>
    <script src="{{ asset('js/respond.min.js') }}"></script>
    <![endif]-->
    <script type="text/javascript">
        window.$crisp=[];
        window.CRISP_WEBSITE_ID="029adb93-f40a-4391-a1f5-ea0ac3b5341c";
        (function(){d=document;s=d.createElement("script");
            s.src="https://client.crisp.chat/l.js";
            s.async=1;d.getElementsByTagName("head")[0].appendChild(s);
        })();
    </script>
    @include('widgets.analytic', ['code' => $config['google_analytic_key'] ])

</head>

<body class="header_sticky">
    <div class="boxed">
        <div class="overlay"></div>

        <div class="preloader"></div>


        @include('web.elements.header')

        @yield('content')


        @include('web.elements.footer')

        @include('web.elements.login-form')

        @include('web.elements.locations')

        @include('web.elements.help')

    </div>

    <script src="{{asset(mix('js/script.js'))}}"></script>

    @include('web.elements.auth')

    <script>
        @if($errors->any() && !isset($widgetErrorOff))

            $(document).ready( function() {
                toastr.error('{{ $errors->first() }}');
            });
        @endif


    </script>


    @stack('scripts')
</body>

</html>
